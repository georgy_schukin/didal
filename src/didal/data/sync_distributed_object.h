#pragma once

#include "local_storage.h"
#include "item_not_found_exception.h"
#include "distributed_promises.h"
#include "didal/comm/remote.h"
#include "didal/comm/node_group.h"
#include "didal/base/promise_storage.h"

#include <map>
#include <cstddef>
#include <memory>
#include <mutex>
#include <functional>
#include <atomic>

namespace ddl {

class Environment;

/**
 * @brief Distributed object (one object on each node) with synchronization by epochs.
 */
template <typename _ObjectType>
class SyncDistributedObject : public DistributedPromises<_ObjectType> {
public:
    using ObjectType = _ObjectType;
    using ObjectPtrType = std::shared_ptr<ObjectType>;
    using CollectionType = SyncDistributedObject<ObjectType>;
    using FutureType = std::future<ObjectPtrType>;

public:
    SyncDistributedObject(Environment *env) :
        DistributedPromises<_ObjectType>(env)
    {
        RemoteMethodMaker<CollectionType> maker(this->getRemote(), this);
        remoteAdd = maker.method(&CollectionType::localAdd).template proc<int, ObjectPtrType>();
        remoteGet = maker.method(&CollectionType::localGet).template func<ObjectPtrType>();
        remoteHas = maker.method(&CollectionType::localHas).template func<bool>();
        remoteRemove = maker.method(&CollectionType::localRemove).template proc<>();
        remoteAddRequest = maker.method(&CollectionType::localAddRequest).template proc<const Node&, size_t, int>();
        this->getRemote()->startListening();
    }

    /// Change epoch for current node.
    /// All future requests from this node will use this epoch.
    void nextEpoch() {
        current_epoch++;
    }

    /// Return current epoch for current node.
    int currentEpoch() const {
        return current_epoch;
    }

    /// Signal that an item with specified id reached current epoch (of current node).
    /// Requests awating this epoch for this item will be processed.
    void signalEpoch() {
        std::vector<RequestHandler> requests;
        std::shared_ptr<ObjectType> obj_ptr;
        {
            std::lock_guard<std::mutex> lock(mutex);
            const auto epoch = current_epoch.load();
            object_epoch = epoch;
            obj_ptr = local_object.get();
            if (obj_ptr) {
                requests = extractPendingRequests(epoch);
            }
        }
        if (obj_ptr) {
            processRequests(requests, obj_ptr);
        }
    }

    template <typename... Args>
    void create(Args&&... args) {
        localAdd(current_epoch, std::make_shared<ObjectType>(std::forward<Args...>(args...)));
    }

    template <typename... Args>
    void createOn(const Node &node, Args&&... args) {
        addOn(node, std::make_shared<ObjectType>(std::forward<Args...>(args...)));
    }

    void add(const ObjectType &obj) {
        localAdd(current_epoch, std::make_shared<ObjectType>(obj));
    }

    void add(ObjectType &&obj) {
        localAdd(current_epoch, std::make_shared<ObjectType>(std::move(obj)));
    }

    void add(std::shared_ptr<ObjectType> obj_ptr) {
        localAdd(current_epoch, obj_ptr);
    }

    void addOn(const Node &node, const ObjectType &obj) {
        addOn(node, std::make_shared<ObjectType>(obj));
    }

    void addOn(const Node &node, ObjectType &&obj) {
        addOn(node, std::make_shared<ObjectType>(std::move(obj)));
    }

    void addOn(const Node &node, std::shared_ptr<ObjectType> obj_ptr) {
        if (this->thisNode() == node) {
            localAdd(current_epoch, obj_ptr);
        } else {
            remoteAdd(node, current_epoch, obj_ptr);
        }
    }

    /// Blocking local get. Throws exception if there is no data with such id.
    ObjectPtrType get() const {
        return localGet();
    }

    /// Blocking remote get. Throws exception if there is no data with such id.
    ObjectPtrType getFrom(const Node &node) const {
        return this->thisNode() == node ? localGet() : remoteGetWithCheck(node);
    }

    /// Nonblocking local request. Data will be returned when it's become available.
    FutureType getWhenReady() {
        return getWhenReadyForEpoch(current_epoch);
    }

    /// Nonblocking remote request. Data will be returned when it's become available.
    FutureType getWhenReadyFrom(const Node &node) {
        return getWhenReadyForEpochFrom(node, current_epoch);
    }

    /// Nonblocking local request. Data will be returned when it's become available.
    FutureType getWhenReadyForEpoch(int epoch) {
        return createRequest(this->thisNode(), epoch);
    }

    /// Nonblocking remote request. Data will be returned when it's become available.
    FutureType getWhenReadyForEpochFrom(const Node &node, int epoch) {
        return createRequest(node, epoch);
    }

    bool has() const {
        return localHas();
    }

    bool hasOn(const Node &node) const {
        return this->thisNode() == node ? localHas() : remoteHas(node).get();
    }

    void remove() {
        localRemove();
    }

    void removeOn(const Node &node) {
        if (this->thisNode() == node) {
            localRemove();
        } else {
            remoteRemove(node);
        }
    }

    ObjectPtrType localObject() const {
        std::lock_guard<std::mutex> lock(mutex);
        return local_object;
    }

    template <typename Func, typename ResultType, typename... Args>
    RemoteFunction<ResultType, Args...> makeGetter(Func func,
        typename std::enable_if<std::is_void<ResultType>::value>::type* = 0) {
        auto set_promise = this->createPromiseSetterVoid();
        auto handler = [set_promise, func](ObjectType &data, const Node &dest_node, size_t promise_id, Args... args) {
            func(data, args...);
            set_promise(dest_node, promise_id);
        };
        return makeHandlerGetter<decltype(handler), ResultType, Args...>(handler);
    }

    template <typename Func, typename ResultType, typename... Args>
    RemoteFunction<ResultType, Args...> makeGetter(Func func,
        typename std::enable_if<!std::is_void<ResultType>::value>::type* = 0) {
        auto set_promise = this->template createPromiseSetter<ResultType>();
        auto handler = [set_promise, func](ObjectType &data, const Node &dest_node, size_t promise_id, Args... args) {
            auto result = func(data, args...);
            set_promise(dest_node, promise_id, result);
        };
        return makeHandlerGetter<decltype(handler), ResultType, Args...>(handler);
    }

    template <typename Method, typename ResultType, typename... Args>
    RemoteFunction<ResultType, Args...> makeMethodGetter(Method method) {
        auto func = [method](ObjectType &obj, Args... args) {
            return (obj.*method)(args...);
        };
        return makeGetter<decltype(func), ResultType, Args...>(func);
    }

    template <typename Proc, typename... Args>
    RemoteFunction<void, Args...> makeCaller(Proc proc) {
        auto caller = [this, proc](Args... args) {
            this->callProcOnDataWhenReady(proc, args...);
        };
        return this->getRemote()->template remoteProcedure<decltype(caller), Args...>(caller);
    }

    template <typename Method, typename... Args>
    RemoteFunction<void, Args...> makeMethodCaller(Method method) {
        auto func = [method](ObjectType &obj, Args... args) {
            (obj.*method)(args...);
        };
        return makeCaller<decltype(func), Args...>(func);
    }

private:
    using RequestHandler = std::function<void(ObjectPtrType)>;

private:
    template <typename Handler, typename ResultType, typename... Args>
    auto makeHandlerGetter(Handler handler) {
        auto add_request_local = [this, handler](int epoch, const Node &dest_node, size_t promise_id, Args... args) {
            this->callProcOnDataWhenReady(handler, epoch, dest_node, promise_id, args...);
        };

        auto add_request = createRequestAdder<decltype(add_request_local), Args...>(add_request_local);

        return createGetter<decltype(add_request), ResultType, Args...>(add_request);
    }

    template <typename Proc, typename... Args>
    void callProcOnDataWhenReady(Proc proc, int epoch, Args... args) {
        std::unique_lock<std::mutex> lock(this->mutex);
        auto obj_ptr = this->local_object;
        auto obj_epoch = object_epoch;
        if (obj_ptr && obj_epoch >= epoch) {
            lock.unlock();
            callProcOnObj(proc, obj_ptr, args...);
        } else {
            auto handler = [this, proc, args...](std::shared_ptr<ObjectType> obj_ptr) {
                callProcOnObj(proc, obj_ptr, args...);
            };
            this->pending_requests[epoch].push_back(handler);
        }
    }

    template <typename Proc, typename... Args>
    void callProcOnObj(Proc proc, std::shared_ptr<ObjectType> obj_ptr, Args... args) {
        if (!obj_ptr) {
            throw std::runtime_error("callProcOnObj: null object pointer");
        }
        auto &obj = *(obj_ptr.get());
        proc(obj, args...);
    }

    template <typename AddRequestType, typename ResultType, typename... Args>
    auto createGetter(AddRequestType add_request) {
        auto getter = [this, add_request](const Node &dest_node, Args... args) {
            auto p = this->getPromises()->template addPromise<ResultType>();
            auto promise = p.first;
            auto promise_id = p.second;
            auto future = promise->get_future();
            add_request(current_epoch, dest_node, promise_id, args...);
            return future;
        };
        return getter;
    }

    template <typename AddRequestLocalType, typename... Args>
    auto createRequestAdder(AddRequestLocalType add_request_local) {
        auto add_request_remote = this->getRemote()->template remoteProcedure<decltype(add_request_local),
                int, const Node&, size_t, Args...>(add_request_local);
        auto add_request = [this, add_request_local, add_request_remote](int epoch,
                const Node &dest_node, size_t promise_id, Args... args) {
            if (this->thisNode() == dest_node) {
                add_request_local(epoch, dest_node, promise_id, args...);
            } else {
                add_request_remote(dest_node, epoch, this->thisNode(), promise_id, args...);
            }
        };
        return add_request;
    }

    void localAdd(int epoch, ObjectPtrType obj_ptr) {
        std::vector<RequestHandler> requests;
        {
            std::lock_guard<std::mutex> lock(mutex);
            local_object = obj_ptr;
            object_epoch = epoch;
            requests = extractPendingRequests(epoch);
        }
        processRequests(requests, obj_ptr);
    }

    template <typename Requests>
    void processRequests(const Requests &requests, std::shared_ptr<ObjectType> obj_ptr) {
        for (auto &handler : requests) {
            handler(obj_ptr);
        }
    }

    std::vector<RequestHandler> extractPendingRequests(int epoch) {
        std::vector<RequestHandler> requests;
        std::vector<int> epochs;
        for (auto &p: pending_requests) {
            auto src_epoch = p.first;
            if (src_epoch <= epoch) {
                epochs.push_back(src_epoch);
                requests.insert(requests.end(), p.second.begin(), p.second.end());
            }
        }
        for (const auto &ep: epochs) {
            pending_requests.erase(ep);
        }
        return requests;
    }

    ObjectPtrType localGet() const {
        std::lock_guard<std::mutex> lock(mutex);
        auto ptr = local_object;
        if (!ptr) {
            throw ItemNotFoundException("Failed to get an item");
        }
        return ptr;
    }

    FutureType createRequest(const Node &dest_node, int epoch) {
        auto p = this->getPromises()->template addPromise<ObjectPtrType>();
        auto promise = p.first;
        auto promise_id = p.second;
        auto future = promise->get_future();
        addRequest(dest_node, promise_id, epoch);
        return future;
    }

    void addRequest(const Node &dest_node, size_t promise_id, int epoch) {
        if (this->thisNode() == dest_node) {
            localAddRequest(dest_node, promise_id, epoch);
        } else {
            remoteAddRequest(dest_node, this->thisNode(), promise_id, epoch);
        }
    }

    void localAddRequest(const Node &dest_node, size_t promise_id, int epoch) {
        std::lock_guard<std::mutex> lock(mutex);
        auto obj_ptr = local_object;
        auto obj_epoch = object_epoch;
        if (obj_ptr && obj_epoch >= epoch) {
            this->setPromiseValue(dest_node, promise_id, obj_ptr);
        } else {
            auto handler = [this, dest_node, promise_id](std::shared_ptr<ObjectType> obj_ptr) {
                this->setPromiseValue(dest_node, promise_id, obj_ptr);
            };
            pending_requests[epoch].push_back(handler);
        }
    }

    std::shared_ptr<ObjectType> remoteGetWithCheck(const Node &node) const {
        try {
            return remoteGet(node).get();
        }
        catch (RemoteCallException &e) {
            throw ItemNotFoundException(e.what());
        }
    }

    bool localHas() const {
        std::lock_guard<std::mutex> lock(mutex);
        return local_object != nullptr;
    }

    void localRemove() {
        std::lock_guard<std::mutex> lock(mutex);
        local_object = nullptr;
        object_epoch = 0;
    }

private:
    std::atomic<int> current_epoch {0};

    ObjectPtrType local_object;

    mutable typename std::map<int, std::vector<RequestHandler>> pending_requests;

    int object_epoch;

    RemoteProcedure<int, std::shared_ptr<ObjectType>> remoteAdd;
    RemoteProcedure<> remoteRemove;
    RemoteFunction<ObjectPtrType> remoteGet;
    RemoteFunction<bool> remoteHas;
    RemoteProcedure<const Node&, size_t, int> remoteAddRequest;

    mutable std::mutex mutex;
};

}
