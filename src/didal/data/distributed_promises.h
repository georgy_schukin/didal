#pragma once

#include "local_storage.h"
#include "item_not_found_exception.h"
#include "didal/comm/remote.h"
#include "didal/comm/node_group.h"
#include "didal/base/promise_storage.h"

#include <map>
#include <cstddef>
#include <memory>
#include <mutex>
#include <functional>
#include <atomic>

namespace ddl {

class Environment;

/**
 * @brief Distributed promises.
 */
template <typename _ObjectType>
class DistributedPromises {
public:
    using ObjectType = _ObjectType;
    using ObjectPtrType = std::shared_ptr<ObjectType>;
    //using CollectionType = SyncDistributedStorage<IdType, ObjectType>;
    //using DataStorageType = LocalStorage<IdType, std::shared_ptr<ObjectType>>;
    //using FutureType = std::future<ObjectPtrType>;

public:
    DistributedPromises(Environment *env) :
        env(env), remote(env->getCommService(), this)
    {
        using CollType = DistributedPromises<ObjectType>;
        RemoteMethodMaker<CollType> maker(&remote, this);
        remoteSetPromiseValue = maker.method(&CollType::localSetPromiseValue).template proc<size_t, ObjectPtrType>();
        remote.startListening();
    }

    Environment* getEnvironment() {
        return env;
    }

    const Environment* getEnvironment() const {
        return env;
    }

    Node thisNode() const {
        return env->thisNode();
    }

protected:
    Remote* getRemote() {
        return &remote;
    }

    PromiseStorage* getPromises() {
        return &request_promises;
    }

    template <typename ResultType>
    auto createPromiseSetter() {
        auto set_promise_local = [this](size_t promise_id, ResultType &value) {
            this->localSetPromiseValueT(promise_id, std::move(value));
        };
        auto set_promise_remote = remote.remoteProcedure<decltype(set_promise_local), size_t, ResultType&>(set_promise_local);
        auto set_promise = [this, set_promise_local, set_promise_remote](const Node &node, size_t promise_id, ResultType &value) {
            if (thisNode() == node) {
                set_promise_local(promise_id, value);
            } else {
                set_promise_remote(node, promise_id, value);
            }
        };
        return set_promise;
    }

    auto createPromiseSetterVoid() {
        auto set_promise_local = [this](size_t promise_id) {
            this->localSetPromiseValueVoid(promise_id);
        };
        auto set_promise_remote = remote.remoteProcedure<decltype(set_promise_local), size_t>(set_promise_local);
        auto set_promise = [this, set_promise_local, set_promise_remote](const Node &node, size_t promise_id) {
            if (thisNode() == node) {
                set_promise_local(promise_id);
            } else {
                set_promise_remote(node, promise_id);
            }
        };
        return set_promise;
    }

    void setPromiseValue(const Node &node, size_t promise_id, ObjectPtrType obj_ptr) {
        if (thisNode() == node) {
            localSetPromiseValue(promise_id, obj_ptr);
        } else {
            remoteSetPromiseValue(node, promise_id, obj_ptr);
        }
    }

    void localSetPromiseValue(size_t promise_id, ObjectPtrType obj_ptr) {
        auto promise = this->getPromises()->template ejectPromise<ObjectPtrType>(promise_id);
        if (promise) {
            promise->set_value(obj_ptr);
        }
    }

    template <typename ValueType>
    void localSetPromiseValueT(size_t promise_id, ValueType &&value) {
        auto promise = this->getPromises()->template ejectPromise<std::remove_reference_t<ValueType>>(promise_id);
        if (promise) {
            promise->set_value(std::forward<ValueType>(value));
        }
    }

    void localSetPromiseValueVoid(size_t promise_id) {
        auto promise = this->getPromises()->template ejectPromise<void>(promise_id);
        if (promise) {
            promise->set_value();
        }
    }

private:
    Environment *env;

    Remote remote;
    RemoteProcedure<size_t, std::shared_ptr<ObjectType>> remoteSetPromiseValue;

    mutable PromiseStorage request_promises;
};

}
