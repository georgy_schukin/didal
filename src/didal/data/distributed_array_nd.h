#pragma once

#include "distributed_collection.h"
#include "didal/decomp/decomposition.h"
#include "didal/decomp/block_decomposition.h"
#include "didal/distr/distribution.h"
#include "didal/env/environment.h"
#include "didal/base/array_nd.h"

#include <cstddef>
#include <utility>
#include <memory>
#include <array>

namespace ddl {

template <typename ElemType,
          size_t Dims = 1,
          //typename DecompositionType = BlockDecomposition,
          class PartType = ArrayND<ElemType, Dims>, typename ShapeType = IndexRangeND<Dims>>
class DistributedArrayND : public DistributedCollection<Index<Dims>, PartType> {
public:

public:
    DistributedArrayND(Environment *env,
                     const std::array<size_t, Dims> &dims,
                     //const Decomposition &decomposition,
                     //const Distribution &distribution,
                     const NodeGroup &nodes):
        DistributedCollection<Index<Dims>, PartType>(env, nodes),
        _dims(dims)
    {
        //_decomposition = std::unique_ptr<Decomposition>(decomposition.clone());
        //_distribution = std::unique_ptr<Distribution>(distribution.clone());
        /*ranges = decomposition.decompose(_size);
        const auto dmap = distribution.distribute(IndexRange(0, ranges.size()).toVector(), nodes);
        const auto it = dmap.find(env->thisNode().rank());
        if (it != dmap.end()) {
            my_ranges_ids = it->second;
        }
        for (const auto &id: my_ranges_ids) {
            this->add(id, PartType(ranges[id]));
        }*/
    }

    size_t size() const {
        return 0;
    }

    const std::vector<Index<Dims>>& localIds() const {
        return my_blocks_ids;
    }

private:
    std::array<size_t, Dims> _dims;
    //std::array<std::vector<IndexRange>, Dims> ranges; // array is decomposed on blocks of ranges;
    std::vector<Index<Dims>> my_blocks_ids; // ids of local blocks
    //std::unique_ptr<Decomposition> _decomposition;
    //std::unique_ptr<Distribution> _distribution;
};

}
