#pragma once

#include <map>
#include <vector>

namespace ddl {

template <class ShapeId, class Shape>
class DistributedShape {
public:
private:
    std::map<ShapeId, Shape> shapes;
    std::vector<ShapeId> local_shape_ids;
};

}
