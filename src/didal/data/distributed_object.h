#pragma once

#include "local_storage.h"
#include "didal/comm/remote.h"
#include "didal/comm/node_group.h"
#include "didal/base/promise_storage.h"

#include <map>
#include <cstddef>
#include <memory>
#include <mutex>
#include <functional>

namespace ddl {

class Environment;

/**
 * @brief Object which is present on each node.
 */
template <typename _ObjectType>
class DistributedObject {
public:
    using ObjectType = _ObjectType;

public:
    template<typename... Args>
    DistributedObject(Environment *env, Args... args) :
        env(env), object(args...), remote(env->getCommService(), this)
    {      
        remote.startListening();
    }

    template <typename Method>
    auto remoteMethod(Method method) {
        RemoteMethodMaker<ObjectType> maker(&remote, this);
        return maker.method(method);
    }

    /*

    /// Blocking local get. Throws exception if there is no data with such id.
    std::shared_ptr<ObjectType> get(const IdType &id) const {
        return localGet(id);
    }

    /// Blocking remote get. Throws exception if there is no data with such id.
    std::shared_ptr<ObjectType> getFrom(const Node &node, const IdType &id) const {
        return env->thisNode() == node ? localGet(id) : remoteGetWithCheck(node, id);
    }

    /// Nonblocking remote get. Throws exception if there is no data with such id.
    std::future<std::shared_ptr<ObjectType>> getFromAsync(const Node &node, const IdType &id) {
        return env->thisNode() == node ?
            std::async(std::launch::async, [this, id]() { return this->localGet(id); }) :
            std::async(std::launch::async, [this, node, id]() { return this->remoteGetWithCheck(node, id); });
    }

    /// Nonblocking local request. Data will be returned when it's become available.
    std::future<std::shared_ptr<ObjectType>> getWhenReady(const IdType &id) {
        return createRequest(env->thisNode(), id);
    }

    /// Nonblocking local request launched in separate thread. Data will be returned when it's become available.
    std::future<std::shared_ptr<ObjectType>> getWhenReadyAsync(const IdType &id) {
        return std::async(std::launch::async, [this, id] () {
            return this->createRequest(this->env->thisNode(), id).get();
        });
    }

    /// Nonblocking remote request. Data will be returned when it's become available.
    std::future<std::shared_ptr<ObjectType>> getWhenReadyFrom(const Node &node, const IdType &id) {
        return createRequest(node, id);
    }

    /// Nonblocking remote request launched in separate thread. Data will be returned when it's become available.
    std::future<std::shared_ptr<ObjectType>> getWhenReadyFromAsync(const Node &node, const IdType &id) {
        return std::async(std::launch::async, [this, node, id] () {
            return this->createRequest(node, id).get();
        });
    }

    template <typename Func, typename ResultType, typename... Args>
    std::function<std::future<ResultType>(const Node&, const IdType&, Args...)> makeGetter(Func func,
        typename std::enable_if<std::is_void<ResultType>::value>::type* = 0) {
        auto set_promise = createPromiseSetterVoid();
        auto handler = [set_promise, func](ObjectType &data, const Node &dest_node, size_t promise_id, Args... args) {
            func(data, args...);
            set_promise(dest_node, promise_id);
        };
        return makeHandlerGetter<decltype(handler), ResultType, Args...>(handler);
    }

    template <typename Func, typename ResultType, typename... Args>
    std::function<std::future<ResultType>(const Node&, const IdType&, Args...)> makeGetter(Func func,
        typename std::enable_if<!std::is_void<ResultType>::value>::type* = 0) {
        auto set_promise = createPromiseSetter<ResultType>();
        auto handler = [set_promise, func](ObjectType &data, const Node &dest_node, size_t promise_id, Args... args) {
            auto result = func(data, args...);
            set_promise(dest_node, promise_id, result);
        };
        return makeHandlerGetter<decltype(handler), ResultType, Args...>(handler);
    }

    template <typename Method, typename ResultType, typename... Args>
    std::function<std::future<ResultType>(const Node&, const IdType&, Args...)> makeMethodGetter(Method method) {
        auto func = [method](ObjectType &obj, Args... args) {
            return (obj.*method)(args...);
        };
        return makeGetter<decltype(func), ResultType, Args...>(func);
    }

    template <typename Proc, typename... Args>
    std::function<void(const Node&, const IdType&, Args...)> makeCaller(Proc proc) {
        auto caller = [this, proc](const IdType &id, Args... args) {
            this->callProcOnDataWhenReady(proc, id, args...);
        };
        return remote.remoteProcedure<decltype(caller), const IdType&, Args...>(caller);
    }

    template <typename Method, typename... Args>
    std::function<void(const Node&, const IdType&, Args...)> makeMethodCaller(Method method) {
        auto func = [method](ObjectType &obj, Args... args) {
            (obj.*method)(args...);
        };
        return makeCaller<decltype(func), Args...>(func);
    }

    Environment* getEnvironment() {
        return env;
    }*/

    ObjectType& localObject() {
        return object;
    }

    const ObjectType& localObject() const {
        return object;
    }

private:
    using RequestHandler = std::function<void(std::shared_ptr<ObjectType>)>;

private:
    /*template <typename Handler, typename ResultType, typename... Args>
    std::function<std::future<ResultType>(const Node&, const IdType&, Args...)> makeHandlerGetter(Handler handler) {
        auto add_request_local = [this, handler](const IdType &id, const Node &dest_node, size_t promise_id, Args... args) {
            this->callProcOnDataWhenReady(handler, id, dest_node, promise_id, args...);
        };

        auto add_request = createRequestAdder<decltype(add_request_local), Args...>(add_request_local);

        return createGetter<decltype(add_request), ResultType, Args...>(add_request);
    }

    template <typename Proc, typename... Args>
    void callProcOnDataWhenReady(Proc proc, const IdType &id, Args... args) {
        std::unique_lock<std::mutex> lock(this->mutex);
        auto obj_ptr = this->local_data.getOrDefault(id);
        if (obj_ptr) {
            lock.unlock();
            callProcOnObj(proc, obj_ptr, args...);
        } else {
            auto handler = [this, proc, args...](std::shared_ptr<ObjectType> obj_ptr) {
                callProcOnObj(proc, obj_ptr, args...);
            };
            this->pending_requests[id].push_back(handler);
        }
    }

    template <typename Proc, typename... Args>
    void callProcOnObj(Proc proc, Args... args) {
        proc(object, args...);
    }

    template <typename ReturnType, typename Method, typename... Args>
    std::future<ReturnType> callMethodOnObj(Method method, Args... args) {
        return (object.*method)(args...);
    }

    template <typename AddRequestType, typename ResultType, typename... Args>
    auto createGetter(AddRequestType add_request) {
        auto getter = [this, add_request](const Node &dest_node, const IdType &id, Args... args) {
            auto p = this->request_promises.template addPromise<ResultType>();
            auto promise = p.first;
            auto promise_id = p.second;
            auto future = promise->get_future();
            add_request(id, dest_node, promise_id, args...);
            return future;
        };
        return getter;
    }

    template <typename ResultType>
    auto createPromiseSetter() {
        auto set_promise_local = [this](size_t promise_id, ResultType &value) {
            this->localSetPromiseValueT(promise_id, std::move(value));
        };
        auto set_promise_remote = remote.remoteProcedure<decltype(set_promise_local), size_t, ResultType&>(set_promise_local);
        auto set_promise = [this, set_promise_local, set_promise_remote](const Node &node, size_t promise_id, ResultType &value) {
            if (this->env->thisNode() == node) {
                set_promise_local(promise_id, value);
            } else {
                set_promise_remote(node, promise_id, value);
            }
        };
        return set_promise;
    }

    auto createPromiseSetterVoid() {
        auto set_promise_local = [this](size_t promise_id) {
            this->localSetPromiseValueVoid(promise_id);
        };
        auto set_promise_remote = remote.remoteProcedure<decltype(set_promise_local), size_t>(set_promise_local);
        auto set_promise = [this, set_promise_local, set_promise_remote](const Node &node, size_t promise_id) {
            if (this->env->thisNode() == node) {
                set_promise_local(promise_id);
            } else {
                set_promise_remote(node, promise_id);
            }
        };
        return set_promise;
    }

    template <typename AddRequestLocalType, typename... Args>
    auto createRequestAdder(AddRequestLocalType add_request_local) {
        auto add_request_remote = remote.remoteProcedure<decltype(add_request_local),
                const IdType&, const Node&, size_t, Args...>(add_request_local);
        auto add_request = [this, add_request_local, add_request_remote](const IdType &id,
                const Node &dest_node, size_t promise_id, Args... args) {
            if (this->env->thisNode() == dest_node) {
                add_request_local(id, dest_node, promise_id, args...);
            } else {
                add_request_remote(dest_node, id, this->env->thisNode(), promise_id, args...);
            }
        };
        return add_request;
    }

    void localAdd(const IdType &id, std::shared_ptr<ObjectType> obj_ptr) {
        std::vector<RequestHandler> requests;
        {
            std::lock_guard<std::mutex> lock(mutex);
            local_data.add(id, obj_ptr);
            requests = extractPendingRequests(id);
        }
        for (auto &handler : requests) {
            handler(obj_ptr);
        }
    }

    std::vector<RequestHandler> extractPendingRequests(const IdType &id) {
        auto it = pending_requests.find(id);
        if (it != pending_requests.end()) {
            auto requests = std::move(it->second);
            pending_requests.erase(it);
            return requests;
        }
        return std::vector<RequestHandler> {};
    }

    std::shared_ptr<ObjectType> localGet(const IdType &id) const {
        std::lock_guard<std::mutex> lock(mutex);
        auto ptr = local_data.getOrDefault(id);
        if (!ptr) {
            throw ItemNotFoundException("Failed to get an item");
        }
        return ptr;
    }

    std::future<std::shared_ptr<ObjectType>> createRequest(const Node &dest_node, const IdType &id) {
        auto p = request_promises.addPromise<std::shared_ptr<ObjectType>>();
        auto promise = p.first;
        auto promise_id = p.second;
        auto future = promise->get_future();
        addRequest(dest_node, promise_id, id);
        return future;
    }

    void addRequest(const Node &dest_node, size_t promise_id, const IdType &id) {
        if (env->thisNode() == dest_node) {
            localAddRequest(dest_node, promise_id, id);
        } else {
            remoteAddRequest(dest_node, env->thisNode(), promise_id, id);
        }
    }

    void localAddRequest(const Node &dest_node, size_t promise_id, const IdType &id) {
        std::lock_guard<std::mutex> lock(mutex);
        auto obj_ptr = local_data.getOrDefault(id);
        if (obj_ptr) {
            setPromiseValue(dest_node, promise_id, obj_ptr);
        } else {
            auto handler = [this, dest_node, promise_id](std::shared_ptr<ObjectType> obj_ptr) {
                this->setPromiseValue(dest_node, promise_id, obj_ptr);
            };
            pending_requests[id].push_back(handler);
        }
    }

    void setPromiseValue(const Node &node, size_t promise_id, std::shared_ptr<ObjectType> obj_ptr) {
        if (env->thisNode() == node) {
            localSetPromiseValue(promise_id, obj_ptr);
        } else {
            remoteSetPromiseValue(node, promise_id, obj_ptr);
        }
    }

    void localSetPromiseValue(size_t promise_id, std::shared_ptr<ObjectType> obj_ptr) {
        auto promise = request_promises.ejectPromise<std::shared_ptr<ObjectType>>(promise_id);
        if (promise) {
            promise->set_value(obj_ptr);
        }
    }

    template <typename ValueType>
    void localSetPromiseValueT(size_t promise_id, ValueType &&value) {
        auto promise = request_promises.ejectPromise<std::remove_reference_t<ValueType>>(promise_id);
        if (promise) {
            promise->set_value(std::forward<ValueType>(value));
        }
    }

    void localSetPromiseValueVoid(size_t promise_id) {
        auto promise = request_promises.ejectPromise<void>(promise_id);
        if (promise) {
            promise->set_value();
        }
    }

    std::shared_ptr<ObjectType> remoteGetWithCheck(const Node &node, const IdType &id) const {
        try {
            return remoteGet(node, id).get();
        }
        catch (RemoteCallException &e) {
            throw ItemNotFoundException(e.what());
        }
    }

    bool localHas(const IdType &id) const {
        std::lock_guard<std::mutex> lock(mutex);
        return local_data.has(id);
    }

    void localRemove(const IdType &id) {
        std::lock_guard<std::mutex> lock(mutex);
        local_data.remove(id);
    }*/

private:
    Environment *env;
    ObjectType object;

    //mutable typename std::map<IdType, std::vector<RequestHandler>> pending_requests;
    mutable PromiseStorage request_promises;

    Remote remote;

    mutable std::mutex mutex;
};

}
