#pragma once

#include "didal/base/basic_exception.h"

namespace ddl {

class ItemNotFoundException : public BasicException {
public:
    ItemNotFoundException(const char *msg) :
        BasicException(msg) {
    }
    ItemNotFoundException(const std::string &msg) :
        BasicException(msg) {
    }
};

}
