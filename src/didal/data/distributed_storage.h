#pragma once

#include "local_storage.h"
#include "distributed_promises.h"
#include "item_not_found_exception.h"
#include "didal/comm/remote.h"
#include "didal/comm/node_group.h"
#include "didal/base/promise_storage.h"

#include <map>
#include <cstddef>
#include <memory>
#include <mutex>
#include <functional>

namespace ddl {

class Environment;

/**
 * @brief Distributed collection (associative array) of items.
 */
template <typename _IdType, typename _ObjectType>
class DistributedStorage : public DistributedPromises<_ObjectType> {
public:
    using IdType = _IdType;
    using ObjectType = _ObjectType;
    using CollectionType = DistributedStorage<IdType, ObjectType>;
    using DataStorageType = LocalStorage<IdType, std::shared_ptr<ObjectType>>;
    //using iterator = typename DataStorageType::iterator;
    //using const_iterator = typename DataStorageType::const_iterator;

public:
    DistributedStorage(Environment *env) :
        DistributedPromises<_ObjectType>(env)
    {
        RemoteMethodMaker<CollectionType> maker(this->getRemote(), this);
        remoteAdd = maker.method(&CollectionType::localAdd).template proc<const IdType&, std::shared_ptr<ObjectType>>();
        remoteGet = maker.method(&CollectionType::localGet).template func<std::shared_ptr<ObjectType>, const IdType&>();
        remoteHas = maker.method(&CollectionType::localHas).template func<bool, const IdType&>();
        remoteRemove = maker.method(&CollectionType::localRemove).template proc<const IdType&>();        
        remoteAddRequest = maker.method(&CollectionType::localAddRequest).template proc<const Node&, size_t, const IdType&>();
        this->getRemote()->startListening();
    }

    /// Construct new item from specified arguments and store it with specified id on current node.
    template <typename... Args>
    void create(const IdType &id, Args&&... args) {
        localAdd(id, std::make_shared<ObjectType>(std::forward<Args...>(args...)));
    }

    /// Construct new item from specified arguments and store it with specified id on specified node.
    template <typename... Args>
    void createOn(const Node &node, const IdType &id, Args&&... args) {
        addOn(node, id, std::make_shared<ObjectType>(std::forward<Args...>(args...)));
    }

    /// Store specified item with specified id on current node.
    void add(const IdType &id, const ObjectType &obj) {
        localAdd(id, std::make_shared<ObjectType>(obj));
    }

    /// Store specified item with specified id on current node.
    void add(const IdType &id, ObjectType &&obj) {
        localAdd(id, std::make_shared<ObjectType>(std::move(obj)));
    }

    /// Store specified item with specified id on current node.
    void add(const IdType &id, std::shared_ptr<ObjectType> obj_ptr) {
        localAdd(id, obj_ptr);
    }

    /// Store specified item with specified id on specified node.
    void addOn(const Node &node, const IdType &id, const ObjectType &obj) {
        addOn(node, id, std::make_shared<ObjectType>(obj));
    }

    /// Store specified item with specified id on specified node.
    void addOn(const Node &node, const IdType &id, ObjectType &&obj) {
        addOn(node, id, std::make_shared<ObjectType>(std::move(obj)));
    }

    /// Store specified item with specified id on specified node.
    void addOn(const Node &node, const IdType &id, std::shared_ptr<ObjectType> obj_ptr) {
        if (this->thisNode() == node) {
            localAdd(id, obj_ptr);
        } else {
            remoteAdd(node, id, obj_ptr);
        }
    }

    /// Blocking local get. Throws exception if there is no item with such id.
    std::shared_ptr<ObjectType> get(const IdType &id) const {
        return localGet(id);
    }

    /// Blocking remote get. Throws exception if there is no item with such id.
    std::shared_ptr<ObjectType> getFrom(const Node &node, const IdType &id) const {
        return this->thisNode() == node ? localGet(id) : remoteGetWithCheck(node, id);
    }

    /// Nonblocking remote get. Throws exception if there is no item with such id.
    std::future<std::shared_ptr<ObjectType>> getFromAsync(const Node &node, const IdType &id) {
        return this->thisNode() == node ?
            std::async(std::launch::async, [this, id]() { return this->localGet(id); }) :
            std::async(std::launch::async, [this, node, id]() { return this->remoteGetWithCheck(node, id); });
    }

    /// Nonblocking local request. Item will be returned when it's become available.
    std::future<std::shared_ptr<ObjectType>> getWhenReady(const IdType &id) {
        return createRequest(this->thisNode(), id);
    }

    /// Nonblocking local request launched in separate thread. Item will be returned when it's become available.
    std::future<std::shared_ptr<ObjectType>> getWhenReadyAsync(const IdType &id) {
        return std::async(std::launch::async, [this, id] () {
            return this->createRequest(this->thisNode(), id).get();
        });
    }

    /// Nonblocking remote request. Item will be returned when it's become available.
    std::future<std::shared_ptr<ObjectType>> getWhenReadyFrom(const Node &node, const IdType &id) {
        return createRequest(node, id);
    }

    /// Nonblocking remote request launched in separate thread. Item will be returned when it's become available.
    std::future<std::shared_ptr<ObjectType>> getWhenReadyFromAsync(const Node &node, const IdType &id) {
        return std::async(std::launch::async, [this, node, id] () {
            return this->createRequest(node, id).get();
        });
    }

    /// Check if item with specified id exists on current node.
    bool has(const IdType &id) const {
        return localHas(id);
    }

    /// Check if item with specified id exists on specified node.
    bool hasOn(const Node &node, const IdType &id) const {
        return this->thisNode() == node ? localHas(id) : remoteHas(node, id).get();
    }

    /// Move (relocate) item with specified id to specified node.
    void moveTo(const IdType &id, const Node &node) {
        if (this->thisNode() == node) {
            return;
        }
        auto object = localGet(id);
        if (object) {
            remoteAdd(node, id, object);
            remove(id);
        }
    }

    /// Remove (delete) item with specified id on current node.
    void remove(const IdType &id) {
        localRemove(id);
    }

    /// Remove (delete) item with specified id on specified node.
    void removeOn(const IdType &id, const Node &node) {
        if (this->thisNode() == node) {
            localRemove(id);
        } else {
            remoteRemove(node, id);
        }
    }

    /// Return contents of current node: array of pairs, each pair contains item's id and a (shared) pointer to the item.
    std::vector<std::pair<IdType, std::shared_ptr<ObjectType>>> localContents() const {
        std::lock_guard<std::mutex> lock(mutex);
        if (update_contents_cache) {
            contents_cache = getLocalContents();
            update_contents_cache = false;
        }
        return contents_cache;
    }

    /*std::vector<std::pair<IdType, std::shared_ptr<const ObjectType>>> localContents() const {
        std::lock_guard<std::mutex> lock(mutex);
        typename std::vector<std::pair<IdType, std::shared_ptr<const ObjectType>>> contents;
        for (const auto &id: local_data.getIds()) {
            contents.push_back(std::make_pair(id, std::const_pointer_cast<const ObjectType>(local_data.get(id))));
        }
        return contents;
    }*/

    /// Return current node's items: array of (shared) pointers to items.
    std::vector<std::shared_ptr<ObjectType>> localObjects() const {
        typename std::vector<std::shared_ptr<ObjectType>> objects;
        for (const auto &p: localContents()) {
            objects.push_back(p.second);
        }
        return objects;
    }

    void finishWrite() {
        std::lock_guard<std::mutex> lock(mutex);
        remote_cache.clear();
    }

    template <typename PartType, typename ShapeType>
    const PartType getAs(const IdType &id, const ShapeType &shape)
    {
        auto data = get(id);
        return PartType {data, shape};
    }

    /// Create async remote function object.
    /// This will allow to call specified function (with void return type) on an item from this collection (item's id and node should be specified during the call).
    /// Returing future can be used to check that the call is completed.
    template <typename Func, typename ResultType, typename... Args>
    std::function<std::future<ResultType>(const Node&, const IdType&, Args...)> makeGetter(Func func,
        typename std::enable_if<std::is_void<ResultType>::value>::type* = 0) {
        auto set_promise = this->createPromiseSetterVoid();
        auto handler = [set_promise, func](ObjectType &data, const Node &dest_node, size_t promise_id, Args... args) {
            func(data, args...);
            set_promise(dest_node, promise_id);
        };
        return makeHandlerGetter<decltype(handler), ResultType, Args...>(handler);
    }

    /// Create async remote function object.
    /// This will allow to call specified function on an item from this collection (item's id and node should be specified during the call).
    /// Returing future can be used to receive the result of the call.
    template <typename Func, typename ResultType, typename... Args>
    std::function<std::future<ResultType>(const Node&, const IdType&, Args...)> makeGetter(Func func,
        typename std::enable_if<!std::is_void<ResultType>::value>::type* = 0) {
        auto set_promise = this->template createPromiseSetter<ResultType>();
        auto handler = [set_promise, func](ObjectType &data, const Node &dest_node, size_t promise_id, Args... args) {
            auto result = func(data, args...);
            set_promise(dest_node, promise_id, result);
        };
        return makeHandlerGetter<decltype(handler), ResultType, Args...>(handler);
    }

    /// Create async remote function object.
    /// This will allow to call specified item's class method on an item from this collection (item's id and node should be specified during the call).
    /// Returing future can be used to receive the result of the call.
    template <typename Method, typename ResultType, typename... Args>
    std::function<std::future<ResultType>(const Node&, const IdType&, Args...)> makeMethodGetter(Method method) {
        auto func = [method](ObjectType &obj, Args... args) {
            return (obj.*method)(args...);
        };
        return makeGetter<decltype(func), ResultType, Args...>(func);
    }

    /// Create async remote procedure object.
    /// This will allow to call specified procedure on an item from this collection (item's id and node should be specified during the call).
    template <typename Proc, typename... Args>
    std::function<void(const Node&, const IdType&, Args...)> makeCaller(Proc proc) {
        auto caller = [this, proc](const IdType &id, Args... args) {
            this->callProcOnDataWhenReady(proc, id, args...);
        };
        return this->getRemote()->template remoteProcedure<decltype(caller), const IdType&, Args...>(caller);
    }

    /// Create async remote procedure object.
    /// This will allow to call specified item's class method as procedure on an item from this collection (item's id and node should be specified during the call).
    template <typename Method, typename... Args>
    std::function<void(const Node&, const IdType&, Args...)> makeMethodCaller(Method method) {
        auto func = [method](ObjectType &obj, Args... args) {
            (obj.*method)(args...);
        };
        return makeCaller<decltype(func), Args...>(func);
    }

private:
    using RequestHandler = std::function<void(std::shared_ptr<ObjectType>)>;

private:
    template <typename Handler, typename ResultType, typename... Args>
    std::function<std::future<ResultType>(const Node&, const IdType&, Args...)> makeHandlerGetter(Handler handler) {
        auto add_request_local = [this, handler](const IdType &id, const Node &dest_node, size_t promise_id, Args... args) {
            this->callProcOnDataWhenReady(handler, id, dest_node, promise_id, args...);
        };

        auto add_request = createRequestAdder<decltype(add_request_local), Args...>(add_request_local);

        return createGetter<decltype(add_request), ResultType, Args...>(add_request);
    }

    template <typename Proc, typename... Args>
    void callProcOnDataWhenReady(Proc proc, const IdType &id, Args... args) {
        std::unique_lock<std::mutex> lock(this->mutex);
        auto obj_ptr = this->local_data.getOrDefault(id);
        if (obj_ptr) {
            lock.unlock();
            callProcOnObj(proc, obj_ptr, args...);
        } else {
            auto handler = [this, proc, args...](std::shared_ptr<ObjectType> obj_ptr) {
                callProcOnObj(proc, obj_ptr, args...);
            };
            this->pending_requests[id].push_back(handler);
        }
    }

    template <typename Proc, typename... Args>
    void callProcOnObj(Proc proc, std::shared_ptr<ObjectType> obj_ptr, Args... args) {
        if (!obj_ptr) {
            throw std::runtime_error("callProcOnObj: null object pointer");
        }
        auto &obj = *(obj_ptr.get());
        proc(obj, args...);
    }

    template <typename AddRequestType, typename ResultType, typename... Args>
    auto createGetter(AddRequestType add_request) {
        auto getter = [this, add_request](const Node &dest_node, const IdType &id, Args... args) {
            auto p = this->getPromises()->template addPromise<ResultType>();
            auto promise = p.first;
            auto promise_id = p.second;
            auto future = promise->get_future();
            add_request(id, dest_node, promise_id, args...);
            return future;
        };
        return getter;
    }

    template <typename AddRequestLocalType, typename... Args>
    auto createRequestAdder(AddRequestLocalType add_request_local) {
        auto add_request_remote = this->getRemote()->template remoteProcedure<decltype(add_request_local),
                const IdType&, const Node&, size_t, Args...>(add_request_local);
        auto add_request = [this, add_request_local, add_request_remote](const IdType &id,
                const Node &dest_node, size_t promise_id, Args... args) {
            if (this->thisNode() == dest_node) {
                add_request_local(id, dest_node, promise_id, args...);
            } else {
                add_request_remote(dest_node, id, this->thisNode(), promise_id, args...);
            }
        };
        return add_request;
    }

    void localAdd(const IdType &id, std::shared_ptr<ObjectType> obj_ptr) {
        std::vector<RequestHandler> requests;
        {
            std::lock_guard<std::mutex> lock(mutex);
            local_data.add(id, obj_ptr);
            update_contents_cache = true;
            requests = extractPendingRequests(id);
        }
        for (auto &handler : requests) {
            handler(obj_ptr);
        }
    }

    std::vector<RequestHandler> extractPendingRequests(const IdType &id) {
        auto it = pending_requests.find(id);
        if (it != pending_requests.end()) {
            auto requests = std::move(it->second);
            pending_requests.erase(it);
            return requests;
        }
        return std::vector<RequestHandler> {};
    }

    std::shared_ptr<ObjectType> localGet(const IdType &id) const {
        std::lock_guard<std::mutex> lock(mutex);
        auto ptr = local_data.getOrDefault(id);
        if (!ptr) {
            throw ItemNotFoundException("Failed to get an item");
        }
        return ptr;
    }

    std::future<std::shared_ptr<ObjectType>> createRequest(const Node &dest_node, const IdType &id) {
        auto p = this->getPromises()->template addPromise<std::shared_ptr<ObjectType>>();
        auto promise = p.first;
        auto promise_id = p.second;
        auto future = promise->get_future();
        addRequest(dest_node, promise_id, id);
        return future;
    }

    void addRequest(const Node &dest_node, size_t promise_id, const IdType &id) {
        if (this->thisNode() == dest_node) {
            localAddRequest(dest_node, promise_id, id);
        } else {
            remoteAddRequest(dest_node, this->thisNode(), promise_id, id);
        }
    }

    void localAddRequest(const Node &dest_node, size_t promise_id, const IdType &id) {
        std::lock_guard<std::mutex> lock(mutex);
        auto obj_ptr = local_data.getOrDefault(id);
        if (obj_ptr) {
            this->setPromiseValue(dest_node, promise_id, obj_ptr);
        } else {
            auto handler = [this, dest_node, promise_id](std::shared_ptr<ObjectType> obj_ptr) {
                this->setPromiseValue(dest_node, promise_id, obj_ptr);
            };
            pending_requests[id].push_back(handler);
        }
    }

    std::shared_ptr<ObjectType> remoteGetWithCheck(const Node &node, const IdType &id) const {
        try {
            return remoteGet(node, id).get();
        }
        catch (RemoteCallException &e) {
            throw ItemNotFoundException(e.what());
        }
    }

    bool localHas(const IdType &id) const {
        std::lock_guard<std::mutex> lock(mutex);
        return local_data.has(id);
    }

    void localRemove(const IdType &id) {
        std::lock_guard<std::mutex> lock(mutex);
        local_data.remove(id);
        update_contents_cache = true;
    }

    std::vector<std::pair<IdType, std::shared_ptr<ObjectType>>> getLocalContents() const {
        typename std::vector<std::pair<IdType, std::shared_ptr<ObjectType>>> contents;
        for (const auto &p: local_data) {
            contents.push_back(std::make_pair(p.first, p.second));
        }
        return contents;
    }

private:
    DataStorageType local_data;
    mutable DataStorageType remote_cache;

    mutable typename std::map<IdType, std::vector<RequestHandler>> pending_requests;    

    RemoteProcedure<const IdType&, std::shared_ptr<ObjectType>> remoteAdd;
    RemoteProcedure<const IdType&> remoteRemove;
    RemoteFunction<std::shared_ptr<ObjectType>, const IdType&> remoteGet;
    RemoteFunction<bool, const IdType&> remoteHas;
    RemoteProcedure<const Node&, size_t, const IdType&> remoteAddRequest;

    mutable std::mutex mutex;

    mutable typename std::vector<std::pair<IdType, std::shared_ptr<ObjectType>>> contents_cache;
    mutable bool update_contents_cache = true;
};

}
