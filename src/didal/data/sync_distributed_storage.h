#pragma once

#include "local_storage.h"
#include "distributed_promises.h"
#include "item_not_found_exception.h"
#include "didal/comm/remote.h"
#include "didal/comm/node_group.h"
#include "didal/base/promise_storage.h"

#include <map>
#include <cstddef>
#include <memory>
#include <mutex>
#include <functional>
#include <atomic>

namespace ddl {

class Environment;

/**
 * @brief Distributed collection of items with synchronization by epochs.
 */
template <typename _IdType, typename _ObjectType>
class SyncDistributedStorage : public DistributedPromises<_ObjectType> {
public:
    using IdType = _IdType;
    using ObjectType = _ObjectType;
    using ObjectPtrType = std::shared_ptr<ObjectType>;
    using CollectionType = SyncDistributedStorage<IdType, ObjectType>;
    using DataStorageType = LocalStorage<IdType, ObjectPtrType>;
    using FutureType = std::future<ObjectPtrType>;

public:
    SyncDistributedStorage(Environment *env) :
        DistributedPromises<_ObjectType>(env)
    {
        RemoteMethodMaker<CollectionType> maker(this->getRemote(), this);
        remoteAdd = maker.method(&CollectionType::localAdd).template proc<const IdType&, int, std::shared_ptr<ObjectType>>();
        remoteGet = maker.method(&CollectionType::localGet).template func<std::shared_ptr<ObjectType>, const IdType&>();
        remoteHas = maker.method(&CollectionType::localHas).template func<bool, const IdType&>();
        remoteRemove = maker.method(&CollectionType::localRemove).template proc<const IdType&>();
        //remoteSetPromiseValue = maker.method(&CollectionType::localSetPromiseValue).template proc<size_t, std::shared_ptr<ObjectType>>();
        remoteAddRequest = maker.method(&CollectionType::localAddRequest).template proc<const Node&, size_t, const IdType&, int>();
        this->getRemote()->startListening();
    }

    /// Change epoch for current node.
    /// All future requests from this node will use this epoch.
    void nextEpoch() {
        current_epoch++;
    }

    /// Return current epoch for current node.
    int currentEpoch() const {
        return current_epoch;
    }

    /// Signal that an item with specified id reached current epoch (of current node).
    /// Requests awating this epoch for this item will be processed.
    void signalEpoch(const IdType &id) {
        std::vector<RequestHandler> requests;
        std::shared_ptr<ObjectType> obj_ptr;
        {
            std::lock_guard<std::mutex> lock(mutex);
            const auto epoch = current_epoch.load();
            data_epochs[id] = epoch;
            obj_ptr = local_data.getOrDefault(id);
            if (obj_ptr) {
                requests = extractPendingRequests(id, epoch);
            }
        }
        if (obj_ptr) {
            processRequests(requests, obj_ptr);
        }
    }

    /// Signal that all items on current node reached its current epoch.
    /// Requests awating this epoch will be processed.
    void signalEpochAllLocal() {
        std::map<std::shared_ptr<ObjectType>, std::vector<RequestHandler>> requests;
        {
            std::lock_guard<std::mutex> lock(mutex);
            const auto epoch = current_epoch.load();
            for (auto &p: local_data) {
                data_epochs[p.first] = epoch;
                auto reqs = extractPendingRequests(p.first, epoch);
                if (!reqs.empty()) {
                    requests[p.second] = std::move(reqs);
                }
            }
        }
        for (auto &p: requests) {
            processRequests(p.second, p.first);
        }
    }

    template <typename... Args>
    void create(const IdType &id, Args&&... args) {
        localAdd(id, current_epoch, std::make_shared<ObjectType>(std::forward<Args...>(args...)));
    }

    template <typename... Args>
    void createOn(const Node &node, const IdType &id, Args&&... args) {
        addOn(node, id, std::make_shared<ObjectType>(std::forward<Args...>(args...)));
    }

    void add(const IdType &id, const ObjectType &obj) {
        localAdd(id, current_epoch, std::make_shared<ObjectType>(obj));
    }

    void add(const IdType &id, ObjectType &&obj) {
        localAdd(id, current_epoch, std::make_shared<ObjectType>(std::move(obj)));
    }

    void add(const IdType &id, std::shared_ptr<ObjectType> obj_ptr) {
        localAdd(id, current_epoch, obj_ptr);
    }

    void addOn(const Node &node, const IdType &id, const ObjectType &obj) {
        addOn(node, id, std::make_shared<ObjectType>(obj));
    }

    void addOn(const Node &node, const IdType &id, ObjectType &&obj) {
        addOn(node, id, std::make_shared<ObjectType>(std::move(obj)));
    }

    void addOn(const Node &node, const IdType &id, std::shared_ptr<ObjectType> obj_ptr) {
        if (this->thisNode() == node) {
            localAdd(id, current_epoch, obj_ptr);
        } else {
            remoteAdd(node, id, current_epoch, obj_ptr);
        }
    }

    /// Blocking local get. Throws exception if there is no data with such id.
    ObjectPtrType get(const IdType &id) const {
        return localGet(id);
    }

    /// Blocking remote get. Throws exception if there is no data with such id.
    ObjectPtrType getFrom(const Node &node, const IdType &id) const {
        return this->thisNode() == node ? localGet(id) : remoteGetWithCheck(node, id);
    }

    /// Nonblocking local request. Data will be returned when it's become available.
    FutureType getWhenReady(const IdType &id) {
        return getWhenReadyForEpoch(id, current_epoch);
    }

    /// Nonblocking remote request. Data will be returned when it's become available.
    FutureType getWhenReadyFrom(const Node &node, const IdType &id) {
        return getWhenReadyForEpochFrom(node, id, current_epoch);
    }

    /// Nonblocking local request. Data will be returned when it's become available.
    FutureType getWhenReadyForEpoch(const IdType &id, int epoch) {
        return createRequest(this->thisNode(), id, epoch);
    }

    /// Nonblocking remote request. Data will be returned when it's become available.
    FutureType getWhenReadyForEpochFrom(const Node &node, const IdType &id, int epoch) {
        return createRequest(node, id, epoch);
    }

    bool has(const IdType &id) const {
        return localHas(id);
    }

    bool hasOn(const Node &node, const IdType &id) const {
        return this->thisNode() == node ? localHas(id) : remoteHas(node, id).get();
    }

    void moveTo(const IdType &id, const Node &node) {
        if (this->thisNode() == node) {
            return;
        }
        auto object = localGet(id);
        if (object) {
            remoteAdd(node, id, data_epochs[id], object);
            remove(id);
        }
    }

    void remove(const IdType &id) {
        localRemove(id);
    }

    void removeOn(const IdType &id, const Node &node) {
        if (this->thisNode() == node) {
            localRemove(id);
        } else {
            remoteRemove(node, id);
        }
    }

    std::vector<std::pair<IdType, ObjectPtrType>> localContents() const {
        std::lock_guard<std::mutex> lock(mutex);
        if (update_contents_cache) {
            contents_cache = getLocalContents();
            update_contents_cache = false;
        }
        return contents_cache;
    }

    std::vector<ObjectPtrType> localObjects() const {
        typename std::vector<std::shared_ptr<ObjectType>> objects;
        for (const auto &p: localContents()) {
            objects.push_back(p.second);
        }
        return objects;
    }

    template <typename PartType, typename ShapeType>
    const PartType getAs(const IdType &id, const ShapeType &shape)
    {
        auto data = get(id);
        return PartType {data, shape};
    }

    template <typename Func, typename ResultType, typename... Args>
    RemoteFunction<ResultType, const IdType&, Args...> makeGetter(Func func,
        typename std::enable_if<std::is_void<ResultType>::value>::type* = 0) {
        auto set_promise = this->createPromiseSetterVoid();
        auto handler = [set_promise, func](ObjectType &data, const Node &dest_node, size_t promise_id, Args... args) {
            func(data, args...);
            set_promise(dest_node, promise_id);
        };
        return makeHandlerGetter<decltype(handler), ResultType, Args...>(handler);
    }

    template <typename Func, typename ResultType, typename... Args>
    RemoteFunction<ResultType, const IdType&, Args...> makeGetter(Func func,
        typename std::enable_if<!std::is_void<ResultType>::value>::type* = 0) {
        auto set_promise = this->template createPromiseSetter<ResultType>();
        auto handler = [set_promise, func](ObjectType &data, const Node &dest_node, size_t promise_id, Args... args) {
            auto result = func(data, args...);
            set_promise(dest_node, promise_id, result);
        };
        return makeHandlerGetter<decltype(handler), ResultType, Args...>(handler);
    }

    template <typename Method, typename ResultType, typename... Args>
    RemoteFunction<ResultType, const IdType&, Args...> makeMethodGetter(Method method) {
        auto func = [method](ObjectType &obj, Args... args) {
            return (obj.*method)(args...);
        };
        return makeGetter<decltype(func), ResultType, Args...>(func);
    }

    template <typename Proc, typename... Args>
    RemoteFunction<void, const IdType&, Args...> makeCaller(Proc proc) {
        auto caller = [this, proc](const IdType &id, Args... args) {
            this->callProcOnDataWhenReady(proc, id, args...);
        };
        return this->getRemote()->template remoteProcedure<decltype(caller), const IdType&, Args...>(caller);
    }

    template <typename Method, typename... Args>
    RemoteFunction<void, const IdType&, Args...> makeMethodCaller(Method method) {
        auto func = [method](ObjectType &obj, Args... args) {
            (obj.*method)(args...);
        };
        return makeCaller<decltype(func), Args...>(func);
    }    

private:
    using RequestHandler = std::function<void(std::shared_ptr<ObjectType>)>;

private:
    template <typename Handler, typename ResultType, typename... Args>
    auto makeHandlerGetter(Handler handler) {
        auto add_request_local = [this, handler](const IdType &id, int epoch, const Node &dest_node, size_t promise_id, Args... args) {
            this->callProcOnDataWhenReady(handler, id, epoch, dest_node, promise_id, args...);
        };

        auto add_request = createRequestAdder<decltype(add_request_local), Args...>(add_request_local);

        return createGetter<decltype(add_request), ResultType, Args...>(add_request);
    }

    template <typename Proc, typename... Args>
    void callProcOnDataWhenReady(Proc proc, const IdType &id, int epoch, Args... args) {
        std::unique_lock<std::mutex> lock(this->mutex);
        auto obj_ptr = this->local_data.getOrDefault(id);
        auto obj_epoch = data_epochs[id];
        if (obj_ptr && obj_epoch >= epoch) {
            lock.unlock();
            callProcOnObj(proc, obj_ptr, args...);
        } else {
            auto handler = [this, proc, args...](std::shared_ptr<ObjectType> obj_ptr) {
                callProcOnObj(proc, obj_ptr, args...);
            };
            this->pending_requests[id][epoch].push_back(handler);
        }
    }

    template <typename Proc, typename... Args>
    void callProcOnObj(Proc proc, std::shared_ptr<ObjectType> obj_ptr, Args... args) {
        if (!obj_ptr) {
            throw std::runtime_error("callProcOnObj: null object pointer");
        }
        auto &obj = *(obj_ptr.get());
        proc(obj, args...);
    }

    template <typename AddRequestType, typename ResultType, typename... Args>
    auto createGetter(AddRequestType add_request) {
        auto getter = [this, add_request](const Node &dest_node, const IdType &id, Args... args) {
            auto p = this->getPromises()->template addPromise<ResultType>();
            auto promise = p.first;
            auto promise_id = p.second;
            auto future = promise->get_future();
            add_request(id, current_epoch, dest_node, promise_id, args...);
            return future;
        };
        return getter;
    }

    template <typename AddRequestLocalType, typename... Args>
    auto createRequestAdder(AddRequestLocalType add_request_local) {
        auto add_request_remote = this->getRemote()->template remoteProcedure<decltype(add_request_local),
                const IdType&, int, const Node&, size_t, Args...>(add_request_local);
        auto add_request = [this, add_request_local, add_request_remote](const IdType &id, int epoch,
                const Node &dest_node, size_t promise_id, Args... args) {
            if (this->thisNode() == dest_node) {
                add_request_local(id, epoch, dest_node, promise_id, args...);
            } else {
                add_request_remote(dest_node, id, epoch, this->thisNode(), promise_id, args...);
            }
        };
        return add_request;
    }

    void localAdd(const IdType &id, int epoch, std::shared_ptr<ObjectType> obj_ptr) {
        std::vector<RequestHandler> requests;
        {
            std::lock_guard<std::mutex> lock(mutex);
            local_data.add(id, obj_ptr);
            update_contents_cache = true;
            data_epochs[id] = epoch;
            requests = extractPendingRequests(id, epoch);
        }
        processRequests(requests, obj_ptr);
    }

    template <typename Requests>
    void processRequests(const Requests &requests, std::shared_ptr<ObjectType> obj_ptr) {
        for (auto &handler : requests) {
            handler(obj_ptr);
        }
    }

    std::vector<RequestHandler> extractPendingRequests(const IdType &id, int epoch) {
        std::vector<RequestHandler> requests;
        auto it = pending_requests.find(id);
        if (it != pending_requests.end()) {
            std::vector<int> epochs;
            for (auto &p: it->second) {
                auto src_epoch = p.first;
                if (src_epoch <= epoch) {
                    epochs.push_back(src_epoch);
                    requests.insert(requests.end(), p.second.begin(), p.second.end());
                }
            }
            for (const auto &ep: epochs) {
                it->second.erase(ep);
            }
            if (it->second.empty()) {
                pending_requests.erase(it);
            }
        }
        return requests;
    }

    ObjectPtrType localGet(const IdType &id) const {
        std::lock_guard<std::mutex> lock(mutex);
        auto ptr = local_data.getOrDefault(id);
        if (!ptr) {
            throw ItemNotFoundException("Failed to get an item");
        }
        return ptr;
    }

    FutureType createRequest(const Node &dest_node, const IdType &id, int epoch) {
        auto p = this->getPromises()->template addPromise<ObjectPtrType>();
        auto promise = p.first;
        auto promise_id = p.second;
        auto future = promise->get_future();
        addRequest(dest_node, promise_id, id, epoch);
        return future;
    }

    void addRequest(const Node &dest_node, size_t promise_id, const IdType &id, int epoch) {
        if (this->thisNode() == dest_node) {
            localAddRequest(dest_node, promise_id, id, epoch);
        } else {
            remoteAddRequest(dest_node, this->thisNode(), promise_id, id, epoch);
        }
    }

    void localAddRequest(const Node &dest_node, size_t promise_id, const IdType &id, int epoch) {
        std::lock_guard<std::mutex> lock(mutex);
        auto obj_ptr = local_data.getOrDefault(id);
        auto obj_epoch = data_epochs[id];
        if (obj_ptr && obj_epoch >= epoch) {
            this->setPromiseValue(dest_node, promise_id, obj_ptr);
        } else {
            auto handler = [this, dest_node, promise_id](std::shared_ptr<ObjectType> obj_ptr) {
                this->setPromiseValue(dest_node, promise_id, obj_ptr);
            };
            pending_requests[id][epoch].push_back(handler);
        }
    }    

    std::shared_ptr<ObjectType> remoteGetWithCheck(const Node &node, const IdType &id) const {
        try {
            return remoteGet(node, id).get();
        }
        catch (RemoteCallException &e) {
            throw ItemNotFoundException(e.what());
        }
    }

    bool localHas(const IdType &id) const {
        std::lock_guard<std::mutex> lock(mutex);
        return local_data.has(id);
    }

    void localRemove(const IdType &id) {
        std::lock_guard<std::mutex> lock(mutex);
        local_data.remove(id);
        data_epochs.erase(id);
        update_contents_cache = true;
    }

    std::vector<std::pair<IdType, ObjectPtrType>> getLocalContents() const {
        typename std::vector<std::pair<IdType, std::shared_ptr<ObjectType>>> contents;
        for (const auto &p: local_data) {
            contents.push_back(std::make_pair(p.first, p.second));
        }
        return contents;
    }

private:    
    std::atomic<int> current_epoch {0};

    DataStorageType local_data;

    mutable typename std::map<IdType, std::map<int, std::vector<RequestHandler>>> pending_requests;    

    std::map<IdType, int> data_epochs;

    RemoteProcedure<const IdType&, int, std::shared_ptr<ObjectType>> remoteAdd;
    RemoteProcedure<const IdType&> remoteRemove;
    RemoteFunction<std::shared_ptr<ObjectType>, const IdType&> remoteGet;
    RemoteFunction<bool, const IdType&> remoteHas;    
    RemoteProcedure<const Node&, size_t, const IdType&, int> remoteAddRequest;

    mutable std::mutex mutex;

    mutable typename std::vector<std::pair<IdType, ObjectPtrType>> contents_cache;
    mutable bool update_contents_cache = true;
};

}
