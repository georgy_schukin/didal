#pragma once

#include "distributed_collection.h"
#include "didal/env/environment.h"
#include "didal/base/index_range.h"
#include "didal/base/array.h"

#include <cstddef>
#include <utility>
#include <memory>

namespace ddl {

template <typename _ElemType, typename _FragmentType = Array<_ElemType>, typename _ShapeType = IndexRange<size_t>>
class DistributedArray : public DistributedCollection<size_t, _FragmentType> {
public:
    using ElemType = _ElemType;
    using FragmentType = _FragmentType;
    using ShapeType = _ShapeType;

public:
    template <typename Decomposition, typename Distribution>
    DistributedArray(Environment *env,
                     size_t size,
                     const Decomposition &decomposition,
                     const Distribution &distribution,
                     const NodeGroup &nodes):
        DistributedCollection<size_t, FragmentType>(env, nodes),
        _size(size)
    {
        shapes = decomposition.decompose(_size);
        const auto dmap = distribution.distribute(shapes.size(), nodes);
        const auto it = dmap.find(env->thisNode().rank());
        if (it != dmap.end()) {
            my_shapes_ids = it->second;
        }
        for (const auto &id: my_shapes_ids) {
            this->add(id, FragmentType(shapes[id]));
        }
    }

    size_t size() const {
        return _size;
    }

    const std::vector<size_t>& localIds() const {
        return my_shapes_ids;
    }

private:
    size_t _size;
    std::vector<ShapeType> shapes; // array is decomposed on blocks of ranges;
    std::vector<size_t> my_shapes_ids; // ids of local ranges
};

}
