#pragma once

#include "local_storage.h"
#include "distributed_storage.h"
#include "didal/locators/locator.h"
#include "didal/locators/tracking_locator.h"
#include "didal/comm/remote.h"
#include "didal/comm/node_group.h"

#include <map>
#include <cstddef>
#include <memory>

namespace ddl {

class Environment;

template <typename _IdType, typename _ObjectType>
class DistributedCollection {
public:
    using IdType = _IdType;
    using ObjectType = _ObjectType;
    using CollectionType = DistributedCollection<IdType, ObjectType>;
    using DataStorageType = LocalStorage<IdType, ObjectType>;
    using iterator = typename DataStorageType::iterator;
    using const_iterator = typename DataStorageType::const_iterator;

public:
    DistributedCollection(Environment *env, const NodeGroup &nodes) :
        storage(env)
    {
        locator = std::make_shared<TrackingLocator<IdType>>(env, nodes);
        //RemoteMethodMaker<CollectionType> maker(&remote, this);
        //getRemoteObject = maker.method(&CollectionType::getLocal).template func<std::pair<ObjectType, bool>, IdType>();
        //remote.startListening();
    }

    template <typename LocatorType, typename... Args>
    void setLocator(const Args&... args) {
        locator = std::make_shared<LocatorType>(args...);
    }

    void add(const IdType &id, const ObjectType &obj) {
        storage.add(id, obj);
        locator->notifyLocationChanged(id, env->thisNode());
    }

    ObjectType& get(const IdType &id) {        
        auto node = locator->locate(id);
        //return storage.getFrom(id, node).get();
        return local_data.get(id);
    }

    const ObjectType& get(const IdType &id) const {
        auto node = locator->locate(id);
        //return storage.getFrom(id, node).get();
        return local_data.get(id);
    }

    bool hasLocal(const IdType &id) const {
        return storage.has(id);
    }

    /*void request(const IdType &id) {
        if (local_data.has(id)) {
            return local_data.get(id);
        } else {
            remote->request(id, locator->getNode(id));
        }
    }*/

    void remove(const IdType &id) {
        const auto node = locator->getNode(id);
        storage.removeOn(id, node);
        /*const auto node = locator->getNode(id);
        if (node == remote->thisNode()) {
            local_data.remove(id);
        } else {
            remote->remove(id, node);
        }*/
    }

    Environment* getEnvironment() const {
        return env;
    }

    /*std::vector<IdType> localIndices() const {
        return local_data.getIds();
    }*/

    const NodeGroup& getNodes() const {
        return nodes;
    }

    iterator localBegin() {
        return local_data.begin();
    }

    const_iterator localBegin() const {
        return local_data.begin();
    }

    iterator localEnd() {
        return local_data.end();
    }

    const_iterator localEnd() const {
        return local_data.end();
    }

private:
    /*std::pair<ObjectType, bool> getLocal(const IdType &id) {
        const auto result = local_data.tryGet(id);
        return std::make_pair(result.first, result.second);
    }*/

private:    
    DistributedStorage<IdType, ObjectType> storage;
    DataStorageType local_data;
    //mutable DataStorageType cache;
    Environment *env;
    NodeGroup nodes;
    std::shared_ptr<Locator<IdType>> locator;

    //Remote remote;
    //RemoteFunction<std::pair<ObjectType, bool>, IdType> getRemoteObject;
};

}
