#pragma once

#include <map>
#include <cstddef>
#include <memory>
#include <vector>

namespace ddl {

/**
 * @brief Local storage for data: dictionary id -> object.
 */
template <typename _IdType, typename _ObjectType>
class LocalStorage {
public:
    using IdType = _IdType;
    using ObjectType = _ObjectType;
    using StorageType = typename std::map<IdType, ObjectType>;
    using iterator = typename StorageType::iterator;
    using const_iterator = typename StorageType::const_iterator;

public:
    LocalStorage() {}

    LocalStorage(const LocalStorage &ls) :
        _data(ls.data) {
    }

    LocalStorage(LocalStorage &&ls) :
        _data(std::move(ls.data)) {
    }

    ~LocalStorage() {}

    template <typename ObjType>
    void add(const IdType &id, ObjType &&obj) {
        auto p = _data.insert(std::make_pair(id, std::forward<ObjType>(obj)));
        if (!p.second) {
            p.first->second = std::forward<ObjType>(obj); // replace with the new value
        }
    }    

    ObjectType& get(const IdType &id) {
        return _data.at(id);
    }

    const ObjectType& get(const IdType &id) const {
        return _data.at(id);
    }

    std::pair<ObjectType&, bool> tryGet(const IdType &id) {
        static ObjectType empty {};
        auto it = _data.find(id);
        if (it != _data.end()) {
            return std::pair<ObjectType&, bool>(it->second, true);
        } else {
            return std::pair<ObjectType&, bool>(empty, false);
        }
    }

    std::pair<const ObjectType&, bool> tryGet(const IdType &id) const {
        static const ObjectType empty {};
        auto it = _data.find(id);
        if (it != _data.end()) {
            return std::pair<const ObjectType&, bool>(it->second, true);
        } else {
            return std::pair<const ObjectType&, bool>(empty, false);
        }
    }

    const ObjectType& getOrDefault(const IdType &id) const {
        static ObjectType empty {};
        auto it = _data.find(id);
        return (it != _data.end() ? it->second : empty);
    }

    bool has(const IdType &id) const {
        return _data.find(id) != _data.end();
    }

    void remove(const IdType &id) {
        _data.erase(id);
    }

    std::vector<IdType> getIds() const {
        std::vector<IdType> result;
        for (const auto &p: _data) {
            result.push_back(p.first);
        }
        return result;
    }

    std::vector<std::reference_wrapper<ObjectType>> getObjects() {
        std::vector<std::reference_wrapper<ObjectType>> result;
        for (auto &p: _data) {
            result.push_back(std::ref(p.second));
        }
        return result;
    }

    std::vector<std::reference_wrapper<const ObjectType>> getObjects() const {
        std::vector<std::reference_wrapper<const ObjectType>> result;
        for (const auto &p: _data) {
            result.push_back(std::cref(p.second));
        }
        return result;
    }

    std::vector<std::pair<IdType, std::reference_wrapper<ObjectType>>> getContents() {
        std::vector<std::pair<IdType, std::reference_wrapper<ObjectType>>> result;
        for (auto &p: _data) {
            result.push_back(std::make_pair(p.first, std::ref(p.second)));
        }
        return result;
    }

    std::vector<std::pair<IdType, std::reference_wrapper<const ObjectType>>> getContents() const {
        std::vector<std::pair<IdType, std::reference_wrapper<const ObjectType>>> result;
        for (const auto &p: _data) {
            result.push_back(std::make_pair(p.first, std::cref(p.second)));
        }
        return result;
    }

    iterator begin() {
        return _data.begin();
    }

    iterator end() {
        return _data.end();
    }

    const_iterator begin() const {
        return _data.begin();
    }

    const_iterator end() const {
        return _data.end();
    }

private:
    StorageType _data;
};

}
