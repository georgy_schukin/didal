#-------------------------------------------------
#
# Project created by QtCreator 2017-08-14T16:41:50
#
#-------------------------------------------------

QT       -= core gui

TARGET = didal
TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

exists(../local.pri) {
    include(../local.pri)
}

INCLUDEPATH += ../

SOURCES += \
    algorithms/barrier.cpp \
    base/array_nd.cpp \
    base/basic_buffer.cpp \
    base/shared_buffer.cpp \
    comm/msgproc/immediate_message_processor.cpp \
    comm/msgproc/immediate_message_sender.cpp \
    comm/msgproc/threaded_message_processor.cpp \
    comm/msgproc/threaded_message_sender.cpp \
    comm/node.cpp \
    comm/node_group.cpp \
    comm/remote_call_exception.cpp \
    decomp/block_decomposition.cpp \
    decomp/cyclic_decomposition.cpp \
    env/args_options.cpp \
    env/environment.cpp \
    comm/mpi/mpi_communicator.cpp \
    distr/uniform_distribution.cpp \
    comm/comm_service.cpp \
    comm/serialize/buffer_reader.cpp \
    comm/serialize/buffer_writer.cpp \
    comm/serialize/serializing_ops.cpp \
    comm/message.cpp \
    comm/comm_id.cpp \
    env/environment_options.cpp \
    tp/mesh_topology.cpp \
    trace/console_tracer.cpp \
    util/args.cpp \
    util/extent.cpp \
    util/hilbert.cpp \
    util/lattice.cpp \
    util/slicer.cpp \
    util/timer.cpp \
    util/trace.cpp \
    util/vector3.cpp

HEADERS += didal.h \
    algorithms/barrier.h \
    algorithms/foreach.h \
    algorithms/map.h \
    base/array.h \
    base/array_nd.h \
    base/array_nd_ops.h \
    base/basic_buffer.h \
    base/basic_exception.h \
    base/buffer.h \
    base/index.h \
    base/memory_buffer.h \
    base/promise_storage.h \
    base/reference.h \
    base/shared_buffer.h \
    comm/msgproc/immediate_message_processor.h \
    comm/msgproc/immediate_message_sender.h \
    comm/msgproc/message_processor.h \
    comm/msgproc/message_sender.h \
    comm/msgproc/threaded_message_processor.h \
    comm/msgproc/threaded_message_sender.h \
    comm/node.h \
    comm/node_group.h \
    comm/remote_call_exception.h \
    data/distributed_object.h \
    data/distributed_promises.h \
    data/distributed_storage.h \
    data/item_not_found_exception.h \
    data/local_storage.h \
    data/sync_distributed_object.h \
    data/sync_distributed_storage.h \
    decomp/block_part.h \
    decomp/multi_block_decomposition.h \
    distr/block_distribution.h \
    distr/distribution.h \
    data/distributed_collection.h \
    data/distributed_array.h \
    decomp/block_decomposition.h \
    distr/distributor.h \
    distr/mesh_block_distribution.h \
    distr/rope/rope_distribution.h \
    distr/rope/rope_distributor.h \
    distr/rope/segment.h \
    distr/uniform_distribution.h \
    decomp/cyclic_decomposition.h \
    decomp/decomposition.h \
    dlb/patch/patch_load_balancer.h \
    dlb/rope/rope_balancer_node.h \
    dlb/rope/rope_load_balancer.h \
    comm/communicator.h \
    comm/comm_service.h \
    comm/message.h \
    base/index_range.h \
    env/args_options.h \
    env/environment.h \
    comm/mpi/mpi_communicator.h \
    base/clonable.h \
    comm/comm_id.h \
    algorithms/reduce.h \
    data/distributed_shape.h \
    base/index_range_nd.h \
    data/distributed_array_nd.h \
    comm/serialize/serializable.h \
    comm/serialize/writable.h \
    comm/serialize/readable.h \
    comm/serialize/buffer_reader.h \
    comm/serialize/buffer_writer.h \
    comm/serialize/serializing_ops.h \
    base/growing_buffer.h \
    env/environment_options.h \
    locators/tracking_locator.h \
    locators/rope_locator.h \
    comm/message_serialize.h \
    locators/locator.h \
    comm/comm_listener.h \
    comm/remote.h \
    algorithms/gather.h \
    task/task.h \
    tp/mesh_topology.h \
    tp/topology.h \
    trace/console_tracer.h \
    trace/counter.h \
    trace/dumpable_tracer.h \
    trace/tick_counter.h \
    trace/traceable.h \
    trace/tracer.h \
    trace/value_counter.h \
    util/args.h \
    util/extent.h \
    util/hilbert.h \
    util/lattice.h \
    util/range.h \
    util/slicer.h \
    util/timer.h \
    util/trace.h \
    util/vector3.h

unix {
    target.path = /usr/lib
    INSTALLS += target
    LIBS += -lpthread
}
