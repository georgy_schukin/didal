#pragma once

#include "didal/base/basic_buffer.h"
#include "didal/comm/serialize/buffer_reader.h"
#include "didal/comm/serialize/buffer_writer.h"
#include "didal/env/environment.h"
#include "didal/comm/communicator.h"
#include "didal/comm/comm_service.h"
#include "didal/comm/serialize/serializing_ops.h"

namespace ddl {

template <typename Value, typename InsertIter>
void gather(Environment *env, const Value &value, InsertIter iter) {
    auto comm = env->getCommService()->getCommunicator();

    BasicBuffer src_buf, dest_buf;

    BufferWriter writer(&src_buf);
    writer << value;

    comm->gatherOnAll(src_buf, dest_buf);

    BufferReader reader(&dest_buf);
    for (int i = 0; i < comm->numOfNodes(); i++) {
        Value val;
        reader >> val;
        iter = std::move(val);
    }    
}

}
