#include "barrier.h"

namespace ddl {

void barrier(Environment *env) {
    env->getCommService()->getCommunicator()->barrier();
}

namespace aux {

void BarrierCounter::addRank(int rank) {
    std::lock_guard<std::mutex> lock(mutex);
    ranks.insert(rank);
    cond.notify_all();
}

void BarrierCounter::deleteRank(int rank) {
    std::lock_guard<std::mutex> lock(mutex);
    ranks.erase(rank);
    cond.notify_all();
}

void BarrierCounter::waitForRanksEmpty() {
    std::unique_lock<std::mutex> lock(mutex);
    cond.wait(lock, [this]() { return ranks.empty(); });
}

Writable& operator<< (Writable &w, const BarrierCounter &bc) {
    return w << bc.ranks;
}

Readable& operator>> (Readable &r, BarrierCounter &bc) {
    return r >> bc.ranks;
}

}

}
