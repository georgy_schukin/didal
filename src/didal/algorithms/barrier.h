#pragma once

#include "didal/env/environment.h"
#include "didal/comm/comm_service.h"
#include "didal/comm/communicator.h"
#include "didal/data/distributed_storage.h"

#include <vector>
#include <set>
#include <mutex>
#include <condition_variable>

namespace ddl {

namespace aux {

class BarrierCounter {
public:
    BarrierCounter() {}
    BarrierCounter(const std::vector<int> &rnks) : ranks(rnks.begin(), rnks.end()) {}
    BarrierCounter(const BarrierCounter &bc) : ranks(bc.ranks) {}
    BarrierCounter(BarrierCounter &&bc) : ranks(std::move(bc.ranks)) {}

    void addRank(int rank);
    void deleteRank(int rank);
    void waitForRanksEmpty();

    friend Writable& operator<< (Writable &w, const BarrierCounter &bc);
    friend Readable& operator>> (Readable &r, BarrierCounter &bc);

private:
    std::set<int> ranks;
    mutable std::mutex mutex;
    mutable std::condition_variable cond;
};

}

// Global barrier (for all nodes).
void barrier(Environment *env);

// Local barrier (for specified npdes only).
template <typename Container>
void localBarrier(ddl::Environment *env, const Container &nodes) {
    const auto this_rank = env->thisNode().rank();

    DistributedStorage<int, aux::BarrierCounter> counters(env);
    auto touch_neighbor = counters.makeMethodGetter<decltype(&aux::BarrierCounter::deleteRank), void, int>(&aux::BarrierCounter::deleteRank);

    std::vector<int> ranks;
    for (const auto &node: nodes) {
        ranks.push_back(Node::toRank(node));
    }
    counters.create(0, aux::BarrierCounter {ranks});

    std::vector<std::future<void>> sync_f;
    for (const auto &node: nodes) {
        sync_f.emplace_back(touch_neighbor(node, 0, this_rank));
    }

    auto local_counter = counters.get(0);

    // Wait until we receive answers from all nodes.
    for (auto &f: sync_f) {
        f.get();
    }

    // Wait until all nodes received answers from us.
    local_counter->waitForRanksEmpty();
}

class Fence {
public:
    Fence(Environment *env) : counters(env) {
    }

    template <typename Container>
    void barrier(const Container &nodes) {
        int count = current_count++;
        counters.create(count, true);
        std::vector<SyncFuture> futures;
        for (const auto &node: nodes) {
            futures.push_back(counters.getWhenReadyFrom(node, count));
        }
        for (auto &f: futures) {
            f.get();
        }
    }

private:
    using SyncFuture = std::future<std::shared_ptr<bool>>;

private:
    int current_count = 0;
    DistributedStorage<int, bool> counters;
};

template <typename Key>
class FenceMap {
public:
    using KeyType = Key;
    using SyncFuture = std::future<std::shared_ptr<bool>>;

public:
    FenceMap(Environment *env) : counters(env) {
    }

    void setLocation(const Key &key, int rank) {
        location[key] = rank;
    }

    void newEpoch() {
        current_count++;
    }

    void signal(const Key &key) {
        counters.create(currIndex(key), true);
    }

    template <typename Container>
    void signalAll(const Container &keys) {
        for (const auto &key: keys) {
            signal(key);
        }
    }

    SyncFuture futureFor(const Key &key) {
        return counters.getWhenReadyFrom(location.at(key), currIndex(key));
    }

    template <typename Container>
    std::map<Key, SyncFuture> futuresFor(const Container &keys) {
        std::map<Key, SyncFuture> futures;
        for (const auto &key: keys) {
            futures[key] = std::move(futureFor(key));
        }
        return futures;
    }

private:
    using IndexType = std::pair<Key, int>;

private:
    IndexType currIndex(const Key &key) const {
        return std::make_pair(key, current_count);
    }

private:
    int current_count = 0;
    DistributedStorage<IndexType, bool> counters;
    std::map<Key, int> location;
};

}
