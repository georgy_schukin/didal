#pragma once

namespace ddl {

template <typename Container, typename Operation>
void forEach(Container &cont, const Operation &oper) {
    for (auto it = cont.localBegin(); it != cont.localEnd(); ++it) {
        oper(it->first, it->second);
    }
}

template <typename Container, typename Operation>
void forEach(const Container &cont, const Operation &oper) {
    for (auto it = cont.localBegin(); it != cont.localEnd(); ++it) {
        oper(it->first, it->second);
    }
}

template <typename Container, typename IdType, typename Operation>
void forOne(Container &cont, const IdType &id,  const Operation &oper) {
    if (cont.hasLocal(id)) {
        oper(id, cont.get(id));
    }
}

template <typename Container, typename IdType, typename Operation>
void forOne(const Container &cont, const IdType &id,  const Operation &oper) {
    if (cont.hasLocal(id)) {
        oper(id, cont.get(id));
    }
}

template <typename Container, typename IterType, typename Operation>
void forSeveral(Container &cont, IterType start, IterType end, const Operation &oper) {
    for (auto it = start; it != end; ++it) {
        if (cont.hasLocal(*it)) {
            oper(*it, cont.get(*it));
        }
    }
}

template <typename Container, typename IterType, typename Operation>
void forSeveral(const Container &cont, IterType start, IterType end, const Operation &oper) {
    for (auto it = start; it != end; ++it) {
        if (cont.hasLocal(*it)) {
            oper(*it, cont.get(*it));
        }
    }
}

template <typename Container, typename IdContainer, typename Operation>
void forSeveral(Container &cont, const IdContainer &ids, const Operation &oper) {
    for (const auto &id: ids) {
        if (cont.hasLocal(id)) {
            oper(id, cont.get(id));
        }
    }
}

template <typename Container, typename IdContainer, typename Operation>
void forSeveral(const Container &cont, const IdContainer &ids, const Operation &oper) {
    for (const auto &id: ids) {
        if (cont.hasLocal(id)) {
            oper(id, cont.get(id));
        }
    }
}

template <typename Container, typename Predicate, typename Operation>
void forSelected(Container &cont, const Predicate &pred, const Operation &oper) {
    for (auto it = cont.localBegin(); it != cont.localEnd(); ++it) {
        if (pred(it->first)) {
            oper(it->first, it->second);
        }
    }
}

template <typename Container, typename Predicate, typename Operation>
void forSelected(const Container &cont, const Predicate &pred, const Operation &oper) {
    for (auto it = cont.localBegin(); it != cont.localEnd(); ++it) {
        if (pred(it->first)) {
            oper(it->first, it->second);
        }
    }
}

}
