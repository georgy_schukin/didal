#pragma once

#include "gather.h"
#include "didal/comm/communicator.h"
#include "didal/base/memory_buffer.h"

#include <iterator>
#include <cmath>
#include <type_traits>

namespace ddl {

namespace aux {
    template <typename T>
    struct CommDataType {
        static constexpr Communicator::DataType value = Communicator::DT_NONE;
    };

    template <>
    struct CommDataType<int> {
        static constexpr Communicator::DataType value = Communicator::DT_INT;
    };

    template <>
    struct CommDataType<double> {
        static constexpr Communicator::DataType value = Communicator::DT_DOUBLE;
    };
}

template <typename T>
struct ReduceOperation {
    using ValueType = T;

    static constexpr Communicator::ReduceOperationType CommReduceOp = Communicator::RO_NONE;

    T operator()(const T&, const T&) const {
        return T {};
    }

    constexpr Communicator::DataType commDataType() const {
        return aux::CommDataType<ValueType>::value;
    }
};

template <typename T>
struct ReduceOperationSum : public ReduceOperation<T> {
    static constexpr Communicator::ReduceOperationType CommReduceOp = Communicator::RO_SUM;

    T operator()(const T &a, const T &b) const {
        return a + b;
    }
};

template <typename T>
struct ReduceOperationMin : public ReduceOperation<T> {
    static constexpr Communicator::ReduceOperationType CommReduceOp = Communicator::RO_MIN;

    T operator()(const T &a, const T &b) const {
        return std::min(a, b);
    }
};

template <typename T>
struct ReduceOperationMax : public ReduceOperation<T> {
    static constexpr Communicator::ReduceOperationType CommReduceOp = Communicator::RO_MAX;

    T operator()(const T &a, const T &b) const {
        return std::max(a, b);
    }
};

template <typename Value, typename ReduceOp, typename std::enable_if<std::is_base_of<ReduceOperation<Value>, ReduceOp>::value, bool>::type = true>
Value reduceValues(Environment *env, const Value &value, const ReduceOp &oper, const Value &init) {
    Value result = init;
    MemoryBuffer send_buf(const_cast<Value*>(&value), sizeof(Value));
    MemoryBuffer recv_buf(&result, sizeof(Value));
    const auto data_type = oper.commDataType();
    const auto reduce_op = ReduceOp::CommReduceOp;
    env->getCommService()->getCommunicator()->reduce(send_buf, recv_buf, data_type, reduce_op);
    return result;
}

template <typename Value, typename ReduceOp, typename std::enable_if<!std::is_base_of<ReduceOperation<Value>, ReduceOp>::value, bool>::type = true>
Value reduceValues(Environment *env, const Value &value, const ReduceOp &oper, const Value &init) {
    std::vector<Value> values;
    gather(env, value, std::back_inserter(values));

    Value global_value = init;
    for (const auto &v: values) {
        global_value = oper(v, global_value);
    }
    return global_value;
}

template <typename Value, typename Container, typename Func, typename ReduceOp>
Value reduce(Container &cont, const Func func, const ReduceOp &oper, const Value &init) {
    Value local_value = init;
    for (const auto &elem: cont.localObjects()) {
        local_value = oper(func(elem), local_value);
    }
    return reduceValues(cont.getEnvironment(), local_value, oper, init);
}

}
