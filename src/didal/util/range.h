#pragma once

namespace ddl {

template <typename T>
class Range {
    Range() {}
    Range(T start, T end) :
        start(start), end(end) {
    }

    T getStart() const {
        return start;
    }

    T getEnd() const {
        return end;
    }

    bool isIn(T value) {
        return (value >= start && value <= end);
    }

private:
    T start;
    T end;
};

}
