#include "timer.h"

using namespace std::chrono;

namespace ddl {

Timer::Timer() {
    reset();
}

void Timer::start() {
    reset();
}

void Timer::reset() {
    start_time = Clock::now();
}

double Timer::time() const {
    auto end_time = Clock::now();
    return duration<double>(end_time - start_time).count();
}

size_t Timer::mcsecTime() const {
    auto end_time = Clock::now();
    return static_cast<size_t>(duration_cast<microseconds>(end_time - start_time).count());
}

}
