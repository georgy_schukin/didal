#include "lattice.h"

#include <cmath>
#include <algorithm>
#include <limits>

namespace ddl {

namespace {
    bool undefined(int size) {
        return size <= 0;
    }

    double avgDev(int x, int y, int z) {
        const double avg = double (x + y + z) / 3;
        const double dev_x = std::abs(x - avg);
        const double dev_y = std::abs(y - avg);
        const double dev_z = std::abs(z - avg);
        return (dev_x + dev_y + dev_z) / 3;
    }

    double avgDev(const std::tuple<int, int, int> &sizes) {
        return avgDev(std::get<0>(sizes), std::get<1>(sizes), std::get<2>(sizes));
    }
}

std::pair<int, int> Lattice::getLatticeSides2D(int num) {
    for (int i = (int)std::sqrt(num); i >= 1; i--) {
        if (num % i == 0) {
            return std::make_pair(num / i, i);
        }
    }
    return std::make_pair(num, 1);
}

std::pair<int, int> Lattice::getLatticeSides2D(int num, int nx, int ny) {
    if (undefined(nx) && undefined(ny)) {
        return getLatticeSides2D(num);
    } else if (undefined(nx)) {
        nx = num / ny;
    } else if (undefined(ny)) {
        ny = num / nx;
    }
    return std::make_pair(nx, ny);
}

std::tuple<int, int, int> Lattice::getLatticeSides3D(int num) {
    auto best = std::make_tuple(num, 1, 1);
    double min_dev = avgDev(best);
    for (int i = (int)std::cbrt(static_cast<double>(num)); i >= 1; i--) {
        if (num % i == 0) {
            auto sides = getLatticeSides2D(num / i);
            auto tup = std::make_tuple(i, sides.first, sides.second);
            auto dev = avgDev(tup);
            if (dev < min_dev) {
                min_dev = dev;
                best = tup;
            }
        }
    }
    return best;
}

std::tuple<int, int, int> Lattice::getLatticeSides3D(int num, int nx, int ny, int nz) {
    if (undefined(nx) && undefined(ny) && undefined(nz)) {
        return getLatticeSides3D(num);
    } else if (undefined(nx) && undefined(ny)) {
        std::tie(nx, ny) = getLatticeSides2D(num / nz);
    } else if (undefined(nx) && undefined(nz)) {
        std::tie(nx, nz) = getLatticeSides2D(num / ny);
    } else if (undefined(ny) && undefined(nz)) {
        std::tie(ny, nz) = getLatticeSides2D(num / nx);
    } else if (undefined(ny)) {
        ny = num / (nx * nz);
    } else if (undefined(nz)) {
        nz = num / (nx * ny);
    }
    return std::make_tuple(nx, ny, nz);
}

}
