#pragma once

#include "didal/comm/serialize/serializable.h"

namespace ddl {

class Vector3 {
public:
    Vector3() {}
    Vector3(double x, double y, double z) :
        x(x), y(y), z(z) {
    }

    inline void null() {
        x = 0.0;
        y = 0.0;
        z = 0.0;
    }

    inline void add(const Vector3 &v) {
        x += v.x;
        y += v.y;
        z += v.z;
    }

    inline void add(double xx, double yy, double zz) {
        x += xx;
        y += yy;
        z += zz;
    }

    inline void sub(const Vector3 &v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }

    inline void sub(double xx, double yy, double zz) {
        x -= xx;
        y -= yy;
        z -= zz;
    }

    inline void operator+=(const Vector3 &v) {
        add(v);
    }

    inline void operator-=(const Vector3 &v) {
        sub(v);
    }

    Vector3 operator+(const Vector3 &v) const;
    Vector3 operator-(const Vector3 &v) const;

    double& value(int dim) {
        static double noval = 0;
        switch(dim) {
            case 0: return x;
            case 1: return y;
            case 2: return z;
            default: return noval;
        }
    }

    const double& value(int dim) const {
        static double noval = 0;
        switch(dim) {
            case 0: return x;
            case 1: return y;
            case 2: return z;
            default: return noval;
        }
    }    

    Vector3 cross(const Vector3 &v) const;
    double dot(const Vector3 &v) const;
    Vector3 normalized() const;
    double length() const;
    double lengthSquared() const;

    bool isZero() const;

    friend ddl::Writable& operator<< (ddl::Writable &w, const Vector3 &v);
    friend ddl::Readable& operator>> (ddl::Readable &r, Vector3 &v);

public:
    double x = 0.0, y = 0.0, z = 0.0;
};

Vector3 operator*(const Vector3 &v, double value);
Vector3 operator*(double value, const Vector3 &v);

}
