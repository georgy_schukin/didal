#include "args.h"

#include <iostream>

namespace ddl {

int intArg(int arg_num, int default_value, int argc, char **argv) {
    if (arg_num >= argc) {
        return default_value;
    }
    try {
        return std::stoi(argv[arg_num]);
    }
    catch (const std::invalid_argument&) {
        std::cerr << "Invalid int argument: " << argv[arg_num] << std::endl;
        return default_value;
    }
}

std::string strArg(int arg_num, const std::string &default_value, int argc, char **argv) {
    return (argc > arg_num ? std::string(argv[arg_num]) : default_value);
}

int ArgsParser::intArg(int default_value) {
    curr_arg++;
    return ddl::intArg(curr_arg, default_value, argc, argv);
}

std::string ArgsParser::strArg(const std::string default_value) {
    curr_arg++;
    return ddl::strArg(curr_arg, default_value, argc, argv);
}

}
