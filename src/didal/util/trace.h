#pragma once

#include <string>

namespace ddl {

int regEventClass(const std::string &cls_name);
int regEvent(int event_class, const std::string &name);

void eventStart(int event_id);
void eventEnd(int event_id);

class TraceEvent {
public:
    TraceEvent(int event_id) :
        event_id(event_id) {
        eventStart(event_id);
    }

    ~TraceEvent() {
        eventEnd(event_id);
    }
private:
    int event_id;
};

#ifdef TRACE
#define TRACE_EVENT_START(ev) ddl::eventStart(ev);
#define TRACE_EVENT_END(ev) ddl::eventEnd(ev);
#define TRACE_EVENT(ev) ddl::TraceEvent _event_(ev);
#else
#define TRACE_EVENT_START(ev)
#define TRACE_EVENT_END(ev)
#define TRACE_EVENT(ev)
#endif

}
