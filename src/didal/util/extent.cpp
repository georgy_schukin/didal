#include "extent.h"

#include <cmath>

namespace ddl {

namespace aux {

template<>
bool isIn<double>(const double &value, const double &start, const double &end) {
    return (value > start || std::abs(value - start) < 1e-12) && value < end;
}

}

}
