#pragma once

#include <utility>
#include <tuple>

namespace ddl {

class Lattice {
public:
    static std::pair<int, int> getLatticeSides2D(int num);
    static std::pair<int, int> getLatticeSides2D(int num, int nx, int ny);

    static std::tuple<int, int, int> getLatticeSides3D(int num);
    static std::tuple<int, int, int> getLatticeSides3D(int num, int nx, int ny, int nz);
};

}
