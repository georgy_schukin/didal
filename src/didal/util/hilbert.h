#pragma once

#include <cstddef>
#include <vector>

namespace ddl {

/**
 * @brief Converts a point in N-dimension space to index(distance) on the Hilbert curve.
 */
class Hilbert {
public:
    /// From N-dim point to index.
    static int getDistance(const std::vector<int> &coord);
};

}
