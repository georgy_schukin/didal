#include "hilbert.h"

#include <cmath>
#include <algorithm>

namespace ddl {

namespace {
    int grayDecode(int code) {
        int index = code;
        while (code > 0) {
            code = code >> 1;
            index ^= code;
        }
        return index;
    }

    int grayEncode(int index) {
        return index ^ (index >> 1);
    }

    int myLog2(int x) {
        int y = 0;
        while (x > 1) {
            x >>= 1;
            y++;
        }
        return y;
    }

    int getMask(int dims) {
        return (1 << dims) - 1;
    }

    int subcubeDecode(int code, int vertex, int edge, int dims) {
        int k = code ^ vertex;
        k = (k >> (edge + 1)) | (k << (dims - edge - 1));
        return grayDecode(k & getMask(dims));
    }

    int subcubeEncode(int index, int vertex, int edge, int dims) {
        int h = grayEncode(index);
        h = (h << (edge + 1)) | (h >> (dims - edge - 1));
        return (h & getMask(dims)) ^ vertex;
    }

    void rotate(int index, int vertex, int edge, int dims, int &v, int &w) {
        v = subcubeEncode(std::max((index - 1) & ~1, 0), vertex, edge, dims);
        w = subcubeEncode(std::min((index + 1) | 1, getMask(dims)), vertex, edge, dims);
        w = log2(v ^ w);
    }
}

int Hilbert::getDistance(const std::vector<int> &coord) {    
    std::vector<int> indices = coord;
    const int dims = coord.size();
    const int m = myLog2(*std::max_element(indices.begin(), indices.end())) + 1;
    std::vector<int> digits(m, 0);
    for (int i = 0; i < m; i++) {
        int digit = 0;
        for (int bit = dims - 1; bit >= 0; bit--) {
            digit = (digit << 1) | (indices[bit] & 1);
            indices[bit] >>= 1;
        }
        digits[i] = digit;
    }

    int vertex = 0;
    int edge = -(m) % dims;
    int index = 0;
    for (auto it = digits.rbegin(); it != digits.rend(); it++) {
        int digit = *it;
        int bits = subcubeDecode(digit, vertex, edge, dims);
        index = (index << dims) | bits;
        rotate(bits, vertex, edge, dims, vertex, edge);
    }
    return index;
}

}
