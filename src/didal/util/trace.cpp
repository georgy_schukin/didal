#include "trace.h"

#include <map>
#include <vector>

#ifdef ITAC
#include "VT.h"
#endif

using namespace std;

namespace ddl {

namespace {

map<string, int> registered_classes;
vector<int> registered_events;

}

int regEventClass(const std::string &cls_name) {
    auto it = registered_classes.find(cls_name);
    if (it != registered_classes.end()) {
        return it->second;
    } else {
    int handle = 0;
#ifdef ITAC
    VT_classdef(cls_name.c_str(), &handle);
#endif
    registered_classes[cls_name] = handle;
    return handle;
    }
}

int regEvent(int event_class, const std::string &name) {
    int handle = 0;
#ifdef ITAC
    VT_funcdef(name.c_str(), event_class, &handle);
#endif
    registered_events.push_back(handle);
    return registered_events.size() - 1;
}

void eventStart(int event_id) {
#ifdef ITAC
    VT_begin(registered_events[event_id]);
#endif
}

void eventEnd(int event_id) {
#ifdef ITAC
    VT_end(registered_events[event_id]);
#endif
}

}
