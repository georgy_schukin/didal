#pragma once

#include "didal/comm/serialize/serializing_ops.h"

#include <array>
#include <cstddef>

namespace ddl {

namespace aux {

template <typename T>
bool isIn(const T &value, const T &start, const T &end) {
    return value >= start && value < end;
}

template<>
bool isIn<double>(const double &value, const double &start, const double &end);

}

template <typename T, size_t Dims>
class Extent {
public:
    Extent() {}

    const T& start(int dim) const {
        return _start[dim];
    }

    T& start(int dim) {
        return _start[dim];
    }

    const T& end(int dim) const {
        return _end[dim];
    }

    T& end(int dim) {
        return _end[dim];
    }

    T size(int dim) const {
        return end(dim) - start(dim);
    }

    bool isIn(int dim, const T &value) const {
        return aux::isIn(value, start(dim), end(dim));
    }

    bool isOut(int dim, const T &value) const {
        return !isIn(dim, value);
    }

    Extent scaledFromCenter(double scale) const {
        Extent scaled;
        for (int i = 0; i < static_cast<int>(Dims); i++) {
            const auto center = start(i) + size(i) * 0.5;
            scaled.start(i) = center - size(i) * 0.5 * scale;
            scaled.end(i) = center + size(i) * 0.5 * scale;
        }
        return scaled;
    }

    Extent scaledFromCenter(const std::array<double, Dims> &scale) const {
        Extent scaled;
        for (int i = 0; i < static_cast<int>(Dims); i++) {
            const auto center = start(i) + size(i) * 0.5;
            scaled.start(i) = center - size(i) * 0.5 * scale[i];
            scaled.end(i) = center + size(i) * 0.5 * scale[i];
        }
        return scaled;
    }

    Extent shifted(const std::array<double, Dims> &shift) {
        Extent shifted_extent;
        for (int i = 0; i < 3; i++) {
            shifted_extent.start(i) = start(i) + shift[i];
            shifted_extent.end(i) = end(i) + shift[i];
        }
        return shifted_extent;
    }

    std::array<double, Dims> getCenter() const {
        std::array<double, Dims> center;
        for (int i = 0; i < static_cast<int>(Dims); i++) {
            center[i] = start(i) + size(i) * 0.5;
        }
        return center;
    }

    friend ddl::Writable& operator<<(ddl::Writable &w, const Extent &ex) {
        return w << ex._start << ex._end;
    }

    friend ddl::Readable& operator>>(ddl::Readable &r, Extent &ex) {
        return r >> ex._start >> ex._end;
    }

private:
    std::array<T, Dims> _start;
    std::array<T, Dims> _end;
};

}
