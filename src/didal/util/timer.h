#pragma once

#include <chrono>

namespace ddl {

class Timer {
public:
    Timer();

    void start();
    void reset();

    /// Elapsed time in seconds.
    double time() const;

    /// Elapsed time in microseconds.
    size_t mcsecTime() const;

private:
    using Clock = std::chrono::steady_clock;

private:
    Clock::time_point start_time;
};

}
