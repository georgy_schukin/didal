#include "vector3.h"
#include "didal/comm/serialize/serializing_ops.h"

#include <cmath>

namespace ddl {

Vector3 Vector3::operator+(const Vector3 &v) const {
    return Vector3 {x + v.x, y + v.y, z + v.z};
}

Vector3 Vector3::operator-(const Vector3 &v) const {
    return Vector3 {x - v.x, y - v.y, z - v.z};
}

Vector3 Vector3::cross(const Vector3 &v) const {
    return Vector3 {y * v.z - z * v.y,
                    z * v.x - x * v.z,
                    x * v.y - y * v.x};
}

double Vector3::dot(const Vector3 &v) const {
    return x * v.x + y * v.y + z * v.z;
}

Vector3 Vector3::normalized() const {
    const auto len = length();
    return (len < 1e-12 ? *this : Vector3 {x / len, y / len, z / len});
}

double Vector3::length() const {
    return std::sqrt(x * x + y * y + z * z);
}

double Vector3::lengthSquared() const {
    return x * x + y * y + z * z;
}

bool Vector3::isZero() const {
    return std::abs(x) < 1e-12 &&
            std::abs(y) < 1e-12 &&
            std::abs(z) < 1e-12;
}

Vector3 operator*(const Vector3 &v, double value) {
    return Vector3 {v.x * value, v.y * value, v.z * value};
}

Vector3 operator*(double value, const Vector3 &v) {
    return Vector3 {v.x * value, v.y * value, v.z * value};
}

ddl::Writable& operator<< (ddl::Writable &w, const Vector3 &v) {
    return w << v.x << v.y << v.z;
}

ddl::Readable& operator>> (ddl::Readable &r, Vector3 &v) {
    return r >> v.x >> v.y >> v.z;
}

}
