#pragma once

#include <string>

namespace ddl {

int intArg(int arg_num, int default_value, int argc, char **argv);
std::string strArg(int arg_num, const std::string &default_value, int argc, char **argv);

class ArgsParser {
public:
    ArgsParser(int argc, char **argv, int start_arg = 0) :
        argc(argc), argv(argv), curr_arg(start_arg) {
    }

    int intArg(int default_value = 0);
    std::string strArg(const std::string default_value = "");

private:
    int argc = 0;
    char **argv = nullptr;
    int curr_arg = 0;
};

}
