#pragma once

#include "locator.h"
#include "didal/util/range.h"

namespace ddl {

template <typename IdType>
class RopeLocator : public Locator<IdType> {
public:
    Node locate(const IdType &id) const override {

    }

    void notifyLocationChanged(const IdType &id, const Node &node) override {

    }

private:
    Range<int> myRange;
};

}
