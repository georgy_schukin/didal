#pragma once

#include "locator.h"
#include "didal/comm/node_group.h"
#include "didal/env/environment.h"
#include "didal/comm/remote.h"

#include <map>
#include <mutex>
#include <functional>

namespace ddl {

template <typename IdType, typename HashFunction = std::hash<IdType>>
class TrackingLocator : public Locator<IdType> {
public:
    using LocatorType = TrackingLocator<IdType, HashFunction>;

public:
    TrackingLocator(Environment *env, const NodeGroup &nodes, const HashFunction &func = HashFunction()) :
        env(env), nodes(nodes), hash_func(func), remote(env->getCommService(), this)
    {
        RemoteMethodMaker<LocatorType> maker(&remote, this);
        setLocationRemote = maker.method(&LocatorType::setLocationLocal).template proc<const IdType&, Node>();
        getLocationRemote = maker.method(&LocatorType::getLocationLocal).template func<std::pair<Node, bool>, const IdType&>();
        remote.startListening();
    }

    Node locate(const IdType &id) const override {
        return getLocation(id);
    }

    void notifyLocationChanged(const IdType &id, const Node &node) override {
        setLocation(id, node);
    }

    void setLocation(const IdType &id, const Node &node) {
        const auto keeper_node = getKeeperNode(id);
        if (keeper_node == env->thisNode()) {
            setLocationLocal(id, node);
        } else {
            setLocationRemote(keeper_node, id, node);
        }
    }

    Node getLocation(const IdType &id) const {
        const auto keeper_node = getKeeperNode(id);
        bool success = false;
        Node result {-1};
        std::tie(result, success) = (keeper_node == env->thisNode() ?
                                        getLocationLocal(id) :
                                        getLocationRemote(keeper_node, id).get());
        if (!success) {
            throw NoLocation {};
        }
        return result;
    }

    class NoLocation {};

private:
    Node getKeeperNode(const IdType &id) const {
        return Node(nodes.getRanks()[hash_func(id) % nodes.size()]);
    }

    void setLocationLocal(const IdType &id, const Node &node) {
        std::lock_guard<std::mutex> lock(mutex);
        current_location[id] = node;
    }

    std::pair<Node, bool> getLocationLocal(const IdType &id) const {
        std::lock_guard<std::mutex> lock(mutex);
        auto it = current_location.find(id);
        return it != current_location.end() ? std::make_pair(it->second, true) : std::make_pair(Node(-1), false);
    }

private:
    Environment *env;
    NodeGroup nodes;
    HashFunction hash_func;
    std::map<IdType, Node> current_location;

    Remote remote;
    mutable RemoteProcedure<const IdType&, Node> setLocationRemote;
    mutable RemoteFunction<std::pair<Node, bool>, const IdType&> getLocationRemote;

    mutable std::mutex mutex;
};

}
