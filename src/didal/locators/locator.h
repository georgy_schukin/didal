#pragma once

#include "didal/base/clonable.h"
#include "didal/comm/node.h"

namespace ddl {

template <typename IdType>
class Locator {
public:
    virtual ~Locator() = default;

    virtual Node locate(const IdType &id) const = 0;

    virtual void notifyLocationChanged(const IdType &id, const Node &node) = 0;
};

}
