#include "node_group.h"

namespace ddl {

NodeGroup::NodeGroup() {
}

NodeGroup::NodeGroup(const std::initializer_list<Node> &nodes) {
    for (const auto &node: nodes) {
        add(node);
    }
}

void NodeGroup::add(const Node &node) {
    _nodes.insert(node);
}

size_t NodeGroup::size() const {
    return _nodes.size();
}

std::vector<int> NodeGroup::getRanks() const {
    std::vector<int> ranks;
    for (const auto &node: _nodes) {
        ranks.push_back(node.rank());
    }
    return ranks;
}

}
