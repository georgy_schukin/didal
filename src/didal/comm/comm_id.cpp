#include "comm_id.h"
#include "didal/comm/serialize/serializing_ops.h"

namespace ddl {

Writable& operator<< (Writable &w, const CommId &id) {
    return w << id._group_id << id._id;
}

Readable& operator>> (Readable &r, CommId &id) {
    return r >> id._group_id >> id._id;
}

std::ostream& operator<<(std::ostream &out, const CommId &id) {
    out << "(" << id.groupId() << ", " << id.id() << ")";
    return out;
}

}
