#include "message.h"
#include "didal/base/basic_buffer.h"
#include "didal/comm/serialize/serializing_ops.h"

#include <cassert>
#include <stdexcept>

namespace ddl {

const size_t Message::HEADER_SIZE;

Message::Message() :
    _buffer(std::make_shared<BasicBuffer>(Message::HEADER_SIZE)) {
}

Message::Message(const size_t &size) :
    _buffer(std::make_shared<BasicBuffer>(size + Message::HEADER_SIZE)) {
}

Message::Message(const CommId &id, int tag) :
    _comm_id(id), _tag(tag), _buffer(std::make_shared<BasicBuffer>(Message::HEADER_SIZE)) {
}

Message::Message(const size_t &size, const CommId &id, int tag) :
    _comm_id(id), _tag(tag), _buffer(std::make_shared<BasicBuffer>(size + Message::HEADER_SIZE)) {
}

Message::Message(const CommId &id, int tag, BufferPtr buf) :
    _comm_id(id), _tag(tag), _buffer(buf){
}

Message::Message(const Message &msg) :
    _comm_id(msg._comm_id), _tag(msg._tag), _buffer(msg._buffer) {
}

Message::Message(Message &&msg) :
    _comm_id(msg._comm_id), _tag(msg._tag), _buffer(std::move(msg._buffer)) {
}

Message& Message::operator=(const Message &msg) {
    _comm_id = msg._comm_id;
    _tag = msg._tag;
    _buffer = msg._buffer;
    return *this;
}

Message& Message::operator=(Message &&msg) {
    _comm_id = msg._comm_id;
    _tag = msg._tag;
    _buffer = std::move(msg._buffer);
    return *this;
}

void Message::setCommId(const CommId &id) {
    _comm_id = id;
}

void Message::setTag(int tag) {
    _tag = tag;
}

CommId Message::commId() const {
    return _comm_id;
}

int Message::tag() const {
    return _tag;
}

void* Message::data() {
    return buffer() ? static_cast<BasicBuffer::ElementType*>(buffer()) + Message::HEADER_SIZE : nullptr;
}

const void* Message::data() const {
    return buffer() ? static_cast<const BasicBuffer::ElementType*>(buffer()) + Message::HEADER_SIZE : nullptr;
}

size_t Message::size() const {
    return buffer() ? bufSize() - Message::HEADER_SIZE : 0;
}

void* Message::buffer() {
    return _buffer->data();
}

const void* Message::buffer() const {
    return _buffer->data();
}

size_t Message::bufSize() const {
    return _buffer ? _buffer->size() : 0;
}

Message Message::fromBufSize(const size_t &buf_size) {
    assert (buf_size >= Message::HEADER_SIZE);
    return Message(buf_size - Message::HEADER_SIZE);
}

Message Message::clone() const {
    checkBuffer();
    auto buf_clone = std::make_shared<BasicBuffer>(_buffer->data(), _buffer->size());
    return Message(_comm_id, _tag, buf_clone);
}

BufferWriter Message::getWriter() {
    checkBuffer();
    return BufferWriter(_buffer.get(), Message::HEADER_SIZE);
}

BufferReader Message::getReader() const {
    checkBuffer();
    return BufferReader(_buffer.get(), Message::HEADER_SIZE);
}

void Message::pack() {
    checkBuffer();
    BufferWriter writer(_buffer.get(), 0);
    writer << _comm_id << _tag;
}

void Message::unpack() {
    checkBuffer();
    BufferReader reader(_buffer.get(), 0);
    reader >> _comm_id >> _tag;
}

void Message::checkBuffer() const {
    if (!_buffer) {
        throw std::runtime_error("Message: no buffer");
    }
}

}
