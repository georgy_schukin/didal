#pragma once

#include "serialize/serializable.h"

#include <cstddef>
#include <iostream>

namespace ddl {

class Node {
public:
    static const int RANK_INVALID = -1;

public:
    Node() {}

    Node(int rank) :
        _rank(rank) {
    }

    int rank() const {
        return _rank;
    }

    bool isValid() const {
        return (_rank != RANK_INVALID);
    }

    bool operator<(const Node &n) const {
        return rank() < n.rank();
    }

    bool operator==(const Node &n) const {
        return rank() == n.rank();
    }

    bool operator!=(const Node &n) const {
        return !(operator==(n));
    }

    friend Writable& operator<< (Writable &w, const Node &n);
    friend Readable& operator>> (Readable &r, Node &n);

    template <typename T> static int toRank(const T &t);

private:
    int _rank = Node::RANK_INVALID;
};

template <> int Node::toRank<int>(const int &rank);
template <> int Node::toRank<Node>(const Node &node);

std::ostream& operator<< (std::ostream &out, const Node &node);

}
