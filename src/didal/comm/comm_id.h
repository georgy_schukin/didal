#pragma once

#include "didal/comm/serialize/serializable.h"

#include <cstddef>
#include <iostream>

namespace ddl {

class CommId {
public:
    CommId() = default;

    CommId(size_t gid, size_t id) :
        _group_id(gid), _id(id) {
    }

    bool operator<(const CommId &cid) const {
        return (_group_id < cid.groupId()) ||
               (_group_id == cid.groupId() && _id < cid.id());
    }

    bool operator==(const CommId &cid) const {
        return (_group_id == cid.groupId() && _id == cid.id());
    }

    size_t groupId() const {
        return _group_id;
    }

    size_t id() const {
        return _id;
    }

    friend Writable& operator<< (Writable &w, const CommId &id);
    friend Readable& operator>> (Readable &r, CommId &id);

private:
    size_t _group_id {0};
    size_t _id {0};
};

std::ostream& operator<<(std::ostream &out, const CommId &id);

}
