#pragma once

#include "comm_id.h"
#include "comm_service.h"
#include "comm_listener.h"
#include "message_serialize.h"
#include "remote_call_exception.h"
#include "didal/env/environment.h"
#include "didal/base/promise_storage.h"

#include <functional>
#include <map>
#include <tuple>
#include <atomic>
#include <utility>
#include <future>
#include <type_traits>
#include <memory>
#include <cassert>
#include <iostream>
#include <thread>
#include <chrono>

namespace ddl {

namespace aux {
    // Call function with given arguments.
    template <typename Function, typename Tuple, size_t... I>
    decltype(auto) callFunction(Function func, Tuple &tup, std::index_sequence<I...>) {
         return func(std::get<I>(tup)...);
    }

    // Call procedure with given arguments.
    template <typename Procedure, typename Tuple, size_t... I>
    void callProcedure(Procedure proc, Tuple &tup, std::index_sequence<I...>) {
         proc(std::get<I>(tup)...);
    }

    // Call method of the object with given arguments.
    template <typename Object, typename Method, typename Tuple, size_t... I>
    decltype(auto) callMethod(Object *obj, Method m, Tuple &tup, std::index_sequence<I...>) {
         return (obj->*m)(std::get<I>(tup)...);
    }

    // Call function with non-void return type and pack result.
    // If there was an exception, pack the exception instead.
    template <typename ResultType, typename Function, typename... Args,
              typename std::enable_if<!std::is_void<ResultType>::value, int>::type = 0>
    Message callFunctionAndPack(Function func, std::tuple<Args...> &args, size_t call_tag) {
        Message answer;
        try {
            ResultType result = aux::callFunction(func, args, std::index_sequence_for<Args...>{});
            return msgSerialize(answer, call_tag, true, result);
        }
        catch (const std::exception &e) {
            return msgSerialize(answer, call_tag, false, RemoteCallException(e.what()));
        }
    }

    // Call function with void return type (message is used as successful completion flag).
    // If there was an exception, pack the exception instead.
    template <typename ResultType, typename Function, typename... Args,
              typename std::enable_if<std::is_void<ResultType>::value, int>::type = 0>
    Message callFunctionAndPack(Function func, std::tuple<Args...> &args, size_t call_tag) {
        Message answer;
        try {
            aux::callFunction(func, args, std::index_sequence_for<Args...>{});
            return msgSerialize(answer, call_tag, true);
        }
        catch (const std::exception &e) {
            return msgSerialize(answer, call_tag, false, RemoteCallException(e.what()));
        }
    }

    // Extract the result of non-void return type method call and set it for the promise.
    template <typename ResultType, typename std::enable_if<!std::is_void<ResultType>::value, int>::type = 0>
    void setPromiseValue(Readable &r, std::promise<ResultType> &promise) {
        ResultType result;
        serialize::unpack(r, result);
        promise.set_value(std::move(result)); // move result into promise
    }

    // Set successful completion of void return type method call for the promise.
    template <typename ResultType, typename std::enable_if<std::is_void<ResultType>::value, int>::type = 0>
    void setPromiseValue(Readable&, std::promise<ResultType> &promise) {
        promise.set_value();
    }

    // Extract the exception and set it for the promize.
    template <typename ResultType>
    void setPromiseException(Readable &r, std::promise<ResultType> &promise) {
        RemoteCallException exception;
        serialize::unpack(r, exception);
        promise.set_exception(std::make_exception_ptr(exception));
    }

    template <typename ResultType>
    void setPromiseResult(Readable &r, std::promise<ResultType> &promise) {
        bool success = false;
        serialize::unpack(r, success);
        if (success) {
            aux::setPromiseValue(r, promise);
        } else {
            aux::setPromiseException(r, promise);
        }
    }
}

template <typename ResultType>
using RemoteResult = std::future<ResultType>;

template <typename ResultType, typename... Args>
using RemoteFunction = std::function<RemoteResult<ResultType>(const Node&, Args...)>;

template <typename... Args>
using RemoteProcedure = std::function<void(const Node&, Args...)>;

/**
 * @brief Allows to call functions and procedures remotely.
 */
class Remote : public CommListener {
public:
    Remote(CommService *cs, CommId cid) :
        comm_service(cs), comm_id(cid), this_node(cs->thisNode()) {
    }

    template <typename Object>
    Remote(CommService *cs, Object* /* obj */) :
        Remote(cs, cs->newCommId<Object>()) {
    }

    explicit Remote(CommService *cs) :
        Remote(cs, cs->newCommId<int>()) {
    }

    Remote(const Remote&) = delete;
    Remote(Remote&&) = delete;
    Remote& operator=(const Remote&) = delete;
    Remote& operator=(Remote&&) = delete;

    ~Remote() override {
        //std::unique_lock<std::mutex> lock(msg_mutex);
        promises.waitForPromisesEmpty();
        while (todo > 0) {
            std::this_thread::sleep_for(std::chrono::microseconds(1));
        }
        comm_service->unsubscribe(comm_id);
    }

    void startListening() {
        comm_service->subscribe(comm_id, this);
    }

    template <typename Func, typename ReturnType, typename... Args>
    RemoteFunction<ReturnType, Args...> remoteFunction(Func func) {
        const auto remote_tag = last_msg_tag++;
        const auto answer_tag = last_msg_tag++;

        // Pack the arguments and send them to the remote node.
        // Return a future to wait for the result.
        auto call_handler = [this, remote_tag, func](const Node &dest, Args... args) {
            if (dest != this->this_node) {
                auto p = promises.addPromise<ReturnType>();
                auto promise = p.first;
                if (!promise) {
                    throw std::runtime_error("Remote: failed to add promise");
                }
                auto promise_id = p.second;
                auto future = promise->get_future();
                Message msg(this->comm_id, remote_tag);
                this->comm_service->sendMessage(msgSerialize(msg, promise_id, args...), dest);
                return future;
            } else {
                std::packaged_task<ReturnType(Args...)> task(func);
                auto future = task.get_future();
                task(args...);
                return future;
            }
        };

        // Unpack the received arguments, call the method, send the answer back.
        auto remote_handler = [this, func, answer_tag](const Message &msg, const Node &src) {
            size_t promise_id;
            std::tuple<std::remove_cv_t<std::remove_reference_t<Args>>...> args;
            msgDeserialize(msg, promise_id, args);
            Message answer = aux::callFunctionAndPack<ReturnType>(func, args, promise_id);
            answer.setCommId(this->comm_id);
            answer.setTag(answer_tag);
            this->comm_service->sendMessage(answer, src);
        };

        // Receive and set the answer.
        auto recv_handler = [this](const Message &msg, const Node& /* src */) {
            auto reader = msg.getReader();
            size_t promise_id = 0;
            serialize::unpack(reader, promise_id);
            auto promise = promises.ejectPromise<ReturnType>(promise_id);
            if (!promise) {
                throw std::runtime_error("Remote: failed to get promise");
            }
            aux::setPromiseResult(reader, *promise.get());
        };

        addHandler(remote_tag, remote_handler);
        addHandler(answer_tag, recv_handler);

        return call_handler;
    }

    template <typename Proc, typename... Args>
    RemoteProcedure<Args...> remoteProcedure(Proc proc) {
        const auto remote_tag = last_msg_tag++;

        // Pack the arguments and send them to the remote node.
        auto call_handler = [this, remote_tag, proc](const Node &dest, Args... args) {
            if (dest != this->this_node) {
                Message msg(this->comm_id, remote_tag);
                this->comm_service->sendMessage(msgSerialize(msg, args...), dest);
            } else {
                proc(args...);
            }
        };

        // Unpack the received arguments and call the method.
        auto remote_handler = [this, proc](const Message &msg, const Node& /* src */) {
            std::tuple<std::remove_cv_t<std::remove_reference_t<Args>>...> args;
            msgDeserialize(msg, args);
            aux::callProcedure(proc, args, std::index_sequence_for<Args...>{});
        };

        addHandler(remote_tag, remote_handler);

        return call_handler;
    }

    template <typename Handler>
    void callHandler(Handler handler, const Message &msg, const Node &src) {
        todo++;
        handler(msg, src);
        todo--;
    }

    template <typename Handler>
    void addHandler(int tag, Handler handler) {
        std::vector<std::pair<Message, Node>> messages;
        {
            std::lock_guard<std::mutex> lock(msg_mutex);
            msg_handlers[tag] = handler;
            messages = extractPendingMessages(tag);
        }
        for (const auto &p: messages) {
            callHandler(handler, p.first, p.second);
        }
    }

    void addToPendingMessages(const Message &msg, const Node &src) {
        pending_messages[msg.tag()].push_back(std::make_pair(msg, src));
    }

    std::vector<std::pair<Message, Node>> extractPendingMessages(int tag) {
        auto it = pending_messages.find(tag);
        if (it != pending_messages.end()) {
            auto messages = std::move(it->second);
            pending_messages.erase(it);
            return messages;
        }
        return std::vector<std::pair<Message, Node>> {};
    }

    void onMessage(const Message &msg, const Node &src) override {
        std::unique_lock<std::mutex> lock(msg_mutex);
        const auto it = msg_handlers.find(msg.tag());
        if (it != msg_handlers.end()) {
            lock.unlock();
            callHandler(it->second, msg, src);
        } else {
            addToPendingMessages(msg, src);
        }
    }

    PromiseStorage* getPromises() {
        return &promises;
    }

private:
    using MessageHandler = std::function<void(const Message&, const Node&)>;

private:
    CommService *comm_service;
    CommId comm_id;
    Node this_node;
    std::atomic<int> last_msg_tag {0};
    std::atomic<size_t> todo {0};
    std::map<int, MessageHandler> msg_handlers;
    std::map<int, std::vector<std::pair<Message, Node>>> pending_messages;
    mutable std::mutex msg_mutex;
    PromiseStorage promises;
};

/**
 * @brief Allows to construct wrappers to call object's methods remotely.
 */
template <typename Object>
class RemoteMethodMaker {
public:
    RemoteMethodMaker(Remote *rem, Object *obj) :
        remote(rem), object(obj) {}

    template <typename Method>
    class RemoteMethod {
    public:
        using MethodType = Method;

    public:
        RemoteMethod(Remote *rem, Object *obj, Method m) :
            remote(rem), object(obj), method(m) {
        }

        template <typename ReturnType, typename... Args>
        auto func() {
            auto handler = [object=object, method=method](Args... args) -> ReturnType {
                return (object->*method)(args...);
            };
            return remote->remoteFunction<decltype(handler), ReturnType, Args...>(handler);
        }

        template <typename... Args>
        auto proc() {
            auto handler = [object=object, method=method](Args... args) -> void {
                (object->*method)(args...);
            };
            return remote->remoteProcedure<decltype(handler), Args...>(handler);
        }
    private:
        Remote *remote;
        Object *object;
        Method method;
    };

    template <typename Method>
    RemoteMethod<Method> method(Method method) {
        return RemoteMethod<Method>(remote, object, method);
    }

private:
    Remote *remote;
    Object *object;
};

}
