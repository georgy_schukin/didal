#include "buffer_reader.h"

#include <stdexcept>
#include <cstring>

namespace ddl {

void BufferReader::read(void *data, size_t size) {
    if (offset + size > buf->size()) {
        throw std::runtime_error("BufferReader: out of memory");
    }
    memcpy(data, static_cast<const unsigned char*>(buf->data()) + offset, size);
    offset += size;
}

}
