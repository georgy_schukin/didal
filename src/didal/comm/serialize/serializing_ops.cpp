#include "serializing_ops.h"

namespace ddl {

Writable& operator<< (Writable &w, const Serializable *ser) {
    return ser->serialize(w);
}

Writable& operator<< (Writable &w, const Serializable &ser) {
    return ser.serialize(w);
}

Readable& operator>> (Readable &r, Serializable *ser) {
    return ser->deserialize(r);
}

Readable& operator>> (Readable &r, Serializable &ser) {
    return ser.deserialize(r);
}

Writable& operator<< (Writable &w, const std::string &str) {
    w << str.size();
    w.write(&str[0], str.size());
    return w;
}

Readable& operator>> (Readable &r, std::string &str) {
    size_t size = 0;
    r >> size;
    str.resize(size);
    r.read(&str[0], size);
    return r;
}

}
