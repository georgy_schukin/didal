#pragma once

#include <cstddef>

namespace ddl {

class Writable {
public:
    virtual ~Writable() = default;
    virtual void write(const void *data, size_t size) = 0;
};

}
