#pragma once

#include "readable.h"
#include "didal/base/buffer.h"

#include <cstddef>

namespace ddl {

class BufferReader : public Readable {
public:
    BufferReader(const Buffer *buf, size_t offset = 0) :
        buf(buf), offset(offset) {
    }

    void read(void *data, size_t size) override;

private:
    const Buffer *buf;
    size_t offset;
};

}
