#pragma once

#include <cstddef>

namespace ddl {

class Readable {
public:
    virtual ~Readable() = default;
    virtual void read(void *data, size_t size) = 0;
};

}
