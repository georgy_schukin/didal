#include "buffer_writer.h"

#include <cstring>

namespace ddl {

void BufferWriter::write(const void *data, size_t size) {
    if (offset + size > buf->size()) {
        buf->resize(offset + size);
    }
    memcpy(static_cast<unsigned char*>(buf->data()) + offset, data, size);
    offset += size;
}

}
