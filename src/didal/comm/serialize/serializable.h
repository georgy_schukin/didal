#pragma once

namespace ddl {

class Readable;
class Writable;

class Serializable {
public:
    virtual ~Serializable() = default;

    virtual Writable& serialize(Writable&) const = 0;
    virtual Readable& deserialize(Readable&) = 0;
};

}
