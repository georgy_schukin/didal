#pragma once

#include "readable.h"
#include "writable.h"
#include "serializable.h"

#include <vector>
#include <map>
#include <set>
#include <list>
#include <iterator>
#include <type_traits>
#include <string>
#include <array>
#include <tuple>
#include <memory>

namespace ddl {

// Scalar types.
template <typename T,
          typename std::enable_if<std::is_trivially_copyable<T>::value ||
                                  std::is_enum<T>::value, int>::type = 0>
Writable& operator<< (Writable &w, const T &value) {
    w.write(&value, sizeof(T));
    return w;
}

template <typename T,
          typename std::enable_if<std::is_trivially_copyable<T>::value ||
                                  std::is_enum<T>::value, int>::type = 0>
Readable& operator>> (Readable &r, T &value) {
    r.read(&value, sizeof(T));
    return r;
}

// Standard containers.
template <typename T1, typename T2>
Writable& operator<< (Writable &w, const typename std::pair<T1, T2> &p) {
    return w << p.first << p.second;
}

template <typename T1, typename T2>
Readable& operator>> (Readable &r, typename std::pair<T1, T2> &p) {
    return r >> p.first >> p.second;
}

template<size_t I = 0, typename... Tp>
typename std::enable_if<I == sizeof...(Tp), void>::type
writeTuple(Writable&, const std::tuple<Tp...>&) { }

template<size_t I = 0, typename... Tp>
typename std::enable_if<I < sizeof...(Tp), void>::type
writeTuple(Writable &w, const std::tuple<Tp...> &tup) {
    w << std::get<I>(tup);
    writeTuple<I + 1, Tp...>(w, tup);
}

template<size_t I = 0, typename... Tp>
typename std::enable_if<I == sizeof...(Tp), void>::type
readTuple(Readable&, std::tuple<Tp...>&) { }

template<size_t I = 0, typename... Tp>
typename std::enable_if<I < sizeof...(Tp), void>::type
readTuple(Readable &r, std::tuple<Tp...> &tup) {
    r >> std::get<I>(tup);
    readTuple<I + 1, Tp...>(r, tup);
}

template <typename... Args>
Writable& operator<< (Writable &w, const typename std::tuple<Args...> &tup) {
    writeTuple(w, tup);
    return w;
}

template <typename... Args>
Readable& operator>> (Readable &r, typename std::tuple<Args...> &tup) {
    readTuple(r, tup);
    return r;
}

template <typename Iter>
Writable& writeSequence(Writable &w, Iter begin, Iter end) {
    for (auto it = begin; it != end; it++) {
        w << *it;
    }
    return w;
}

template <typename InsertIter>
Readable& readSequence(Readable &r, size_t size, InsertIter iter) {
    typename InsertIter::container_type::value_type value;
    for (size_t i = 0; i < size; i++) {
        r >> value;
        iter = value;
    }
    return r;
}

template <typename Container>
Writable& writeContainer(Writable &w, const Container &cont) {
    w << cont.size();
    return writeSequence(w, cont.begin(), cont.end());
}

template <typename Container>
Readable& readContainer(Readable &r, Container &cont) {
    size_t size = 0;
    r >> size;
    return readSequence(r, size, std::inserter(cont, cont.end()));
}

template <typename Container>
Readable& readKeyValueContainer(Readable &r, Container &cont) {
    size_t size = 0;
    r >> size;
    for (size_t i = 0; i < size; i++) {
        typename Container::key_type key;
        typename Container::mapped_type value;
        r >> key >> value;
        cont.insert(typename Container::value_type(key, value));
    }
    return r;
}

template <typename T>
Writable& operator<< (Writable &w, const typename std::vector<T> &vec) {
    if (std::is_trivially_copyable<T>::value) {
        w << vec.size();
        w.write(&vec[0], vec.size()*sizeof(T));
        return w;
    } else {
        return writeContainer(w, vec);
    }
}

template <typename T, size_t Size>
Writable& operator<< (Writable &w, const typename std::array<T, Size> &arr) {
    if (std::is_trivially_copyable<T>::value) {
        w.write(&arr[0], Size*sizeof(T));
        return w;
    } else {
        for (size_t i = 0; i < Size; i++) {
            w << arr[i];
        }
        return w;
    }
}

template <typename T>
Writable& operator<< (Writable &w, const typename std::list<T> &list) {
    return writeContainer(w, list);
}

template <typename T, typename Compare>
Writable& operator<< (Writable &w, const typename std::set<T, Compare> &set) {
    return writeContainer(w, set);
}

template <typename Key, typename Value, typename Compare>
Writable& operator<< (Writable &w, const typename std::map<Key, Value, Compare> &map) {
    return writeContainer(w, map);
}

template <typename T>
Readable& operator>> (Readable &r, typename std::vector<T> &vec) {
    if (std::is_trivially_copyable<T>::value) {
        size_t size = 0;
        r >> size;
        vec.resize(size);
        r.read(&vec[0], vec.size()*sizeof(T));
        return r;
    } else {
        return readContainer(r, vec);
    }
}

template <typename T, size_t Size>
Readable& operator>> (Readable &r, typename std::array<T, Size> &arr) {
    if (std::is_trivially_copyable<T>::value) {
        r.read(&arr[0], Size*sizeof(T));
        return r;
    } else {
        for (size_t i = 0; i < Size; i++) {
            r >> arr[i];
        }
        return r;
    }
}

template <typename T>
Readable& operator>> (Readable &r, typename std::list<T> &list) {
    return readContainer(r, list);
}

template <typename T, typename Compare>
Readable& operator>> (Readable &r, typename std::set<T, Compare> &set) {
    return readContainer(r, set);
}

template <typename Key, typename Value, typename Compare>
Readable& operator>> (Readable &r, typename std::map<Key, Value, Compare> &map) {
    return readKeyValueContainer(r, map);
}

// Smart pointers.
template <typename T>
Writable& operator<< (Writable &w, const typename std::shared_ptr<T> &ptr) {
    if (ptr) {
        return w << true << *(ptr.get());
    } else {
        return w << false;
    }
}

template <typename T>
Readable& operator>> (Readable &r, typename std::shared_ptr<T> &ptr) {
    bool pointer_is_not_null = false;
    r >> pointer_is_not_null;
    if (pointer_is_not_null) {
        ptr = std::make_shared<T>();
        r >> *(ptr.get());
    } else {
        ptr = nullptr;
    }
    return r;
}

// Serializable objects.
Writable& operator<< (Writable &w, const Serializable *ser);
Writable& operator<< (Writable &w, const Serializable &ser);

Readable& operator>> (Readable &r, Serializable *ser);
Readable& operator>> (Readable &r, Serializable &ser);

// Standard strings.
Writable& operator<< (Writable &w, const std::string &str);
Readable& operator>> (Readable &r, std::string &str);

// Mass packing/unpacking help routines.
namespace serialize {
    template <typename T>
    Writable& writeArguments(Writable &w, const T& arg) {
        return w << arg;
    }

    template <typename T, typename... U>
    Writable& writeArguments(Writable &w, const T& arg, const U&... rest) {
        w << arg;
        return writeArguments(w, rest...);
    }

    template <typename T>
    Readable& readArguments(Readable &r, T& arg) {
        return r >> arg;
    }

    template <typename T, typename... U>
    Readable& readArguments(Readable &r, T& arg, U&... rest) {
        r >> arg;
        return readArguments(r, rest...);
    }

    template <typename Arg, typename... Rest>
    Writable& pack(Writable &w, const Arg& arg, const Rest&... rest) {
        return writeArguments(w, arg, rest...);
    }

    template <typename Arg, typename... Rest>
    Readable& unpack(Readable &r, Arg& arg, Rest&... rest) {
        return readArguments(r, arg, rest...);
    }
}

}
