#pragma once

#include "writable.h"
#include "didal/base/growing_buffer.h"

#include <cstddef>

namespace ddl {

class BufferWriter : public Writable {
public:
    BufferWriter(GrowingBuffer *buf, size_t offset = 0) :
        buf(buf), offset(offset) {
    }

    void write(const void *data, size_t size) override;

private:
    GrowingBuffer *buf;
    size_t offset;
};

}
