#pragma once

#include "didal/trace/traceable.h"

#include <cstddef>
#include <utility>
#include <vector>

namespace ddl {

class Message;
class Node;
class Buffer;
class GrowingBuffer;

class Communicator : public Traceable {
public:
    enum ReduceOperationType {
        RO_NONE = 0,
        RO_SUM,
        RO_MIN,
        RO_MAX
    };

    enum DataType {
        DT_NONE = 0,
        DT_INT,
        DT_DOUBLE
    };

public:
    virtual ~Communicator() = default;

    virtual int thisRank() const = 0;
    virtual int numOfNodes() const = 0;

    virtual void send(const Message &msg, const Node &node) = 0;
    virtual std::pair<Message, Node> recvFromAny() = 0;
    virtual std::pair<Message, Node> tryRecvFromAny() = 0;

    virtual void waitForSendsCompletion() = 0;

    virtual void gatherOn(const Buffer &send_buf, GrowingBuffer &recv_buf, const Node &root) = 0;
    virtual void gatherOnAll(const Buffer &send_buf, GrowingBuffer &recv_buf) = 0;
    virtual void broadcastFrom(GrowingBuffer &buf, const Node &root) = 0;    
    virtual void reduce(const Buffer &send_buf, Buffer &recv_buf, DataType type, ReduceOperationType oper) = 0;
    virtual void barrier() = 0;
};

}
