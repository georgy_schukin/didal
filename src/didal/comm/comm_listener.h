#pragma once

#include "message.h"

namespace ddl {

class Node;

class CommListener {
public:
    virtual ~CommListener() = default;

    virtual void onMessage(const Message &msg, const Node &src) = 0;
};

}
