#pragma once

#include "message.h"
#include "serialize/serializing_ops.h"

namespace ddl {

template <typename Arg, typename... Rest>
Message& msgSerialize(Message &msg, const Arg& arg, const Rest&... rest) {
    auto writer = msg.getWriter();    
    serialize::pack(writer, arg, rest...);
    return msg;
}

template <typename Arg, typename... Rest>
const Message& msgDeserialize(const Message &msg, Arg& arg, Rest&... rest) {
    auto reader = msg.getReader();
    serialize::unpack(reader, arg, rest...);
    return msg;
}

}
