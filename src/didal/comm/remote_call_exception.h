#pragma once

#include "serialize/serializable.h"

#include <exception>
#include <string>

namespace ddl {

class RemoteCallException : public std::exception {
public:
    RemoteCallException() {}
    RemoteCallException(const std::string &str) :
        message(str) {
    }

    const char* what() const noexcept override {
        return message.data();
    }

    friend Writable& operator<< (Writable &w, const RemoteCallException &e);
    friend Readable& operator>> (Readable &r, RemoteCallException &e);

private:
    std::string message;
};

}
