#pragma once

#include "didal/comm/communicator.h"
#include "didal/comm/message.h"
#include "didal/util/timer.h"

#include <mpi.h>
#include <atomic>
#include <list>
#include <utility>
#include <mutex>
#include <memory>
#include <chrono>
#include <thread>
#include <condition_variable>

namespace ddl {

class MPICommunicator : public Communicator {
public:
    MPICommunicator(int *argc, char ***argv, bool enable_progress_thread = false);
    ~MPICommunicator() override;

    int thisRank() const override;
    int numOfNodes() const override;

    void send(const Message &msg, const Node &node) override;
    std::pair<Message, Node> recvFromAny() override;
    std::pair<Message, Node> tryRecvFromAny() override;

    void waitForSendsCompletion() override;

    void gatherOn(const Buffer &send_buf, GrowingBuffer &recv_buf, const Node &root) override;
    void gatherOnAll(const Buffer &send_buf, GrowingBuffer &recv_buf) override;
    void broadcastFrom(GrowingBuffer &buf, const Node &root) override;
    void reduce(const Buffer &send_buf, Buffer &recv_buf, DataType type, ReduceOperationType oper) override;
    void barrier() override;

    void setTracer(Tracer *tracer) override;

    void useNonBlockingProbe(bool use) {
        use_iprobe = use;
    }

    void useProgressThread(bool use) {
        use_progress_thread = use;
    }

    void useAdaptiveProbeInterval(bool use) {
        use_adaptive_probe_interval = use;
    }

    void setSendsCheckInterval(size_t interval_mcsec) {
        sends_check_interval_mcsec = interval_mcsec;
    }

    void setProbeInterval(size_t interval_mcsec) {
        probe_interval_mcsec = interval_mcsec;
    }

    void setMaxProbeInterval(size_t interval_mcsec) {
        max_probe_interval_mcsec = interval_mcsec;
    }

private:
    struct SendData {
        Message message;
        MPI_Request request;

        SendData(const Message &msg) :
            message(msg) {
        }

        // Make send data not-copyable.
        SendData(const SendData&) = delete;
        SendData(SendData&&) = delete;
        SendData& operator=(const SendData&) = delete;
        SendData& operator=(SendData&&) = delete;
    };

    using SendDataPtr = std::shared_ptr<SendData>;

    struct RecvInfo {
        MPI_Status status;
#if defined(MPI_VERSION) && MPI_VERSION >= 3
        MPI_Message message;
#endif
    };

private:
    std::pair<Message, Node> recvMessage(const RecvInfo &recv_info);

    SendDataPtr makeSendData(const Message &msg);
    void addNewSend(const SendDataPtr &data);
    void addNewSendAndCheckSendsCompletion(const SendDataPtr &data);
    void checkSendsCompletion();
    bool checkSendIsCompleted(const SendDataPtr &data);

    void doCheckSendsCompletion();
    void waitForNewSends();

    void checkSuccess(int error_code);

    int nextTag();

private:
    int rank;
    int size;
    MPI_Comm comm;
    MPI_Comm collective_comm;
    int current_tag {0};
    int max_tag;
    mutable std::mutex send_mutex;
    mutable std::mutex tag_mutex;
    std::vector<SendDataPtr> ongoing_sends;

    Tracer *tracer {nullptr};

    Timer last_sends_check_timer;

    bool use_iprobe {true};
    bool use_progress_thread {false};
    bool use_adaptive_probe_interval {false};
    size_t sends_check_interval_mcsec {1000};
    size_t probe_interval_mcsec {10};
    size_t max_probe_interval_mcsec {100000};

    std::thread progress_thread;
    std::condition_variable new_send_cond;
    bool is_working {true};
};

}
