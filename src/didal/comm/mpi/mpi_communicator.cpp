#include "mpi_communicator.h"
#include "didal/comm/message.h"
#include "didal/comm/node.h"
#include "didal/trace/tracer.h"
#include "didal/trace/value_counter.h"
#include "didal/util/timer.h"

#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <chrono>
#include <limits>
#include <numeric>
#include <thread>
#include <cassert>
#include <algorithm>

using namespace std;

namespace ddl {

namespace {
    std::string getModeName(const int mode) {
        switch (mode) {
        case MPI_THREAD_SINGLE:
            return "MPI_THREAD_SINGLE";
        case MPI_THREAD_FUNNELED:
            return "MPI_THREAD_FUNNELED";
        case MPI_THREAD_SERIALIZED:
            return "MPI_THREAD_SERIALIZED";
        case MPI_THREAD_MULTIPLE:
            return "MPI_THREAD_MULTIPLE";
        default:
            return "Unknown";
        }
    }

    MPI_Datatype sizeType() {
        const auto max = sizeof(size_t);
        if (max == sizeof(unsigned char)) {
            return MPI_UNSIGNED_CHAR;
        } else if (max == sizeof(unsigned short)) {
            return MPI_UNSIGNED_SHORT;
        } else if (max == sizeof(unsigned int)) {
            return MPI_UNSIGNED;
        } else if (max == sizeof(unsigned long)) {
            return MPI_UNSIGNED_LONG;
        } else if (max == sizeof(unsigned long long)) {
            return MPI_UNSIGNED_LONG_LONG;
        } else {
            return MPI_UNSIGNED;
        }
    }

    const static auto SIZE_TYPE = sizeType();

    MPI_Op mpiOp(Communicator::ReduceOperationType oper) {
        switch (oper) {
            case Communicator::RO_SUM: return MPI_SUM;
            case Communicator::RO_MIN: return MPI_MIN;
            case Communicator::RO_MAX: return MPI_MAX;
            default: {
                throw std::runtime_error("Unknown reduce operation: " + std::to_string(oper));
            }
        }
    }

    MPI_Datatype mpiType(Communicator::DataType type) {
        switch (type) {
            case Communicator::DT_INT: return MPI_INT;
            case Communicator::DT_DOUBLE: return MPI_DOUBLE;
            default: {
                throw std::runtime_error("Unknown data type: " + std::to_string(type));
            }
        }
    }
}

MPICommunicator::MPICommunicator(int *argc, char ***argv, bool enable_progress_thread) :
    use_progress_thread(enable_progress_thread) {
    int required_mode = MPI_THREAD_MULTIPLE;
    int provided_mode = 0;
    checkSuccess(MPI_Init_thread(argc, argv, required_mode, &provided_mode));
    if (provided_mode != required_mode) {
        std::ostringstream out;
        out << "MPI Init thread error : get only "
            << getModeName(provided_mode)
            << " mode instead of "
            << getModeName(required_mode) << " mode\n";
        std::cerr << out.str();
    }
    checkSuccess(MPI_Comm_dup(MPI_COMM_WORLD, &comm));
    checkSuccess(MPI_Comm_dup(comm, &collective_comm));
    checkSuccess(MPI_Comm_rank(comm, &rank));
    checkSuccess(MPI_Comm_size(comm, &size));

    int tag_ub_success = 0;
    int* tag_ub_value = nullptr;
    checkSuccess(MPI_Comm_get_attr(MPI_COMM_WORLD, MPI_TAG_UB, &tag_ub_value, &tag_ub_success));
    max_tag = (tag_ub_success ? *(tag_ub_value) : std::numeric_limits<int>::max());

    last_sends_check_timer.reset();

    if (use_progress_thread) {
        progress_thread = std::thread([this]() {
            while (is_working) {
                waitForSendsCompletion();
                waitForNewSends();
            }
        });
    }
}

MPICommunicator::~MPICommunicator() {
    is_working = false;
    new_send_cond.notify_all();
    if (use_progress_thread) {
        progress_thread.join();
    }
    checkSuccess(MPI_Comm_free(&comm));
    checkSuccess(MPI_Comm_free(&collective_comm));
    checkSuccess(MPI_Finalize());
}

int MPICommunicator::thisRank() const {
    return rank;
}

int MPICommunicator::numOfNodes() const {
    return size;
}

void MPICommunicator::send(const Message &msg, const Node &node) {
    static auto isend_size = (tracer ? tracer->newCounter<ValueCounter<size_t>>("MPI Isend size") : nullptr);
    static auto isend_time = (tracer ? tracer->newCounter<ValueCounter<double>>("MPI Isend time") : nullptr);

    auto send_data = makeSendData(msg);
    const auto &send_msg = send_data->message;
    auto &send_request = send_data->request;

    ddl::Timer timer;
    const auto tag = nextTag();
    assert (send_msg.buffer() != nullptr);
    checkSuccess(MPI_Isend(send_msg.buffer(), static_cast<int>(send_msg.bufSize()), MPI_BYTE, node.rank(), tag, comm, &send_request));    
    const auto time = timer.time();

    if (isend_size) {
        isend_size->add(send_msg.bufSize());
    }
    if (isend_time) {
        isend_time->add(time);
    }

    addNewSendAndCheckSendsCompletion(send_data);
}

MPICommunicator::SendDataPtr MPICommunicator::makeSendData(const Message &msg) {
    auto send_data = make_shared<SendData>(msg);
    send_data->message.pack();
    return send_data;
}

void MPICommunicator::addNewSend(const SendDataPtr &data) {
    std::lock_guard<std::mutex> lock(send_mutex);
    ongoing_sends.push_back(data);
    new_send_cond.notify_one();
}

void MPICommunicator::checkSendsCompletion() {
    std::lock_guard<std::mutex> lock(send_mutex);
    doCheckSendsCompletion();
}

void MPICommunicator::addNewSendAndCheckSendsCompletion(const SendDataPtr &data) {
    // Check completion right away first. This may help to force MPI to actually start sending the data.
    if (checkSendIsCompleted(data)) {
        return;
    }
    std::lock_guard<std::mutex> lock(send_mutex);
    ongoing_sends.push_back(data);
    new_send_cond.notify_one();
    if (!use_progress_thread) {
        doCheckSendsCompletion();
    }
}

void MPICommunicator::doCheckSendsCompletion() {    
    static auto test_time = (tracer ? tracer->newCounter<ValueCounter<double>>("MPI Test time") : nullptr);

    if (ongoing_sends.empty() || (last_sends_check_timer.mcsecTime() < sends_check_interval_mcsec)) {
        return;
    }

    std::vector<SendDataPtr> remaining_sends;
    for (auto &send: ongoing_sends) {
        if (!checkSendIsCompleted(send)) {
            remaining_sends.push_back(send);
        }
    }
    ongoing_sends = std::move(remaining_sends);

    last_sends_check_timer.reset();
}

bool MPICommunicator::checkSendIsCompleted(const SendDataPtr &data) {
    static auto test_time = (tracer ? tracer->newCounter<ValueCounter<double>>("MPI Test time") : nullptr);

    int completed = 0;    
    Timer timer;
    checkSuccess(MPI_Test(&data->request, &completed, MPI_STATUS_IGNORE));
    if (test_time) {
        test_time->add(timer.time());
    }
    return completed;
}

void MPICommunicator::waitForNewSends() {
     std::unique_lock<std::mutex> lock(send_mutex);
     new_send_cond.wait(lock, [this]() { return !ongoing_sends.empty() || !is_working; });
}

void MPICommunicator::waitForSendsCompletion() {
    std::vector<SendDataPtr> waiting_sends;
    {
        std::lock_guard<std::mutex> lock(send_mutex);        
        waiting_sends = std::move(ongoing_sends); // move active sends to waiting
    }    
    for (auto &data: waiting_sends) {
        checkSuccess(MPI_Wait(&data->request, MPI_STATUSES_IGNORE));
    }
}

int MPICommunicator::nextTag() {
    std::lock_guard<std::mutex> lock(tag_mutex);
    if (current_tag < 0 || current_tag > max_tag) {
        current_tag = 0;
    }
    return current_tag++;
}

std::pair<Message, Node> MPICommunicator::recvFromAny() {
    static auto mprobe_time = (tracer ? tracer->newCounter<ValueCounter<double>>("MPI Mprobe time") : nullptr);

    RecvInfo recv_info {};

    auto current_probe_interval = probe_interval_mcsec;

    double time = 0;
    if (use_iprobe) {
        int flag = 0;
        do {
            Timer timer;
#if defined(MPI_VERSION) && MPI_VERSION >= 3
            checkSuccess(MPI_Improbe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &flag, &(recv_info.message), &(recv_info.status)));
#else
            checkSuccess(MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &flag, &(recv_info.status)));
#endif
            time += timer.time();
            if (!flag) {
                std::this_thread::sleep_for(std::chrono::microseconds(current_probe_interval));
                if (use_adaptive_probe_interval) {
                    current_probe_interval = std::min(current_probe_interval * 2, max_probe_interval_mcsec);
                }
            }
        } while (!flag);
    } else {
        Timer timer;
#if defined(MPI_VERSION) && MPI_VERSION >= 3
        checkSuccess(MPI_Mprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &(recv_info.message), &(recv_info.status)));
#else
        checkSuccess(MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &(recv_info.status)));
#endif
        time = timer.time();
    }

    if (mprobe_time) {
        mprobe_time->add(time);
    }

    return recvMessage(recv_info);
}

std::pair<Message, Node> MPICommunicator::tryRecvFromAny() {
    RecvInfo recv_info {};
    int has_recv = 0;
#if defined(MPI_VERSION) && MPI_VERSION >= 3
    checkSuccess(MPI_Improbe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &has_recv, &(recv_info.message), &(recv_info.status)));
#else
    checkSuccess(MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &has_recv, &(recv_info.status)));
#endif
    if (!has_recv) {
        return std::make_pair(Message {}, Node {});
    } else {
        return recvMessage(recv_info);
    }
}

std::pair<Message, Node> MPICommunicator::recvMessage(const RecvInfo &recv_info) {
    static auto mrecv_size = (tracer ? tracer->newCounter<ValueCounter<size_t>>("MPI Mrecv size") : nullptr);
    static auto mrecv_time = (tracer ? tracer->newCounter<ValueCounter<double>>("MPI Mrecv time") : nullptr);

    int buf_size = 0;
    auto status = recv_info.status;
    checkSuccess(MPI_Get_count(&status, MPI_BYTE, &buf_size));
    auto msg = Message::fromBufSize(static_cast<size_t>(buf_size));
    assert (msg.buffer() != nullptr);

    Timer timer;
#if defined(MPI_VERSION) && MPI_VERSION >= 3
    auto message = recv_info.message;
    checkSuccess(MPI_Mrecv(msg.buffer(), buf_size, MPI_BYTE,
                           &message, MPI_STATUS_IGNORE));
#else
    checkSuccess(MPI_Recv(msg.buffer(), buf_size, MPI_BYTE,
                          status.MPI_SOURCE, status.MPI_TAG, comm, MPI_STATUS_IGNORE));
#endif
    const auto time = timer.time();

    if (mrecv_size) {
        mrecv_size->add(msg.bufSize());
    }
    if (mrecv_time) {
        mrecv_time->add(time);
    }

    msg.unpack();
    return std::make_pair(msg, Node(status.MPI_SOURCE));
}

/*void MPICommunicator::gather(void *source, size_t source_size, void *dest, const Node &root) {
    checkSuccess(MPI_Gather(source, source_size, MPI_BYTE, dest, size*source_size, MPI_BYTE, root.rank(), comm));
}

void MPICommunicator::scatter(void *source, size_t source_size, void *dest, const Node &root) {
    checkSuccess(MPI_Scatter(source, source_size, MPI_BYTE, dest, size*source_size, MPI_BYTE, root.rank(), comm));
}

void MPICommunicator::allGather(const Buffer &buf, Buffer &dest) {
    const auto num_of_nodes = static_cast<size_t>(numOfNodes());
    std::vector<int> buf_sizes(num_of_nodes, 0);
    std::vector<int> buf_displs(num_of_nodes, 0);
    const auto src_size = static_cast<int>(buf.size());
    checkSuccess(MPI_Allgather(&src_size, 1, MPI_INT, &buf_sizes[0], 1, MPI_INT, comm));
    for (size_t i = 1; i < num_of_nodes; i++) {
        buf_displs[i] = buf_displs[i - 1] + buf_sizes[i - 1];
    }
    dest.resize(buf_displs[num_of_nodes - 1] + buf_sizes[num_of_nodes - 1]);
    checkSuccess(MPI_Allgatherv(buf.data(), buf.size(), MPI_BYTE, dest.data(), &buf_sizes[0], &buf_displs[0], MPI_BYTE, comm));
}*/

void MPICommunicator::gatherOn(const Buffer &send_buf, GrowingBuffer &recv_buf, const Node &root) {
    vector<int> buf_sizes(size);
    auto send_buf_size = static_cast<int>(send_buf.size());
    checkSuccess(MPI_Gather(&send_buf_size, 1, MPI_INT, buf_sizes.data(), 1, MPI_INT, root.rank(), collective_comm));
    vector<int> displacements(1, 0);
    for (auto sz: buf_sizes) {
        displacements.push_back(displacements.back() + sz);
    }
    recv_buf.resize(displacements.back());
    checkSuccess(MPI_Gatherv(send_buf.data(), send_buf_size, MPI_BYTE, recv_buf.data(), buf_sizes.data(), displacements.data(), MPI_BYTE, root.rank(), collective_comm));
}

void MPICommunicator::gatherOnAll(const Buffer &send_buf, GrowingBuffer &recv_buf) {
    /*const auto root = Node(0);
    gatherOn(send_buf, recv_buf, root);
    broadcastFrom(recv_buf, root);*/
    vector<int> buf_sizes(size);
    auto send_buf_size = static_cast<int>(send_buf.size());
    checkSuccess(MPI_Allgather(&send_buf_size, 1, MPI_INT, buf_sizes.data(), 1, MPI_INT, collective_comm));
    vector<int> displacements(1, 0);
    for (auto sz: buf_sizes) {
        displacements.push_back(displacements.back() + sz);
    }
    recv_buf.resize(displacements.back());
    checkSuccess(MPI_Allgatherv(send_buf.data(), send_buf_size, MPI_BYTE, recv_buf.data(), buf_sizes.data(), displacements.data(), MPI_BYTE, collective_comm));
}

void MPICommunicator::broadcastFrom(GrowingBuffer &buf, const Node &root) {
    int buf_size = 0;
    if (rank == root.rank()) {
        buf_size = static_cast<int>(buf.size());
    }
    checkSuccess(MPI_Bcast(&buf_size, 1, MPI_INT, root.rank(), collective_comm));
    buf.resize(buf_size);
    checkSuccess(MPI_Bcast(buf.data(), buf_size, MPI_BYTE, root.rank(), collective_comm));
}

void MPICommunicator::reduce(const Buffer &send_buf, Buffer &recv_buf, DataType type, ReduceOperationType oper) {
    static auto reduce_time = (tracer ? tracer->newCounter<ValueCounter<double>>("MPI Allreduce time") : nullptr);

    const auto mpi_type = mpiType(type);
    const auto mpi_op = mpiOp(oper);

    Timer timer;
    checkSuccess(MPI_Allreduce(send_buf.data(), recv_buf.data(), 1, mpi_type, mpi_op, collective_comm));
    if (reduce_time) {
        reduce_time->add(timer.time());
    }
}

void MPICommunicator::barrier() {
    static auto barrier_time = (tracer ? tracer->newCounter<ValueCounter<double>>("MPI Barrier time") : nullptr);

    Timer timer;
    checkSuccess(MPI_Barrier(collective_comm));
    if (barrier_time) {
        barrier_time->add(timer.time());
    }
}

void MPICommunicator::checkSuccess(int error_code) {
    if (error_code != MPI_SUCCESS) {
        char error_string[MPI_MAX_ERROR_STRING];
        int error_string_length = 0;
        MPI_Error_string(error_code, error_string, &error_string_length);
        std::ostringstream out;
        out << "[" << rank << "]: MPI error: " << error_string;
        throw std::runtime_error(out.str());
    }
}

void MPICommunicator::setTracer(Tracer *tracer) {
    this->tracer = tracer;
}

}
