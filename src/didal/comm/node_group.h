#pragma once

#include "node.h"

#include <vector>
#include <set>
#include <cstddef>

namespace ddl {

class NodeGroup {
public:
    typedef std::set<Node>::iterator iterator;
    typedef std::set<Node>::const_iterator const_iterator;

public:
    NodeGroup();
    NodeGroup(const std::initializer_list<Node> &nodes);

    void add(const Node &node);

    size_t size() const;

    std::vector<int> getRanks() const;

    iterator begin() {
        return _nodes.begin();
    }

    iterator end() {
        return _nodes.end();
    }

    const_iterator begin() const {
        return _nodes.begin();
    }

    const_iterator end() const {
        return _nodes.end();
    }

private:
    std::set<Node> _nodes;
};

}
