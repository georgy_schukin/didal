#include "remote_call_exception.h"
#include "serialize/serializing_ops.h"

namespace ddl {

Writable& operator<<(Writable &w, const RemoteCallException &e) {
    return w << e.message;
}

Readable& operator>>(Readable &r, RemoteCallException &e) {
    return r >> e.message;
}

}
