#pragma once

#include "message_processor.h"

namespace ddl {

class ImmediateMessageProcessor : public MessageProcessor {
public:
    ImmediateMessageProcessor() {}

    virtual void addMessage(CommListener *listener, const Message &message, const Node &src_node) override;
    virtual void addMessages(CommListener *listener, const std::vector<std::pair<Message, Node>> &messages) override;

    virtual void shutdown() override;

    void setTracer(Tracer *tracer) override;

protected:
    void processMessage(CommListener *listener, const Message &message, const Node &src_node);

    Tracer* getTracer() {
        return tracer;
    }

private:
    Tracer *tracer = nullptr;
};

}
