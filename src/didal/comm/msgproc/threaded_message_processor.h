#pragma once

#include "immediate_message_processor.h"

#include <queue>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <tuple>
#include <vector>

namespace ddl {

class ThreadedMessageProcessor : public ImmediateMessageProcessor {
public:
    ThreadedMessageProcessor();
    ~ThreadedMessageProcessor() override;

    void addMessage(CommListener *listener, const Message &message, const Node &src_node) override;
    void addMessages(CommListener *listener, const std::vector<std::pair<Message, Node>> &messages) override;

    void shutdown() override;

private:
    void procFunc();

private:
    std::atomic<bool> is_working {true};

    std::queue<std::tuple<CommListener*, Message, Node>> msg_queue;

    std::mutex queue_mutex;
    std::condition_variable new_message_cond;

    std::thread proc_thread;
};

}
