#pragma once

#include "didal/comm/message.h"
#include "didal/trace/traceable.h"

#include <vector>

namespace ddl {

class CommListener;

class MessageSender : public Traceable {
public:
    virtual ~MessageSender() = default;

    virtual void sendMessage(const Message &message, const Node &dest_node) = 0;

    virtual void waitForSendsCompletion() = 0;

    virtual void shutdown() = 0;
};

}
