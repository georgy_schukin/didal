#include "threaded_message_processor.h"

#include <chrono>

namespace ddl {

ThreadedMessageProcessor::ThreadedMessageProcessor() {
    proc_thread = std::thread(&ThreadedMessageProcessor::procFunc, this);
}

ThreadedMessageProcessor::~ThreadedMessageProcessor() {
    ThreadedMessageProcessor::shutdown();
    proc_thread.join();
}

void ThreadedMessageProcessor::addMessage(CommListener *listener, const Message &message, const Node &src_node) {
    std::lock_guard<std::mutex> lock(queue_mutex);
    msg_queue.push(std::make_tuple(listener, message, src_node));
    new_message_cond.notify_one();
}

void ThreadedMessageProcessor::addMessages(CommListener *listener, const std::vector<std::pair<Message, Node>> &messages) {
    std::lock_guard<std::mutex> lock(queue_mutex);
    for (auto &p: messages) {
        msg_queue.push(std::make_tuple(listener, p.first, p.second));
    }
    new_message_cond.notify_all();
}

void ThreadedMessageProcessor::procFunc() {
    while (is_working) {
        CommListener *listener = nullptr;
        Message message;
        Node src_node;
        {
            std::unique_lock<std::mutex> lock(queue_mutex);
            new_message_cond.wait_for(lock, std::chrono::seconds(1),
                [this]() { return !msg_queue.empty() || !is_working; });
            if (!msg_queue.empty()) {
                std::tie(listener, message, src_node) = msg_queue.front();
                msg_queue.pop();
            }
        }
        if (listener) {
            processMessage(listener, message, src_node);
        }
    }
}

void ThreadedMessageProcessor::shutdown() {
    is_working = false;
    new_message_cond.notify_all();
}

}
