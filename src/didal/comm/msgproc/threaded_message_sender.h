#pragma once

#include "immediate_message_sender.h"

#include <queue>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <tuple>
#include <vector>

namespace ddl {

class ThreadedMessageSender : public ImmediateMessageSender {
public:
    ThreadedMessageSender(Communicator *comm);
    ~ThreadedMessageSender() override;

    virtual void sendMessage(const Message &message, const Node &dest_node) override;

    void shutdown() override;

private:
    void sendFunc();

private:
    std::atomic<bool> is_working {true};

    std::queue<std::tuple<Message, Node>> send_queue;

    std::mutex queue_mutex;
    std::condition_variable new_message_cond;

    std::thread send_thread;
};

}
