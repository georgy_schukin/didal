#include "immediate_message_processor.h"
#include "didal/comm/comm_listener.h"
#include "didal/trace/tracer.h"
#include "didal/trace/value_counter.h"
#include "didal/util/timer.h"

namespace ddl {

void ImmediateMessageProcessor::addMessage(CommListener *listener, const Message &message, const Node &src_node) {
    processMessage(listener, message, src_node);
}

void ImmediateMessageProcessor::addMessages(CommListener *listener, const std::vector<std::pair<Message, Node>> &messages) {
    for (const auto &p: messages) {
        processMessage(listener, p.first, p.second);
    }
}

void ImmediateMessageProcessor::processMessage(CommListener *listener, const Message &message, const Node &src_node) {
    static auto proc_time = (tracer ? tracer->newCounter<ValueCounter<double>>("MSG proc time") : nullptr);

    Timer timer;
    listener->onMessage(message, src_node);
    const auto time = timer.time();

    if (proc_time) {
        proc_time->add(time);
    }
}

void ImmediateMessageProcessor::shutdown() {
}

void ImmediateMessageProcessor::setTracer(Tracer *tracer) {
    this->tracer = tracer;
}

}
