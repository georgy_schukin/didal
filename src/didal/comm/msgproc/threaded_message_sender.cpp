#include "threaded_message_sender.h"

#include <chrono>

namespace ddl {

ThreadedMessageSender::ThreadedMessageSender(Communicator *comm) :
    ImmediateMessageSender(comm) {
    send_thread = std::thread(&ThreadedMessageSender::sendFunc, this);
}

ThreadedMessageSender::~ThreadedMessageSender() {
    ThreadedMessageSender::shutdown();
    send_thread.join();
}

void ThreadedMessageSender::sendMessage(const Message &message, const Node &dest_node) {
    std::lock_guard<std::mutex> lock(queue_mutex);
    send_queue.push(std::make_tuple(message, dest_node));
    new_message_cond.notify_one();
}

void ThreadedMessageSender::sendFunc() {
    while (is_working) {
        Message message;
        Node dest_node;
        bool has_message = false;
        {
            std::unique_lock<std::mutex> lock(queue_mutex);
            new_message_cond.wait_for(lock, std::chrono::seconds(1),
                [this]() { return !send_queue.empty() || !is_working; });
            if (!send_queue.empty()) {
                std::tie(message, dest_node) = send_queue.front();
                send_queue.pop();
                has_message = true;
            }
        }
        if (has_message) {
            ImmediateMessageSender::sendMessage(message, dest_node);
        }
    }
}

void ThreadedMessageSender::shutdown() {
    is_working = false;
    new_message_cond.notify_all();
}

}
