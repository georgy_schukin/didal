#include "immediate_message_sender.h"
#include "didal/comm/comm_listener.h"
#include "didal/trace/tracer.h"
#include "didal/trace/value_counter.h"
#include "didal/util/timer.h"

namespace ddl {

void ImmediateMessageSender::sendMessage(const Message &message, const Node &dest_node) {
    comm->send(message, dest_node);
}

void ImmediateMessageSender::waitForSendsCompletion() {
    comm->waitForSendsCompletion();
}

void ImmediateMessageSender::shutdown() {
}

void ImmediateMessageSender::setTracer(Tracer *tracer) {
    this->tracer = tracer;
}

}
