#pragma once

#include "didal/comm/message.h"
#include "didal/trace/traceable.h"

#include <vector>

namespace ddl {

class CommListener;

class MessageProcessor : public Traceable {
public:
    virtual ~MessageProcessor() = default;

    virtual void addMessage(CommListener *listener, const Message &message, const Node &src_node) = 0;
    virtual void addMessages(CommListener *listener, const std::vector<std::pair<Message, Node>> &messages) = 0;

    virtual void shutdown() = 0;
};

}
