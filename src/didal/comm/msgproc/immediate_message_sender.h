#pragma once

#include "message_sender.h"
#include "didal/comm/communicator.h"

namespace ddl {

class ImmediateMessageSender : public MessageSender {
public:
    ImmediateMessageSender(Communicator *comm) :
        comm(comm) {
    }

    virtual void sendMessage(const Message &message, const Node &dest_node) override;

    virtual void waitForSendsCompletion() override;

    virtual void shutdown() override;

    void setTracer(Tracer *tracer) override;

protected:
    Tracer* getTracer() {
        return tracer;
    }

private:
    Communicator *comm;
    Tracer *tracer = nullptr;
};

}
