#include "comm_service.h"
#include "comm_listener.h"
#include "communicator.h"
#include "message.h"
#include "msgproc/immediate_message_processor.h"
#include "msgproc/threaded_message_processor.h"
#include "msgproc/immediate_message_sender.h"
#include "msgproc/threaded_message_sender.h"
#include "didal/trace/tracer.h"
#include "didal/trace/value_counter.h"

#include <iostream>
#include <chrono>

namespace ddl {

namespace {
    enum MessageTags : int {
        SHUTDOWN = 0
    };
}

CommService::CommService(Communicator *comm, MessageSenderType send_type, MessageProcessorType proc_type) :
    communicator(comm)
{
    own_comm_id = newCommId<CommService>();
    recv_thread = std::thread(&CommService::recvFunc, this);        
    setMessageSender(send_type);
    setMessageProcessor(proc_type);
}

CommService::~CommService() {
    for (auto node: allNodes()) {
        sendMessage(Message(0, own_comm_id, SHUTDOWN), node);
    }    
    message_sender->waitForSendsCompletion();
    message_sender->shutdown();
    message_processor->shutdown();    
    recv_thread.join();
}

CommId CommService::newCommId(const size_t &hash) {
    std::lock_guard<std::mutex> lock(listener_mutex);
    auto &last_comm_id = comm_ids[hash];
    return CommId(hash, last_comm_id++);
}

void CommService::setMessageSender(MessageSenderType type) {
    message_sender = makeMessageSender(type);
}

void CommService::setMessageProcessor(MessageProcessorType type) {
    message_processor = makeMessageProcessor(type);
}

void CommService::subscribe(const CommId &id, CommListener *listener) {
    addListener(id, listener);
    processPendingMessages(id, listener);
}

void CommService::unsubscribe(const CommId &id) {
    removeListener(id);
}

void CommService::addListener(const CommId &id, CommListener *listener) {
    std::lock_guard<std::mutex> lock(listener_mutex);
    //std::cout << thisNode().rank() << " LISTEN " << id << std::endl;
    listeners[id] = listener;
}

void CommService::removeListener(const CommId &id) {
    std::lock_guard<std::mutex> lock(listener_mutex);
    listeners.erase(id);
}

CommListener* CommService::getListener(const CommId &id) const {
    std::lock_guard<std::mutex> lock(listener_mutex);
    auto it = listeners.find(id);
    return (it != listeners.end() ? it->second : nullptr);
}

void CommService::recvFunc() {
    Message message;
    Node src_node;
    while (true) {        
        std::tie(message, src_node) = communicator->recvFromAny();
        //std::cout << "Recv " << message.commId() << " " << message.tag() << std::endl;
        if (!src_node.isValid()) {
            //std::cerr << "Invalid rank " << src_node.rank() << std::endl;
            continue;
        }
        if (isShutdownMessage(message)) {
            if (++shutdown_cnt >= communicator->numOfNodes()) {
                break;
            }
        } else {
            processMessage(message, src_node);
        }
    }
    // Process messages if there are some still left.
    bool some_left = true;
    while (some_left) {
        std::tie(message, src_node) = communicator->tryRecvFromAny();
        if (src_node.isValid()) {
            processMessage(message, src_node);
        } else {
            some_left = false;
        }
    }
}

bool CommService::isShutdownMessage(const Message &msg) const {
    return (msg.commId() == own_comm_id && msg.tag() == SHUTDOWN);
}

void CommService::processMessage(const Message &msg, const Node &src) {
    std::unique_lock<std::mutex> lock(listener_mutex);
    auto it = listeners.find(msg.commId());
    if (it == listeners.end()) {
        pending_messages[msg.commId()].push_back(std::make_pair(msg, src));
    } else {
        lock.unlock();
        message_processor->addMessage(it->second, msg, src);
    }
}

void CommService::addToPending(const Message &msg, const Node &src) {
    std::lock_guard<std::mutex> lock(listener_mutex);
    pending_messages[msg.commId()].push_back(std::make_pair(msg, src));
}

void CommService::processPendingMessages(const CommId &id, CommListener *listener) {
    std::vector<std::pair<Message, Node>> messages;
    {
        std::unique_lock<std::mutex> lock(listener_mutex);
        auto it = pending_messages.find(id);
        if (it == pending_messages.end()) {
            return;
        }
        messages = std::move(it->second);
        pending_messages.erase(it);
    }
    message_processor->addMessages(listener, messages);
}

void CommService::sendMessage(const Message &msg, const Node &node) {
    message_sender->sendMessage(msg, node);
}

Node CommService::thisNode() const {
    return Node(communicator->thisRank());
}

NodeGroup CommService::allNodes() const {
    NodeGroup all_nodes;
    for (int i = 0; i < communicator->numOfNodes(); i++) {
        all_nodes.add(Node(i));
    }
    return all_nodes;
}

void CommService::setTracer(Tracer *tracer) {
    this->tracer = tracer;
    message_sender->setTracer(tracer);
    message_processor->setTracer(tracer);
}

std::unique_ptr<MessageSender> CommService::makeMessageSender(MessageSenderType type) {
    switch (type) {
        case MessageSenderType::Immediate: return std::make_unique<ImmediateMessageSender>(communicator);
        case MessageSenderType::Threaded: return std::make_unique<ThreadedMessageSender>(communicator);
        default: return std::make_unique<ImmediateMessageSender>(communicator);
    }
}

std::unique_ptr<MessageProcessor> CommService::makeMessageProcessor(MessageProcessorType type) {
    switch (type) {
        case MessageProcessorType::Immediate: return std::make_unique<ImmediateMessageProcessor>();
        case MessageProcessorType::Threaded: return std::make_unique<ThreadedMessageProcessor>();
        default: return std::make_unique<ImmediateMessageProcessor>();
    }
}

}
