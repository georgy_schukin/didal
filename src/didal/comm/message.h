#pragma once

#include "comm_id.h"
#include "node.h"
#include "serialize/buffer_reader.h"
#include "serialize/buffer_writer.h"
#include "didal/base/growing_buffer.h"

#include <cstddef>
#include <memory>

namespace ddl {

class Message {
public:
    Message();
    explicit Message(const size_t &size);
    Message(const CommId &id, int tag);
    Message(const size_t &size, const CommId &id, int tag);
    Message(const Message &msg);
    Message(Message &&msg);
    ~Message() = default;

    Message& operator=(const Message &msg);
    Message& operator=(Message &&msg);

    void* data();
    const void* data() const;
    size_t size() const;

    void setCommId(const CommId &id);
    void setTag(int tag);

    CommId commId() const;
    int tag() const;

    BufferWriter getWriter();
    BufferReader getReader() const;

    Message clone() const;

private:
    static const size_t HEADER_SIZE = sizeof(CommId) + sizeof(int);

    using BufferPtr = std::shared_ptr<GrowingBuffer>;

private:
    friend class MPICommunicator;

    void* buffer();
    const void* buffer() const;
    size_t bufSize() const;

    void pack();
    void unpack();
    void checkBuffer() const;

    static Message fromBufSize(const size_t &buf_size);

    Message(const CommId &id, int tag, BufferPtr buf);

private:    
    CommId _comm_id;
    int _tag;    
    BufferPtr _buffer;
};

}
