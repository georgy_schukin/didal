#include "node.h"
#include "serialize/serializing_ops.h"

namespace ddl {

Writable& operator<< (Writable &w, const Node &n) {
    return w << n._rank;
}

Readable& operator>> (Readable &r, Node &n) {    
    return r >> n._rank;
}

std::ostream& operator<< (std::ostream &out, const Node &node) {
    out << node.rank();
    return out;
}

template <> int Node::toRank<int>(const int &rank) {
    return rank;
}

template <> int Node::toRank<Node>(const Node &node) {
    return node.rank();
}

}
