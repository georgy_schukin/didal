#pragma once

#include "comm_id.h"
#include "node_group.h"
#include "didal/trace/traceable.h"

#include <string>
#include <map>
#include <memory>
#include <mutex>
#include <type_traits>
#include <thread>
#include <atomic>

namespace ddl {

class CommListener;
class Communicator;
class Message;
class MessageProcessor;
class MessageSender;

class CommService : public Traceable {
public:
    enum class MessageProcessorType : int {
        Immediate = 0,
        Threaded
    };

    enum class MessageSenderType : int {
        Immediate = 0,
        Threaded
    };

public:
    CommService(Communicator *comm,
                MessageSenderType send_type = MessageSenderType::Immediate,
                MessageProcessorType proc_type = MessageProcessorType::Immediate);

    ~CommService() override;

    template <typename T>
    CommId newCommId() {
        return newCommId(typeid(T).hash_code());
    }

    CommId newCommId(const size_t &hash);

    Node thisNode() const;
    NodeGroup allNodes() const;

    void setMessageSender(MessageSenderType type);
    void setMessageProcessor(MessageProcessorType type);

    void subscribe(const CommId &id, CommListener *listener);
    void unsubscribe(const CommId &id);
    void sendMessage(const Message &msg, const Node &node);

    Communicator* getCommunicator() const {
        return communicator;
    }

    void setTracer(Tracer *tracer) override;

private:
    void recvFunc();
    bool isShutdownMessage(const Message &msg) const;
    void processMessage(const Message &msg, const Node &src);

    void addListener(const CommId &id, CommListener *listener);
    void removeListener(const CommId &id);
    CommListener* getListener(const CommId &id) const;

    void addToPending(const Message &msg, const Node &src);
    void processPendingMessages(const CommId &id, CommListener *listener);

    std::unique_ptr<MessageSender> makeMessageSender(MessageSenderType type);
    std::unique_ptr<MessageProcessor> makeMessageProcessor(MessageProcessorType type);    

private:
    Communicator *communicator = nullptr;
    std::map<size_t, size_t> comm_ids;
    std::map<CommId, CommListener*> listeners;
    std::thread recv_thread;
    mutable std::mutex listener_mutex;
    mutable std::mutex pending_mutex;
    std::atomic<int> shutdown_cnt {0};
    CommId own_comm_id;
    std::map<CommId, std::vector<std::pair<Message, Node>>> pending_messages;

    Tracer *tracer = nullptr;

    std::unique_ptr<MessageSender> message_sender;
    std::unique_ptr<MessageProcessor> message_processor;    
};

}
