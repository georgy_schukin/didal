#pragma once

#include "didal/env/environment.h"

#include "didal/base/array.h"
#include "didal/base/array_nd.h"
#include "didal/base/index.h"
#include "didal/base/index_range.h"
#include "didal/base/index_range_nd.h"

#include "didal/data/distributed_array.h"
#include "didal/data/distributed_array_nd.h"
#include "didal/data/distributed_storage.h"
#include "didal/data/sync_distributed_storage.h"

#include "didal/decomp/block_decomposition.h"
#include "didal/decomp/cyclic_decomposition.h"

#include "didal/distr/uniform_distribution.h"

#include "didal/algorithms/foreach.h"
#include "didal/algorithms/reduce.h"

