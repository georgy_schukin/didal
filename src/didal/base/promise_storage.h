#pragma once

#include <functional>
#include <map>
#include <tuple>
#include <atomic>
#include <utility>
#include <future>
#include <type_traits>
#include <memory>
#include <cassert>
#include <iostream>

namespace ddl {

class PromiseStorage {
public:
    PromiseStorage() {}

    PromiseStorage(const PromiseStorage&) = delete;
    PromiseStorage(PromiseStorage&&) = delete;
    PromiseStorage& operator=(const PromiseStorage&) = delete;
    PromiseStorage& operator=(PromiseStorage&&) = delete;

    ~PromiseStorage() {}

    template <typename ReturnType>
    std::pair<std::promise<ReturnType>*, size_t> addPromise() {
        const auto promise_id = last_promise_id++;
        auto promise_ptr = std::make_shared<std::promise<ReturnType>>();
        {
            std::lock_guard<std::mutex> lock(promise_mutex);
            promises[promise_id] = promise_ptr;
        }
        return std::make_pair(promise_ptr.get(), promise_id);
    }

    template <typename ReturnType>
    std::promise<ReturnType>* getPromise(size_t promise_id) const {
        std::lock_guard<std::mutex> lock(promise_mutex);
        auto it = promises.find(promise_id);
        if (it == promises.end()) {
            return nullptr;
        }
        auto promise_ptr = std::static_pointer_cast<std::promise<ReturnType>>(it->second);
        return (promise_ptr ? promise_ptr.get() : nullptr);
    }

    template <typename ReturnType>
    std::shared_ptr<std::promise<ReturnType>> ejectPromise(size_t promise_id) {
        std::lock_guard<std::mutex> lock(promise_mutex);
        auto it = promises.find(promise_id);
        if (it == promises.end()) {
            return nullptr;
        }
        auto promise_ptr = std::static_pointer_cast<std::promise<ReturnType>>(it->second);
        if (promise_ptr) {
            promises.erase(it);
            promise_cond.notify_all();
        }
        return promise_ptr;
    }

    void removePromise(size_t promise_id) {
        std::lock_guard<std::mutex> lock(promise_mutex);
        promises.erase(promise_id);
        promise_cond.notify_all();
    }

    void waitForPromisesEmpty() {
        std::unique_lock<std::mutex> lock(promise_mutex);
        while (!promises.empty()) {
            promise_cond.wait(lock, [this](){ return promises.empty(); });
        }
    }

private:
    std::atomic<size_t> last_promise_id {0};
    std::map<size_t, std::shared_ptr<void>> promises;
    mutable std::mutex promise_mutex;
    mutable std::condition_variable promise_cond;
};

}
