#pragma once

#include "growing_buffer.h"

#include <vector>
#include <cstddef>

namespace ddl {

/**
 * Buffer of data.
 */
class BasicBuffer : public GrowingBuffer {
public:
    BasicBuffer();
    BasicBuffer(const size_t &size);
    BasicBuffer(const void *data, const size_t &size);

    void* data() override;
    const void* data() const override;
    size_t size() const override;

    void resize(size_t sz) override;

    bool isEmpty() const;

public:
    using ElementType = unsigned char ;

private:
    std::vector<ElementType> _data;
};

}
