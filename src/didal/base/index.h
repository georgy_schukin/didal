#pragma once

#include "didal/comm/serialize/serializing_ops.h"

#include <array>
#include <initializer_list>

namespace ddl {

template <size_t Dims, typename BaseType = int>
class Index {
public:
    Index() = default;

    template <typename U>
    Index(std::initializer_list<U> idxs) {
        int pos = 0;
        for (auto index: idxs) {
            indices[pos++] = static_cast<BaseType>(index);
        }
    }

    template <typename U>
    Index(const std::array<U, Dims> &idxs) {
        for (size_t dim = 0; dim < Dims; dim++) {
            indices[dim] = static_cast<BaseType>(idxs[dim]);
        }
    }

    Index(const std::array<BaseType, Dims> &idxs) :
        indices(idxs) {
    }

    template <typename U>
    Index(const Index<Dims, U> &ind) {
        for (size_t dim = 0; dim < Dims; dim++) {
            indices[dim] = static_cast<BaseType>(ind[dim]);
        }
    }

    template <typename U>
    Index(Index<Dims, U> &&ind) {
        for (size_t dim = 0; dim < Dims; dim++) {
            indices[dim] = static_cast<BaseType>(ind[dim]);
        }
    }

    Index(const Index &) = default;
    Index(Index &&) = default;

    template <typename U>
    Index& operator=(const Index<Dims, U> &ind) {
        for (size_t dim = 0; dim < Dims; dim++) {
            indices[dim] = static_cast<BaseType>(ind[dim]);
        }
        return *this;
    }

    template <typename U>
    Index& operator=(Index<Dims, U> &&ind) {
        for (size_t dim = 0; dim < Dims; dim++) {
            indices[dim] = static_cast<BaseType>(ind[dim]);
        }
        return *this;
    }

    Index& operator=(const Index &) = default;
    Index& operator=(Index &&) = default;

    BaseType get(size_t index) const {
        return indices[index];
    }

    BaseType& get(size_t index) {
        return indices[index];
    }

    BaseType operator[](size_t index) const {
        return get(index);
    }

    BaseType& operator[](size_t index) {
        return get(index);
    }

    bool operator<(const Index &i) const {
        return indices < i.indices;
    }

    bool operator==(const Index &i) const {
        return indices == i.indices;
    }

    bool operator!=(const Index &i) const {
        return indices != i.indices;
    }

    friend Writable& operator<< (Writable &w, const Index &index) {
        return w << index.indices;
    }

    friend Readable& operator>> (Readable &r, Index &index) {
        return r >> index.indices;
    }

private:
    typename std::array<BaseType, Dims> indices;
};

}

// Hash implementation for index
namespace std {

template <size_t Dims, typename BaseType>
struct hash<ddl::Index<Dims, BaseType>> {
    size_t operator()(const ddl::Index<Dims, BaseType> &index) const {
        std::hash<size_t> hasher;
        size_t result = 0;
        for (size_t i = 0; i < Dims; ++i) {
            result = result * 31 + hasher(index[i]);
        }
        return result;
    }
};

}


