#pragma once

#include "array_nd.h"

#include <type_traits>

namespace ddl {

template <typename ArrayType, typename IndexType>
auto get_slice_1d(const ArrayType &array,
                    size_t dim,
                    size_t size,
                    const IndexType &start) {
    typename ArrayType::template SubArrayType<1> slice {size};
    auto curr = start;
    for (size_t i = 0; i < size; i++) {
        curr[dim] = start[dim] + i;
        slice(i) = array[curr];
    }
    return slice;
}

template <typename ArrayType, typename DimsType, typename SizeType, typename IndexType>
auto get_slice_2d(const ArrayType &array,
                    const DimsType &dims,
                    const SizeType &size,
                    const IndexType &start) {
    typename ArrayType::template SubArrayType<2> slice {size[0], size[1]};
    auto curr = start;
    for (size_t i = 0; i < size[0]; i++)
    for (size_t j = 0; j < size[1]; j++) {
        curr[dims[0]] = start[dims[0]] + i;
        curr[dims[1]] = start[dims[1]] + j;
        slice(i, j) = array[curr];
    }
    return slice;
}

template <typename ArrayType, typename DimsType, typename SizeType, typename IndexType>
auto get_slice_3d(const ArrayType &array,
                    const DimsType &dims,
                    const SizeType &size,
                    const IndexType &start) {
    typename ArrayType::template SubArrayType<3> slice {size[0], size[1], size[2]};
    auto curr = start;
    for (size_t i = 0; i < size[0]; i++)
    for (size_t j = 0; j < size[1]; j++)
    for (size_t k = 0; k < size[2]; k++) {
        curr[dims[0]] = start[dims[0]] + i;
        curr[dims[1]] = start[dims[1]] + j;
        curr[dims[2]] = start[dims[2]] + k;
        slice(i, j, k) = array[curr];
    }
    return slice;
}

template <typename ArrayType, typename SizeType, typename IndexType>
auto get_slice_3d(const ArrayType &array,
                    const SizeType &size,
                    const IndexType &start) {
    typename ArrayType::template SubArrayType<3> slice {size[0], size[1], size[2]};
    for (size_t i = 0; i < size[0]; i++)
    for (size_t j = 0; j < size[1]; j++)
    for (size_t k = 0; k < size[2]; k++) {
        slice(i, j, k) =  array(start[0] + i, start[1] + j, start[2] + k);
    }
    return slice;
}

template <typename ResultType, typename ArrayType, typename SizeType, typename IndexType, typename Func>
auto get_slice_func_3d(const ArrayType &array,
                    const SizeType &size,
                    const IndexType &start, Func func) {
    ArrayND<ResultType, 3> slice {size[0], size[1], size[2]};
    for (size_t i = 0; i < size[0]; i++)
    for (size_t j = 0; j < size[1]; j++)
    for (size_t k = 0; k < size[2]; k++) {
        slice(i, j, k) = func(array(start[0] + i, start[1] + j, start[2] + k));
    }
    return slice;
}

template <typename ArrayType, typename SliceType, typename IndexType>
void set_slice_1d(ArrayType &array,
                    const SliceType &slice,
                    size_t dim,
                    const IndexType &start) {
    auto curr = start;
    for (size_t i = 0; i < slice.size(0); i++) {
        curr[dim] = start[dim] + i;
        array[curr] = slice(i);
    }
}

template <typename ArrayType, typename SliceType, typename DimsType, typename IndexType>
void set_slice_2d(ArrayType &array,
                    const SliceType &slice,
                    const DimsType &dims,
                    const IndexType &start) {
    auto curr = start;
    for (size_t i = 0; i < slice.size(0); i++)
    for (size_t j = 0; j < slice.size(1); j++) {
        curr[dims[0]] = start[dims[0]] + i;
        curr[dims[1]] = start[dims[1]] + j;
        array[curr] = slice(i, j);
    }
}

template <typename ArrayType, typename SliceType, typename DimsType, typename IndexType>
void set_slice_3d(ArrayType &array,
                    const SliceType &slice,
                    const DimsType &dims,
                    const IndexType &start) {
    auto curr = start;
    for (size_t i = 0; i < slice.size(0); i++)
    for (size_t j = 0; j < slice.size(1); j++)
    for (size_t k = 0; k < slice.size(2); k++) {
        curr[dims[0]] = start[dims[0]] + i;
        curr[dims[1]] = start[dims[1]] + j;
        curr[dims[2]] = start[dims[2]] + k;
        array[curr] = slice(i, j, k);
    }
}

template <typename ArrayType, typename SliceType, typename IndexType>
void set_slice_3d(ArrayType &array,
                    const SliceType &slice,
                    const IndexType &start) {
    for (size_t i = 0; i < slice.size(0); i++)
    for (size_t j = 0; j < slice.size(1); j++)
    for (size_t k = 0; k < slice.size(2); k++) {
        array(start[0] + i, start[1] + j, start[2] + k) = slice(i, j, k);
    }
}

template <typename ArrayType, typename SliceType, typename IndexType, typename Func>
void slice_func_1d(ArrayType &array,
                    const SliceType &slice,
                    size_t dim,
                    const IndexType &start,
                    Func func) {
    auto curr = start;
    for (size_t i = 0; i < slice.size(0); i++) {
        curr[dim] = start[dim] + i;
        func(array[curr], slice(i));
    }
}

template <typename ArrayType, typename SliceType, typename DimsType, typename IndexType, typename Func>
void slice_func_2d(ArrayType &array,
                    const SliceType &slice,
                    const DimsType &dims,
                    const IndexType &start,
                    Func func) {
    auto curr = start;
    for (size_t i = 0; i < slice.size(0); i++)
    for (size_t j = 0; j < slice.size(1); j++) {
        curr[dims[0]] = start[dims[0]] + i;
        curr[dims[1]] = start[dims[1]] + j;
        func(array[curr], slice(i, j));
    }
}

template <typename ArrayType, typename SliceType, typename DimsType, typename IndexType, typename Func>
void slice_func_3d(ArrayType &array,
                    const SliceType &slice,
                    const DimsType &dims,
                    const IndexType &start,
                    Func func) {
    auto curr = start;
    for (size_t i = 0; i < slice.size(0); i++)
    for (size_t j = 0; j < slice.size(1); j++)
    for (size_t k = 0; k < slice.size(2); k++) {
        curr[dims[0]] = start[dims[0]] + i;
        curr[dims[1]] = start[dims[1]] + j;
        curr[dims[2]] = start[dims[2]] + k;
        func(array[curr], slice(i, j, k));
    }
}

template <typename ArrayType, typename SliceType, typename IndexType, typename Func>
void slice_func_3d(ArrayType &array,
                    const SliceType &slice,
                    const IndexType &start,
                    Func func) {
    for (size_t i = 0; i < slice.size(0); i++)
    for (size_t j = 0; j < slice.size(1); j++)
    for (size_t k = 0; k < slice.size(2); k++) {
        func(array(start[0] + i, start[1] + j, start[2] + k), slice(i, j, k));
    }
}

}
