#include "shared_buffer.h"

#include <cstring>
#include <cassert>

namespace ddl {

SharedBuffer::SharedBuffer() {
}

SharedBuffer::SharedBuffer(const size_t &sz) {
    resize(sz);
}

SharedBuffer::SharedBuffer(const void *dt, const size_t& sz) {
    resize(sz);
    memcpy(data(), dt, sz);
}

SharedBuffer::SharedBuffer(const SharedBuffer &buf) :
    _data(buf._data), shared(true)
{
}

SharedBuffer::SharedBuffer(SharedBuffer &&buf) :
    _data(buf._data)
{
    buf._data.reset();
    buf.shared = false;
}

SharedBuffer& SharedBuffer::operator=(const SharedBuffer &buf) {
    _data = buf._data;
    shared = true;
    return *this;
}

SharedBuffer& SharedBuffer::operator=(SharedBuffer &&buf) {
    _data = buf._data;
    buf._data.reset();
    shared = false;
    buf.shared = false;
    return *this;
}

void* SharedBuffer::data() {
    if (isEmpty()) {
        return nullptr;
    }
    if (shared) {
        _data = std::make_shared<ContainerType>(*_data.get());
        shared = false;
    }
    return &(_data->at(0));
}

const void* SharedBuffer::data() const {
    if (isEmpty()) {
        return nullptr;
    }
    return &(_data->at(0));
}

size_t SharedBuffer::size() const {
    if (!_data) {
        return 0;
    }
    return _data->size();
}

bool SharedBuffer::isEmpty() const {
    return !_data || _data->empty();
}

void SharedBuffer::resize(size_t sz) {
    if (!_data) {
        _data = std::make_shared<ContainerType>();
    }
    _data->resize(sz);
}

}
