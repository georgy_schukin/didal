#pragma once

#include "didal/base/index.h"
#include "didal/base/index_range_nd.h"
#include "didal/comm/serialize/serializable.h"
#include "didal/comm/serialize/serializing_ops.h"

#include <memory>
#include <cstddef>
#include <vector>
#include <cassert>

namespace ddl {

namespace aux {
    template <size_t Dims, size_t N, typename U1, typename U2>
    size_t getLinearIndex(const IndexRangeND<Dims, U1> &range, const Index<Dims, U2> &index) {
        return N > 0 ? index[N - 1]  + range.template size<N - 1>() * getLinearIndex<Dims, N - 1>(range, index) : 0;
    }

    template <size_t Dims, typename U1, typename U2, typename std::enable_if_t<(Dims > 4), int> = 0>
    size_t linearIndex(const IndexRangeND<Dims, U1> &range, const Index<Dims, U2> &index) {
        return getLinearIndex<Dims, Dims>(range, index);
    }

    // Linear index specializations for special cases.
    template <size_t Dims, typename U1, typename U2, typename std::enable_if_t<(Dims == 0), int> = 0>
    size_t linearIndex(const IndexRangeND<Dims, U1>&, const Index<Dims, U2>&) {
        return 0;
    }

    template <size_t Dims, typename U1, typename U2, typename std::enable_if_t<(Dims == 1), int> = 0>
    size_t linearIndex(const IndexRangeND<Dims, U1>&, const Index<Dims, U2> &index) {
        return index[0];
    }

    template <size_t Dims, typename U1, typename U2, typename std::enable_if_t<(Dims == 2), int> = 0>
    size_t linearIndex(const IndexRangeND<Dims, U1> &range, const Index<Dims, U2> &index) {
        return index[0] * range.size(1) +
               index[1];
    }

    template <size_t Dims, typename U1, typename U2, typename std::enable_if_t<(Dims == 3), int> = 0>
    size_t linearIndex(const IndexRangeND<Dims, U1> &range, const Index<Dims, U2> &index) {
        return index[0] * range.size(1) * range.size(2) +
               index[1] * range.size(2) +
               index[2];
    }

    template <size_t Dims, typename U1, typename U2, typename std::enable_if_t<(Dims == 4), int> = 0>
    size_t linearIndex(const IndexRangeND<Dims, U1> &range, const Index<Dims, U2> &index) {
        return index[0] * range.size(1) * range.size(2) * range.size(3) +
               index[1] * range.size(2) * range.size(3) +
               index[2] * range.size(3) +
               index[4];
    }

    template<size_t Dims, size_t N, typename U, typename std::enable_if_t<(N >= Dims), int> = 0>
    size_t shiftSize(const IndexRangeND<Dims, U>&) {
        return 1;
    }

    template<size_t Dims, size_t N, typename U, typename std::enable_if_t<(N < Dims), int> = 0>
    size_t shiftSize(const IndexRangeND<Dims, U> &range) {
        return N < Dims ? range.template size<N>() * shiftSize<Dims, N + 1>(range) : 1;
    }

    template <size_t Dims, size_t N, typename U>
    size_t getLinearIndexI(const IndexRangeND<Dims, U>&) {
        return 0;
    }

    template <size_t Dims, size_t N, typename U, typename Index>
    size_t getLinearIndexI(const IndexRangeND<Dims, U>&, Index index) {
        return index;
    }

    template <size_t Dims, size_t N, typename U, typename Index, typename... Indices>
    size_t getLinearIndexI(const IndexRangeND<Dims, U> &range, Index index, Indices... rest) {
        return index * shiftSize<Dims, N, U>(range) + getLinearIndexI<Dims, N + 1>(range, rest...);
    }

    template <size_t Dims, typename U, typename Index, typename std::enable_if_t<(Dims > 4), int>, typename... Indices>
    size_t linearIndexI(const IndexRangeND<Dims, U> &range, Index index, Indices... rest) {
        return getLinearIndexI<Dims, 1>(range, index, rest...);
    }

    template <size_t Dims, typename U, typename Index, typename std::enable_if_t<(Dims == 1), int> = 0>
    size_t linearIndexI(const IndexRangeND<Dims, U> &range, Index index) {
        return index;
    }

    template <size_t Dims, typename U, typename Index, typename std::enable_if_t<(Dims == 2), int> = 0>
    size_t linearIndexI(const IndexRangeND<Dims, U> &range, Index i1, Index i2) {
       return i1 * range.size(1) +
              i2;
    }

    template <size_t Dims, typename U, typename Index, typename std::enable_if_t<(Dims == 3), int> = 0>
    size_t linearIndexI(const IndexRangeND<Dims, U> &range, Index i1, Index i2, Index i3) {
       return i1 * range.size(1) * range.size(2) +
              i2 * range.size(2) +
              i3;
    }

    template <size_t Dims, typename U, typename Index, typename std::enable_if_t<(Dims == 4), int> = 0>
    size_t linearIndexI(const IndexRangeND<Dims, U> &range, Index i1, Index i2, Index i3, Index i4) {
       return i1 * range.size(1) * range.size(2) * range.size(3) +
              i2 * range.size(2) * range.size(3) +
              i3 * range.size(3) +
              i4;
    }
}

/**
* @brief Multi dimensional array of elements of type T.
*/
template <typename T, size_t Dims, typename _RangeBaseType = int>
class ArrayND {
public:
    using ValueType = T;
    using RangeBaseType = _RangeBaseType;
    using RangeType = IndexRangeND<Dims, RangeBaseType>;
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;

    template <size_t SubDims>
    using SubArrayType = ArrayND<ValueType, SubDims, RangeBaseType>;

public:
    ArrayND() {}

    template <typename U>
    ArrayND(const IndexRangeND<Dims, U> &range) :
        _range(range), _data(range.totalSize()) {
    }

    template <typename U>
    ArrayND(const IndexRangeND<Dims, U> &range, const T &init_value) :
        _range(range), _data(range.totalSize(), init_value) {
    }

    ArrayND(const std::initializer_list<size_t> &sizes) :
        ArrayND(RangeType (sizes)) {
    }

    ArrayND(const ArrayND&) = default;
    ArrayND(ArrayND&&) = default;

    ArrayND& operator=(const ArrayND&) = default;
    ArrayND& operator=(ArrayND&&) = default;

    bool empty() const {
        return _data.empty();
    }

    size_t size() const {
        return _data.size();
    }

    size_t size(size_t dim_num) const {
        return _range.size(dim_num);
    }

    template <typename U>
    const T& operator[](const Index<Dims, U> &index) const {
        const auto l_ind = toLinearIndex(index);
        assert (l_ind >= 0 && l_ind < _data.size());
        return _data[l_ind];
    }

    template <typename U>
    T& operator[](const Index<Dims, U> &index) {
        const auto l_ind = toLinearIndex(index);
        assert (l_ind >= 0 && l_ind < _data.size());
        return _data[l_ind];
    }

    const T& operator[](size_t index) const {
        return _data[index];
    }

    T& operator[](size_t index) {
        return _data[index];
    }

    template <typename Index, typename... Indices>
    const T& operator()(Index index, Indices... rest) const {
        const auto l_ind = toLinearIndex<Index, Indices...>(index, rest...);
        assert (l_ind >= 0 && l_ind < _data.size());
        return _data[l_ind];
    }

    template <typename Index, typename... Indices>
    T& operator()(Index index, Indices... rest) {
        const auto l_ind = toLinearIndex<Index, Indices...>(index, rest...);
        assert (l_ind >= 0 && l_ind < _data.size());
        return _data[l_ind];
    }

    iterator begin() {
        return _data.begin();
    }

    iterator end() {
        return _data.end();
    }

    const_iterator begin() const {
        return _data.begin();
    }

    const_iterator end() const {
        return _data.end();
    }

    const RangeType& range() const {
        return _range;
    }

    template <typename U>
    Index<Dims, RangeBaseType> toGlobalIndex(const Index<Dims, U> &index) const {
        return _range.index(index);
    }

    template <typename U>
    size_t toLinearIndex(const Index<Dims, U> &index) const {
        return ddl::aux::linearIndex<Dims>(_range, index);
    }

    template <typename Index, typename... Indices>
    size_t toLinearIndex(Index index, Indices... rest) const {
        return ddl::aux::linearIndexI<Dims>(_range, index, rest...);
    }

    friend Writable& operator<< (Writable &w, const ArrayND &a) {
        return w << a._range << a._data;
    }

    friend Readable& operator>> (Readable &r, ArrayND &a) {
        return r >> a._range >> a._data;
    }

    std::vector<T>& data() {
        return _data;
    }

    const std::vector<T>& data() const {
        return _data;
    }

    constexpr size_t numOfDims() const {
        return Dims;
    }

    constexpr static size_t nDims() {
        return Dims;
    }

private:
    RangeType _range;
    typename std::vector<T> _data;
};

}
