#include "basic_buffer.h"

#include <cstring>
#include <cassert>

namespace ddl {

BasicBuffer::BasicBuffer() {
}

BasicBuffer::BasicBuffer(const size_t &sz) {
    resize(sz);
}

BasicBuffer::BasicBuffer(const void *dt, const size_t& sz) {
    resize(sz);
    memcpy(data(), dt, sz);
}

void* BasicBuffer::data() {
    return isEmpty() ? nullptr : (void*)(&_data[0]);
}

const void* BasicBuffer::data() const {
    return isEmpty() ? nullptr : (const void*)(&_data[0]);
}

size_t BasicBuffer::size() const {
    return _data.size();
}

bool BasicBuffer::isEmpty() const {
    return _data.empty();
}

void BasicBuffer::resize(size_t sz) {
    _data.resize(sz);
}

}
