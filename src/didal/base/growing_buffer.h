#pragma once

#include "buffer.h"

namespace ddl {

class GrowingBuffer: public Buffer {
public:
    virtual ~GrowingBuffer() = default;

    virtual void resize(size_t size) = 0;
};

}
