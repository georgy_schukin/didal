#pragma once

#include "index.h"
#include "index_range.h"
#include "didal/comm/serialize/serializing_ops.h"

#include <cstddef>
#include <vector>
#include <array>
#include <algorithm>
#include <cassert>

namespace ddl {

template <size_t Dims, typename BaseType = int>
class IndexRangeND {
public:
    IndexRangeND() {
    }

    IndexRangeND(const std::array<BaseType, Dims> &start,
                 const std::array<size_t, Dims> &size,
                 const std::array<BaseType, Dims> &step)
    {
        for (size_t i = 0; i < Dims; i++) {
            _ranges[i] = IndexRange<BaseType>(start[i], size[i], step[i]);
        }
    }

    IndexRangeND(const std::array<size_t, Dims> &sizes) {
        for (size_t i = 0; i < Dims; i++) {
            _ranges[i] = IndexRange<BaseType>(sizes[i]);
        }
    }

    IndexRangeND(const std::array<IndexRange<BaseType>, Dims> &ranges) :
        _ranges(ranges) {
    }

    IndexRangeND(const std::initializer_list<IndexRange<BaseType>> &ranges) {
        assert (ranges.size() == Dims);
        size_t i = 0;
        for (const auto &range: ranges) {
            _ranges[i++] = range;
        }
    }

    IndexRangeND(const std::initializer_list<size_t> &sizes) {
        assert (sizes.size() == Dims);
        size_t i = 0;
        for (auto size: sizes) {
            _ranges[i++] = IndexRange<BaseType>(size);
        }
    }

    template <typename U>
    IndexRangeND(const IndexRangeND<Dims, U> &range) {
        for (size_t i = 0; i < Dims; i++) {
            _ranges[i] = range.range(i);
        }
    }

    template <typename U>
    IndexRangeND(IndexRangeND<Dims, U> &&range) {
        for (size_t i = 0; i < Dims; i++) {
            _ranges[i] = std::move(range.range(i));
        }
    }

    IndexRangeND(const IndexRangeND&) = default;
    IndexRangeND(IndexRangeND&&) = default;

    template <typename U>
    IndexRangeND& operator=(const IndexRangeND<Dims, U> &range) {
        for (size_t i = 0; i < Dims; i++) {
            _ranges[i] = range.range(i);
        }
        return *this;
    }

    template <typename U>
    IndexRangeND& operator=(IndexRangeND<Dims, U> &&range) {
        for (size_t i = 0; i < Dims; i++) {
            _ranges[i] = std::move(range.range(i));
        }
        return *this;
    }

    IndexRangeND& operator=(const IndexRangeND&) = default;
    IndexRangeND& operator=(IndexRangeND&&) = default;

    const IndexRange<BaseType>& range(size_t dim) const {
        return _ranges[dim];
    }

    BaseType start(size_t dim) const {
        return _ranges[dim].start();
    }

    template <size_t Dim>
    BaseType start() const {
        return _ranges[Dim].start();
    }

    BaseType end(size_t dim) const {
        return _ranges[dim].end();
    }

    template <size_t Dim>
    BaseType end() const {
        return _ranges[Dim].end();
    }


    size_t size(size_t dim) const {
        return _ranges[dim].size();
    }

    template <size_t Dim>
    size_t size() const {
        return _ranges[Dim].size();
    }

    BaseType step(size_t dim) const {
        return _ranges[dim].step();
    }

    template <size_t Dim>
    BaseType step() const {
        return _ranges[Dim].step();
    }

    size_t totalSize() const {
        size_t total_size = 1;
        for (const auto &range: _ranges) {
            total_size *= range.size();
        }
        return total_size;
    }

    Index<Dims, BaseType> index(const Index<Dims, size_t> &nums) const {
        Index<Dims, BaseType> result;
        for (size_t i = 0; i < Dims; i++) {
            result[i] = _ranges[i].index(nums[i]);
        }
        return result;
    }

    friend Writable& operator<< (Writable &w, const IndexRangeND &range) {
        return w << range._ranges;
    }

    friend Readable& operator>> (Readable &r, IndexRangeND &range) {
        return r >> range._ranges;
    }

private:
    std::array<IndexRange<BaseType>, Dims> _ranges;
};

}
