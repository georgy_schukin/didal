#pragma once

#include <cstddef>

namespace ddl {

class Buffer {
public:
    virtual ~Buffer() = default;

    virtual void* data() = 0;
    virtual const void* data() const = 0;
    virtual size_t size() const = 0;
};

}
