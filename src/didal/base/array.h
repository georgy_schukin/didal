#pragma once

#include "didal/base/index.h"
#include "didal/base/index_range.h"
#include "didal/comm/serialize/serializable.h"
#include "didal/comm/serialize/serializing_ops.h"

#include <memory>
#include <cstddef>
#include <vector>

namespace ddl {

/**
 * @brief One dimensional array of elements of type T.
 */
template <typename T, typename _RangeBaseType = int>
class Array {
public:
    using ValueType = T;
    using RangeBaseType = _RangeBaseType;
    using RangeType = IndexRange<RangeBaseType>;
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;

    template <size_t SubDims>
    using SubArrayType = Array<ValueType, RangeBaseType>;

public:
    Array() = default;

    Array(size_t size) :
        _data(size), _range(size) {
    }

    Array(size_t size, const T &init_value) :
        _data(size, init_value), _range(size) {
    }

    Array(const RangeType &range) :
        _data(range.size()), _range(range) {
    }

    Array(const RangeType &range, const T &init_value) :
        _data(range.size(), init_value), _range(range) {
    }

    size_t size() const {
        return _data.size();
    }

    const ValueType& operator[](size_t index) const {
        return _data[index];
    }

    ValueType& operator[](size_t index) {
        return _data[index];
    }

    template <typename U>
    const ValueType& operator[](const Index<1, U> &index) const {
        return _data[static_cast<size_t>(index[0])];
    }

    template <typename U>
    ValueType& operator[](const Index<1, U> &index) {
        return _data[static_cast<size_t>(index[0])];
    }

    template <typename Index>
    const ValueType& operator()(const Index &index) const {
        return _data[static_cast<size_t>(index)];
    }

    template <typename Index>
    ValueType& operator()(const Index &index) {
        return _data[static_cast<size_t>(index)];
    }

    iterator begin() {
        return _data.begin();
    }

    iterator end() {
        return _data.end();
    }

    const_iterator begin() const {
        return _data.begin();
    }

    const_iterator end() const {
        return _data.end();
    }

    const RangeType& range() const {
        return _range;
    }

    size_t toGlobalIndex(size_t index) const {
        return range().index(index);
    }

    friend Writable& operator<< (Writable &w, const Array &a) {
        return w << a._range << a._data;
    }

    friend Readable& operator>> (Readable &r, Array &a) {
        return r >> a._range >> a._data;
    }

    constexpr size_t numOfDims() const {
        return 1;
    }

    constexpr static size_t nDims() {
        return 1;
    }

private:
    std::vector<ValueType> _data;
    RangeType _range;
};

}
