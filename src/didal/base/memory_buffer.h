#pragma once

#include "buffer.h"

namespace ddl {

/**
 * @brief Represents allocated memory as a buffer.
 */
class MemoryBuffer : public Buffer {
public:
    MemoryBuffer(void *memory, size_t size) :
        memory(memory), memory_size(size) {
    }

    void* data() override {
        return memory;
    }

    const void* data() const override {
        return memory;
    }

    size_t size() const override {
        return memory_size;
    }

private:
    void *memory;
    size_t memory_size;
};

}
