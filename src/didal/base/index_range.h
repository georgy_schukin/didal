#pragma once

#include "didal/comm/serialize/serializable.h"

#include <cstddef>
#include <vector>

namespace ddl {

template <typename BaseType = int>
class IndexRange {
public:
    IndexRange() :
        _start(0), _size(0), _step(0) {
    }

    template <typename U>
    IndexRange(U start, size_t size, U step = 1) :
        _start(start), _size(size), _step(step) {
    }

    explicit IndexRange(size_t size) :
        IndexRange(0, size, 1) {
    }

    template <typename U>
    IndexRange(const IndexRange<U> &range) :
        _start(static_cast<BaseType>(range._start)),
        _size(range._size),
        _step(static_cast<BaseType>(range._step)) {
    }

    template <typename U>
    IndexRange(IndexRange<U> &&range) :
        _start(static_cast<BaseType>(range._start)),
        _size(range._size),
        _step(static_cast<BaseType>(range._step)) {
    }

    IndexRange(const IndexRange&) = default;
    IndexRange(IndexRange&&) = default;

    template <typename U>
    IndexRange& operator=(const IndexRange<U> &range) {
        _start = static_cast<BaseType>(range.start());
        _size = range.size();
        _step = static_cast<BaseType>(range.step());
        return *this;
    }

    template <typename U>
    IndexRange& operator=(IndexRange<U> &&range) {
        _start = static_cast<BaseType>(range.start());
        _size = range.size();
        _step = static_cast<BaseType>(range.step());
        return *this;
    }

    IndexRange& operator=(const IndexRange&) = default;
    IndexRange& operator=(IndexRange&&) = default;

    std::vector<BaseType> allIndices() const {
        std::vector<BaseType> result;
        for (size_t i = 0; i < _size; i++) {
            result.push_back(_start + i * _step);
        }
        return result;
    }

    BaseType index(size_t num) const {
        /*if (num >= _size) {
            throw std::out_of_range("Range index is out of range: " + std::to_string(num));
        }*/
        return _start + num * _step;
    }

    /// Start of the range.
    BaseType start() const {
        return _start;
    }

    /// End of the range - one index past the last.
    BaseType end() const {
        return _start + _size * _step;
    }

    /// Size of the range - number of indices in it.
    size_t size() const {
        return _size;
    }

    /// Step between indices in the range.
    BaseType step() const {
        return _step;
    }

    friend Writable& operator<< (Writable &w, const IndexRange &range) {
        return w << range._start << range._size << range._step;
    }

    friend Readable& operator>> (Readable &r, IndexRange &range) {
        return r >> range._start >> range._size >> range._step;
    }

private:
    BaseType _start;
    size_t _size;
    BaseType _step;
};

}
