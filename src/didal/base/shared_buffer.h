#pragma once

#include "growing_buffer.h"

#include <vector>
#include <cstddef>
#include <memory>

namespace ddl {

/**
 * Buffer of data with shared-on-copy optimization.
 */
class SharedBuffer : public GrowingBuffer {
public:
    SharedBuffer();
    SharedBuffer(const size_t &size);
    SharedBuffer(const void *data, const size_t &size);
    SharedBuffer(const SharedBuffer &buf);
    SharedBuffer(SharedBuffer &&buf);

    SharedBuffer& operator=(const SharedBuffer &buf);
    SharedBuffer& operator=(SharedBuffer &&buf);

    void* data() override;
    const void* data() const override;
    size_t size() const override;

    void resize(size_t sz) override;

    bool isEmpty() const;

public:
    using ElementType = unsigned char;
    using ContainerType = std::vector<ElementType>;

private:
    std::shared_ptr<ContainerType> _data;
    bool shared = false;
};

}
