#pragma once

#include <exception>
#include <string>

namespace ddl {

class BasicException : public std::exception {
public:
    BasicException() {}
    BasicException(const char *msg) :
        message(msg) {}
    BasicException(const std::string &msg) :
        message(msg) {}

    const char * what() const noexcept override {
        return message.data();
    }

private:
    std::string message;
};

}
