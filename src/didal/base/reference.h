#pragma once

#include <memory>

namespace ddl {

template <typename T>
class Reference {
public:
    Reference() {}

    Reference makeLocal(T *ptr) {
        return Reference(ptr, nullptr);
    }

    Reference makeShared(std::shared_ptr<T> shared_ptr) {
        return Reference(nullptr, shared_ptr);
    }

    bool isLocal() const {
        return (pointer != nullptr);
    }

    bool isShared() const {
        return (shared_pointer != nullptr);
    }

    bool isNull() const {
        return !(pointer || shared_pointer);
    }

    T& get() {
        return (pointer ? *pointer : *(shared_pointer.get()));
    }

    const T& get() const {
        return (pointer ? *pointer : *(shared_pointer.get()));
    }

private:
    Reference(T *ptr, std::shared_ptr<T> shared_ptr) :
        pointer(ptr), shared_pointer(shared_ptr) {
    }

private:
    T *pointer;
    std::shared_ptr<T> shared_pointer;
};

}
