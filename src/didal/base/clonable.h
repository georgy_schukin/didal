#pragma once

namespace ddl {

template <class T>
class Clonable {
public:
    virtual ~Clonable() = default;
    virtual T* clone() const = 0;
};

}
