#pragma once

#include "didal/comm/node.h"
#include "didal/comm/remote.h"
#include "didal/distr/distributor.h"
#include "didal/distr/rope/segment.h"

#include <utility>

namespace ddl {

template <typename IdType, typename Mapper, typename ValueType = int>
class RopeDistributor : public Distributor<IdType> {
public:
    RopeDistributor(CommService *cs, Mapper mapper) :
        mapper(mapper), remote(cs, this) {
        RemoteMethodMaker<RopeDistributor> maker(&remote, this);
        remoteNodeFor = maker.method(&RopeDistributor::getNodeFor).template func<int, IdType>();
        remote.startListening();
    }

    virtual ~RopeDistributor() override = default;

    int getNodeFor(const IdType &id) const override {
        const auto value = mapper(id);
        if (segment.isIn(value)) {
            return this_node;
        } else if (segment.isLess(value)) {
            return getNodeForFrom(id, left_node);
        } else {
            return getNodeForFrom(id, right_node);
        }
    }

private:
    int getNodeForFrom(const IdType &id, const Node &node) {
        if (node.rank() == this_node) {
            return getNodeFor(id);
        } else {
            return remoteNodeFor(node, id).get();
        }
    }

private:
    Segment<ValueType> segment;
    Mapper mapper;
    int this_node;
    int left_node;
    int right_node;
    Remote remote;
    RemoteFunction<int, IdType> remoteNodeFor;
};

}
