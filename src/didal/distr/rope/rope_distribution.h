#pragma once

#include "didal/distr/rope/segment.h"
#include "didal/util/slicer.h"
#include <map>
#include <vector>

namespace ddl {

template <typename ValueType = int>
class RopeDistribution {
public:
    RopeDistribution(ValueType max_value, int num_of_segments):
        RopeDistribution(Slicer::getShifts(max_value, num_of_segments)) {
    }

    RopeDistribution(const std::vector<Segment<ValueType>> &segments):
        segments(segments) {
    }

    RopeDistribution(const std::vector<ValueType> &shifts) {
        for (size_t i = 0; i < shifts.size() - 1; i++) {
            segments.push_back({shifts[i], shifts[i + 1]});
        }
    }

    const Segment<ValueType>& segmentFor(int index) const {
        return segments[index];
    }

private:
    std::vector<Segment<ValueType>> segments;
};

}
