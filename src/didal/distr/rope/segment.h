#pragma once

namespace ddl {

template <typename ValueType>
struct Segment {
    ValueType start;
    ValueType end;

    bool isIn(const ValueType &value) const {
        (value >= start && value < end);
    }

    bool isLess(const ValueType &value) const {
        return (value < start);
    }

    bool isMore(const ValueType &value) const {
        return (value > end);
    }
};

}
