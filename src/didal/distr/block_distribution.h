#pragma once

#include "distribution.h"
#include "didal/comm/node_group.h"
#include "didal/base/index.h"
#include "didal/tp/mesh_topology.h"
#include "didal/decomp/block_decomposition.h"
#include "didal/util/slicer.h"

#include <cstddef>
#include <map>
#include <vector>
#include <cstddef>

namespace ddl {

// Distribution of blocks.
template <size_t Dims>
class BlockDistribution : public Distribution<Index<Dims>> {
public:
    using IndexType = typename Distribution<Index<Dims>>::IndexType;

public:
    BlockDistribution(BlockDecomposition<Dims> *decomp, MeshTopology<Dims> *topology) :
        decomp(decomp), topology(topology) {
        for (int dim = 0; dim < Dims; dim++) {
            // Distribute num of blocks per num of nodes for each dimension.
            block_shifts[dim] = ddl::Slicer::getShifts(decomp->numOfBlocks(dim), topology->getDimension(dim));
        }
    }

    // Get node for specified block index.
    virtual Node getNode(const IndexType &block_index) const {
        auto it = distr_map.find(block_index);
        if (it != distr_map.end()) {
            return it->second;
        }
        Index<Dims> node_index;
        for (size_t dim = 0; dim < Dims; dim++) {
            node_index[dim] = nodeIndex(dim, block_index[dim]);
        }
        auto node = topology->nodeForIndex(node_index);
        distr_map.insert({block_index, node});
        return node;
    }

private:
    int nodeIndex(size_t dim, int block_index) const {
        return ddl::Slicer::shiftIndexFor(block_shifts[dim], block_index);
    }

private:
    BlockDecomposition<Dims> *decomp;
    MeshTopology<Dims> *topology;
    std::array<std::vector<int>, Dims> block_shifts;
    mutable std::map<IndexType, Node> distr_map;
};

}
