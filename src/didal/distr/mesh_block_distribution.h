#pragma once

#include "didal/util/slicer.h"
#include "didal/base/index.h"

#include <vector>
#include <array>
#include <initializer_list>

namespace ddl {

template <size_t Dims>
class MeshBlockDistribution {
public:
    MeshBlockDistribution() {}
    MeshBlockDistribution(const std::array<std::pair<int, int>, Dims> &init) {
        for (int i = 0; i < static_cast<int>(Dims); i++) {
            // Distribute num of blocks per num of nodes for each dimension.
            blocks_per_dim[i] = ddl::Slicer::getShifts(init[i].first, init[i].second);
        }
    }
    MeshBlockDistribution(const std::initializer_list<std::pair<int, int>> &init) {
        int index = 0;
        for (const auto &p: init) {
            blocks_per_dim[index] = ddl::Slicer::getShifts(p.first, p.second);
            index++;
        }
    }

    int nodeIndex(int dim, int block_index) const {
        return ddl::Slicer::shiftIndexFor(blocks_per_dim[dim], block_index);
    }

    ddl::Index<Dims> nodeIndex(const ddl::Index<Dims> &block_index) const {
        ddl::Index<Dims> node_index;
        for (int i = 0; i < static_cast<int>(Dims); i++) {
            node_index[i] = nodeIndex(i, block_index[i]);
        }
        return node_index;
    }

    size_t numOfNodes(int dim) const {
        return blocks_per_dim[dim].size() - 1;
    }

private:
    std::array<std::vector<int>, Dims> blocks_per_dim;
};

}
