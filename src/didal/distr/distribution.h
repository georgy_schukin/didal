#pragma once

#include "didal/base/clonable.h"
#include "didal/comm/node_group.h"

#include <cstddef>
#include <map>
#include <vector>

namespace ddl {

// Repsesents distribution of something onto nodes.
template <typename _IndexType>
class Distribution {
public:
    using IndexType = _IndexType;
    using DistributionMap = std::map<_IndexType, Node>;

public:
    virtual ~Distribution() = default;

    // Get node for specified index.
    virtual Node getNode(const IndexType &index) const = 0;

    //virtual DistributionMap distribute(const std::vector<size_t> &indices, const NodeGroup &nodes) const = 0;
};

}
