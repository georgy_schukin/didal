#pragma once

#include "didal/base/clonable.h"
#include "didal/comm/node_group.h"

namespace ddl {

template <typename IdType>
class Distributor {
public:
    virtual ~Distributor() = default;

    virtual int getNodeFor(const IdType &id) const = 0;
};

}
