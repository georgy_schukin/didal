#pragma once

namespace ddl {

class Tracer;

class Traceable {
public:
    virtual ~Traceable() {}

    virtual void setTracer(Tracer*) = 0;
};

}
