#pragma once

#include <ostream>

namespace ddl {

class Counter {
public:
    virtual ~Counter() {}

    virtual std::ostream& output(std::ostream&) const = 0;

    friend std::ostream& operator<<(std::ostream &out, const Counter &counter) {
        return counter.output(out);
    }
};

}
