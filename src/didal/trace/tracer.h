#pragma once

#include "counter.h"

#include <string>
#include <map>
#include <memory>
#include <mutex>
#include <vector>
#include <stdexcept>

namespace ddl {

class Tracer {
public:
    virtual ~Tracer() {}

    template <typename CounterType, typename... Rest>
    std::shared_ptr<CounterType> newCounter(const std::string &name, Rest... args) {
        std::lock_guard<std::mutex> lock(mutex);
        auto it = counters.find(name);
        if (it != counters.end()) {
            return std::dynamic_pointer_cast<CounterType>(it->second);
        }
        auto counter = std::make_shared<CounterType>(args...);
        if (!counter) {
            throw std::runtime_error("Failed to create counter " + name);
        }        
        counters[name] = counter;
        return counter;
    }

protected:
    std::vector<std::pair<std::string, std::shared_ptr<Counter>>> getCounters() {
        std::lock_guard<std::mutex> lock(mutex);
        std::vector<std::pair<std::string, std::shared_ptr<Counter>>> result;
        for (auto &p: counters) {
            result.push_back(std::make_pair(p.first, p.second));
        }
        return result;
    }

private:
    std::map<std::string, std::shared_ptr<Counter>> counters;
    std::mutex mutex;
};

}
