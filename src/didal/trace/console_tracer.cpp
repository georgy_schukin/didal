#include "console_tracer.h"

#include <sstream>
#include <iostream>

namespace ddl {

void ConsoleTracer::dumpCounters() {
    std::ostringstream out;
    std::string tag = std::string("Node[") + std::to_string(this_rank) + std::string("]: ");
    for (auto &p: getCounters()) {
        out << tag << p.first << ": " << *(p.second.get()) << "\n";
    }
    std::cout << out.str();
}

}
