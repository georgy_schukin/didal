#pragma once

#include "counter.h"

#include <cstddef>
#include <algorithm>

namespace ddl {

class TickCounter : public Counter {
public:
    size_t getCount() const {
        return count;
    }

    void inc() {
        count++;
    }

    std::ostream& output(std::ostream &out) const override {
        out << "Count: " << getCount();
        return out;
    }

private:
    size_t count {0};
};

}
