#pragma once

#include "counter.h"

#include <limits>
#include <algorithm>

namespace ddl {

template <typename T>
class ValueCounter : public Counter {
public:
    ValueCounter() = default;

    T getSumValue() const {
        return sum_value;
    }

    T getMin() const {
        return min;
    }

    T getMax() const {
        return max;
    }

    T getCount() const {
        return count;
    }

    double getAvg() const {
        return (count > 0 ? double(sum_value) / count : double(sum_value));
    }

    void add(T val) {
        sum_value += val;
        count++;
        max = std::max(val, max);
        min = std::min(val, min);
    }

    std::ostream& output(std::ostream &out) const override {
        out << "Sum: " << getSumValue() << ", " <<
               "Min: " << getMin() << ", " <<
               "Max: " << getMax() << ", " <<
               "Avg: " << getAvg() << ", " <<
               "Count: " << getCount();
        return out;
    }

private:
    T sum_value {0};
    T min {std::numeric_limits<T>::max()};
    T max {std::numeric_limits<T>::min()};
    size_t count {0};
};

}
