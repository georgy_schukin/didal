#pragma once

#include "tracer.h"

namespace ddl {

class DumpableTracer : public Tracer {
public:
    virtual void dumpCounters() = 0;
};

}
