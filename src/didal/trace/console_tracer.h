#pragma once

#include "dumpable_tracer.h"

namespace ddl {

class ConsoleTracer : public DumpableTracer {
public:
    ConsoleTracer(int rank) :
        this_rank(rank) {
    }

    void dumpCounters() override;

private:
    int this_rank;
};

}
