#pragma once

#include <vector>

namespace ddl {

template <typename BinType>
class RopeBalancerNode {
public:
    RopeBalancerNode() {}
    RopeBalancerNode(const typename std::vector<BinType> &bins):
        bins(bins) {
    }

    double getTotalLoad() const {
        double load = 0;
        for (const auto &bin: bins) {
            load += bin.load;
        }
        return load;
    }

    const std::vector<double> getLoads() const {
        std::vector<double> loads;
        for (const auto &bin: bins) {
            loads.push_back(bin.load);
        }
        return loads;
    }

    const std::vector<int> getIds() const {
        std::vector<int> ids;
        for (const auto &bin: bins) {
            ids.push_back(bin.id);
        }
        return ids;
    }

    void addBins(const typename std::vector<BinType> &new_bins, bool to_end) {
        if (to_end) {
            bins.insert(bins.end(), new_bins.begin(), new_bins.end());
        } else {
            bins.insert(bins.begin(), new_bins.begin(), new_bins.end());
        }
    }

    const typename std::vector<BinType>& getBins() const {
        return bins;
    }

private:
    typename std::vector<BinType> bins;
    int start, end;
};

}
