#pragma once

#include "rope_balancer_node.h"
#include "didal/data/sync_distributed_object.h"

#include <cmath>

namespace ddl {

template <typename BinType>
class RopeLoadBalancer : public ddl::SyncDistributedObject<RopeBalancerNode<BinType>> {
public:
    using RopeNodeType = RopeBalancerNode<BinType>;

public:
    RopeLoadBalancer(ddl::Environment *env):
        ddl::SyncDistributedObject<RopeNodeType>(env) {
        get_load = this->template makeMethodGetter<decltype(&RopeNodeType::getTotalLoad),
                double>(&RopeNodeType::getTotalLoad);
    }

    void setNeighbors(ddl::Node left, ddl::Node right) {
        left_neighbor = left;
        right_neighbor = right;
    }

    std::future<double> getLoadFrom(ddl::Node node) {
        return get_load(node);
    }

    void init(const typename std::vector<BinType> &bins) {
        this->create(bins);
    }

    void balance() {
        auto left_load_f = get_load(left_neighbor);
        auto right_load_f = get_load(right_neighbor);
        auto left_load = left_load_f.get();
        auto right_load = right_load_f.get();
        auto my_load = this->localObject()->getTotalLoad();
        const auto avg_load = (left_load + right_load + my_load) / 3.0;
        while (std::abs(my_load - avg_load) > balance_threshold) {

        }
    }

    void balanceStep(ddl::Node node, double load) {

    }

private:
    ddl::RemoteFunction<double> get_load;
    ddl::Node left_neighbor, right_neighbor;
    double balance_threshold = 0.05;
};

}
