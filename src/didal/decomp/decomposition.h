#pragma once

#include "didal/base/index_range.h"
#include "didal/base/clonable.h"

#include <vector>
#include <cstddef>

namespace ddl {

/// Represents partitioning of something onto parts.
template <typename _IndexType, typename _PartType>
class Decomposition {
public:
    using IndexType = _IndexType;
    using PartType = _PartType;

public:
    virtual ~Decomposition() = default;

    /// Get part for specified index.
    virtual PartType getPart(const IndexType &index) const = 0;

    /// Returns array of indices groups, each group - one fragment.
    //virtual std::vector<IndexRange> decompose(size_t array_size) const = 0;
};

}
