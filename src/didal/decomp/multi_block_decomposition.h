#pragma once

#include "didal/util/slicer.h"

#include <vector>
#include <array>
#include <initializer_list>
#include <cstddef>

namespace ddl {

template <size_t Dims>
class MultuBlockDecomposition {
public:
    MultuBlockDecomposition() {}
    MultuBlockDecomposition(const std::array<std::pair<int, int>, Dims> &init) {
        for (int i = 0; i < Dims; i++) {
            block_sizes[i] = Slicer::getSlices(init[i].first, init[i].second);
            block_shifts[i] = Slicer::getShifts(block_sizes[i]);
        }
    }
    MultuBlockDecomposition(const std::initializer_list<std::pair<int, int>> &init) {
        int index = 0;
        for (const auto &p: init) {
            block_sizes[index] = Slicer::getSlices(p.first, p.second);
            block_shifts[index] = Slicer::getShifts(block_sizes[index]);
            index++;
        }
    }

    int blockSize(int dim, int block_index) const {
        return block_sizes[dim][block_index];
    }

    int blockShift(int dim, int block_index) const {
        return block_shifts[dim][block_index];
    }

    int blockIndex(int dim, int elem_index) const {
        return Slicer::shiftIndexFor(block_shifts[dim], elem_index);
    }

    size_t numOfBlocks(int dim) const {
        return block_sizes[dim].size();
    }

private:
    std::array<std::vector<int>, Dims> block_sizes;
    std::array<std::vector<int>, Dims> block_shifts;
};

}
