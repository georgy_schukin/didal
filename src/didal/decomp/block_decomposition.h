#pragma once

#include "decomposition.h"
#include "didal/base/index.h"
#include "didal/base/index_range_nd.h"
#include "didal/util/slicer.h"

#include <vector>
#include <array>
#include <initializer_list>
#include <cstddef>
#include <unordered_map>

namespace ddl {

template <size_t Dims>
class BlockDecomposition : public Decomposition<Index<Dims>, IndexRangeND<Dims>> {
public:
    using IndexType = Index<Dims>;
    using PartType = IndexRangeND<Dims>;

public:
    BlockDecomposition(const std::array<std::pair<int, int>, Dims> &init) {
        for (size_t i = 0; i < Dims; i++) {
            block_sizes[i] = Slicer::getSlices(init[i].first, init[i].second);
            block_shifts[i] = Slicer::getShifts(block_sizes[i]);
        }
    }

    BlockDecomposition(const std::initializer_list<std::pair<int, int>> &init) {
        size_t index = 0;
        for (const auto &p: init) {
            block_sizes[index] = Slicer::getSlices(p.first, p.second);
            block_shifts[index] = Slicer::getShifts(block_sizes[index]);
            index++;
        }
    }

    int blockSize(size_t dim, int block_index) const {
        return block_sizes[dim][block_index];
    }

    int blockShift(size_t dim, int block_index) const {
        return block_shifts[dim][block_index];
    }

    int blockIndex(size_t dim, int elem_index) const {
        return Slicer::shiftIndexFor(block_shifts[dim], elem_index);
    }

    size_t numOfBlocks(size_t dim) const {
        return block_sizes[dim].size();
    }

    virtual PartType getPart(const IndexType &index) const {
        auto it = parts.find(index);
        if (it != parts.end()) {
            return it->second;
        }
        std::array<IndexRange<>, Dims> ranges;
        for (size_t i = 0; i < Dims; i++) {
            ranges[i] = IndexRange<>(blockShift(i, index[i]), blockSize(i, index[i]), 1);
        }
        auto p = parts.insert({index, PartType(ranges)});
        return p.first->second;
    }

    const std::vector<IndexType>& allIndices() const {
        if (all_indices.empty()) {
            all_indices = getAllIndices();
        }
        return all_indices;
    }

private:
    std::vector<IndexType> getAllIndices() const {
        std::vector<IndexType> indices {IndexType()};
        for (size_t d = 0; d < Dims; d++) {
            std::vector<IndexType> new_indices;
            for (const auto &ind: indices) {
                auto index = ind;
                for (size_t i = 0; i < numOfBlocks(d); i++) {
                    index[d] = i;
                    new_indices.push_back(index);
                }
            }
            indices = new_indices;
        }
        return indices;
    }

private:
    std::array<std::vector<int>, Dims> block_sizes;
    std::array<std::vector<int>, Dims> block_shifts;
    mutable std::unordered_map<IndexType, PartType> parts;
    mutable std::vector<IndexType> all_indices;
};

}
