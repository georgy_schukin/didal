#pragma once

#include <cstddef>
#include <array>

namespace ddl {

template <size_t Dims>
class BlockPart {
public:
    BlockPart() {}

    BlockPart(const std::array<int, Dims> &start, const std::array<int, Dims> &end):
        start(start), end(end) {
    }

    int getStart(int dim) const {
        return start[dim];
    }

    int getEnd(int dim) const {
        return end[dim];
    }

    int getSize(int dim) const {
        return end[dim] - start[dim];
    }

private:
    typename std::array<int, Dims> start;
    typename std::array<int, Dims> end;
};

}
