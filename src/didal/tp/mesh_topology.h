#pragma once

#include "topology.h"
#include "didal/base/index.h"
#include "didal/util/lattice.h"

#include <cstddef>
#include <vector>
#include <array>

namespace ddl {

namespace aux {

template <size_t Dims>
std::array<int, Dims> getMeshSize(int num, const std::array<int, Dims> &sizes) {
    throw std::logic_error("Not implemented!");
}

template <> std::array<int, 1> getMeshSize(int num, const std::array<int, 1> &sizes);
template <> std::array<int, 2> getMeshSize(int num, const std::array<int, 2> &sizes);
template <> std::array<int, 3> getMeshSize(int num, const std::array<int, 3> &sizes);

}

template <size_t Dims>
class MeshTopology : public Topology {
public:
    using IndexType = Index<Dims>;

public:
    MeshTopology(Node this_node, int num_of_nodes) :
        this_node(this_node) {
        dimensions = aux::getMeshSize<Dims>(num_of_nodes, {});
        this_index = indexForNode(this_node);
    }

    MeshTopology(Node this_node, int num_of_nodes, const std::array<int, Dims> &sizes, const std::array<bool, Dims> &periodic = {}) :
        this_node(this_node), periodic(periodic) {
        dimensions = aux::getMeshSize<Dims>(num_of_nodes, sizes);
        this_index = indexForNode(this_node);
    }

    virtual Node thisNode() const {
        return this_node;
    }

    virtual int numOfNodes() const {
        int num = 1;
        for (size_t dim = 0; dim < Dims; dim++) {
            num *= dimensions[dim];
        }
        return num;
    }

    IndexType thisIndex() const {
        return this_index;
    }

    int getDimension(int dim) const {
        return dimensions[dim];
    }

    const std::array<int, Dims>& getDimensions() const {
        return dimensions;
    }

    Node nodeForIndex(const IndexType &index) const {
        int rank = 0;
        for (size_t dim = 0; dim < Dims; dim++) {
            int scale = 1;
            for (size_t s = dim + 1; s < Dims; s++) {
                scale *= dimensions[s];
            }
            rank += index[dim] * scale;
        }
        return Node(rank);
    }

    IndexType indexForNode(const Node &node) const {
        IndexType index;
        auto rank = node.rank();
        for (size_t dim = 0; dim < Dims; dim++) {
            int scale = 1;
            for (size_t s = dim + 1; s < Dims; s++) {
                scale *= dimensions[s];
            }
            index[dim] = rank / scale;
            rank -= index[dim] * scale;
        }
        return index;
    }

    /// Returns all unique neighbors for the index by all dimensions.
    std::vector<IndexType> neighborsForIndex(const IndexType &index) {
        std::set<IndexType> neigh_indices;
        for (size_t dim = 0; dim < Dims; dim++) {
            for (const auto &neigh_index: neighborsForIndex(index, dim)) {
                neigh_indices.insert(neigh_index);
            }
        }
        return std::vector<IndexType>(neigh_indices.begin(), neigh_indices.end());
    }

    /// Returns unique neighbors for the index by the dimension.
    std::vector<IndexType> neighborsForIndex(const IndexType &index, int dim) {
        IndexType left_index = index, right_index = index;
        const auto dim_size = dimensions[dim];
        if (left_index[dim] > 0) {
            left_index[dim] -= 1;
        } else if (periodic[dim]) {
            left_index[dim] = (left_index[dim] + dim_size - 1) % dim_size;
        }
        if (right_index[dim] < dim_size - 1) {
            right_index[dim] += 1;
        } else if (periodic[dim]) {
            right_index[dim] = (right_index[dim] + 1) % dim_size;
        }
        const auto is_l = (left_index != index);
        const auto is_r = (right_index != index);
        const auto is_neq = (left_index != right_index);
        if (is_l && is_r) {
            if (is_neq) {
                return {left_index, right_index};
            } else {
                return {left_index};
            }
        } else if (is_l) {
            return {left_index};
        } else if (is_r) {
            return {right_index};
        } else {
            return {};
        }
    }

private:
    IndexType this_index;
    Node this_node;
    std::array<int, Dims> dimensions;
    std::array<bool, Dims> periodic;
};

}
