#include "mesh_topology.h"

namespace ddl {

namespace aux {

template <>
std::array<int, 1> getMeshSize(int num, const std::array<int, 1> &sizes) {
    return {num};
}

template <>
std::array<int, 2> getMeshSize(int num, const std::array<int, 2> &sizes) {
    auto sz = Lattice::getLatticeSides2D(num, sizes[0], sizes[1]);
    return {sz.first, sz.second};
}

template <>
std::array<int, 3> getMeshSize(int num, const std::array<int, 3> &sizes) {
    auto sz = Lattice::getLatticeSides3D(num, sizes[0], sizes[1], sizes[2]);
    return {std::get<0>(sz), std::get<1>(sz), std::get<2>(sz)};
}

}

}
