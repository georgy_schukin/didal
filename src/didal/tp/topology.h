#pragma once

#include "didal/comm/node.h"

namespace ddl {

class Topology {
public:
    virtual ~Topology() = default;

    virtual Node thisNode() const = 0;
    virtual int numOfNodes() const = 0;
};

}
