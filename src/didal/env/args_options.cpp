#include "args_options.h"

#include <iostream>

namespace ddl {

namespace {

void shiftArgs(int arg, int *argc, char ***argv) {
    for (int i = arg; i < *argc - 1; i++) {
        (*argv)[i] = (*argv)[i + 1];
    }
    (*argc)--;
}

}

void ArgsOptions::addOption(const std::string &key, bool has_value, const std::string &description, Handler handler) {
   options[key] = Option(has_value, description, handler);
}

void ArgsOptions::printOptions() {
    for (const auto &p: options) {
        const auto &opt = p.second;
        if (opt.hasValue()) {
            std::cout << p.first << " <value>: " << opt.getDescription() << std::endl;
        } else {
            std::cout << p.first << ": " << opt.getDescription() << std::endl;
        }
    }
}

void ArgsOptions::parse(int *argc, char ***argv) {
    for (int i = 0; i < *argc;) {
        const std::string key((*argv)[i]);
        auto it = options.find(key);
        if (it == options.end()) {
            i++;
            continue;
        }
        auto &opt = it->second;
        if (opt.hasValue()) {
            const std::string value((*argv)[i + 1]);
            opt.handle(value);
            shiftArgs(i, argc, argv);
            shiftArgs(i, argc, argv);
        } else {
            opt.handle("");
            shiftArgs(i, argc, argv);
        }
    }
}

}
