#include "environment.h"
#include "environment_options.h"
#include "didal/comm/comm_service.h"
#include "didal/comm/mpi/mpi_communicator.h"
#include "didal/trace/console_tracer.h"

namespace ddl {

Environment::Environment(int *argc, char ***argv) {
    EnvironmentOptions options;
    options.parseArgs(argc, argv);

    communicator = std::make_unique<MPICommunicator>(argc, argv, options.comm_use_wait_thread);

    auto mpi_comm = dynamic_cast<MPICommunicator*>(communicator.get());
    if (mpi_comm) {
        mpi_comm->setSendsCheckInterval(options.comm_sends_check_interval);
        mpi_comm->useNonBlockingProbe(options.comm_use_iprobe);
        mpi_comm->setProbeInterval(options.comm_probe_interval);
        mpi_comm->setMaxProbeInterval(options.comm_max_probe_interval);
        mpi_comm->useAdaptiveProbeInterval(options.comm_use_adaptive_probe_interval);
    }

    if (options.enable_tracing) {
        tracer = std::make_unique<ConsoleTracer>(communicator->thisRank());
    }

    if (tracer) {
        communicator->setTracer(tracer.get());
    }

    comm_service = std::make_unique<CommService>(communicator.get(),
                                                 options.msg_sender_type,
                                                 options.msg_proc_type);
    if (tracer) {
        comm_service->setTracer(tracer.get());
    }
}

Environment::~Environment() {
    if (tracer) {
        tracer->dumpCounters();
    }
    comm_service.reset();
    communicator.reset();
    tracer.reset();
}

Node Environment::thisNode() const {
    return comm_service->thisNode();
}

NodeGroup Environment::allNodes() const {
    return comm_service->allNodes();
}

}
