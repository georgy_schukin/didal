#pragma once

#include "didal/comm/node_group.h"

#include <memory>

namespace ddl {

class CommService;
class Communicator;
class DumpableTracer;

class Environment {
public:
    Environment(int *argc, char ***argv);
    ~Environment();

    Node thisNode() const;
    NodeGroup allNodes() const;

    CommService* getCommService() const {
        return comm_service.get();
    }

private:
    void processParameters(int *argc, char ***argv);

private:
    std::unique_ptr<DumpableTracer> tracer;
    std::unique_ptr<Communicator> communicator;
    std::unique_ptr<CommService> comm_service;    
};

}
