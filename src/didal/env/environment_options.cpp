#include "environment_options.h"
#include "args_options.h"

#include <cstring>
#include <string>
#include <cstdlib>
#include <iostream>

namespace {

template <typename T>
T parseInt(const std::string &str, T default_value) {
    try {
        return static_cast<T>(std::stoi(str));
    }
    catch (const std::invalid_argument&) {
        std::cerr << "Failed to parse int: " << str << std::endl;
        return default_value;
    }
}

}

namespace ddl {

void EnvironmentOptions::parseArgs(int *argc, char ***argv) {
    ArgsOptions options;
    options.addOption("--trace", false, "Enable tracing", [this](auto) {
        this->enable_tracing = true;
    });
    options.addOption("--mp-immediate", false, "Process incoming messages in the receving thread", [this](auto) {
        this->msg_proc_type = CommService::MessageProcessorType::Immediate;
    });
    options.addOption("--mp-threaded", false, "Use separate thread to process incoming messages", [this](auto) {
        this->msg_proc_type = CommService::MessageProcessorType::Threaded;
    });
    options.addOption("--ms-immediate", false, "Send outgoing messages in the same thread", [this](auto) {
        this->msg_sender_type = CommService::MessageSenderType::Immediate;
    });
    options.addOption("--ms-threaded", false, "Send outgoing messages in the separate thread", [this](auto) {
        this->msg_sender_type = CommService::MessageSenderType::Threaded;
    });
    options.addOption("--comm-check-interval", true, "Interval to check outgoing messages (mcsec)", [this](const auto &v) {
        this->comm_sends_check_interval = parseInt(v, this->comm_sends_check_interval);
    });
    options.addOption("--comm-probe-interval", true, "Interval to check for incoming messages (mcsec)", [this](const auto &v) {
        this->comm_probe_interval = parseInt(v, this->comm_probe_interval);
    });
    options.addOption("--comm-max-probe-interval", true, "Max interval to check for incoming messages (mcsec)", [this](const auto &v) {
        this->comm_max_probe_interval = parseInt(v, this->comm_max_probe_interval);
    });
    options.addOption("--comm-blocking-probe", false, "Use blocking probe", [this](auto) {
        this->comm_use_iprobe = false;
    });
    options.addOption("--comm-non-blocking-probe", false, "Use non-blocking probe", [this](auto) {
        this->comm_use_iprobe = true;
    });
    options.addOption("--comm-adaptive-probe-interval", false, "Use adaptive probe interval (up to max)", [this](auto) {
        this->comm_use_adaptive_probe_interval = true;
    });
    options.addOption("--comm-progress-thread", false, "Use separate thread to check outgoing messages", [this](auto) {
        this->comm_use_wait_thread = true;
    });
    options.addOption("--help", false, "Print all options", [&options](auto) {
        std::cout << "Environment options:" << std::endl;
        options.printOptions();
    });

    options.parse(argc, argv);
}

}
