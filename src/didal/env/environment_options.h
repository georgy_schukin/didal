#pragma once

#include "didal/comm/comm_service.h"

#include <cstddef>

namespace ddl {

class EnvironmentOptions {
public:
    EnvironmentOptions() {}

    void parseArgs(int *argc, char ***argv);

public:
    CommService::MessageProcessorType msg_proc_type = CommService::MessageProcessorType::Immediate;
    CommService::MessageSenderType msg_sender_type = CommService::MessageSenderType::Immediate;
    size_t comm_sends_check_interval = 1000;
    size_t comm_probe_interval = 10;
    size_t comm_max_probe_interval = 100000;
    bool comm_use_iprobe = true;
    bool comm_use_adaptive_probe_interval = false;
    bool comm_use_wait_thread = false;
    bool enable_tracing = false;
};

}
