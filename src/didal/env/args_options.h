#pragma once

#include <string>
#include <functional>
#include <map>

namespace ddl {

class ArgsOptions {
public:
    using Handler = std::function<void(const std::string&)>;

public:
    ArgsOptions() {}

    void addOption(const std::string &key, bool has_value, const std::string &description, Handler handler);
    void printOptions();

    void parse(int *argc, char ***argv);

private:
    class Option {
        public:
            Option() {}
            Option(bool has_value, const std::string & descr, Handler handler):
                has_value(has_value), description(descr), handler(handler) {
            }

            bool hasValue() const {
                return has_value;
            }

            const std::string& getDescription() const {
                return description;
            }

            void handle(const std::string &value) {
                handler(value);
            }

        private:
            bool has_value;
            std::string description;
            Handler handler;
    };

    std::map<std::string, Option> options;
};

}
