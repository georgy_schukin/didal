win32:CONFIG(release, debug|release): LIBS += -L$${DIDAL_OUT_DIR}/release/ -ldidal
else:win32:CONFIG(debug, debug|release): LIBS += -L$${DIDAL_OUT_DIR}/debug/ -ldidal
else:unix: LIBS += -L$${DIDAL_OUT_DIR}/ -ldidal

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $${DIDAL_OUT_DIR}/release/libdidal.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $${DIDAL_OUT_DIR}/debug/libdidal.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $${DIDAL_OUT_DIR}/release/didal.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $${DIDAL_OUT_DIR}/debug/didal.lib
else:unix: PRE_TARGETDEPS += $${DIDAL_OUT_DIR}/libdidal.a

exists(local.pri) {
    include(local.pri)
}
