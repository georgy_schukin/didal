#include "didal.h"
#include "base/index.h"
#include "util/slicer.h"
#include "util/lattice.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>
#include <vector>
#include <map>
#include <algorithm>
#include <chrono>
#include <stdexcept>

using Index2D = ddl::Index<2>;
using Range2D = ddl::IndexRangeND<2>;
using Block2D = ddl::ArrayND<double, 2>;

Block2D init(const Range2D &range, double value) {
    Block2D block(range);
    std::fill(block.data().begin(), block.data().end(), value);
    return block;
}

Block2D multmm(const Block2D &a, const Block2D &b) {
    Range2D c_range {a.range().range(0), b.range().range(1)};
    Block2D c(c_range);
    for (size_t i = 0; i < c_range.size(0); i++) {
        for (size_t j = 0; j < c_range.size(1); j++) {
            double val = 0.0;
            for (size_t k = 0; k < a.range().range(1).size(); k++) {
                val += a(i, k) * b(k, j);
            }
            c(i, j) = val;
        }
    }
    return c;
}

void sum(Block2D &a, const Block2D &b) {
    if (a.empty()) {
        a = Block2D(b.range());
    }
    auto aa = a.data();
    auto bb = b.data();
    for (size_t i = 0; i < aa.size(); i++) {
        aa[i] += bb[i];
    }
}

int intArg(int arg_num, int default_value, int argc, char **argv) {
    return (argc > arg_num ? std::stoi(argv[arg_num]) : default_value);
}

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    int size_x = intArg(1, 1000, argc, argv);
    int size_y = intArg(2, 1000, argc, argv);
    int num_of_fragments = intArg(3, 10, argc, argv);
    int nodes_by_x = intArg(4, 0, argc, argv);
    int nodes_by_y = intArg(5, 0, argc, argv);
    const bool scale_size = intArg(6, 0, argc, argv);
    const bool scale_fragments = intArg(7, 0, argc, argv);

    const int my_rank = env.thisNode().rank();
    const int num_of_nodes = env.allNodes().size();

    std::tie(nodes_by_x, nodes_by_y) = ddl::Lattice::getLatticeSides2D(num_of_nodes, nodes_by_x, nodes_by_y);

    if (nodes_by_x <=0 || nodes_by_y <= 0 || nodes_by_x * nodes_by_y != num_of_nodes) {
        throw std::runtime_error("Incorrect grid sizes!");
    }

    if (scale_size) {
        size_x *= nodes_by_x;
        size_y *= nodes_by_y;
    }

    if (scale_fragments) {
        num_of_fragments *= num_of_nodes;
    }

    const auto frag_sizes_x = ddl::Slicer::getSlices(size_x, num_of_fragments);
    const auto frag_shifts_x = ddl::Slicer::getShifts(frag_sizes_x);
    const auto fragments_per_node_x = ddl::Slicer::getShifts(num_of_fragments, nodes_by_x);
    const auto frag_sizes_y = ddl::Slicer::getSlices(size_y, num_of_fragments);
    const auto frag_shifts_y = ddl::Slicer::getShifts(frag_sizes_y);
    const auto fragments_per_node_y = ddl::Slicer::getShifts(num_of_fragments, nodes_by_y);

    std::map<Index2D, int> a_distribution, b_distribution, c_distribution;
    std::vector<Index2D> my_c_indices;

    for (int i = 0; i < num_of_fragments; i++) {
        for (int j = 0; j < num_of_fragments; j++) {
            Index2D a_index {i, j};
            Index2D b_index {j, i};
            Index2D c_index {i, j};
            const auto nx = ddl::Slicer::shiftIndexFor(fragments_per_node_x, i);
            const auto ny = ddl::Slicer::shiftIndexFor(fragments_per_node_y, j);
            const auto node = nx * nodes_by_y + ny;
            a_distribution[a_index] = node;
            b_distribution[b_index] = node;
            c_distribution[c_index] = node;
            if (c_distribution[c_index] == my_rank) {
                my_c_indices.push_back(c_index);
            }
        }
    }

    ddl::DistributedStorage<Index2D, Block2D> matr_a(&env);
    ddl::DistributedStorage<Index2D, Block2D> matr_b(&env);
    ddl::DistributedStorage<Index2D, Block2D> matr_c(&env);

    for (const auto &p: a_distribution) {
        if (p.second == my_rank) {
            const auto &index = p.first;
            Range2D range {ddl::IndexRange(frag_shifts_x[index[0]], frag_sizes_x[index[0]]),
                           ddl::IndexRange(frag_shifts_y[index[1]], frag_sizes_y[index[1]])};
            matr_a.create(index, init(range, 1.5));
        }
    }

    for (const auto &p: b_distribution) {
        if (p.second == my_rank) {
            const auto &index = p.first;
            Range2D range {ddl::IndexRange(frag_shifts_y[index[0]], frag_sizes_y[index[0]]),
                           ddl::IndexRange(frag_shifts_x[index[1]], frag_sizes_x[index[1]])};
            matr_b.create(index, init(range, 2.5));
        }
    }

    env.getCommService()->getCommunicator()->barrier();

    double comp_time = 0;
    double wait_time = 0;

    const auto ts = std::chrono::high_resolution_clock::now();

    using BlockPtr = std::shared_ptr<Block2D>;

    for (auto c_index: my_c_indices) {
        Block2D c_block;
        std::vector<std::pair<std::future<BlockPtr>, std::future<BlockPtr>>> futures;
        for (int k = 0; k < num_of_fragments; k++) {
            Index2D a_index {c_index[0], k};
            Index2D b_index {k, c_index[1]};
            auto a_block = matr_a.getWhenReadyFrom(a_distribution[a_index], a_index);
            auto b_block = matr_b.getWhenReadyFrom(b_distribution[b_index], b_index);
            futures.push_back(std::make_pair(std::move(a_block), std::move(b_block)));
        }
        for (auto &p: futures) {
            const auto t1 = std::chrono::high_resolution_clock::now();
            auto aa = *(p.first.get().get());
            auto bb = *(p.second.get().get());
            const auto t2 = std::chrono::high_resolution_clock::now();
            sum(c_block, multmm(aa, bb));
            const auto t3 = std::chrono::high_resolution_clock::now();
            wait_time += std::chrono::duration<double>(t2 - t1).count();
            comp_time += std::chrono::duration<double>(t3 - t2).count();
        }
        matr_c.create(c_index, std::move(c_block));
    }

    const auto te_own = std::chrono::high_resolution_clock::now();

    env.getCommService()->getCommunicator()->barrier();

    const auto te = std::chrono::high_resolution_clock::now();
    const auto time = std::chrono::duration<double>(te - ts).count();
    const auto own_time = std::chrono::duration<double>(te_own - ts).count();

    std::ostringstream out;

    if (my_rank == 0) {
        out << "Size = " << size_x << " x " << size_y <<
               ", grid = " << nodes_by_x << " x " << nodes_by_y <<
               ", fragments = " << num_of_fragments << " x " << num_of_fragments <<
               ", time = " << time <<
               std::endl;
    }
    out << "[Node " << my_rank << "]: own time = " << own_time <<
           ", comp time = " << comp_time <<
           ", wait time = " << wait_time <<
           std::endl;

    std::cout << out.str();

    return 0;
}
