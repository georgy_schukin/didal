TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    vector_sum_complex.cpp \
    complex.cpp

HEADERS += \
    complex.h

include(../../didal_lib.pri)
