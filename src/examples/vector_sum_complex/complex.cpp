#include "complex.h"

std::ostream& operator<<(std::ostream &out, const Complex &c) {
    out << "(" << c.real << ", " << c.imag << ")";
    return out;
}

ddl::Writable& operator<<(ddl::Writable &w, const Complex &c) {
    return w << c.real << c.imag;
}

ddl::Readable& operator>>(ddl::Readable &r, Complex &c) {
    return r >> c.real >> c.imag;
}
