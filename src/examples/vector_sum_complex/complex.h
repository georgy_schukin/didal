#pragma once

#include "didal.h"

#include <iostream>

class Complex {
public:
    Complex() = default;
    Complex(double r, double i) :
        real(r), imag(i) {
    }

public:
    double real {0.0};
    double imag {0.0};
};

std::ostream& operator<<(std::ostream &out, const Complex &c);

ddl::Writable& operator<<(ddl::Writable &w, const Complex &c);
ddl::Readable& operator>>(ddl::Readable &r, Complex &c);
