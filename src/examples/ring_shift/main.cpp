#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <chrono>

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    const int block_size = (argc > 1 ? std::stoi(argv[1]) : 1000);
    const int num_of_blocks = (argc > 2 ? std::stoi(argv[2]) : 1);

    const int my_rank = env.thisNode().rank();
    const int num_of_nodes = env.allNodes().size();

    ddl::DistributedStorage<int, std::vector<double>> a(&env);

    a.create(my_rank, std::vector<double>(block_size, 1.0f));

    env.getCommService()->getCommunicator()->barrier();

    const auto ts = std::chrono::high_resolution_clock::now();

    auto prev_rank = (my_rank + num_of_nodes - 1) % num_of_nodes;    
    for (int i = 0; i < num_of_blocks; i++) {
        // Each call will usually lead to send-recv of data.
        auto a_next = a.getWhenReadyFrom(ddl::Node(prev_rank), prev_rank).get();
    }

    const auto te_own = std::chrono::high_resolution_clock::now();

    env.getCommService()->getCommunicator()->barrier();

    const auto te = std::chrono::high_resolution_clock::now();
    const auto time = std::chrono::duration<double>(te - ts).count();
    const auto own_time = std::chrono::duration<double>(te_own - ts).count();

    std::ostringstream out;

    if (my_rank == 0) {        
        out << "Block size = " << block_size <<
               ", num of blocks = " << num_of_blocks <<
                ", time = " << time << std::endl;        
    }    
    out << "[Node " << my_rank << "]: own time = " << own_time << std::endl;

    std::cout << out.str();

    return 0;
}
