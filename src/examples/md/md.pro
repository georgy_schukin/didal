TEMPLATE = subdirs

SUBDIRS += \   
    md \
    md_fp \
    md_lib \
    md_mpi \
    md_sync 

md.depends = md_lib
md_fp.depends = md_lib
md_mpi.depends = md_lib
md_sync.depends = md_lib
