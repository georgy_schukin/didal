#include "particle.h"

#include "didal/comm/serialize/serializing_ops.h"

namespace md {

bool Particle::isIn(const Extent3 &extent) const {
    return extent.isIn(0, pos.x) &&
           extent.isIn(1, pos.y) &&
           extent.isIn(2, pos.z);
}

ddl::Writable& operator<< (ddl::Writable &w, const Particle &p) {
    return w << p.pos << p.velocity << p.force << p.force_prev << p.mass;
}

ddl::Readable& operator>> (ddl::Readable &r, Particle &p) {
    return r >> p.pos >> p.velocity >> p.force >> p.force_prev >> p.mass;
}

}
