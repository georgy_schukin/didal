#pragma once

#include "didal/util/extent.h"

namespace md {

using Extent3 = ddl::Extent<double, 3>;

}
