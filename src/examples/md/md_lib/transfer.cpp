#include "transfer.h"
#include "neighbor.h"
#include "cell_block.h"
#include "md.h"

namespace md {

namespace {
std::map<Index3, InputTransfer> getInputTransfers(const CellBlockParams &params, const NeighborIndices &neigh_indices) {
    const auto &shadow = params.shadow_start;
    const auto size = params.size;
    static const int NEXT = 1;
    static const int PREV = -1;
    static const int START = 0;
    static const int END = -1;
    static const int S_PREV = END;
    static const int S_NEXT = START;
    static const int D_PREV = START;
    static const int D_NEXT = END;
    std::map<Index3, InputTransfer> all_input_transfers = {
        {{PREV, 0, 0}, InputTransfer(DIM_X, S_PREV, 0, 0, D_PREV, shadow[1], shadow[2], size[1], size[2])}, // front X
        {{NEXT, 0, 0}, InputTransfer(DIM_X, S_NEXT, 0, 0, D_NEXT, shadow[1], shadow[2], size[1], size[2])}, // back X
        {{0, PREV, 0}, InputTransfer(DIM_Y, 0, S_PREV, 0, shadow[0], D_PREV, shadow[2], size[0], size[2])}, // front Y
        {{0, NEXT, 0}, InputTransfer(DIM_Y, 0, S_NEXT, 0, shadow[0], D_NEXT, shadow[2], size[0], size[2])}, // back Y
        {{0, 0, PREV}, InputTransfer(DIM_Z, 0, 0, S_PREV, shadow[0], shadow[1], D_PREV, size[0], size[1])}, // front Z
        {{0, 0, NEXT}, InputTransfer(DIM_Z, 0, 0, S_NEXT, shadow[0], shadow[1], D_NEXT, size[0], size[1])}, // back Z
        {{PREV, PREV, 0}, InputTransfer(DIM_X, S_PREV, S_PREV, 0, D_PREV, D_PREV, shadow[2], 1, size[2])}, // sides XY (Z changes)
        {{PREV, NEXT, 0}, InputTransfer(DIM_X, S_PREV, S_NEXT, 0, D_PREV, D_NEXT, shadow[2], 1, size[2])},
        {{NEXT, PREV, 0}, InputTransfer(DIM_X, S_NEXT, S_PREV, 0, D_NEXT, D_PREV, shadow[2], 1, size[2])},
        {{NEXT, NEXT, 0}, InputTransfer(DIM_X, S_NEXT, S_NEXT, 0, D_NEXT, D_NEXT, shadow[2], 1, size[2])},
        {{PREV, 0, PREV}, InputTransfer(DIM_X, S_PREV, 0, S_PREV, D_PREV, shadow[1], D_PREV, size[1], 1)}, // sides XZ (Y changes)
        {{PREV, 0, NEXT}, InputTransfer(DIM_X, S_PREV, 0, S_NEXT, D_PREV, shadow[1], D_NEXT, size[1], 1)},
        {{NEXT, 0, PREV}, InputTransfer(DIM_X, S_NEXT, 0, S_PREV, D_NEXT, shadow[1], D_PREV, size[1], 1)},
        {{NEXT, 0, NEXT}, InputTransfer(DIM_X, S_NEXT, 0, S_NEXT, D_NEXT, shadow[1], D_NEXT, size[1], 1)},
        {{0, PREV, PREV}, InputTransfer(DIM_Y, 0, S_PREV, S_PREV, shadow[0], D_PREV, D_PREV, size[0], 1)}, // sides YZ (X changes)
        {{0, PREV, NEXT}, InputTransfer(DIM_Y, 0, S_PREV, S_NEXT, shadow[0], D_PREV, D_NEXT, size[0], 1)},
        {{0, NEXT, PREV}, InputTransfer(DIM_Y, 0, S_NEXT, S_PREV, shadow[0], D_NEXT, D_PREV, size[0], 1)},
        {{0, NEXT, NEXT}, InputTransfer(DIM_Y, 0, S_NEXT, S_NEXT, shadow[0], D_NEXT, D_NEXT, size[0], 1)},
        {{PREV, PREV, PREV}, InputTransfer(DIM_X, S_PREV, S_PREV, S_PREV, D_PREV, D_PREV, D_PREV, 1, 1)}, // corners XYZ
        {{PREV, PREV, NEXT}, InputTransfer(DIM_X, S_PREV, S_PREV, S_NEXT, D_PREV, D_PREV, D_NEXT, 1, 1)},
        {{PREV, NEXT, PREV}, InputTransfer(DIM_X, S_PREV, S_NEXT, S_PREV, D_PREV, D_NEXT, D_PREV, 1, 1)},
        {{PREV, NEXT, NEXT}, InputTransfer(DIM_X, S_PREV, S_NEXT, S_NEXT, D_PREV, D_NEXT, D_NEXT, 1, 1)},
        {{NEXT, PREV, PREV}, InputTransfer(DIM_X, S_NEXT, S_PREV, S_PREV, D_NEXT, D_PREV, D_PREV, 1, 1)},
        {{NEXT, PREV, NEXT}, InputTransfer(DIM_X, S_NEXT, S_PREV, S_NEXT, D_NEXT, D_PREV, D_NEXT, 1, 1)},
        {{NEXT, NEXT, PREV}, InputTransfer(DIM_X, S_NEXT, S_NEXT, S_PREV, D_NEXT, D_NEXT, D_PREV, 1, 1)},
        {{NEXT, NEXT, NEXT}, InputTransfer(DIM_X, S_NEXT, S_NEXT, S_NEXT, D_NEXT, D_NEXT, D_NEXT, 1, 1)}
    };
    std::map<Index3, InputTransfer> transfers;
    for (const auto &nd: neigh_indices.getNeighborDisplacements(params.neighbor_indices_type)) {
        auto it = all_input_transfers.find(nd);
        if (it != all_input_transfers.end()) {
            transfers.insert(*it);
        }
    }
    return transfers;
}

std::map<Index3, OutputTransfer> getOutputTransfers(const CellBlockParams &params, const NeighborIndices &neigh_indices) {
    const auto size = params.size;
    static const int NEXT = 1;
    static const int PREV = -1;
    static const int START = 0;
    static const int END = -1;
    static const int S_PREV = START;
    static const int S_NEXT = END;
    std::map<Index3, OutputTransfer> all_output_transfers = {
        {{PREV, 0, 0}, OutputTransfer(DIM_X, S_PREV, 0, 0, size[1], size[2])}, // front X
        {{NEXT, 0, 0}, OutputTransfer(DIM_X, S_NEXT, 0, 0, size[1], size[2])}, // back X
        {{0, PREV, 0}, OutputTransfer(DIM_Y, 0, S_PREV, 0, size[0], size[2])}, // front Y
        {{0, NEXT, 0}, OutputTransfer(DIM_Y, 0, S_NEXT, 0, size[0], size[2])}, // back Y
        {{0, 0, PREV}, OutputTransfer(DIM_Z, 0, 0, S_PREV, size[0], size[1])}, // front Z
        {{0, 0, NEXT}, OutputTransfer(DIM_Z, 0, 0, S_NEXT, size[0], size[1])}, // back Z
        {{PREV, PREV, 0}, OutputTransfer(DIM_X, S_PREV, S_PREV, 0, 1, size[2])}, // sides XY (Z changes)
        {{PREV, NEXT, 0}, OutputTransfer(DIM_X, S_PREV, S_NEXT, 0, 1, size[2])},
        {{NEXT, PREV, 0}, OutputTransfer(DIM_X, S_NEXT, S_PREV, 0, 1, size[2])},
        {{NEXT, NEXT, 0}, OutputTransfer(DIM_X, S_NEXT, S_NEXT, 0, 1, size[2])},
        {{PREV, 0, PREV}, OutputTransfer(DIM_X, S_PREV, 0, S_PREV, size[1], 1)}, // sides XZ (Y changes)
        {{PREV, 0, NEXT}, OutputTransfer(DIM_X, S_PREV, 0, S_NEXT, size[1], 1)},
        {{NEXT, 0, PREV}, OutputTransfer(DIM_X, S_NEXT, 0, S_PREV, size[1], 1)},
        {{NEXT, 0, NEXT}, OutputTransfer(DIM_X, S_NEXT, 0, S_NEXT, size[1], 1)},
        {{0, PREV, PREV}, OutputTransfer(DIM_Y, 0, S_PREV, S_PREV, size[0], 1)}, // sides YZ (X changes)
        {{0, PREV, NEXT}, OutputTransfer(DIM_Y, 0, S_PREV, S_NEXT, size[0], 1)},
        {{0, NEXT, PREV}, OutputTransfer(DIM_Y, 0, S_NEXT, S_PREV, size[0], 1)},
        {{0, NEXT, NEXT}, OutputTransfer(DIM_Y, 0, S_NEXT, S_NEXT, size[0], 1)},
        {{PREV, PREV, PREV}, OutputTransfer(DIM_X, S_PREV, S_PREV, S_PREV, 1, 1)}, // corners XYZ
        {{PREV, PREV, NEXT}, OutputTransfer(DIM_X, S_PREV, S_PREV, S_NEXT, 1, 1)},
        {{PREV, NEXT, PREV}, OutputTransfer(DIM_X, S_PREV, S_NEXT, S_PREV, 1, 1)},
        {{PREV, NEXT, NEXT}, OutputTransfer(DIM_X, S_PREV, S_NEXT, S_NEXT, 1, 1)},
        {{NEXT, PREV, PREV}, OutputTransfer(DIM_X, S_NEXT, S_PREV, S_PREV, 1, 1)},
        {{NEXT, PREV, NEXT}, OutputTransfer(DIM_X, S_NEXT, S_PREV, S_NEXT, 1, 1)},
        {{NEXT, NEXT, PREV}, OutputTransfer(DIM_X, S_NEXT, S_NEXT, S_PREV, 1, 1)},
        {{NEXT, NEXT, NEXT}, OutputTransfer(DIM_X, S_NEXT, S_NEXT, S_NEXT, 1, 1)}
    };
    std::map<Index3, OutputTransfer> transfers;
    for (const auto &nd: neigh_indices.getNeighborDisplacements(params.neighbor_indices_type)) {
        auto it = all_output_transfers.find(nd);
        if (it != all_output_transfers.end()) {
            transfers.insert(*it);
        }
    }
    return transfers;
}

}

std::map<Index3, InputTransfer> getBlockInputTransfers(const Index3 &block_index, const CellBlockParams &params,
                                                       const NeighborIndices &neigh_indices, const MeshParams &mesh_params) {
    auto transfers = getInputTransfers(params, neigh_indices);
    std::map<Index3, InputTransfer> result;
    for (const auto &p: transfers) {
        const auto neigh_index = neighIndex(block_index, p.first, mesh_params.num_blocks);
        result[neigh_index] = p.second;
    }
    return result;
}

std::map<Index3, OutputTransfer> getBlockOutputTransfers(const Index3 &block_index, const CellBlockParams &params,
                                                       const NeighborIndices &neigh_indices, const MeshParams &mesh_params) {
    auto transfers = getOutputTransfers(params, neigh_indices);
    std::map<Index3, OutputTransfer> result;
    for (const auto &p: transfers) {
        const auto neigh_index = neighIndex(block_index, p.first, mesh_params.num_blocks);
        result[neigh_index] = p.second;
    }
    return result;
}

}
