#pragma once

#include "cell.h"

#include "didal/base/array_nd.h"
#include "didal/comm/serialize/serializable.h"

namespace md {

struct CellBlockParams {
    int size[3]; // num of cells in each dimension
    int shift[3]; // shift from start in cells in each dimension
    int shadow_start[3];
    int shadow_end[3];
    int full_size[3]; // total num of cells in each dimenison
    int neighbor_indices_type = 0;
};

struct MeshParams;

class CellBlock {
public:
    using CellArray3 = ddl::ArrayND<Cell, 3>;
    using CellArray2 = ddl::ArrayND<Cell, 2>;

public:
    CellBlock() {}
    CellBlock(const CellBlockParams &params);

    CellArray3& getCells() {
        return cells;
    }

    const CellArray3& getCells() const {
        return cells;
    }

    void setParams(const CellBlockParams &params);

    const CellBlockParams& getParams() const {
        return params;
    }

    Extent3 getExtent(const MeshParams &m_params) const;

    CellArray2 getSlice(int dimension, int x, int y, int z, int n1, int n2);

    void setSlice(int dimension, int x, int y, int z, const CellArray2 &slice);
    void setSlice(int dimension, int x, int y, int z, CellArray2 &&slice);

    void setSliceShadow(int dimension, int x, int y, int z, const CellArray2 &slice);
    void setSliceShadow(int dimension, int x, int y, int z, CellArray2 &&slice);

    std::vector<Particle> extractAndMoveOutParticles(const MeshParams &mesh_params);
    std::vector<Particle> extractParticlesForExtent(const Extent3 &extent);
    void addParticles(const std::vector<Particle> &particles, const MeshParams &mesh_params);

    friend ddl::Writable& operator<< (ddl::Writable &w, const CellBlock &cb);
    friend ddl::Readable& operator>> (ddl::Readable &r, CellBlock &cb);

    template <typename Proc, typename... Args>
    void forEachCell(Proc proc, Args... args) {
        const auto p = getParams();
        for (int i = p.shadow_start[0]; i < p.full_size[0] - p.shadow_end[0]; i++)
        for (int j = p.shadow_start[1]; j < p.full_size[1] - p.shadow_end[1]; j++)
        for (int k = p.shadow_start[2]; k < p.full_size[2] - p.shadow_end[2]; k++) {
            proc(cells(i, j, k), args...);
        }
    }

    template <typename Proc, typename... Args>
    void forEachCell(Proc proc, Args... args) const {
        const auto p = getParams();
        for (int i = p.shadow_start[0]; i < p.full_size[0] - p.shadow_end[0]; i++)
        for (int j = p.shadow_start[1]; j < p.full_size[1] - p.shadow_end[1]; j++)
        for (int k = p.shadow_start[2]; k < p.full_size[2] - p.shadow_end[2]; k++) {
            proc(cells(i, j, k), args...);
        }
    }

    template <typename Proc, typename... Args>
    void forEachCellInd(Proc proc, Args... args) {
        const auto p = getParams();
        for (int i = p.shadow_start[0]; i < p.full_size[0] - p.shadow_end[0]; i++)
        for (int j = p.shadow_start[1]; j < p.full_size[1] - p.shadow_end[1]; j++)
        for (int k = p.shadow_start[2]; k < p.full_size[2] - p.shadow_end[2]; k++) {
            const auto index = ddl::Index<3> {i, j, k};
            proc(cells[index], index, args...);
        }
    }

    template <typename Proc, typename... Args>
    void forEachCellInd(Proc proc, Args... args) const {
        const auto p = getParams();
        for (int i = p.shadow_start[0]; i < p.full_size[0] - p.shadow_end[0]; i++)
        for (int j = p.shadow_start[1]; j < p.full_size[1] - p.shadow_end[1]; j++)
        for (int k = p.shadow_start[2]; k < p.full_size[2] - p.shadow_end[2]; k++) {
            const auto index = ddl::Index<3> {i, j, k};
            proc(cells[index], index, args...);
        }
    }

    template <typename Proc, typename... Args>
    void forEachParticle(Proc proc, Args... args) {
        forEachCell([proc, args...](Cell &cell) {
            cell.forEachParticle(proc, args...);
        });
    }

    template <typename Proc, typename... Args>
    void forEachParticle(Proc proc, Args... args) const {
        forEachCell([proc, args...](const Cell &cell) {
            cell.forEachParticle(proc, args...);
        });
    }

    Cell& getCell(const ddl::Index<3> &cell_index) {
        return cells[cell_index];
    }

    const Cell& getCell(const ddl::Index<3> &cell_index) const {
        return cells[cell_index];
    }

    size_t getNumOfParticles() const;

private:
    int toIndex(int index, int dim) const;
    int toIndexFull(int index, int dim) const;
    void initCells();

private:
    CellBlockParams params;
    CellArray3 cells;
};

}
