#pragma once

#include "didal/base/index.h"

namespace md {

using Index3 = ddl::Index<3>;
using Index4 = ddl::Index<4>;
using Index5 = ddl::Index<5>;

}
