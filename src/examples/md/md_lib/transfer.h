#pragma once

#include <map>

#include "index.h"

namespace md {

struct CellBlockParams;
struct MeshParams;
class NeighborIndices;

enum Dimension {
    DIM_X = 0,
    DIM_Y,
    DIM_Z
};

struct InputTransfer {
    int dimension;
    int src1, src2, src3;
    int dst1, dst2, dst3;
    int size1, size2;

    InputTransfer() {}
    InputTransfer(int dim, int s1, int s2, int s3,
                  int d1, int d2, int d3, int sz1, int sz2) :
        dimension(dim),
        src1(s1), src2(s2), src3(s3),
        dst1(d1), dst2(d2), dst3(d3),
        size1(sz1), size2(sz2) {
    }
};

struct OutputTransfer {
    int dimension;
    int src1, src2, src3;
    int size1, size2;

    OutputTransfer() {}
    OutputTransfer(int dim, int s1, int s2, int s3, int sz1, int sz2) :
        dimension(dim),
        src1(s1), src2(s2), src3(s3),
        size1(sz1), size2(sz2) {
    }
};

std::map<Index3, InputTransfer> getBlockInputTransfers(const Index3 &block_index, const CellBlockParams &params,
                                                       const NeighborIndices &neigh_indices, const MeshParams &mesh_params);

std::map<Index3, OutputTransfer> getBlockOutputTransfers(const Index3 &block_index, const CellBlockParams &params,
                                                       const NeighborIndices &neigh_indices, const MeshParams &mesh_params);
}
