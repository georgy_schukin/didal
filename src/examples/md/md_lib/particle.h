#pragma once

#include "extent.h"
#include "didal/util/vector3.h"
#include "didal/comm/serialize/serializable.h"

namespace md {

using Vector3 = ddl::Vector3;

class Particle {
public:
    Particle() {}

    bool isIn(const Extent3 &extent) const;

public:
    Vector3 pos;
    Vector3 velocity;
    Vector3 force;
    Vector3 force_prev;
    double mass = 1.0;
};

ddl::Writable& operator<< (ddl::Writable &w, const Particle &p);
ddl::Readable& operator>> (ddl::Readable &r, Particle &p);

}
