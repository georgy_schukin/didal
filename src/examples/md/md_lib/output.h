#pragma once

#include "cell_block.h"

#include <string>
#include <vector>
#include <memory>

namespace md {

struct MDParams;

using BlockArray = std::vector<std::shared_ptr<CellBlock>>;

std::string pieceSource(const std::string &path, int iter, int this_node);
std::string parallelSource(const std::string &path, int iter);

void outputVTKPiece(const std::string &path, int iter, int this_node, const BlockArray &blocks);
void outputVTKParallel(const std::string &path, int iter, int num_of_nodes);
void outputVTKSeries(const std::string &path, int num_of_iters, const MDParams &md_params);
void outputVTKNode(const std::string &path, int iter, int this_node, int num_of_nodes, const BlockArray &blocks);

}
