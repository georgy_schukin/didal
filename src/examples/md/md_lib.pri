win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../md_lib/release/ -lmd_lib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../md_lib/debug/ -lmd_lib
else:unix: LIBS += -L$$OUT_PWD/../md_lib/ -lmd_lib

INCLUDEPATH += $$PWD/md_lib
DEPENDPATH += $$PWD/md_lib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../md_lib/release/libmd_lib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../md_lib/debug/libmd_lib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../md_lib/release/md_lib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../md_lib/debug/md_lib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../md_lib/libmd_lib.a
