#include "mesh3d.h"
#include "md.h"
#include "output.h"
#include "neighbor.h"
#include "particle_ops.h"
#include "transfer.h"
#include "didal/util/args.h"
#include "didal/util/slicer.h"
#include "didal/util/timer.h"
#include "didal/util/lattice.h"

#include <mpi.h>
#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <stdexcept>
#include <fstream>
#include <cmath>

using namespace md;

static double recv_time = 0;

enum MessageTag : int {
    MT_PARTICLES = 0,
    MT_PARTICLES_SIZE,
    MT_CELLS,
    MT_CELLS_INFO
};

int mpiRank(const Index3 &index, MPI_Comm comm) {
    const int coord[3] = {index[0], index[1], index[2]};
    int rank = 0;
    MPI_Cart_rank(comm, coord, &rank);
    return rank;
}

double inExtent(double value, double start, double end) {
    if (value < start) {
        return start + std::fmod(start - value, end - start);
    } else if (value >= end) {
        return start + std::fmod(value - start, end - start);
    } else {
        return value;
    }
}

Index3 getBlockIndex(const Vector3 &pos, const MeshParams &mesh_params, const MeshDecomposition &mesh_decomp) {
    Index3 index;
    std::array<double, 3> p {pos.x, pos.y, pos.z};
    for (size_t i = 0; i < 3; i++) {
        const auto cell_index = int((p[i] - mesh_params.origin[i]) / mesh_params.step[i]);
        index[i] = mesh_decomp.blockIndex(static_cast<int>(i), cell_index);
    }
    return index;
}

void calcForces(CellBlock &block, const MDParams &md_params, const NeighborIndices &neigh_indices) {
    calcForcesBlock(block, md_params, neigh_indices);
}

void updatePositions(CellBlock &block, const MDParams &md_params) {
    updatePositionsBlock(block, md_params.delta_t);
}

void updateVelocities(CellBlock &block, const MDParams &md_params) {
    updateVelocitiesBlock(block, md_params.delta_t);
}

void waitForFinish(std::vector<MPI_Request> &reqs) {
    MPI_Waitall(static_cast<int>(reqs.size()), reqs.data(), MPI_STATUSES_IGNORE);
}

void timedRecv(void *buf, int count, MPI_Datatype type, int source, int tag, MPI_Comm comm, MPI_Status *stat) {
    ddl::Timer timer;
    MPI_Recv(buf, count, type, source, tag, comm, stat);
    recv_time += timer.time();
}

using ParticlesSendData = std::pair<int, std::vector<Particle>>;

std::vector<MPI_Request> sendParticlesTo(const std::map<int, ParticlesSendData> &data, MPI_Comm comm) {
    std::vector<MPI_Request> reqs;
    for (auto &p: data) {
        const int &num_of_particles = p.second.first;
        const auto &particles = p.second.second;
        reqs.push_back(MPI_Request {});
        MPI_Isend(&num_of_particles, 1, MPI_INT, p.first, MT_PARTICLES_SIZE, comm, &reqs.back());
        if (!particles.empty()) {
            reqs.push_back(MPI_Request {});
            MPI_Isend(particles.data(), static_cast<int>(num_of_particles * sizeof(Particle)),
                      MPI_BYTE, p.first, MT_PARTICLES, comm, &reqs.back());
        }
    }
    return reqs;
}

void recvParticlesFrom(CellBlock &block, const std::vector<int> &ranks, const MeshParams &mesh_params, MPI_Comm comm) {
    for (const auto src_rank: ranks) {
        int num_of_particles = 0;
        timedRecv(&num_of_particles, 1, MPI_INT, src_rank, MT_PARTICLES_SIZE, comm, MPI_STATUS_IGNORE);
        if (num_of_particles > 0) {
            std::vector<Particle> particles(static_cast<size_t>(num_of_particles));
            timedRecv(particles.data(), num_of_particles * sizeof(Particle),
                     MPI_BYTE, src_rank, MT_PARTICLES, comm, MPI_STATUS_IGNORE);
            block.addParticles(particles, mesh_params);
        }
    }
}

template <typename Proc>
void initParticles(Proc proc, CellBlock &block, int my_rank, int num_of_nodes, MPI_Comm comm, const MeshParams &mesh_params,
                   const MeshDecomposition &decomp, const IndexMap &block_distr) {
    const int root_rank = 0;
    if (my_rank == root_rank) {
        const auto particles = proc();
        std::map<int, ParticlesSendData> particles_to_send;
        for (int i = 0; i < num_of_nodes; i++) {
            particles_to_send[i] = std::make_pair(0, std::vector<Particle> {});
        }
        for (const auto &particle: particles) {
            particles_to_send[block_distr.at(getBlockIndex(particle.pos, mesh_params, decomp))].second.push_back(particle);
        }
        for (auto &p: particles_to_send) {
            p.second.first = static_cast<int>(p.second.second.size());
        }
        auto reqs = sendParticlesTo(particles_to_send, comm);
        recvParticlesFrom(block, {root_rank}, mesh_params, comm);
        waitForFinish(reqs);
    } else {
        recvParticlesFrom(block, {root_rank}, mesh_params, comm);
    }
}

int moveDir(const Extent3 &extent, int dim, double value) {
    if (value < extent.start(dim)) {
        return -1;
    } else if (value >= extent.end(dim)) {
        return 1;
    } else {
        return 0;
    }
}

void moveParticles(CellBlock &block, const Index3 &my_index, MPI_Comm comm, const MeshParams &mesh_params,
                   const std::vector<Index3> &neigh_displs, const IndexMap &block_distr) {
    const auto mesh_extent = mesh_params.getExtent();
    const auto block_extent = block.getExtent(mesh_params);
    const bool account_periodic = mesh_params.periodic[0] || mesh_params.periodic[1] || mesh_params.periodic[2];

    auto moved_particles = block.extractAndMoveOutParticles(mesh_params);

    std::map<Index3, std::vector<Particle>> particles_to_move;
    for (auto &particle: moved_particles) {
        const auto neigh_displ = Index3 {
            moveDir(block_extent, 0, particle.pos.x),
            moveDir(block_extent, 1, particle.pos.y),
            moveDir(block_extent, 2, particle.pos.z),
        };
        if (account_periodic && !particle.isIn(mesh_extent)) {
            particle.pos.x = mesh_params.periodic[0] ? inExtent(particle.pos.x, mesh_extent.start(0), mesh_extent.end(0)) : particle.pos.x;
            particle.pos.y = mesh_params.periodic[1] ? inExtent(particle.pos.y, mesh_extent.start(1), mesh_extent.end(1)) : particle.pos.y;
            particle.pos.z = mesh_params.periodic[2] ? inExtent(particle.pos.z, mesh_extent.start(2), mesh_extent.end(2)) : particle.pos.z;
        }
        particles_to_move[neigh_displ].push_back(particle);
    }

    std::map<int, ParticlesSendData> particles_to_send;
    std::vector<int> neigh_ranks;
    for (const auto &nd: neigh_displs) {
        const auto neigh_index = neighIndex(my_index, nd, mesh_params.num_blocks);
        const auto neigh_rank = block_distr.at(neigh_index);
        auto it = particles_to_move.find(nd);
        if (it != particles_to_move.end()) {
            particles_to_send[neigh_rank] = std::make_pair(static_cast<int>(it->second.size()), std::move(it->second));
        } else {
            particles_to_send[neigh_rank] = std::make_pair(0, std::vector<Particle> {});
        }
        neigh_ranks.push_back(neigh_rank);
    }

    auto reqs = sendParticlesTo(particles_to_send, comm); // send to all neighors
    recvParticlesFrom(block, neigh_ranks, mesh_params, comm); // recv from all neighbors
    waitForFinish(reqs);
}

using CellSendData = std::pair<std::vector<int>, std::vector<Particle>>;

std::vector<MPI_Request> sendCellsTo(const std::map<int, CellSendData> &data, MPI_Comm comm) {
    std::vector<MPI_Request> reqs;
    for (auto &p: data) {
        const auto &cells_info = p.second.first;
        const auto &particles = p.second.second;
        reqs.push_back(MPI_Request {});
        MPI_Isend(cells_info.data(), static_cast<int>(cells_info.size()), MPI_INT, p.first, MT_CELLS_INFO, comm, &reqs.back());
        if (!particles.empty()) {
            reqs.push_back(MPI_Request {});
            MPI_Isend(particles.data(), static_cast<int>(particles.size() * sizeof(Particle)), MPI_BYTE, p.first, MT_CELLS, comm, &reqs.back());
        }
    }
    return reqs;
}

std::map<int, CellSendData> getSendCellsData(CellBlock &block, const std::map<int, OutputTransfer> &transfers) {
    std::map<int, CellSendData> data;
    for (const auto &p: transfers) {
        const auto &output = p.second;
        const auto slice = block.getSlice(output.dimension, output.src1, output.src2, output.src3, output.size1, output.size2);
        std::vector<int> cells_info(1 + static_cast<size_t>(output.size1 * output.size2));
        std::vector<Particle> particles;
        for (int i = 0; i < output.size1; i++)
        for (int j = 0; j < output.size2; j++) {
            const auto &parts = slice(i, j).getParticles();
            cells_info[1 + static_cast<size_t>(i * output.size2 + j)] = static_cast<int>(parts.size());
            particles.insert(particles.end(), parts.begin(), parts.end());
        }
        cells_info[0] = static_cast<int>(particles.size());
        data[p.first] = std::make_pair(std::move(cells_info), std::move(particles));
    }
    return data;
}

void recvCellsFrom(CellBlock &block, const std::map<int, InputTransfer> &transfers, MPI_Comm comm) {
    for (const auto &p: transfers) {
        MPI_Status stat;
        MPI_Probe(p.first, MT_CELLS_INFO, comm, &stat);
        int cells_info_size = 0;
        MPI_Get_count(&stat, MPI_INT, &cells_info_size);
        std::vector<int> cells_info(static_cast<size_t>(cells_info_size));
        timedRecv(cells_info.data(), cells_info_size, MPI_INT, stat.MPI_SOURCE, stat.MPI_TAG, comm, MPI_STATUS_IGNORE);
        int num_of_particles = cells_info[0];
        if (num_of_particles > 0) {
            std::vector<Particle> particles(static_cast<size_t>(num_of_particles));
            timedRecv(particles.data(), num_of_particles * sizeof(Particle), MPI_BYTE, p.first, MT_CELLS, comm, MPI_STATUS_IGNORE);
            const auto &input = p.second;
            CellBlock::CellArray2 slice {ddl::IndexRangeND<2> {static_cast<size_t>(input.size1), static_cast<size_t>(input.size2)}};
            int shift = 0;
            for (int i = 0; i < input.size1; i++)
            for (int j = 0; j < input.size2; j++) {
                int size = cells_info[1 + static_cast<size_t>(i * input.size2 + j)];
                if (size > 0) {
                    slice(i, j).addParticles(std::vector<Particle>(particles.begin() + shift, particles.begin() + shift + size));
                    shift += size;
                }
            }
            block.setSliceShadow(input.dimension, input.dst1, input.dst2, input.dst3, std::move(slice));
        }
    }
}

void updateShadows(CellBlock &block, const std::map<int, InputTransfer> &input_transfers,
                   const std::map<int, OutputTransfer> &output_transfers, MPI_Comm comm) {
    auto data = getSendCellsData(block, output_transfers);
    auto reqs = sendCellsTo(data, comm);
    recvCellsFrom(block, input_transfers, comm);
    waitForFinish(reqs);
}

template <typename Proc, typename... Args>
double timed(Proc proc, Args&&... args) {
    ddl::Timer timer;
    proc(std::forward<Args>(args)...);
    return timer.time();
}

double timedBarrier(MPI_Comm comm) {
    ddl::Timer timer;
    MPI_Barrier(comm);
    return timer.time();
}

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    ddl::ArgsParser args(argc, argv);

    int size_x = args.intArg(100);
    int size_y = args.intArg(100);
    int size_z = args.intArg(100);
    int num_of_particles = args.intArg(1000);
    int num_of_iters = args.intArg(100);
    const bool scale_size = args.intArg(0);
    const bool scale_particles = args.intArg(0);
    const bool debug = args.intArg(0);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Comm cart_comm;

    int dims[3] = {0, 0, 0};
    MPI_Dims_create(size, 3, dims);
    int periods[3] = {0, 0, 0};
    MPI_Cart_create(MPI_COMM_WORLD, 3, dims, periods, 0, &cart_comm);

    const int my_rank = rank;
    const int num_of_nodes = size;

    int coords[3];
    MPI_Cart_coords(cart_comm, my_rank, 3, coords);

    const auto my_index = Index3 {coords[0], coords[1], coords[2]};

    // Size scaling mode: mesh size denotes mesh block size for each node.
    // Scale global mesh size appropriately.
    if (scale_size) {
        size_x *= dims[0];
        size_y *= dims[1];
        size_z *= dims[2];
    }

    if (scale_particles) {
        num_of_particles *= num_of_nodes;
    }

    MDParams md_params;
    md_params.epsilon = 5.0;
    md_params.sigma = 1.0;
    md_params.cutoff_radius = 2.5 * md_params.sigma;
    md_params.delta_t = 1e-5;

    md_params.load("conf.txt");

    const bool do_output = !md_params.output.empty();

    MeshParams mesh_params;
    mesh_params.mesh_size[0] = size_x;
    mesh_params.mesh_size[1] = size_y;
    mesh_params.mesh_size[2] = size_z;
    mesh_params.num_blocks[0] = dims[0];
    mesh_params.num_blocks[1] = dims[1];
    mesh_params.num_blocks[2] = dims[2];
    mesh_params.origin[0] = 0.0;
    mesh_params.origin[1] = 0.0;
    mesh_params.origin[2] = 0.0;
    mesh_params.step[0] = md_params.cutoff_radius;
    mesh_params.step[1] = md_params.cutoff_radius;
    mesh_params.step[2] = md_params.cutoff_radius;
    mesh_params.area_size[0] = mesh_params.step[0] * mesh_params.mesh_size[0];
    mesh_params.area_size[1] = mesh_params.step[1] * mesh_params.mesh_size[1];
    mesh_params.area_size[2] = mesh_params.step[2] * mesh_params.mesh_size[2];
    for (int i = 0; i < 3; i++) {
        mesh_params.periodic[i] = md_params.periodic[i];
    }

    MeshDecomposition mesh_decomp {{mesh_params.mesh_size[0], mesh_params.num_blocks[0]},
                                  {mesh_params.mesh_size[1], mesh_params.num_blocks[1]},
                                  {mesh_params.mesh_size[2], mesh_params.num_blocks[2]}};

    MeshDistribution mesh_distr {{mesh_params.num_blocks[0], dims[0]},
                                 {mesh_params.num_blocks[1], dims[1]},
                                 {mesh_params.num_blocks[2], dims[2]}};

    // Block index(=node index) -> MPI rank.
    IndexMap distribution;

    for (int i = 0; i < mesh_params.num_blocks[0]; i++) {
        for (int j = 0; j < mesh_params.num_blocks[1]; j++) {
            for (int k = 0; k < mesh_params.num_blocks[2]; k++) {
                Index3 index {i, j, k};
                const auto ni = mesh_distr.nodeIndex(index);
                distribution[index] = mpiRank(ni, cart_comm);
            }
        }
    }

    CellBlockParams block_params;
    for (size_t i = 0; i < 3; i++) {
        block_params.size[i] = mesh_decomp.blockSize(static_cast<int>(i), my_index[i]);
        block_params.shift[i] = mesh_decomp.blockShift(static_cast<int>(i), my_index[i]);
        block_params.shadow_start[i] = mesh_params.periodic[i] ? 1 : (my_index[i] > 0 ? 1 : 0);
        block_params.shadow_end[i] = mesh_params.periodic[i] ? 1 : (my_index[i] < mesh_params.num_blocks[i] - 1 ? 1 : 0);
        block_params.full_size[i] = block_params.size[i] + block_params.shadow_start[i] + block_params.shadow_end[i];
    }
    block_params.neighbor_indices_type = NeighborIndices::getType(
                block_params.shadow_start[0], block_params.shadow_end[0],
                block_params.shadow_start[1], block_params.shadow_end[1],
                block_params.shadow_start[2], block_params.shadow_end[2]);

    auto block_ptr = std::make_shared<CellBlock>(block_params);
    auto blocks = std::vector<std::shared_ptr<CellBlock>> {block_ptr};
    CellBlock &my_block = *(block_ptr.get());

    NeighborIndices neighbor_indices;

    const auto neighbor_displacements = neighbor_indices.getNeighborDisplacements(block_params.neighbor_indices_type);
    std::map<int, InputTransfer> my_input_transfers;
    for (const auto &p: getBlockInputTransfers(my_index, block_params, neighbor_indices, mesh_params)) {
        my_input_transfers[distribution.at(p.first)] = p.second;
    }
    std::map<int, OutputTransfer> my_output_transfers;
    for (const auto &p: getBlockOutputTransfers(my_index, block_params, neighbor_indices, mesh_params)) {
        my_output_transfers[distribution.at(p.first)] = p.second;
    }

    initParticles([&](){
        return initParticles(num_of_particles, md_params, mesh_params);
    }, my_block, my_rank, num_of_nodes, cart_comm, mesh_params, mesh_decomp, distribution);

    if (do_output) {
        outputVTKNode(md_params.output, 0, my_rank, num_of_nodes, blocks);
    }

    double update_shadows_time = 0;
    double calc_forces_time = 0;
    double update_pos_time = 0;
    double update_vel_time = 0;
    double move_particles_time = 0;

    size_t np = 0;

    ddl::Timer total_timer;

    update_shadows_time += timed(updateShadows, my_block, my_input_transfers, my_output_transfers, cart_comm);
    calc_forces_time += timed(calcForces, my_block, md_params, neighbor_indices);

    for (int iter = 1; iter <= num_of_iters; iter++) {
        if (debug) {
            std::ostringstream out;
            out << my_rank <<
                   ": iter " << iter <<
                   ", particles: " << my_block.getNumOfParticles() <<
                   std::endl;
            std::cout << out.str();
        }

        update_pos_time += timed(updatePositions, my_block, md_params);

        move_particles_time += timed(moveParticles, my_block, my_index, cart_comm,
                                     mesh_params, neighbor_displacements, distribution);

        update_shadows_time += timed(updateShadows, my_block, my_input_transfers, my_output_transfers, cart_comm);

        calc_forces_time += timed(calcForces, my_block, md_params, neighbor_indices);
        update_vel_time += timed(updateVelocities, my_block, md_params);

        if (do_output && (iter % md_params.output_step == 0)) {
            outputVTKNode(md_params.output, iter, my_rank, num_of_nodes, blocks);
        }

        np += my_block.getNumOfParticles();
    }

    const auto own_time = total_timer.time();

    MPI_Barrier(cart_comm);

    const auto time = total_timer.time();

    std::ostringstream out;

    if (my_rank == 0) {
        out << "Mesh: " << mesh_params.mesh_size[0] << " x " << mesh_params.mesh_size[1] << " x " << mesh_params.mesh_size[2] <<
               ", grid: " << dims[0] << " x " << dims[1] << " x " << dims[2] <<
               ", particles: " << num_of_particles <<
               ", iters: " << num_of_iters <<
               ", time: " << time <<
               std::endl;
    }
    out << "[Node " << my_rank << "]: own time: " << own_time <<
           ", calc forces time: " << calc_forces_time <<
           ", move particles time: " << move_particles_time <<
           ", update shadows time: " << update_shadows_time <<
           ", update pos time: " << update_pos_time <<
           ", update velocity time: " << update_vel_time <<
           ", recv time: " << recv_time <<
           ", avg particles: " << np / static_cast<size_t>(num_of_iters) <<
           std::endl;

    std::cout << out.str();

    MPI_Finalize();

    return 0;
}
