#pragma once

#include "didal/decomp/multi_block_decomposition.h"
#include "didal/distr/mesh_block_distribution.h"
#include "index.h"
#include "md.h"

using MeshDecomposition = ddl::MultuBlockDecomposition<3>;
using MeshDistribution = ddl::MeshBlockDistribution<3>;
using IndexMap = std::map<md::Index3, int>;
