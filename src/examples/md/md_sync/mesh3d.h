#pragma once

#include "didal/didal.h"
#include "didal/decomp/multi_block_decomposition.h"
#include "didal/distr/mesh_block_distribution.h"
#include "index.h"
#include "md.h"

using namespace md;

using MeshDecomposition = ddl::MultuBlockDecomposition<3>;
using MeshDistribution = ddl::MeshBlockDistribution<3>;
using BlockMap = std::map<Index3, int>;

template <typename IndexType>
class BlockMesh : public ddl::SyncDistributedStorage<IndexType, CellBlock> {
public:
    BlockMesh(ddl::Environment *env, MeshParams m_params) :
        ddl::SyncDistributedStorage<IndexType, CellBlock>(env),
        mesh_params(m_params) {
        auto particle_add = [this](CellBlock &block, const std::vector<Particle> &particles) {
            block.addParticles(particles, this->mesh_params);
        };
        add_particles = this->template makeGetter<decltype(particle_add), void, const std::vector<Particle>&>(particle_add);
        extract_particles = this->template makeMethodGetter<decltype(&CellBlock::extractParticlesForExtent), std::vector<Particle>, const Extent3&>(&CellBlock::extractParticlesForExtent);
        get_slice = this->template makeMethodGetter<decltype(&CellBlock::getSlice), CellBlock::CellArray2, int, int, int, int, int, int>(&CellBlock::getSlice);
    }

    std::future<void> addParticlesOn(ddl::Node node, const IndexType &index, const std::vector<Particle> &particles) {
        return add_particles(node, index, particles);
    }

    std::future<std::vector<Particle>> extractParticlesForExtentFrom(ddl::Node node, const IndexType &index, const Extent3 &extent) {
        return extract_particles(node, index, extent);
    }

    std::future<CellBlock::CellArray2> getSliceFrom(ddl::Node node, const IndexType &index, int dim, int x, int y, int z, int n1, int n2) {
        return get_slice(node, index, dim, x, y, z, n1, n2);
    }

    const MeshParams& getParams() const {
        return mesh_params;
    }

private:
    MeshParams mesh_params;
    std::function<std::future<void>(const ddl::Node&, const IndexType&, const std::vector<Particle>&)> add_particles;
    std::function<std::future<std::vector<Particle>>(const ddl::Node&, const IndexType&, const Extent3&)> extract_particles;
    std::function<std::future<CellBlock::CellArray2>(const ddl::Node&, const IndexType&, int, int, int, int, int, int)> get_slice;
};

using Mesh3D = BlockMesh<Index3>;
