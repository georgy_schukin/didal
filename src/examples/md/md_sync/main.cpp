#include "mesh3d.h"
#include "md.h"
#include "output.h"
#include "neighbor.h"
#include "particle_ops.h"
#include "transfer.h"
#include "didal/util/args.h"
#include "didal/util/slicer.h"
#include "didal/util/timer.h"
#include "didal/util/lattice.h"

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <stdexcept>
#include <fstream>
#include <cmath>

using namespace md;

static double slice_get_time = 0;
static double slice_set_time = 0;
static double wait_time = 0;
static double sync_time = 0;

int toRank(const Index3 &index, const MeshDistribution &distr) {
    return index[0] * static_cast<int>(distr.numOfNodes(1)) * static_cast<int>(distr.numOfNodes(2)) +
           index[1] * static_cast<int>(distr.numOfNodes(2)) +
           index[2];
}

double inExtent(double value, double start, double end) {
    if (value < start) {
        return start + std::fmod(start - value, end - start);
    } else if (value >= end) {
        return start + std::fmod(value - start, end - start);
    } else {
        return value;
    }
}

Index3 getBlockIndex(const Vector3 &pos, const MeshParams &mesh_params, const MeshDecomposition &mesh_decomp) {
    Index3 index;
    std::array<double, 3> p {pos.x, pos.y, pos.z};
    for (size_t i = 0; i < 3; i++) {
        const auto cell_index = int((p[i] - mesh_params.origin[i]) / mesh_params.step[i]);
        index[i] = mesh_decomp.blockIndex(static_cast<int>(i), cell_index);
    }
    return index;
}

void calcForces(Mesh3D &mesh, const MDParams &md_params, const NeighborIndices &neigh_indices) {
    for (auto &p: mesh.localContents()) {
        auto &block = *(p.second.get());
        calcForcesBlock(block, md_params, neigh_indices);
    }
}

void updatePositions(Mesh3D &mesh, const MDParams &md_params) {
    for (auto &p: mesh.localContents()) {
        auto &block = *(p.second.get());
        updatePositionsBlock(block, md_params.delta_t);
    }
}

void updateVelocities(Mesh3D &mesh, const MDParams &md_params) {
    for (auto &p: mesh.localContents()) {
        auto &block = *(p.second.get());
        updateVelocitiesBlock(block, md_params.delta_t);
    }
}

void distributeParticles(Mesh3D &mesh, const std::vector<Particle> &particles,
                           const MeshDecomposition &decomp, const BlockMap &block_distr) {
    std::map<Index3, std::vector<Particle>> particles_for_blocks;
    const auto &mesh_params = mesh.getParams();

    for (const auto &particle: particles) {
        particles_for_blocks[getBlockIndex(particle.pos, mesh_params, decomp)].push_back(particle);
    }

    mesh.nextEpoch();

    std::vector<std::future<void>> futures;
    for (auto &p: particles_for_blocks) {
        auto it = block_distr.find(p.first);
        if (it != block_distr.end()) {
            futures.push_back(mesh.addParticlesOn(it->second, it->first, p.second));
        }
    }

    mesh.signalEpochAllLocal();

    // Wait when remote operations are completed.
    for (auto &f: futures) {
        f.get();
    }
}

template <typename Proc>
void initParticles(Proc proc, Mesh3D &mesh, int my_rank,
                   const MeshDecomposition &decomp, const BlockMap &block_distr) {
    std::vector<Particle> particles;
    if (my_rank == 0) {
        particles = proc();
    }
    distributeParticles(mesh, particles, decomp, block_distr);
}

void moveParticles(Mesh3D &mesh, const MeshDecomposition &decomp, const BlockMap &block_distr) {
    const auto &mesh_params = mesh.getParams();
    const auto extent = mesh_params.getExtent();
    const bool account_periodic = mesh_params.periodic[0] || mesh_params.periodic[1] || mesh_params.periodic[2];

    std::vector<Particle> moved_particles;
    for (auto &p: mesh.localContents()) {
        auto out_particles = p.second->extractAndMoveOutParticles(mesh_params);
        if (account_periodic) {
            // Change particle positions to account periodicity.
            for (auto &particle: out_particles) {
                if (!particle.isIn(extent)) {
                    particle.pos.x = mesh_params.periodic[0] ? inExtent(particle.pos.x, extent.start(0), extent.end(0)) : particle.pos.x;
                    particle.pos.y = mesh_params.periodic[1] ? inExtent(particle.pos.y, extent.start(1), extent.end(1)) : particle.pos.y;
                    particle.pos.z = mesh_params.periodic[2] ? inExtent(particle.pos.z, extent.start(2), extent.end(2)) : particle.pos.z;
                }
            }
        }
        moved_particles.insert(moved_particles.end(), out_particles.begin(), out_particles.end());
    }

    distributeParticles(mesh, moved_particles, decomp, block_distr);
}

size_t numOfParticles(Mesh3D &mesh) {
    size_t num = 0;
    for (const auto &p: mesh.localContents()) {
        num += p.second->getNumOfParticles();
    }
    return num;
}

size_t numOfBlocks(Mesh3D &mesh) {
    return mesh.localContents().size();
}

struct Transfer {
    int dimension;
    int x, y, z;
    std::future<CellBlock::CellArray2> data;

    Transfer(int dim, int xx, int yy, int zz, std::future<CellBlock::CellArray2> &&dt) :
        dimension(dim), x(xx), y(yy), z(zz), data(std::move(dt)) {
    }

    void process(CellBlock &block) {
        ddl::Timer timer;
        auto cells = data.get();
        wait_time += timer.time();

        timer.reset();
        block.setSliceShadow(dimension, x, y, z, std::move(cells));
        slice_set_time += timer.time();
    }
};


void updateShadows(Mesh3D &mesh, const BlockMap &block_distr,
                   const std::map<Index3, std::map<Index3, InputTransfer>> &transfer_inputs) {
    mesh.nextEpoch();
    std::vector<std::pair<CellBlock*, Transfer>> transfers;
    for (auto &p: mesh.localContents()) {
        auto index = p.first;
        auto *block = p.second.get();
        for (const auto &p: transfer_inputs.at(index)) {
            const auto &neigh_index = p.first;
            const auto &t_input = p.second;
            if (block_distr.find(neigh_index) != block_distr.end()) {
                auto future = mesh.getSliceFrom(block_distr.at(neigh_index), neigh_index,
                                                t_input.dimension, t_input.src1, t_input.src2, t_input.src3,
                                                t_input.size1, t_input.size2);
                transfers.emplace_back(block, Transfer(t_input.dimension, t_input.dst1, t_input.dst2, t_input.dst3,
                                                       std::move(future)));
            }
        }
    }

    mesh.signalEpochAllLocal();

    for (auto &p: transfers) {
        p.second.process(*p.first);
    }
}

template <typename Proc, typename... Args>
double timed(Proc proc, Args&&... args) {
    ddl::Timer timer;
    proc(std::forward<Args>(args)...);
    return timer.time();
}

double timedBarrier(ddl::CommService *cs) {
    ddl::Timer timer;
    cs->getCommunicator()->barrier();
    return timer.time();
}

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::ArgsParser args(argc, argv);

    int size_x = args.intArg(100);
    int size_y = args.intArg(100);
    int size_z = args.intArg(100);
    int num_of_fragments_x = args.intArg(10);
    int num_of_fragments_y = args.intArg(10);
    int num_of_fragments_z = args.intArg(10);
    int num_of_particles = args.intArg(1000);
    int num_of_iters = args.intArg(100);
    int nodes_by_x = args.intArg(0);
    int nodes_by_y = args.intArg(0);
    int nodes_by_z = args.intArg(0);
    const bool scale_size = args.intArg(0);
    const bool scale_fragments = args.intArg(0);
    const bool scale_particles = args.intArg(0);
    const bool debug = args.intArg(0);

    const int my_rank = env.thisNode().rank();
    const int num_of_nodes = env.allNodes().size();

    std::tie(nodes_by_x, nodes_by_y, nodes_by_z) = ddl::Lattice::getLatticeSides3D(num_of_nodes, nodes_by_x, nodes_by_y, nodes_by_z);

    if (nodes_by_x <=0 || nodes_by_y <= 0 || nodes_by_z <= 0 || nodes_by_x * nodes_by_y * nodes_by_z != num_of_nodes) {
        throw std::runtime_error("Incorrect grid sizes!");
    }

    // Size scaling mode: mesh size denotes mesh block size for each node.
    // Scale global mesh size appropriately.
    if (scale_size) {
        size_x *= nodes_by_x;
        size_y *= nodes_by_y;
        size_z *= nodes_by_z;
    }

    // Num of fragments scaling mode: num of fragments denotes num of fragments on each node.
    // Scale global num of fragments appropriately.
    if (scale_fragments) {
        num_of_fragments_x *= nodes_by_x;
        num_of_fragments_y *= nodes_by_y;
        num_of_fragments_z *= nodes_by_z;
    }

    if (scale_particles) {
        num_of_particles *= num_of_nodes;
    }

    MDParams md_params;
    md_params.epsilon = 5.0;
    md_params.sigma = 1.0;
    md_params.cutoff_radius = 2.5 * md_params.sigma;
    md_params.delta_t = 1e-5;

    md_params.load("conf.txt");

    const bool do_output = !md_params.output.empty();

    MeshParams mesh_params;
    mesh_params.mesh_size[0] = size_x;
    mesh_params.mesh_size[1] = size_y;
    mesh_params.mesh_size[2] = size_z;
    mesh_params.num_blocks[0] = num_of_fragments_x;
    mesh_params.num_blocks[1] = num_of_fragments_y;
    mesh_params.num_blocks[2] = num_of_fragments_z;
    mesh_params.origin[0] = 0.0;
    mesh_params.origin[1] = 0.0;
    mesh_params.origin[2] = 0.0;
    mesh_params.step[0] = md_params.cutoff_radius;
    mesh_params.step[1] = md_params.cutoff_radius;
    mesh_params.step[2] = md_params.cutoff_radius;
    mesh_params.area_size[0] = mesh_params.step[0] * mesh_params.mesh_size[0];
    mesh_params.area_size[1] = mesh_params.step[1] * mesh_params.mesh_size[1];
    mesh_params.area_size[2] = mesh_params.step[2] * mesh_params.mesh_size[2];
    for (int i = 0; i < 3; i++) {
        mesh_params.periodic[i] = md_params.periodic[i];
    }

    MeshDecomposition mesh_decomp {{mesh_params.mesh_size[0], mesh_params.num_blocks[0]},
                                  {mesh_params.mesh_size[1], mesh_params.num_blocks[1]},
                                  {mesh_params.mesh_size[2], mesh_params.num_blocks[2]}};

    MeshDistribution mesh_distr {{mesh_params.num_blocks[0], nodes_by_x},
                                 {mesh_params.num_blocks[1], nodes_by_y},
                                 {mesh_params.num_blocks[2], nodes_by_z}};

    BlockMap distribution;

    for (int i = 0; i < mesh_params.num_blocks[0]; i++) {
        for (int j = 0; j < mesh_params.num_blocks[1]; j++) {
            for (int k = 0; k < mesh_params.num_blocks[2]; k++) {
                Index3 index {i, j, k};
                const auto ni = mesh_distr.nodeIndex(index);
                const auto node = toRank(ni, mesh_distr);
                distribution[index] = node;
            }
        }
    }

    Mesh3D mesh(&env, mesh_params);

    NeighborIndices neighbor_indices;

    for (const auto &p: distribution) {
        const auto &index = p.first;
        if (p.second == my_rank) {
            CellBlockParams b_params;
            for (size_t i = 0; i < 3; i++) {
                b_params.size[i] = mesh_decomp.blockSize(static_cast<int>(i), index[i]);
                b_params.shift[i] = mesh_decomp.blockShift(static_cast<int>(i), index[i]);
                b_params.shadow_start[i] = mesh_params.periodic[i] ? 1 : (index[i] > 0 ? 1 : 0);
                b_params.shadow_end[i] = mesh_params.periodic[i] ? 1 : (index[i] < mesh_params.num_blocks[i] - 1 ? 1 : 0);
                b_params.full_size[i] = b_params.size[i] + b_params.shadow_start[i] + b_params.shadow_end[i];
            }
            b_params.neighbor_indices_type = NeighborIndices::getType(
                        b_params.shadow_start[0], b_params.shadow_end[0],
                        b_params.shadow_start[1], b_params.shadow_end[1],
                        b_params.shadow_start[2], b_params.shadow_end[2]);
            mesh.create(index, CellBlock(b_params));
        }
    }

    std::map<Index3, std::map<Index3, InputTransfer>> block_transfer_inputs;

    for (const auto &p: mesh.localContents()) {
        const auto index = p.first;
        const auto &params = p.second->getParams();
        block_transfer_inputs[index] = getBlockInputTransfers(index, params, neighbor_indices, mesh_params);
    }

    env.getCommService()->getCommunicator()->barrier();

    initParticles([&](){
        return initParticles(num_of_particles, md_params, mesh_params);
    }, mesh, my_rank, mesh_decomp, distribution);

    env.getCommService()->getCommunicator()->barrier();

    if (do_output) {
        outputVTKNode(md_params.output, 0, my_rank, num_of_nodes, mesh.localObjects());
    }

    double update_shadows_time = 0;
    double calc_forces_time = 0;
    double update_pos_time = 0;
    double update_vel_time = 0;
    double move_particles_time = 0;

    size_t np = 0;

    ddl::Timer total_timer;

    ddl::Timer tm;

    tm.reset();
    updateShadows(mesh, distribution, block_transfer_inputs);
    update_shadows_time += tm.time();

    tm.reset();
    calcForces(mesh, md_params, neighbor_indices);
    calc_forces_time += tm.time();

    env.getCommService()->getCommunicator()->barrier();

    for (int iter = 1; iter <= num_of_iters; iter++) {
        if (debug) {
            std::ostringstream out;
            out << my_rank <<
                   ": iter " << iter <<
                   ", blocks: " << numOfBlocks(mesh)  <<
                   ", particles: " << numOfParticles(mesh) <<
                   std::endl;
            std::cout << out.str();
        }

        tm.reset();
        updatePositions(mesh, md_params);
        update_pos_time += tm.time();

        tm.reset();
        moveParticles(mesh, mesh_decomp, distribution);
        move_particles_time += tm.time();

        tm.reset();
        updateShadows(mesh, distribution, block_transfer_inputs);
        update_shadows_time += tm.time();

        tm.reset();
        calcForces(mesh, md_params, neighbor_indices);
        calc_forces_time += tm.time();

        tm.reset();
        updateVelocities(mesh, md_params);
        update_vel_time += tm.time();

        if (do_output && (iter % md_params.output_step == 0)) {
            outputVTKNode(md_params.output, iter, my_rank, num_of_nodes, mesh.localObjects());
        }

        np += numOfParticles(mesh);
    }

    const auto own_time = total_timer.time();

    env.getCommService()->getCommunicator()->barrier();

    const auto time = total_timer.time();

    std::ostringstream out;

    if (my_rank == 0) {
        out << "Mesh: " << mesh_params.mesh_size[0] << " x " << mesh_params.mesh_size[1] << " x " << mesh_params.mesh_size[2] <<
               ", fragments: " << mesh_params.num_blocks[0] << " x " << mesh_params.num_blocks[1] << " x " << mesh_params.num_blocks[2] <<
               ", grid: " << nodes_by_x << " x " << nodes_by_y << " x " << nodes_by_z <<
               ", particles: " << num_of_particles <<
               ", iters: " << num_of_iters <<
               ", time: " << time <<
               std::endl;
    }
    out << "[Node " << my_rank << "]: own time: " << own_time <<
           ", calc forces time: " << calc_forces_time <<
           ", move particles time: " << move_particles_time <<
           ", update shadows time: " << update_shadows_time <<
           ", slice get time: " << slice_get_time <<
           ", slice set time: " << slice_set_time <<
           ", update pos time: " << update_pos_time <<
           ", update velocity time: " << update_vel_time <<
           ", wait time: " << wait_time <<
           ", sync time: " << sync_time <<
           ", avg particles: " << np / static_cast<size_t>(num_of_iters) <<
           std::endl;

    std::cout << out.str();

    return 0;
}
