#pragma once

#include "didal/didal.h"
#include "didal/decomp/multi_block_decomposition.h"
#include "didal/distr/mesh_block_distribution.h"
#include "index.h"
#include "pic.h"
#include <iostream>

using namespace pic;

using MeshDecomposition = ddl::MultuBlockDecomposition<3>;
using MeshDistribution = ddl::MeshBlockDistribution<3>;
using BlockMap = std::map<Index3, int>;

using NeighMap = std::map<Index3, std::vector<Index3>>;

template <typename IndexType>
class BlockMesh : public ddl::SyncDistributedStorage<IndexType, CellBlock> {
public:
    BlockMesh(ddl::Environment *env, MeshParams m_params) :
        ddl::SyncDistributedStorage<IndexType, CellBlock>(env),
        mesh_params(m_params) {
        auto particle_add = [this](CellBlock &block, const std::vector<Particle> &particles) {
            block.addParticles(particles, this->mesh_params);
        };
        add_particles = this->template makeGetter<decltype(particle_add), void, const std::vector<Particle>&>(particle_add);
        extract_particles = this->template makeMethodGetter<decltype(&CellBlock::extractParticlesForExtent),
                std::vector<Particle>, const Extent3&>(&CellBlock::extractParticlesForExtent);
        using SliceType = DArray3(CellBlock::*)(CellBlock::FieldType, int, int, int, int, int, int, const std::array<bool, 3>&);
        get_slice = this->template makeMethodGetter<SliceType,
                DArray3, CellBlock::FieldType, int, int, int, int, int, int, const std::array<bool, 3> &>(static_cast<SliceType>(&CellBlock::getSlice));
    }

    std::future<void> addParticlesOn(ddl::Node node, const IndexType &index, const std::vector<Particle> &particles) {
        return add_particles(node, index, particles);
    }

    std::future<std::vector<Particle>> extractParticlesForExtentFrom(ddl::Node node, const IndexType &index, const Extent3 &extent) {
        return extract_particles(node, index, extent);
    }

    std::future<DArray3> getSliceFrom(ddl::Node node, const IndexType &index, CellBlock::FieldType field, int x, int y, int z, int n1, int n2, int n3,
                                      const std::array<bool, 3> &src_shadow) {
        return get_slice(node, index, field, x, y, z, n1, n2, n3, src_shadow);
    }

    const MeshParams& getParams() const {
        return mesh_params;
    }

private:
    MeshParams mesh_params;
    ddl::RemoteFunction<void, const IndexType&, const std::vector<Particle>&> add_particles;
    ddl::RemoteFunction<std::vector<Particle>, const IndexType&, const Extent3&> extract_particles;
    ddl::RemoteFunction<DArray3, const IndexType&, CellBlock::FieldType, int, int, int, int, int, int, const std::array<bool, 3>&> get_slice;
};

using Mesh3D = BlockMesh<Index3>;
