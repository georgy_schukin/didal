#include "pic_ops.h"

#include <random>
#include <cmath>

namespace pic {

namespace {

Vector3 getPerpendicular(const Vector3 &v) {
    const Vector3 p1 {v.z, v.z, -v.x - v.y};
    const Vector3 p2 {-v.y - v.z, v.x, v.x};
    const Vector3 p3 {v.y, -v.x - v.z, v.y};
    if (!p1.isZero()) {
        return p1;
    } else if (!p2.isZero()) {
        return p2;
    } else {
        return p3;
    }
}

Vector3 getCenter(const MeshParams &mesh_params) {
    const auto center = mesh_params.getExtent().getCenter();
    return Vector3 {center[0], center[1], center[2]};
}

Vector3 getCloudAxis(const PICParams &pic_params) {
    return Vector3 {pic_params.cloud_axis[0], pic_params.cloud_axis[1], pic_params.cloud_axis[2]}.normalized();
}

}

std::vector<Particle> createParticlesRandom(const PICParams &pic_params, const MeshParams &mesh_params) {
    std::vector<Particle> particles;
    const auto extent = mesh_params.getExtent();
    std::mt19937 re;
    std::uniform_real_distribution<double> dist(0, 1);
    for (int i = 0; i < pic_params.num_of_particles; i++) {        
        const Vector3 p {extent.start(0) + dist(re) * extent.size(0),
                         extent.start(1) + dist(re) * extent.size(1),
                         extent.start(2) + dist(re) * extent.size(2)};
        particles.push_back(Particle {p});
    }
    return particles;
}

std::vector<Particle> createParticlesBall(const PICParams &pic_params, const MeshParams &mesh_params) {
    std::vector<Particle> particles;
    const auto center = getCenter(mesh_params);
    std::mt19937 re;
    std::uniform_real_distribution<double> dist(-pic_params.cloud_radius, pic_params.cloud_radius);
    while (particles.size() < static_cast<size_t>(pic_params.num_of_particles)) {
        const Vector3 p {dist(re), dist(re), dist(re)};
        // Check that the coord is inside the ball
        if (p.lengthSquared() > pic_params.cloud_radius * pic_params.cloud_radius) {
            continue;
        }        
        particles.push_back(Particle {center + p});
    }
    return particles;
}

std::vector<Particle> createParticlesCircle(const PICParams &pic_params, const MeshParams &mesh_params) {
    std::vector<Particle> particles;
    const auto center = getCenter(mesh_params);
    const auto axis = getCloudAxis(pic_params);
    const auto perp = getPerpendicular(axis).normalized();
    std::mt19937 re;
    std::uniform_real_distribution<double> dist(0, 1);
    static const double PI = 3.141592653589793238463;
    while (particles.size() < static_cast<size_t>(pic_params.num_of_particles)) {
        const auto angle = dist(re) * 2 * PI;
        const auto radius = dist(re) * pic_params.cloud_radius;
        const auto vect = perp * radius;
        const auto s = std::sin(angle);
        const auto c = std::cos(angle);
        const auto rotated = vect * c + axis.cross(vect) * s + axis * axis.dot(vect) * (1.0 - c); // Rodrigues' rotation formula
        particles.push_back(Particle {center + rotated});
    }
    return particles;
}

void initParticlesVelocityRandom(std::vector<Particle> &particles, const PICParams &pic_params, const MeshParams &mesh_params) {
    std::mt19937 re;
    std::uniform_real_distribution<double> dist(-1, 1);
    for (auto &particle: particles) {
        particle.velocity = Vector3 {dist(re), dist(re), dist(re)} * pic_params.init_speed;
    }
}

void initParticlesVelocityRotation(std::vector<Particle> &particles, const PICParams &pic_params, const MeshParams &mesh_params) {
    const auto center = getCenter(mesh_params);
    const auto axis = getCloudAxis(pic_params);
    for (auto &particle: particles) {        
        const auto to_particle = particle.pos - center;
        const auto denom = axis.dot(axis);
        if (denom < 1e-12) {
            continue;
        }
        const auto t = axis.dot(to_particle) / denom;
        const auto left = to_particle - axis * t;
        particle.velocity = left.cross(axis).normalized() * pic_params.init_speed;
    }
}

std::vector<Particle> initParticlesPosition(const PICParams &pic_params, const MeshParams &mesh_params) {
    switch (pic_params.init_position) {
        case PICParams::INIT_POSITION_RANDOM:
            return createParticlesRandom(pic_params, mesh_params);
        case PICParams::INIT_POSITION_BALL:
            return createParticlesBall(pic_params, mesh_params);
        case PICParams::INIT_POSITION_CIRCLE:
            return createParticlesCircle(pic_params, mesh_params);
    }
    return std::vector<Particle> {};
}

void initParticlesVelocity(std::vector<Particle> &particles, const PICParams &pic_params, const MeshParams &mesh_params) {
    switch (pic_params.init_velocity) {
        case PICParams::INIT_VELOCITY_RANDOM:
            initParticlesVelocityRandom(particles, pic_params, mesh_params);
            break;
        case PICParams::INIT_VELOCITY_ROTATION:
            initParticlesVelocityRotation(particles, pic_params, mesh_params);
            break;
    }
}

std::vector<Particle> initParticles(const PICParams &pic_params, const MeshParams &mesh_params) {
    auto particles = initParticlesPosition(pic_params, mesh_params);
    initParticlesVelocity(particles, pic_params, mesh_params);
    return particles;
}

}
