#include "particle.h"

#include "didal/comm/serialize/serializing_ops.h"

namespace pic {

bool Particle::isIn(const Extent3 &extent) const {
    return extent.isIn(0, pos.x) &&
           extent.isIn(1, pos.y) &&
           extent.isIn(2, pos.z);
}

ddl::Writable& operator<< (ddl::Writable &w, const Particle &p) {
    return w << p.pos << p.velocity;
}

ddl::Readable& operator>> (ddl::Readable &r, Particle &p) {
    return r >> p.pos >> p.velocity;
}

}
