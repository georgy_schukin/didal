#pragma once

#include "didal/util/extent.h"

namespace pic {

using Extent2 = ddl::Extent<double, 2>;
using Extent3 = ddl::Extent<double, 3>;

}
