#pragma once

#include "didal/util/vector3.h"
#include "didal/util/extent.h"
#include "didal/comm/serialize/serializable.h"

namespace pic {

using Vector3 = ddl::Vector3;
using Extent3 = ddl::Extent<double, 3>;

class Particle {
public:
    Particle() {}
    Particle(const Vector3 &pos, const Vector3 &velocity = {0, 0, 0}) :
        pos(pos), velocity(velocity) {
    }

    bool isIn(const Extent3 &extent) const;

public:
    Vector3 pos;
    Vector3 velocity;
    Vector3 force;
};

ddl::Writable& operator<< (ddl::Writable &w, const Particle &p);
ddl::Readable& operator>> (ddl::Readable &r, Particle &p);

}
