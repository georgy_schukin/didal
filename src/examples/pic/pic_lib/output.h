#pragma once

#include "cell_block.h"
#include "index.h"
#include "params.h"

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <ostream>
#include <functional>

namespace pic {

using CellBlockArray = std::vector<std::shared_ptr<CellBlock>>;

class VTKOutput {
public:
    VTKOutput(int rank, int size, const MeshParams &params) :
        this_node(rank), num_of_nodes(size), mesh_params(params) {
    }

    void outputPiece(const std::string &path, OutputField field, int iter, const CellBlockArray &blocks);
    void outputParallel(const std::string &path, OutputField field, int iter);
    void outputNode(const std::string &path, OutputField field, int iter, const CellBlockArray &blocks);
    void outputNode(const std::string &path, const std::vector<OutputField> &fields, int iter, const CellBlockArray &blocks);
    void outputNodeAll(const std::string &path, int iter, const CellBlockArray &blocks);

private:
    using AccessFunc = std::function<const DArray3&(std::shared_ptr<CellBlock>)>;

private:
    void outputPieceParticles(std::ostream &out, const CellBlockArray &blocks);    
    void outputPieceArray(std::ostream &out, const CellBlockArray &blocks, const std::string &array_name, AccessFunc func);
    void outputPieceArray2D(std::ostream &out, const std::array<size_t, 3> &dims, const CellBlockArray &blocks, const std::string &array_name, AccessFunc func);

    void outputParallelParticles(std::ostream &out, const std::vector<std::string> &sources_files);   
    void outputParallelArray(std::ostream &out, const std::vector<std::string> &sources_files, const std::string &array_name);
    void outputParallelArray2D(std::ostream &out, const std::array<size_t, 3> &dims, const std::vector<std::string> &sources_files, const std::string &array_name);

    std::vector<std::string> getSourceFiles(const std::string &path, OutputField field, int iter);

private:
    int this_node;
    int num_of_nodes;
    MeshParams mesh_params;
};

}
