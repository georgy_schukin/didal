#pragma once

#include "didal/base/array_nd.h"

namespace pic {
    using DArray2 = ddl::ArrayND<double, 2>;
    using DArray3 = ddl::ArrayND<double, 3>;
}
