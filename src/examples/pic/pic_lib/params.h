#pragma once

#include "extent.h"
#include <string>
#include <array>

namespace pic {

enum OutputField {
    OUT_PARTICLES = 0,
    OUT_DENSITY,
    OUT_DENSITY_XY,
    OUT_DENSITY_YZ,
    OUT_DENSITY_XZ,
    OUT_POTENTIAL
};

struct PICParams {
    double delta_t = 1e-5; // time step

    std::string output;
    int output_step = 1;
    std::vector<OutputField> output_fields = {OUT_PARTICLES, OUT_DENSITY};

    bool periodic[3] = {false, false, false};

    enum InitPositionType {
        INIT_POSITION_RANDOM = 0,
        INIT_POSITION_BALL,
        INIT_POSITION_CIRCLE
    };

    enum InitVelocityType {
        INIT_VELOCITY_RANDOM = 0,
        INIT_VELOCITY_ROTATION
    };

    InitPositionType init_position = INIT_POSITION_RANDOM;
    InitVelocityType init_velocity = INIT_VELOCITY_RANDOM;

    double init_speed = 1.0;
    double min_mass = 0.1;
    double max_mass = 1.0;

    std::array<double, 3> cloud_axis = {0, 0, 1};

    //std::array<double, 3> init_scale = {0.9, 0.9, 0.9};
    //double init_shift[3] = {0.0, 0.0, 0.0};

    //double step_coeff = 1.0;

    int num_of_time_steps = 0;     // int : total number of time steps
    //int ntp;    // int : number of time steps between printing statistics (0 - no statistics)
    //int nts;    // int : number of time steps between saving density to files (0 - don't save)

    // Domain parameters
    int nx; // int : mesh size X
    int ny; // int : mesh size Y
    int nz; // int : mesh size Z

    // Domain fragmentation parameters
    //int nfx; // int : subdomains grid size X
    //int nfy; // int : subdomains grid size Y
    //int nfz; // int : subdomains grid size Z

    // Domain parameters
    std::array<double, 3> area_size = {1.0, 1.0, 1.0}; // real : domain size X, Y, Z
    std::array<double, 3> area_origin = {0, 0, 0};
    //real cx; // real : position of sun and center of particles X
    //real cy; // real : position of sun and center of particles Y
    //real cz; // real : position of sun and center of particles Z
    //real vx; // real : sun and center velocity X
    //real vy; // real : sun and center velocity Y
    //real vz; // real : sun and center velocity Z
    double hx; // real : mesh step X
    double hy; // real : mesh step Y
    double hz; // real : mesh step Z

    // Particles parameters
    int num_of_particles = 0;
    double particle_mass = 1.0; // real : single particle mass
    double particles_total_mass = 0.5;     // real : particles total mass

    double cloud_radius = 1.0; // real : initial particles distribution parameter: radius of a dust cloud

    //fill_shape_t    shape;    // FILL_SHAPE_CIRCLE_XY, FILL_SHAPE_CIRCLE_XZ, FILL_SHAPE_CIRCLE_YZ, FILL_SHAPE_BALL
    //fill_density_t  density;  // FILL_DENSITY_SQRT, FILL_DENSITY_EXP, FILL_DENSITY_LINE, FILL_DENSITY_CONST
    //fill_velocity_t velocity; // FILL_VELOCITY_SHAPE, FILL_VELOCITY_CIRCLE_XY, FILL_VELOCITY_CIRCLE_XZ, FILL_VELOCITY_CIRCLE_YZ, FILL_VELOCITY_BALL

    //int waves; // int : number of velocity dispersion waves (0 - none)
    //double dvr;  // real : initial particles distribution parameter: radius velocity dispersions (circle/ball shape)
    //double dvf;  // real : initial particles distribution parameter: angle velocity dispersions (circle/ball shape)
    //double dvz;  // real : initial particles distribution parameter: Z velocity dispersions (circle shape)

    // Other parameters
    double central_body_mass = 0.5; // real : central body mass

    // Poisson solver parameters
    double epsilon = 1e-8; // Poisson solver precision (stop threshold)
    double poisson_w = 1.0;   // Poisson solver acceleration parameter
    int max_poisson_iters = 10000;

    // Saving density to files
    //int sxy; // int : 1 - save density sum(Z) to file / 0 - don't save
    //int sxz; // int : 1 - save density sum(Y) to file / 0 - don't save
    //int syz; // int : 1 - save density sum(X) to file / 0 - don't save
    //int cxy; // int : 1 - save density cut of XY plane to file / 0 - don't save
    //int cxz; // int : 1 - save density cut of XZ plane to file / 0 - don't save
    //int cyz; // int : 1 - save density cut of YZ plane to file / 0 - don't save

    bool load(const std::string &filename);
};

struct MeshParams {
    std::array<int, 3> mesh_size; // num of cells in each dimension
    std::array<int, 3> num_blocks; // num of cell blocks in each dimension
    std::array<double, 3> area_size; // size of area
    std::array<double, 3> origin = {0, 0, 0}; // origin - coordinate of (0, 0, 0) local point
    std::array<double, 3> step; // cell size in each dimension
    std::array<bool, 3> periodic = {false, false, false};

    Extent3 getExtent() const;
};

}
