#include "cell_block.h"
#include "params.h"
//#include "neighbor.h"
#include "didal/base/array_nd_ops.h"

#include <map>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <algorithm>

namespace pic {

using Range2D = ddl::IndexRangeND<2>;
using Range3D = ddl::IndexRangeND<3>;

namespace {

void add(double &a, const double &b) {
    a += b;
}

ddl::Index<2, size_t> getDims(int dimension) {
    switch (dimension) {
        case 0: return {1, 2};
        case 1: return {0, 2};
        case 2: return {0, 1};
    }
    return {0, 1};
}

ddl::Index<2, size_t> getSize(int n1, int n2) {
    const auto s1 = static_cast<size_t>(n1);
    const auto s2 = static_cast<size_t>(n2);
    return {s1, s2};
}

ddl::Index<3, size_t> getSize(int n1, int n2, int n3) {
    const auto s1 = static_cast<size_t>(n1);
    const auto s2 = static_cast<size_t>(n2);
    const auto s3 = static_cast<size_t>(n3);
    return {s1, s2, s3};
}

ddl::Index<3> getStart(int x, int y, int z) {
    return {x, y, z};
}

}

CellBlock::CellBlock(CellBlock &&cb) :
    params(cb.params),
    force_x(std::move(cb.force_x)), force_y(std::move(cb.force_y)), force_z(std::move(cb.force_z)),
    density(std::move(cb.density)),
    density_partial(std::move(cb.density_partial)),
    potential(std::move(cb.potential)),
    potential_prev(std::move(cb.potential_prev)),
    particles(std::move(cb.particles)),
    block_mutex {} {
}

CellBlock::CellBlock(const CellBlockParams &params) :
    params(params) {
    initCells();
}

void CellBlock::setParams(const CellBlockParams &params) {
    this->params = params;
    initCells();
}

void CellBlock::initCells() {
    Range3D range_full {static_cast<size_t>(params.full_size[0]),
                        static_cast<size_t>(params.full_size[1]),
                        static_cast<size_t>(params.full_size[2])};
    force_x = DArray3 {range_full};
    force_y = DArray3 {range_full};
    force_z = DArray3 {range_full};
    density = DArray3 {range_full};
    density_partial = DArray3 {range_full};
    potential = DArray3 {range_full};
    potential_prev = DArray3 {range_full};
    /*cells = CellArray3(Range3D {static_cast<size_t>(params.full_size[0]),
                                static_cast<size_t>(params.full_size[1]),
                                static_cast<size_t>(params.full_size[2])});
    forEachCellInd([this](Cell &cell, const ddl::ArrayIndex<3> &index) {
        Index3 ind {static_cast<int>(index[0]),
                    static_cast<int>(index[1]),
                    static_cast<int>(index[2])};
        cell.setIndex(Index3 {params.shift[0] + ind[0] - params.shadow_start[0],
                              params.shift[1] + ind[1] - params.shadow_start[1],
                              params.shift[2] + ind[2] - params.shadow_start[2]});
        const auto neigh_type = NeighborIndices::getType(ind[0] > 0, ind[0] < params.full_size[0] - 1,
                                                         ind[1] > 0, ind[1] < params.full_size[1] - 1,
                                                         ind[2] > 0, ind[2] < params.full_size[2] - 1);
        cell.setNeighborIndicesType(neigh_type);
    });*/
}

Extent3 CellBlock::getExtent(const MeshParams &m_params) const {
    Extent3 extent;
    for (int i = 0; i < 3; i++) {
        extent.start(i) = m_params.origin[i] + params.shift[i] * m_params.step[i];
        extent.end(i) = extent.start(i) + params.size[i] * m_params.step[i];
    }
    return extent;
}

DArray3 CellBlock::getSlice(FieldType field, int x, int y, int z, int n1, int n2, int n3, bool include_shadow) {
    return getSlice(field, x, y, z, n1, n2, n3, {include_shadow, include_shadow, include_shadow});
}

DArray3 CellBlock::getSlice(FieldType field, int x, int y, int z, int n1, int n2, int n3, const std::array<bool, 3> &include_shadow) {
    x = include_shadow[0] ? toIndexFull(x, 0) : toIndex(x, 0);
    y = include_shadow[1] ? toIndexFull(y, 1) : toIndex(y, 1);
    z = include_shadow[2] ? toIndexFull(z, 2) : toIndex(z, 2);
    switch (field) {
        case FT_POTENTIAL: return getSliceOfData(potential, x, y, z, n1, n2, n3);
        case FT_DENSITY: return getSliceOfData(density, x, y, z, n1, n2, n3);
        case FT_DENSITY_PARTIAL: return getSliceOfData(density_partial, x, y, z, n1, n2, n3);
    }
    return DArray3();
}

void CellBlock::setSlice(FieldType field, int x, int y, int z, const DArray3 &slice, bool include_shadow) {
    x = include_shadow ? toIndexFull(x, 0) : toIndex(x, 0);
    y = include_shadow ? toIndexFull(y, 1) : toIndex(y, 1);
    z = include_shadow ? toIndexFull(z, 2) : toIndex(z, 2);
    switch (field) {
        case FT_POTENTIAL: setSliceOfData(potential, x, y, z, slice); break;
        case FT_DENSITY: setSliceOfData(density, x, y, z, slice); break;
        case FT_DENSITY_PARTIAL: setSliceOfData(density_partial, x, y, z, slice); break;
    }
}

void CellBlock::addSlice(FieldType field, int x, int y, int z, const DArray3 &slice, bool include_shadow) {
    x = include_shadow ? toIndexFull(x, 0) : toIndex(x, 0);
    y = include_shadow ? toIndexFull(y, 1) : toIndex(y, 1);
    z = include_shadow ? toIndexFull(z, 2) : toIndex(z, 2);
    switch (field) {
        case FT_POTENTIAL: addSliceOfData(potential, x, y, z, slice); break;
        case FT_DENSITY: addSliceOfData(density, x, y, z, slice); break;
        case FT_DENSITY_PARTIAL: addSliceOfData(density_partial, x, y, z, slice); break;
    }
}

DArray3 CellBlock::getSliceOfData(const DArray3 &data, int x, int y, int z, int n1, int n2, int n3) {
    //std::unique_lock<std::mutex> lock(block_mutex);
    return ddl::get_slice_3d(data, getSize(n1, n2, n3), getStart(x, y, z));
}

void CellBlock::setSliceOfData(DArray3 &data, int x, int y, int z, const DArray3 &slice) {
    //std::unique_lock<std::mutex> lock(block_mutex);
    ddl::set_slice_3d(data, slice, getStart(x, y, z));
}

void CellBlock::addSliceOfData(DArray3 &data, int x, int y, int z, const DArray3 &slice) {
    //std::unique_lock<std::mutex> lock(block_mutex);
    ddl::slice_func_3d(data, slice, getStart(x, y, z), &add);
}

std::vector<Particle> CellBlock::extractAndMoveOutParticles(const MeshParams &mesh_params) {
    std::unique_lock<std::mutex> lock(block_mutex);
    std::vector<Particle> out_particles;
    std::vector<Particle> in_particles;
    const auto extent = getExtent(mesh_params);
    for (const auto &particle: particles) {
        if (particle.isIn(extent)) {
            in_particles.push_back(particle);
        } else {
            out_particles.push_back(particle);
        }
    }
    particles = std::move(in_particles);
    return out_particles;
}

std::vector<Particle> CellBlock::extractParticlesForExtent(const Extent3 &extent) {
    std::vector<Particle> out_particles;
    /*forEachCell([&extent, &out_particles](Cell &cell) {
        const auto out_ps = cell.extractOutParticlesForExtent(extent);
        out_particles.insert(out_particles.end(), out_ps.begin(), out_ps.end());
    });*/
    return out_particles;
}

void CellBlock::addParticles(const std::vector<Particle> &particles, const MeshParams &mesh_params) {
    std::unique_lock<std::mutex> lock(block_mutex);
    this->particles.insert(this->particles.end(), particles.begin(), particles.end());
}

size_t CellBlock::getNumOfParticles() const {
    std::unique_lock<std::mutex> lock(block_mutex);
    return particles.size();
}

int CellBlock::toIndex(int index, int dim) const {
    return index >= 0 ? index + params.shadow_start[dim] : params.full_size[dim] - params.shadow_end[dim] + index; // negative indices count from end backwards
}

int CellBlock::toIndexFull(int index, int dim) const {
    return index >= 0 ? index : params.full_size[dim] + index; // negative indices count from end backwards
}

int CellBlock::toLocalIndexFull(int global_index, int dim) const {
    return global_index - params.full_shift[dim];
}

int CellBlock::getLocalIndexFullSide(const double &global_rel, int dim, double &local_rel) const {
    auto index = static_cast<int>(std::floor(global_rel));
    local_rel = global_rel - index;
    return toLocalIndexFull(index, dim);
}

int CellBlock::getLocalIndexFullCenter(const double &global_rel, int dim, double &local_rel) const {
    return getLocalIndexFullSide(global_rel - 0.5, dim, local_rel);
}

void CellBlock::calcParticlesForce(const PICParams &pic_params, const MeshParams &mesh_params) {
    forEachParticle([this, &mesh_params](Particle &particle) {
        particle.force = forceForParticle(particle, mesh_params);
    });
}

Vector3 CellBlock::forceForParticle(const Particle &particle, const MeshParams &mesh_params) const {
    // вычисление координат ячейки, в которой находится частица
//          real xb = x/hx;         // глобальный номер ячейки от 0   (от границы ячейки)
//          real yb = y/hy;
//          real zb = z/hz;
//          real xa = xb - 0.5;     // глобальный номер ячейки от 0.5 (от середины ячейки)
//          real ya = yb - 0.5;
//          real za = zb - 0.5;
//          int ib = floor(xb); xb -= ib; ib = ig2l(ib,sx);
//          int kb = floor(yb); yb -= kb; kb = kg2l(kb,sy);
//          int lb = floor(zb); zb -= lb; lb = lg2l(lb,sz);
//          int ia = floor(xa); xa -= ia; ia = ig2l(ia,sx);
//          int ka = floor(ya); ya -= ka; ka = kg2l(ka,sy);
//          int la = floor(za); za -= la; la = lg2l(la,sz);
//          // вычисление компонентов сил гравитации
//          *rfx = (1-xb)*((1-ya)*((1-za)*(*fx)[ib  ][ka  ][la]+za*(*fx)[ib  ][ka  ][la+1])+
//                            ya *((1-za)*(*fx)[ib  ][ka+1][la]+za*(*fx)[ib  ][ka+1][la+1]))+
//                    xb *((1-ya)*((1-za)*(*fx)[ib+1][ka  ][la]+za*(*fx)[ib+1][ka  ][la+1])+
//                            ya *((1-za)*(*fx)[ib+1][ka+1][la]+za*(*fx)[ib+1][ka+1][la+1]));
//          *rfy = (1-xa)*((1-yb)*((1-za)*(*fy)[ia  ][kb  ][la]+za*(*fy)[ia  ][kb  ][la+1])+
//                            yb *((1-za)*(*fy)[ia  ][kb+1][la]+za*(*fy)[ia  ][kb+1][la+1]))+
//                    xa *((1-yb)*((1-za)*(*fy)[ia+1][kb  ][la]+za*(*fy)[ia+1][kb  ][la+1])+
//                            yb *((1-za)*(*fy)[ia+1][kb+1][la]+za*(*fy)[ia+1][kb+1][la+1]));
//          *rfz = (1-xa)*((1-ya)*((1-zb)*(*fz)[ia  ][ka  ][lb]+zb*(*fz)[ia  ][ka  ][lb+1])+
//                            ya *((1-zb)*(*fz)[ia  ][ka+1][lb]+zb*(*fz)[ia  ][ka+1][lb+1]))+
//                    xa *((1-ya)*((1-zb)*(*fz)[ia+1][ka  ][lb]+zb*(*fz)[ia+1][ka  ][lb+1])+
//                            ya *((1-zb)*(*fz)[ia+1][ka+1][lb]+zb*(*fz)[ia+1][ka+1][lb+1]));

    const auto rx = particle.pos.x / mesh_params.step[0];
    const auto ry = particle.pos.y / mesh_params.step[1];
    const auto rz = particle.pos.z / mesh_params.step[2];
    double xa, ya, za, xb, yb, zb;
    const auto ia = getLocalIndexFullCenter(rx, 0, xa);
    const auto ja = getLocalIndexFullCenter(ry, 1, ya);
    const auto ka = getLocalIndexFullCenter(rz, 2, za);
    const auto ib = getLocalIndexFullSide(rx, 0, xb);
    const auto jb = getLocalIndexFullSide(ry, 1, yb);
    const auto kb = getLocalIndexFullSide(rz, 2, zb);
    const auto nxb = 1.0 - xb;
    const auto nyb = 1.0 - yb;
    const auto nzb = 1.0 - zb;
    const auto nxa = 1.0 - xa;
    const auto nya = 1.0 - ya;
    const auto nza = 1.0 - za;
    Vector3 force;
    force.x = nxb * (nya * (nza * force_x(ib, ja, ka) + za * force_x(ib, ja, ka + 1)) +
                     ya *(nza * force_x(ib, ja + 1, ka) + za * force_x(ib, ja + 1, ka + 1))) +
              xb * (nya * (nza * force_x(ib + 1, ja, ka) + za * force_x(ib + 1, ja, ka + 1)) +
                    ya *(nza * force_x(ib + 1, ja + 1, ka) + za * force_x(ib + 1, ja + 1, ka + 1)));
    force.y = nxa * (nyb * (nza * force_y(ia, jb, ka) + za * force_y(ia, jb, ka+1)) +
                     yb *(nza * force_y(ia, jb + 1, ka) + za * force_y(ia, jb + 1, ka + 1))) +
              xa * (nyb * (nza * force_y(ia + 1, jb, ka) + za * force_y(ia + 1, jb, ka + 1)) +
                    yb * (nza * force_y(ia + 1, jb + 1, ka) + za  *force_y(ia + 1, jb + 1, ka + 1)));
    force.z = nxa * (nya * (nzb * force_z(ia, ja, kb) + zb * force_z(ia, ja, kb + 1)) +
                     ya *(nzb * force_z(ia, ja + 1, kb) + zb * force_z(ia, ja + 1, kb + 1))) +
              xa * (nya * (nzb * force_z(ia + 1, ja, kb) + zb * force_z(ia + 1, ja, kb + 1)) +
                    ya * (nzb * force_z(ia + 1, ja + 1, kb) + zb * force_z(ia + 1, ja + 1, kb + 1)));
    return force;
}

void CellBlock::updateParticlesVelocityAndPosition(const PICParams &pic_params, const MeshParams &mesh_params) {
    forEachParticle([&pic_params](Particle &particle) {
        particle.velocity += particle.force * (pic_params.delta_t / pic_params.particle_mass);
        particle.pos += particle.velocity * pic_params.delta_t;
    });
}

void CellBlock::calcDensity(const PICParams &pic_params, const MeshParams &mesh_params) {
//    for (int i=0;i<nx;i++)
//      for (int k=0;k<ny;k++)
//        for (int l=0;l<nz;l++)
//          (*ro)[i][k][l] = 0;

//    for (particle_count_t j=0;j<np;j++)
//    { real rx = p[j].x/hx - 0.5; int i = floor(rx); rx = rx - i; i = ig2l(i,sx);
//      real ry = p[j].y/hy - 0.5; int k = floor(ry); ry = ry - k; k = kg2l(k,sy);
//      real rz = p[j].z/hz - 0.5; int l = floor(rz); rz = rz - l; l = lg2l(l,sz);
//      (*ro)[i  ][k  ][l  ] += (1-rx)*(1-ry)*(1-rz);
//      (*ro)[i  ][k  ][l+1] += (1-rx)*(1-ry)*   rz ;
//      (*ro)[i  ][k+1][l  ] += (1-rx)*   ry *(1-rz);
//      (*ro)[i  ][k+1][l+1] += (1-rx)*   ry *   rz ;
//      (*ro)[i+1][k  ][l  ] +=    rx *(1-ry)*(1-rz);
//      (*ro)[i+1][k  ][l+1] +=    rx *(1-ry)*   rz ;
//      (*ro)[i+1][k+1][l  ] +=    rx *   ry *(1-rz);
//      (*ro)[i+1][k+1][l+1] +=    rx *   ry *   rz ;
//    }

//    real s = am/(hx*hy*hz);

//    for (int i=0;i<nx;i++)
//      for (int k=0;k<ny;k++)
//        for (int l=0;l<nz;l++)
//          (*ro)[i][k][l] *= s;

    std::fill(density.begin(), density.end(), 0); // zero dencity
    for (const auto &particle: particles) {
        double rx, ry, rz;
        const auto i = getLocalIndexFullCenter(particle.pos.x / mesh_params.step[0], 0, rx);
        const auto j = getLocalIndexFullCenter(particle.pos.y / mesh_params.step[1], 1, ry);
        const auto k = getLocalIndexFullCenter(particle.pos.z / mesh_params.step[2], 2, rz);
        const auto nrx = 1.0 - rx;
        const auto nry = 1.0 - ry;
        const auto nrz = 1.0 - rz;
        density(i, j, k) += nrx * nry * nrz;
        density(i, j, k + 1) += nrx * nry * rz;
        density(i, j + 1, k) += nrx * ry * nrz;
        density(i, j + 1, k + 1) += nrx * ry * rz;
        density(i + 1, j, k) += rx * nry * nrz;
        density(i + 1, j, k + 1) += rx * nry * rz;
        density(i + 1, j + 1, k) += rx * ry * nrz;
        density(i + 1, j + 1, k + 1) += rx * ry * rz;
    }
    const auto s = pic_params.particle_mass / (mesh_params.step[0] * mesh_params.step[1] * mesh_params.step[2]);
    for (auto &d: density) {
        d *= s;
    }
    density_partial = density; // remember input from our particles as partial density
}

void CellBlock::addPotentialFromCentralBody(const PICParams &pic_params, const MeshParams &mesh_params) {
    if (pic_params.central_body_mass < 1e-12) {
        return;
    }
    const auto center = mesh_params.getExtent().getCenter();
    const auto nx = params.full_size[0];
    const auto ny = params.full_size[1];
    const auto nz = params.full_size[2];
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            for (int k = 0; k < nz; k++) {
                const auto dist = distanceToCenter({i, j, k}, center, mesh_params);
                if (dist > 1e-12) {
                    potential(i, j, k) -= pic_params.central_body_mass / dist;
                }
            }
        }
    }
}

void CellBlock::calcPotentialInitWithValue(double value) {
    std::fill(potential.begin(), potential.end(), value);
}

void CellBlock::calcPotentialInitFromPrev() {
    potential = potential_prev;
}

void CellBlock::calcPotentialCopyIntoPrev() {
    potential_prev = potential;
}

void CellBlock::calcPotentialInitBoundary(const PICParams &pic_params, const MeshParams &mesh_params) {
    const auto nx = params.full_size[0];
    const auto ny = params.full_size[1];
    const auto nz = params.full_size[2];
    const auto pm = pic_params.particles_total_mass;
    const auto center = mesh_params.getExtent().getCenter();

    //    if (ifx == 0)
//    { real x = -0.5*hx - cx;
//      real x2 = x*x;
//      for (int k=0;k<ny;k++)
//      { real y = (kl2g(k,sy)+0.5)*hy - cy;
//        real x2py2 = x2 + y*y;
//        for (int l=0;l<nz;l++)
//        { real z = (ll2g(l,sz)+0.5)*hz - cz;
//          real r = sqrt(x2py2 + z*z);
//          (*fi)[0][k][l] = -pm/r;
//        }
//      }
//    }

    if (params.index[0] == 0) {
        for (int j = 0; j < ny; j++) {
            for (int k = 0; k < nz; k++) {
                const auto r = distanceToCenter({0, j, k}, center, mesh_params);
                potential(0, j, k) = -pm / r;
            }
        }
    }

//    if (ifx == nfx-1)
//    { real x = (il2g(nx-1,sx)+0.5)*hx - cx;
//      real x2 = x*x;
//      for (int k=0;k<ny;k++)
//      { real y = (kl2g(k,sy)+0.5)*hy - cy;
//        real x2py2 = x2+y*y;
//        for (int l=0;l<nz;l++)
//        { real z = (ll2g(l,sz)+0.5)*hz - cz;
//          real r = sqrt(x2py2 + z*z);
//          (*fi)[nx-1][k][l] = -pm/r;
//        }
//      }
//    }

    if (params.index[0] == mesh_params.num_blocks[0] - 1) {
        for (int j = 0; j < ny; j++) {
            for (int k = 0; k < nz; k++) {
                const auto r = distanceToCenter({nx - 1, j, k}, center, mesh_params);
                potential(nx - 1, j, k) = -pm / r;
            }
        }
    }

//    if (ify == 0)
//    { real y = -0.5*hy - cy;
//      real y2 = y*y;
//      for (int i=0;i<nx;i++)
//      { real x = (il2g(i,sx)+0.5)*hx - cx;
//        real x2py2 = x*x+y2;
//        for (int l=0;l<nz;l++)
//        { real z = (ll2g(l,sz)+0.5)*hz - cz;
//          real r = sqrt(x2py2 + z*z);
//          (*fi)[i][0][l] = -pm/r;
//        }
//      }
//    }

    if (params.index[1] == 0) {
        for (int i = 0; i < nx; i++) {
            for (int k = 0; k < nz; k++) {
                const auto r = distanceToCenter({i, 0, k}, center, mesh_params);
                potential(i, 0, k) = -pm / r;
            }
        }
    }

//    if (ify == nfy-1)
//    { real y = (kl2g(ny-1,sy)+0.5)*hy - cy;
//      real y2 = y*y;
//      for (int i=0;i<nx;i++)
//      { real x = (il2g(i,sx)+0.5)*hx - cx;
//        real x2py2 = x*x + y2;
//        for (int l=0;l<nz;l++)
//        { real z = (ll2g(l,sz)+0.5)*hz - cz;
//          real r = sqrt(x2py2 + z*z);
//          (*fi)[i][ny-1][l] = -pm/r;
//        }
//      }
//    }

    if (params.index[1] == mesh_params.num_blocks[1] - 1) {
        for (int i = 0; i < nx; i++) {
            for (int k = 0; k < nz; k++) {
                const auto r = distanceToCenter({i, ny - 1, k}, center, mesh_params);
                potential(i, ny - 1, k) = -pm / r;
            }
        }
    }


//    if (ifz == 0)
//    { real z = -0.5*hz - cz;
//      real z2 = z*z;
//      for (int i=0;i<nx;i++)
//      { real x = (il2g(i,sx)+0.5)*hx - cx;
//        real x2pz2 = x*x + z2;
//        for (int k=0;k<ny;k++)
//        { real y=(kl2g(k,sy)+0.5)*hy - cy;
//          real r = sqrt(x2pz2 + y*y);
//          (*fi)[i][k][0] = -pm/r;
//        }
//      }
//    }

    if (params.index[2] == 0) {
        for (int i = 0; i < nx; i++) {
            for (int j = 0; j < ny; j++) {
                const auto r = distanceToCenter({i, j, 0}, center, mesh_params);
                potential(i, j, 0) = -pm / r;
            }
        }
    }


//    if (ifz == nfz-1)
//    { real z = (ll2g(nz-1,sz)+0.5)*hz - cz;
//      real z2 = z*z;
//      for (int i=0;i<nx;i++)
//      { real x = (il2g(i,sx)+0.5)*hx - cx;
//        real x2pz2 = x*x + z2;
//        for (int k=0;k<ny;k++)
//        { real y = (kl2g(k,sy)+0.5)*hy - cy;
//          real r = sqrt(x2pz2 + y*y);
//          (*fi)[i][k][nz-1] = -pm/r;
//        }
//      }
//    }

    if (params.index[2] == mesh_params.num_blocks[2] - 1) {
        for (int i = 0; i < nx; i++) {
            for (int j = 0; j < ny; j++) {
                const auto r = distanceToCenter({i, j, nz - 1}, center, mesh_params);
                potential(i, j, nz - 1) = -pm / r;
            }
        }
    }
}

double CellBlock::distanceToCenter(const Index3 &cell_index, const std::array<double, 3> &center, const MeshParams &mesh_params) const {
    const auto x = (cell_index[0] + params.full_shift[0] + 0.5) * mesh_params.step[0] - center[0];
    const auto y = (cell_index[1] + params.full_shift[1] + 0.5) * mesh_params.step[1] - center[1];
    const auto z = (cell_index[2] + params.full_shift[2] + 0.5) * mesh_params.step[2] - center[2];
    return std::sqrt(x * x + y * y + z * z);
}

double CellBlock::calcPotentialIterate(const PICParams &pic_params, const MeshParams &mesh_params) {
//    const real rhx2 = 1.0/(hx*hx),
//               rhy2 = 1.0/(hy*hy),
//               rhz2 = 1.0/(hz*hz),
//               a = 0.5*wp/(rhx2+rhy2+rhz2);
//    real dmax = 0.0;
//    for (int i=1;i<nx-1;i++)
//      for (int k=1;k<ny-1;k++)
//        for (int l=1;l<nz-1;l++)
//        { real s = a*(((*fi)[i-1][k][l]+(*fi)[i+1][k][l])*rhx2
//                     +((*fi)[i][k-1][l]+(*fi)[i][k+1][l])*rhy2
//                     +((*fi)[i][k][l-1]+(*fi)[i][k][l+1])*rhz2
//                     -(*ro)[i][k][l]*crhs
//                     ) + (1.0-wp)*(*fi)[i][k][l];
//          real ds = fabs((*fi)[i][k][l] - s);
//          if (ds > dmax) dmax = ds;
//          (*finew)[i][k][l] = s;
//        }
//    *delta = dmax;
//    // copy border values to the new fragment
//    if (ifx ==     0) for (int k=0;k<ny;k++) for (int l=0;l<nz;l++) (*finew)[   0][k][l] = (*fi)[   0][k][l];
//    if (ifx == nfx-1) for (int k=0;k<ny;k++) for (int l=0;l<nz;l++) (*finew)[nx-1][k][l] = (*fi)[nx-1][k][l];
//    if (ify ==     0) for (int i=0;i<nx;i++) for (int l=0;l<nz;l++) (*finew)[i][   0][l] = (*fi)[i][   0][l];
//    if (ify == nfy-1) for (int i=0;i<nx;i++) for (int l=0;l<nz;l++) (*finew)[i][ny-1][l] = (*fi)[i][ny-1][l];
//    if (ifz ==     0) for (int i=0;i<nx;i++) for (int k=0;k<ny;k++) (*finew)[i][k][   0] = (*fi)[i][k][   0];
//    if (ifz == nfz-1) for (int i=0;i<nx;i++) for (int k=0;k<ny;k++) (*finew)[i][k][nz-1] = (*fi)[i][k][nz-1];
    static const double PI = 3.141592653589793238463;
    potential_prev = potential; // copy potential into potential_prev
    const auto nx = params.full_size[0];
    const auto ny = params.full_size[1];
    const auto nz = params.full_size[2];
    const auto hx = mesh_params.step[0];
    const auto hy = mesh_params.step[1];
    const auto hz = mesh_params.step[2];
    const auto rhx2 = 1.0 / (hx * hx);
    const auto rhy2 = 1.0 / (hy * hy);
    const auto rhz2 = 1.0 / (hz * hz);
    const auto a = 0.5 * pic_params.poisson_w / (rhx2 + rhy2 + rhz2);
    const auto crhs = 4.0 * PI;
    double d_max = 0.0;
    for (int i = 1; i < nx - 1; i++)
    for (int j = 1; j < ny - 1; j++)
    for (int k = 1; k < nz - 1; k++) {
        auto s = a *(rhx2 * (potential_prev(i - 1, j, k) + potential_prev(i + 1, j, k)) +
                     rhy2 * (potential_prev(i, j - 1, k) + potential_prev(i, j + 1, k)) +
                     rhz2 * (potential_prev(i, j, k - 1) + potential_prev(i, j, k + 1)) -
                     crhs * density(i, j, k)) +
                (1.0 - pic_params.poisson_w) * potential_prev(i, j, k);
        const auto ds = std::abs(potential_prev(i, j, k) - s);
        d_max = std::max(d_max, ds);
        potential(i, j, k) = s;
    }
    return d_max;
}

void CellBlock::calcForces(const PICParams &pic_params, const MeshParams &mesh_params) {
//    for (int i=1;i<nx;i++)
//       for (int k=1;k<ny;k++)
//         for (int l=1;l<nz;l++)
//         { (*fx)[i][k][l] = ((*fi)[i-1][k  ][l  ]-(*fi)[i][k][l])/hx;
//           (*fy)[i][k][l] = ((*fi)[i  ][k-1][l  ]-(*fi)[i][k][l])/hy;
//           (*fz)[i][k][l] = ((*fi)[i  ][k  ][l-1]-(*fi)[i][k][l])/hz;
//         }

//     for (int k=1;k<ny;k++)
//       for (int l=1;l<nz;l++)
//       { (*fy)[0][k][l] = ((*fi)[0][k-1][l  ]-(*fi)[0][k][l])/hy;
//         (*fz)[0][k][l] = ((*fi)[0][k  ][l-1]-(*fi)[0][k][l])/hz;
//       }

//     for (int i=1;i<nx;i++)
//       for (int l=1;l<nz;l++)
//       { (*fx)[i][0][l] = ((*fi)[i-1][0][l  ]-(*fi)[i][0][l])/hx;
//         (*fz)[i][0][l] = ((*fi)[i  ][0][l-1]-(*fi)[i][0][l])/hz;
//       }

//     for (int i=1;i<nx;i++)
//       for (int k=1;k<ny;k++)
//       { (*fx)[i][k][0] = ((*fi)[i-1][k  ][0]-(*fi)[i][k][0])/hx;
//         (*fy)[i][k][0] = ((*fi)[i  ][k-1][0]-(*fi)[i][k][0])/hy;
//       }

//     for (int i=1;i<nx;i++)
//       (*fx)[i][0][0] = ((*fi)[i-1][0][0]-(*fi)[i][0][0])/hx;

//     for (int k=1;k<ny;k++)
//       (*fy)[0][k][0] = ((*fi)[0][k-1][0]-(*fi)[0][k][0])/hy;

//     for (int l=1;l<nz;l++)
//       (*fz)[0][0][l] = ((*fi)[0][0][l-1]-(*fi)[0][0][l])/hz;
    const auto nx = params.full_size[0];
    const auto ny = params.full_size[1];
    const auto nz = params.full_size[2];
    const auto hx = mesh_params.step[0];
    const auto hy = mesh_params.step[1];
    const auto hz = mesh_params.step[2];

    for (int i = 1; i < nx; i++)
    for (int j = 1; j < ny; j++)
    for (int k = 1; k < nz; k++) {
        force_x(i, j, k) = (potential(i - 1, j, k) - potential(i, j, k)) / hx;
        force_y(i, j, k) = (potential(i, j - 1, k) - potential(i, j, k)) / hy;
        force_z(i, j, k) = (potential(i, j, k - 1) - potential(i, j, k)) / hz;
    }

    for (int j = 1; j < ny; j++)
    for (int k = 1; k < nz; k++) {
        force_y(0, j, k) = (potential(0, j - 1, k) - potential(0, j, k)) / hy;
        force_z(0, j, k) = (potential(0, j, k - 1) - potential(0, j, k)) / hz;
    }

    for (int i = 1; i < nx; i++)
    for (int k = 1; k < nz; k++) {
        force_y(i, 0, k) = (potential(i - 1, 0, k) - potential(i, 0, k)) / hy;
        force_z(i, 0, k) = (potential(i, 0, k - 1) - potential(i, 0, k)) / hz;
    }

    for (int i = 1; i < nx; i++)
    for (int j = 1; j < ny; j++) {
        force_x(i, j, 0) = (potential(i - 1, j, 0) - potential(i, j, 0)) / hx;
        force_y(i, j, 0) = (potential(i, j - 1, 0) - potential(i, j, 0)) / hy;
    }

    for (int i = 1; i < nx; i++) {
        force_x(i, 0, 0) = (potential(i - 1, 0, 0) - potential(i, 0, 0)) / hx;
    }

    for (int j = 1; j < ny; j++) {
        force_y(0, j, 0) = (potential(0, j - 1, 0) - potential(0, j, 0)) / hy;
    }

    for (int k = 1; k < nz; k++) {
        force_z(0, 0, k) = (potential(0, 0, k - 1) - potential(0, 0, k)) / hz;
    }
}

ddl::Writable& operator<< (ddl::Writable &w, const CellBlock &cb) {
    return w << cb.params <<
                cb.force_x << cb.force_y << cb.force_z <<
                cb.density << cb.potential << cb.potential_prev << cb.particles;
}

ddl::Readable& operator>> (ddl::Readable &r, CellBlock &cb) {
    return r >> cb.params >>
                cb.force_x >> cb.force_y >> cb.force_z >>
                cb.density >> cb.potential >> cb.potential_prev >> cb.particles;
}

}
