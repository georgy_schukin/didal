#include "params.h"

#include <fstream>
#include <sstream>
#include <iostream>

namespace pic {

namespace {
    std::vector<std::string> readList(std::istream &is) {
        std::vector<std::string> values;
        std::string value;
        is >> value;
        if (value == "[") {
            while (value != "]") {
                is >> value;
                if (value != "]") {
                    values.push_back(value);
                }
            }
        } else {
            values.push_back(value);
        }
        return values;
    }

    template <typename Map>
    typename Map::mapped_type getMappedValue(std::istream &is, const Map &m, const std::string key_name = "") {
        typename Map::key_type key;
        is >> key;
        auto it = m.find(key);
        if (it == m.end()) {
            std::ostringstream out;
            out << "Unknown " << key_name << ": " << key;
            throw std::runtime_error(out.str());
        }
        return it->second;
    }

    template <typename Map>
    std::vector<typename Map::mapped_type> getMappedValues(std::istream &is, const Map &m, const std::string key_name = "") {
        std::vector<typename Map::mapped_type> values;
        const auto keys = readList(is);
        for (const auto &key: keys) {
            auto it = m.find(key);
            if (it == m.end()) {
                std::ostringstream out;
                out << "Unknown " << key_name << ": " << key;
                throw std::runtime_error(out.str());
            }
            values.push_back(it->second);
        }
        return values;
    }
}

bool PICParams::load(const std::string &filename) {
    static const std::map<std::string, std::pair<InitPositionType, InitVelocityType>> init_types = {
        {"random", {INIT_POSITION_RANDOM, INIT_VELOCITY_RANDOM}},
        {"ball", {INIT_POSITION_BALL, INIT_VELOCITY_RANDOM}}
    };
    static const std::map<std::string, InitPositionType> init_pos_types = {
        {"random", INIT_POSITION_RANDOM},
        {"ball", INIT_POSITION_BALL},
        {"circle", INIT_POSITION_CIRCLE}
    };
    static const std::map<std::string, InitVelocityType> init_vel_types = {
        {"random", INIT_VELOCITY_RANDOM},
        {"rotation", INIT_VELOCITY_ROTATION}
    };
    static const std::map<std::string, OutputField> output_field_types = {
        {"particles", OUT_PARTICLES},
        {"density", OUT_DENSITY},
        {"density_xy", OUT_DENSITY_XY},
        {"density_xz", OUT_DENSITY_XZ},
        {"density_yz", OUT_DENSITY_YZ},
        {"potential", OUT_POTENTIAL}
    };
    std::ifstream in(filename.c_str());
    if (!in.is_open()) {
        return false;
    }
    while (!in.eof()) {
        std::string tag;
        in >> tag;
        if (tag.empty()) {
            continue;
        }
        if (tag == "hx") {
            in >> hx;
        } else if (tag == "hy") {
            in >> hy;
        } else if (tag == "hz") {
            in >> hz;
        } else if (tag == "h" || tag == "step") {
            in >> hx >> hy >> hz;
        } else if (tag == "area_size") {
            in >> area_size[0] >> area_size[1] >> area_size[2];
        } else if (tag == "area_origin") {
            in >> area_origin[0] >> area_origin[1] >> area_origin[2];
        } else if (tag == "particle_mass") {
            in >> particle_mass;
        } else if (tag == "particles_total_mass" || tag == "total_mass") {
            in >> particles_total_mass;
        } else if (tag == "central_body_mass" || tag == "central_mass") {
            in >> central_body_mass;
        } else if (tag == "delta_t") {
            in >> delta_t;
        } else if (tag == "init") {
            const auto p = getMappedValue(in, init_types, "init type");
            init_position = p.first;
            init_velocity = p.second;
        } else if (tag == "init_pos" || tag == "init_position") {
            init_position = getMappedValue(in, init_pos_types, "init position type");
        } else if (tag == "init_vel" || tag == "init_velocity") {
            init_velocity = getMappedValue(in, init_vel_types, "init velocity type");
        } else if (tag == "init_speed") {
            in >> init_speed;
        } else if (tag == "cloud_axis") {
            in >> cloud_axis[0] >> cloud_axis[1] >> cloud_axis[2];
        } else if (tag == "time_steps") {
            in >> num_of_time_steps;
        } else if (tag == "particles") {
            in >> num_of_particles;
        } else if (tag == "cloud_radius") {
            in >> cloud_radius;
        } else if (tag == "eps" || tag == "epsilon" || tag == "poisson_epsilon") {
            in >> epsilon;
        } else if (tag == "poisson_max_iters") {
            in >> max_poisson_iters;
        } else if (tag == "output") {
            in >> output;
        } else if (tag == "output_step") {
            in >> output_step;
        } else if (tag == "output_fields") {
            output_fields = getMappedValues(in, output_field_types, "output field type");
        } else{
            std::cerr << "Unknown tag: " << tag << std::endl;
        }
    }
    return true;
}

Extent3 MeshParams::getExtent() const {
    Extent3 extent;
    for (int i = 0; i < 3; i++) {
        extent.start(i) = origin[i];
        extent.end(i) = extent.start(i) + mesh_size[i] * step[i];
    }
    return extent;
}

}
