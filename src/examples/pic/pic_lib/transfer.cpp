#include "transfer.h"
#include "neighbor.h"
#include "cell_block.h"
#include "params.h"

namespace pic {

namespace {

template <typename MapType>
MapType selectDisplTransfers(const MapType &displ_transfers, const NeighborIndices &neigh_indices, int neigh_indices_type) {
    MapType selected;
    for (const auto &nd: neigh_indices.getNeighborDisplacements(neigh_indices_type)) {
        auto it = displ_transfers.find(nd);
        if (it != displ_transfers.end()) {
            selected.insert(*it);
        }
    }
    return selected;
}

std::map<Index3, InputTransfer> getDisplInputTransfers(const CellBlockParams &params, const NeighborIndices &neigh_indices, bool src_shadow) {
    static const int NEXT = 1;
    static const int PREV = -1;
    static const int START = 0;
    static const int END = -1;
    static const int S_PREV = END;
    static const int S_NEXT = START;
    static const int D_PREV = START;
    static const int D_NEXT = END;

    const auto shadow = params.shadow_start;
    const auto size = params.size;
    const int w = 1;
    std::map<Index3, InputTransfer> all_input_transfers = {
        {{PREV, 0, 0}, InputTransfer(DIM_X, S_PREV, 0, 0, D_PREV, shadow[1], shadow[2], w, size[1], size[2], {src_shadow, 0, 0})}, // front X
        {{NEXT, 0, 0}, InputTransfer(DIM_X, S_NEXT, 0, 0, D_NEXT, shadow[1], shadow[2], w, size[1], size[2], {src_shadow, 0, 0})}, // back X
        {{0, PREV, 0}, InputTransfer(DIM_Y, 0, S_PREV, 0, shadow[0], D_PREV, shadow[2], size[0], w, size[2], {0, src_shadow, 0})}, // front Y
        {{0, NEXT, 0}, InputTransfer(DIM_Y, 0, S_NEXT, 0, shadow[0], D_NEXT, shadow[2], size[0], w, size[2], {0, src_shadow, 0})}, // back Y
        {{0, 0, PREV}, InputTransfer(DIM_Z, 0, 0, S_PREV, shadow[0], shadow[1], D_PREV, size[0], size[1], w, {0, 0, src_shadow})}, // front Z
        {{0, 0, NEXT}, InputTransfer(DIM_Z, 0, 0, S_NEXT, shadow[0], shadow[1], D_NEXT, size[0], size[1], w, {0, 0, src_shadow})}, // back Z
        {{PREV, PREV, 0}, InputTransfer(DIM_X, S_PREV, S_PREV, 0, D_PREV, D_PREV, shadow[2], w, 1, size[2], {src_shadow, src_shadow, 0})}, // sides XY (Z changes)
        {{PREV, NEXT, 0}, InputTransfer(DIM_X, S_PREV, S_NEXT, 0, D_PREV, D_NEXT, shadow[2], w, 1, size[2], {src_shadow, src_shadow, 0})},
        {{NEXT, PREV, 0}, InputTransfer(DIM_X, S_NEXT, S_PREV, 0, D_NEXT, D_PREV, shadow[2], w, 1, size[2], {src_shadow, src_shadow, 0})},
        {{NEXT, NEXT, 0}, InputTransfer(DIM_X, S_NEXT, S_NEXT, 0, D_NEXT, D_NEXT, shadow[2], w, 1, size[2], {src_shadow, src_shadow, 0})},
        {{PREV, 0, PREV}, InputTransfer(DIM_X, S_PREV, 0, S_PREV, D_PREV, shadow[1], D_PREV, w, size[1], 1, {src_shadow, 0, src_shadow})}, // sides XZ (Y changes)
        {{PREV, 0, NEXT}, InputTransfer(DIM_X, S_PREV, 0, S_NEXT, D_PREV, shadow[1], D_NEXT, w, size[1], 1, {src_shadow, 0, src_shadow})},
        {{NEXT, 0, PREV}, InputTransfer(DIM_X, S_NEXT, 0, S_PREV, D_NEXT, shadow[1], D_PREV, w, size[1], 1, {src_shadow, 0, src_shadow})},
        {{NEXT, 0, NEXT}, InputTransfer(DIM_X, S_NEXT, 0, S_NEXT, D_NEXT, shadow[1], D_NEXT, w, size[1], 1, {src_shadow, 0, src_shadow})},
        {{0, PREV, PREV}, InputTransfer(DIM_Y, 0, S_PREV, S_PREV, shadow[0], D_PREV, D_PREV, size[0], w, 1, {0, src_shadow, src_shadow})}, // sides YZ (X changes)
        {{0, PREV, NEXT}, InputTransfer(DIM_Y, 0, S_PREV, S_NEXT, shadow[0], D_PREV, D_NEXT, size[0], w, 1, {0, src_shadow, src_shadow})},
        {{0, NEXT, PREV}, InputTransfer(DIM_Y, 0, S_NEXT, S_PREV, shadow[0], D_NEXT, D_PREV, size[0], w, 1, {0, src_shadow, src_shadow})},
        {{0, NEXT, NEXT}, InputTransfer(DIM_Y, 0, S_NEXT, S_NEXT, shadow[0], D_NEXT, D_NEXT, size[0], w, 1, {0, src_shadow, src_shadow})},
        {{PREV, PREV, PREV}, InputTransfer(DIM_X, S_PREV, S_PREV, S_PREV, D_PREV, D_PREV, D_PREV, w, 1, 1, {src_shadow, src_shadow, src_shadow})}, // corners XYZ
        {{PREV, PREV, NEXT}, InputTransfer(DIM_X, S_PREV, S_PREV, S_NEXT, D_PREV, D_PREV, D_NEXT, w, 1, 1, {src_shadow, src_shadow, src_shadow})},
        {{PREV, NEXT, PREV}, InputTransfer(DIM_X, S_PREV, S_NEXT, S_PREV, D_PREV, D_NEXT, D_PREV, w, 1, 1, {src_shadow, src_shadow, src_shadow})},
        {{PREV, NEXT, NEXT}, InputTransfer(DIM_X, S_PREV, S_NEXT, S_NEXT, D_PREV, D_NEXT, D_NEXT, w, 1, 1, {src_shadow, src_shadow, src_shadow})},
        {{NEXT, PREV, PREV}, InputTransfer(DIM_X, S_NEXT, S_PREV, S_PREV, D_NEXT, D_PREV, D_PREV, w, 1, 1, {src_shadow, src_shadow, src_shadow})},
        {{NEXT, PREV, NEXT}, InputTransfer(DIM_X, S_NEXT, S_PREV, S_NEXT, D_NEXT, D_PREV, D_NEXT, w, 1, 1, {src_shadow, src_shadow, src_shadow})},
        {{NEXT, NEXT, PREV}, InputTransfer(DIM_X, S_NEXT, S_NEXT, S_PREV, D_NEXT, D_NEXT, D_PREV, w, 1, 1, {src_shadow, src_shadow, src_shadow})},
        {{NEXT, NEXT, NEXT}, InputTransfer(DIM_X, S_NEXT, S_NEXT, S_NEXT, D_NEXT, D_NEXT, D_NEXT, w, 1, 1, {src_shadow, src_shadow, src_shadow})}
    };
    return selectDisplTransfers(all_input_transfers, neigh_indices, params.neighbor_indices_type);
}

std::map<Index3, InputTransfer> getDisplInputTransfersFull(const CellBlockParams &params, const NeighborIndices &neigh_indices, int width, bool src_shadow) {
    const int NEXT = 1;
    const int PREV = -1;
    const int START = 0;
    const int END = -width;
    const int S_PREV = END;
    const int S_NEXT = START;
    const int D_PREV = START;
    const int D_NEXT = END;

    const auto fsize = params.full_size;
    const std::array<bool, 3> use_shadow = {src_shadow, src_shadow, src_shadow};
    std::map<Index3, InputTransfer> all_input_transfers = {
        {{PREV, 0, 0}, InputTransfer(DIM_X, S_PREV, 0, 0, D_PREV, 0, 0, width, fsize[1], fsize[2], use_shadow)}, // front X
        {{NEXT, 0, 0}, InputTransfer(DIM_X, S_NEXT, 0, 0, D_NEXT, 0, 0, width, fsize[1], fsize[2], use_shadow)}, // back X
        {{0, PREV, 0}, InputTransfer(DIM_Y, 0, S_PREV, 0, 0, D_PREV, 0, fsize[0], width, fsize[2], use_shadow)}, // front Y
        {{0, NEXT, 0}, InputTransfer(DIM_Y, 0, S_NEXT, 0, 0, D_NEXT, 0, fsize[0], width, fsize[2], use_shadow)}, // back Y
        {{0, 0, PREV}, InputTransfer(DIM_Z, 0, 0, S_PREV, 0, 0, D_PREV, fsize[0], fsize[1], width, use_shadow)}, // front Z
        {{0, 0, NEXT}, InputTransfer(DIM_Z, 0, 0, S_NEXT, 0, 0, D_NEXT, fsize[0], fsize[1], width, use_shadow)}, // back Z
        {{PREV, PREV, 0}, InputTransfer(DIM_X, S_PREV, S_PREV, 0, D_PREV, D_PREV, 0, width, width, fsize[2], use_shadow)}, // sides XY (Z changes)
        {{PREV, NEXT, 0}, InputTransfer(DIM_X, S_PREV, S_NEXT, 0, D_PREV, D_NEXT, 0, width, width, fsize[2], use_shadow)},
        {{NEXT, PREV, 0}, InputTransfer(DIM_X, S_NEXT, S_PREV, 0, D_NEXT, D_PREV, 0, width, width, fsize[2], use_shadow)},
        {{NEXT, NEXT, 0}, InputTransfer(DIM_X, S_NEXT, S_NEXT, 0, D_NEXT, D_NEXT, 0, width, width, fsize[2], use_shadow)},
        {{PREV, 0, PREV}, InputTransfer(DIM_X, S_PREV, 0, S_PREV, D_PREV, 0, D_PREV, width, fsize[1], width, use_shadow)}, // sides XZ (Y changes)
        {{PREV, 0, NEXT}, InputTransfer(DIM_X, S_PREV, 0, S_NEXT, D_PREV, 0, D_NEXT, width, fsize[1], width, use_shadow)},
        {{NEXT, 0, PREV}, InputTransfer(DIM_X, S_NEXT, 0, S_PREV, D_NEXT, 0, D_PREV, width, fsize[1], width, use_shadow)},
        {{NEXT, 0, NEXT}, InputTransfer(DIM_X, S_NEXT, 0, S_NEXT, D_NEXT, 0, D_NEXT, width, fsize[1], width, use_shadow)},
        {{0, PREV, PREV}, InputTransfer(DIM_Y, 0, S_PREV, S_PREV, 0, D_PREV, D_PREV, fsize[0], width, width, use_shadow)}, // sides YZ (X changes)
        {{0, PREV, NEXT}, InputTransfer(DIM_Y, 0, S_PREV, S_NEXT, 0, D_PREV, D_NEXT, fsize[0], width, width, use_shadow)},
        {{0, NEXT, PREV}, InputTransfer(DIM_Y, 0, S_NEXT, S_PREV, 0, D_NEXT, D_PREV, fsize[0], width, width, use_shadow)},
        {{0, NEXT, NEXT}, InputTransfer(DIM_Y, 0, S_NEXT, S_NEXT, 0, D_NEXT, D_NEXT, fsize[0], width, width, use_shadow)},
        {{PREV, PREV, PREV}, InputTransfer(DIM_X, S_PREV, S_PREV, S_PREV, D_PREV, D_PREV, D_PREV, width, width, width, use_shadow)}, // corners XYZ
        {{PREV, PREV, NEXT}, InputTransfer(DIM_X, S_PREV, S_PREV, S_NEXT, D_PREV, D_PREV, D_NEXT, width, width, width, use_shadow)},
        {{PREV, NEXT, PREV}, InputTransfer(DIM_X, S_PREV, S_NEXT, S_PREV, D_PREV, D_NEXT, D_PREV, width, width, width, use_shadow)},
        {{PREV, NEXT, NEXT}, InputTransfer(DIM_X, S_PREV, S_NEXT, S_NEXT, D_PREV, D_NEXT, D_NEXT, width, width, width, use_shadow)},
        {{NEXT, PREV, PREV}, InputTransfer(DIM_X, S_NEXT, S_PREV, S_PREV, D_NEXT, D_PREV, D_PREV, width, width, width, use_shadow)},
        {{NEXT, PREV, NEXT}, InputTransfer(DIM_X, S_NEXT, S_PREV, S_NEXT, D_NEXT, D_PREV, D_NEXT, width, width, width, use_shadow)},
        {{NEXT, NEXT, PREV}, InputTransfer(DIM_X, S_NEXT, S_NEXT, S_PREV, D_NEXT, D_NEXT, D_PREV, width, width, width, use_shadow)},
        {{NEXT, NEXT, NEXT}, InputTransfer(DIM_X, S_NEXT, S_NEXT, S_NEXT, D_NEXT, D_NEXT, D_NEXT, width, width, width, use_shadow)}
    };
    return selectDisplTransfers(all_input_transfers, neigh_indices, params.neighbor_indices_type);
}

std::map<Index3, InputTransfer> getDisplInputTransfersCross(const CellBlockParams &params, const NeighborIndices &neigh_indices, bool src_shadow) {
    static const int NEXT = 1;
    static const int PREV = -1;
    static const int START = 0;
    static const int END = -1;
    static const int S_PREV = END;
    static const int S_NEXT = START;
    static const int D_PREV = START;
    static const int D_NEXT = END;

    const auto shadow = params.shadow_start;
    const auto size = params.size;
    const int w = 1;
    std::map<Index3, InputTransfer> all_input_transfers = {
        {{PREV, 0, 0}, InputTransfer(DIM_X, S_PREV, 0, 0, D_PREV, shadow[1], shadow[2], w, size[1], size[2], {src_shadow, 0, 0})}, // front X
        {{NEXT, 0, 0}, InputTransfer(DIM_X, S_NEXT, 0, 0, D_NEXT, shadow[1], shadow[2], w, size[1], size[2], {src_shadow, 0, 0})}, // back X
        {{0, PREV, 0}, InputTransfer(DIM_Y, 0, S_PREV, 0, shadow[0], D_PREV, shadow[2], size[0], w, size[2], {0, src_shadow, 0})}, // front Y
        {{0, NEXT, 0}, InputTransfer(DIM_Y, 0, S_NEXT, 0, shadow[0], D_NEXT, shadow[2], size[0], w, size[2], {0, src_shadow, 0})}, // back Y
        {{0, 0, PREV}, InputTransfer(DIM_Z, 0, 0, S_PREV, shadow[0], shadow[1], D_PREV, size[0], size[1], w, {0, 0, src_shadow})}, // front Z
        {{0, 0, NEXT}, InputTransfer(DIM_Z, 0, 0, S_NEXT, shadow[0], shadow[1], D_NEXT, size[0], size[1], w, {0, 0, src_shadow})}, // back Z
    };
    return selectDisplTransfers(all_input_transfers, neigh_indices, params.neighbor_indices_type);
}

std::map<Index3, OutputTransfer> getDisplOutputTransfersFull(const CellBlockParams &params, const NeighborIndices &neigh_indices, int width, bool src_shadow) {
    const int NEXT = 1;
    const int PREV = -1;
    const int START = 0;
    const int END = -width;
    const int S_PREV = START;
    const int S_NEXT = END;

    const auto fsize = params.full_size;
    const std::array<bool, 3> use_shadow = {src_shadow, src_shadow, src_shadow};
    std::map<Index3, OutputTransfer> all_output_transfers = {
        {{PREV, 0, 0}, OutputTransfer(DIM_X, S_PREV, 0, 0, width, fsize[1], fsize[2], use_shadow)}, // front X
        {{NEXT, 0, 0}, OutputTransfer(DIM_X, S_NEXT, 0, 0, width, fsize[1], fsize[2], use_shadow)}, // back X
        {{0, PREV, 0}, OutputTransfer(DIM_Y, 0, S_PREV, 0, fsize[0], width, fsize[2], use_shadow)}, // front Y
        {{0, NEXT, 0}, OutputTransfer(DIM_Y, 0, S_NEXT, 0, fsize[0], width, fsize[2], use_shadow)}, // back Y
        {{0, 0, PREV}, OutputTransfer(DIM_Z, 0, 0, S_PREV, fsize[0], fsize[1], width, use_shadow)}, // front Z
        {{0, 0, NEXT}, OutputTransfer(DIM_Z, 0, 0, S_NEXT, fsize[0], fsize[1], width, use_shadow)}, // back Z
        {{PREV, PREV, 0}, OutputTransfer(DIM_X, S_PREV, S_PREV, 0, width, width, fsize[2], use_shadow)}, // sides XY (Z changes)
        {{PREV, NEXT, 0}, OutputTransfer(DIM_X, S_PREV, S_NEXT, 0, width, width, fsize[2], use_shadow)},
        {{NEXT, PREV, 0}, OutputTransfer(DIM_X, S_NEXT, S_PREV, 0, width, width, fsize[2], use_shadow)},
        {{NEXT, NEXT, 0}, OutputTransfer(DIM_X, S_NEXT, S_NEXT, 0, width, width, fsize[2], use_shadow)},
        {{PREV, 0, PREV}, OutputTransfer(DIM_X, S_PREV, 0, S_PREV, width, fsize[1], width, use_shadow)}, // sides XZ (Y changes)
        {{PREV, 0, NEXT}, OutputTransfer(DIM_X, S_PREV, 0, S_NEXT, width, fsize[1], width, use_shadow)},
        {{NEXT, 0, PREV}, OutputTransfer(DIM_X, S_NEXT, 0, S_PREV, width, fsize[1], width, use_shadow)},
        {{NEXT, 0, NEXT}, OutputTransfer(DIM_X, S_NEXT, 0, S_NEXT, width, fsize[1], width, use_shadow)},
        {{0, PREV, PREV}, OutputTransfer(DIM_Y, 0, S_PREV, S_PREV, fsize[0], width, width, use_shadow)}, // sides YZ (X changes)
        {{0, PREV, NEXT}, OutputTransfer(DIM_Y, 0, S_PREV, S_NEXT, fsize[0], width, width, use_shadow)},
        {{0, NEXT, PREV}, OutputTransfer(DIM_Y, 0, S_NEXT, S_PREV, fsize[0], width, width, use_shadow)},
        {{0, NEXT, NEXT}, OutputTransfer(DIM_Y, 0, S_NEXT, S_NEXT, fsize[0], width, width, use_shadow)},
        {{PREV, PREV, PREV}, OutputTransfer(DIM_X, S_PREV, S_PREV, S_PREV, width, width, width, use_shadow)}, // corners XYZ
        {{PREV, PREV, NEXT}, OutputTransfer(DIM_X, S_PREV, S_PREV, S_NEXT, width, width, width, use_shadow)},
        {{PREV, NEXT, PREV}, OutputTransfer(DIM_X, S_PREV, S_NEXT, S_PREV, width, width, width, use_shadow)},
        {{PREV, NEXT, NEXT}, OutputTransfer(DIM_X, S_PREV, S_NEXT, S_NEXT, width, width, width, use_shadow)},
        {{NEXT, PREV, PREV}, OutputTransfer(DIM_X, S_NEXT, S_PREV, S_PREV, width, width, width, use_shadow)},
        {{NEXT, PREV, NEXT}, OutputTransfer(DIM_X, S_NEXT, S_PREV, S_NEXT, width, width, width, use_shadow)},
        {{NEXT, NEXT, PREV}, OutputTransfer(DIM_X, S_NEXT, S_NEXT, S_PREV, width, width, width, use_shadow)},
        {{NEXT, NEXT, NEXT}, OutputTransfer(DIM_X, S_NEXT, S_NEXT, S_NEXT, width, width, width, use_shadow)}
    };
    return selectDisplTransfers(all_output_transfers, neigh_indices, params.neighbor_indices_type);
}

std::map<Index3, OutputTransfer> getDisplOutputTransfersCross(const CellBlockParams &params, const NeighborIndices &neigh_indices, bool src_shadow) {
    static const int NEXT = 1;
    static const int PREV = -1;
    static const int START = 0;
    static const int END = -1;
    static const int S_PREV = START;
    static const int S_NEXT = END;

    const auto size = params.size;
    const int w = 1;
    std::map<Index3, OutputTransfer> all_output_transfers = {
        {{PREV, 0, 0}, OutputTransfer(DIM_X, S_PREV, 0, 0, w, size[1], size[2], {src_shadow, 0, 0})}, // front X
        {{NEXT, 0, 0}, OutputTransfer(DIM_X, S_NEXT, 0, 0, w, size[1], size[2], {src_shadow, 0, 0})}, // back X
        {{0, PREV, 0}, OutputTransfer(DIM_Y, 0, S_PREV, 0, size[0], w, size[2], {0, src_shadow, 0})}, // front Y
        {{0, NEXT, 0}, OutputTransfer(DIM_Y, 0, S_NEXT, 0, size[0], w, size[2], {0, src_shadow, 0})}, // back Y
        {{0, 0, PREV}, OutputTransfer(DIM_Z, 0, 0, S_PREV, size[0], size[1], w, {0, 0, src_shadow})}, // front Z
        {{0, 0, NEXT}, OutputTransfer(DIM_Z, 0, 0, S_NEXT, size[0], size[1], w, {0, 0, src_shadow})}, // back Z
    };
    return selectDisplTransfers(all_output_transfers, neigh_indices, params.neighbor_indices_type);
}

template <typename TransferType>
IndexMap<TransferType> getTransferMap(const std::map<Index3, TransferType> &displ, const Index3 &index, const MeshParams &mesh_params) {
    IndexMap<TransferType> result;
    for (const auto &p: displ) {
        const auto neigh_index = neighIndex(index, p.first, mesh_params.num_blocks);
        result[neigh_index] = p.second;
    }
    return result;
}

}

InputTransferMap getInputTransfers(const Index3 &block_index, const CellBlockParams &params,
                                    const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                    bool src_shadow) {
    const auto transfers = getDisplInputTransfers(params, neigh_indices, src_shadow);
    return getTransferMap(transfers, block_index, mesh_params);
}

InputTransferMap getInputTransfersFull(const Index3 &block_index, const CellBlockParams &params,
                                    const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                    int width, bool src_shadow) {
    const auto transfers = getDisplInputTransfersFull(params, neigh_indices, width, src_shadow);
    return getTransferMap(transfers, block_index, mesh_params);
}

InputTransferMap getInputTransfersCross(const Index3 &block_index, const CellBlockParams &params,
                                        const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                        bool src_shadow) {
    const auto transfers = getDisplInputTransfersCross(params, neigh_indices, src_shadow);
    return getTransferMap(transfers, block_index, mesh_params);
}

OutputTransferMap getOutputTransfersFull(const Index3 &block_index, const CellBlockParams &params,
                                    const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                    int width, bool src_shadow) {
    const auto transfers = getDisplOutputTransfersFull(params, neigh_indices, width, src_shadow);
    return getTransferMap(transfers, block_index, mesh_params);
}

OutputTransferMap getOutputTransfersCross(const Index3 &block_index, const CellBlockParams &params,
                                    const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                    bool src_shadow) {
    const auto transfers = getDisplOutputTransfersCross(params, neigh_indices, src_shadow);
    return getTransferMap(transfers, block_index, mesh_params);
}

}
