#pragma once

#include "particle.h"
#include "array.h"
#include "index.h"

#include "didal/util/vector3.h"
#include "didal/comm/serialize/serializable.h"

#include <mutex>

namespace pic {

struct CellBlockParams {
    int index[3]; // block index by each dimension
    int size[3]; // num of cells by each dimension
    int shift[3]; // shift from start in cells by each dimension
    int shadow_start[3];
    int shadow_end[3];
    int full_size[3]; // total num of cells by each dimenison
    int full_shift[3]; // shift for full cells (incl. shadow) by each dimension
    int neighbor_indices_type = 0;
};

struct MeshParams;
struct PICParams;

class CellBlock {
public:
    enum FieldType : int {
        FT_POTENTIAL = 0,
        FT_DENSITY,
        FT_DENSITY_PARTIAL
    };

public:
    CellBlock() {}
    CellBlock(const CellBlockParams &params);

    CellBlock(CellBlock &&cb);

    void setParams(const CellBlockParams &params);

    const CellBlockParams& getParams() const {
        return params;
    }

    Extent3 getExtent(const MeshParams &m_params) const;

    DArray3 getSlice(FieldType field, int x, int y, int z, int n1, int n2, int n3, bool include_shadow = false);
    DArray3 getSlice(FieldType field, int x, int y, int z, int n1, int n2, int n3, const std::array<bool, 3> &include_shadow = {false, false, false});

    void setSlice(FieldType field, int x, int y, int z, const DArray3 &slice, bool include_shadow = false);
    void addSlice(FieldType field, int x, int y, int z, const DArray3 &slice, bool include_shadow = false);

    std::vector<Particle> extractAndMoveOutParticles(const MeshParams &mesh_params);
    std::vector<Particle> extractParticlesForExtent(const Extent3 &extent);
    void addParticles(const std::vector<Particle> &particles, const MeshParams &mesh_params);

    void calcParticlesForce(const PICParams &pic_params, const MeshParams &mesh_params);
    void updateParticlesVelocityAndPosition(const PICParams &pic_params, const MeshParams &mesh_params);
    void calcDensity(const PICParams &pic_params, const MeshParams &mesh_params);
    void addPotentialFromCentralBody(const PICParams &pic_params, const MeshParams &mesh_params);

    void calcPotentialInitWithValue(double value);
    void calcPotentialInitFromPrev();
    void calcPotentialCopyIntoPrev();
    void calcPotentialInitBoundary(const PICParams &pic_params, const MeshParams &mesh_params);
    double calcPotentialIterate(const PICParams &pic_params, const MeshParams &mesh_params);

    void calcForces(const PICParams &pic_params, const MeshParams &mesh_params);

    template <typename Proc, typename... Args>
    void forEachParticle(Proc proc, Args&&... args) {
        std::lock_guard<std::mutex> lock(block_mutex);
        for (auto &particle: particles) {
            proc(particle, std::forward<Args>(args)...);
        }
    }

    template <typename Proc, typename... Args>
    void forEachParticle(Proc proc, Args&&... args) const {
        std::lock_guard<std::mutex> lock(block_mutex);
        for (const auto &particle: particles) {
            proc(particle, std::forward<Args>(args)...);
        }
    }

    template <typename Proc, typename... Args>
    void forEachMeshPoint(const DArray3 &data, Proc proc, Args&&... args) const {
        for (int i = params.shadow_start[0]; i < params.full_size[0] - params.shadow_end[0]; i++)
        for (int j = params.shadow_start[1]; j < params.full_size[1] - params.shadow_end[1]; j++)
        for (int k = params.shadow_start[2]; k < params.full_size[2] - params.shadow_end[2]; k++) {
            proc(data(i, j, k), std::forward<Args>(args)...);
        }
    }

    // Process points in reversed (Fortran-like) order.
    template <typename Proc, typename... Args>
    void forEachMeshPointFlip(const DArray3 &data, Proc proc, Args&&... args) const {
        for (int k = params.shadow_start[2]; k < params.full_size[2] - params.shadow_end[2]; k++)
        for (int j = params.shadow_start[1]; j < params.full_size[1] - params.shadow_end[1]; j++)
        for (int i = params.shadow_start[0]; i < params.full_size[0] - params.shadow_end[0]; i++) {
            proc(data(i, j, k), std::forward<Args>(args)...);
        }
    }

    // Process reduced points (reduce dim is the last dim in dims).
    template <typename ReduceOp, typename Proc, typename... Args>
    void forEachMeshPointReduce(const DArray3 &data, ReduceOp reduce_op, double init,
                                const std::array<size_t, 3> &dims, Proc proc, Args&&... args) const {
        for (int i1 = params.shadow_start[dims[0]]; i1 < params.full_size[dims[0]] - params.shadow_end[dims[0]]; i1++)
        for (int i2 = params.shadow_start[dims[1]]; i2 < params.full_size[dims[1]] - params.shadow_end[dims[1]]; i2++) {
            double value = init;
            for (int i3 = params.shadow_start[dims[2]]; i3 < params.full_size[dims[2]] - params.shadow_end[dims[2]]; i3++) {
                ddl::Index<3> index;
                index[dims[0]] = i1;
                index[dims[1]] = i2;
                index[dims[2]] = i3;
                value = reduce_op(data[index], value);
            }
            proc(value, std::forward<Args>(args)...);
        }
    }

    size_t getNumOfParticles() const;

    Vector3 forceForParticle(const Particle &particle, const MeshParams &mesh_params) const;

    friend ddl::Writable& operator<< (ddl::Writable &w, const CellBlock &cb);
    friend ddl::Readable& operator>> (ddl::Readable &r, CellBlock &cb);

    const DArray3& getDensity() const {
        return density;
    }

    const DArray3& getPotential() const {
        return potential;
    }

private:
    int toIndex(int index, int dim) const;
    int toIndexFull(int index, int dim) const;
    void initCells();

    int toLocalIndexFull(int global_index, int dim) const;

    int getLocalIndexFullSide(const double &global_rel, int dim, double &local_rel) const;
    int getLocalIndexFullCenter(const double &global_rel, int dim, double &local_rel) const;

    DArray3 getSliceOfData(const DArray3 &data, int x, int y, int z, int n1, int n2, int n3);
    void setSliceOfData(DArray3 &data, int x, int y, int z, const DArray3 &slice);
    void addSliceOfData(DArray3 &data, int x, int y, int z, const DArray3 &slice);

    double distanceToCenter(const Index3 &cell_index, const std::array<double, 3> &center, const MeshParams &mesh_params) const;

private:
    CellBlockParams params;
    DArray3 force_x, force_y, force_z; // force on cells' faces (array of 2d slices for each dimension) (no shadow)
    DArray3 density; // density in cells' centers (use shadow)
    DArray3 density_partial; // density in cells' centers (use shadow)
    DArray3 potential; // gravitational potential in cells' centers (use shadow)
    DArray3 potential_prev; // gravitational potential in cells' centers (use shadow)
    std::vector<Particle> particles;
    //ddl::ArrayND<std::vector<int>, 3> particles_by_cells; // (no shadow)

    mutable std::mutex block_mutex;
};

}
