#pragma once

#include "params.h"
#include "particle.h"

#include <vector>

namespace pic {

std::vector<Particle> createParticlesRandom(const PICParams &pic_params, const MeshParams &mesh_params);
std::vector<Particle> createParticlesBall(const PICParams &pic_params, const MeshParams &mesh_params);
std::vector<Particle> createParticlesCircle(const PICParams &pic_params, const MeshParams &mesh_params);

void initParticlesVelocityRandom(std::vector<Particle> &particles, const PICParams &pic_params, const MeshParams &mesh_params);
void initParticlesVelocityRotation(std::vector<Particle> &particles, const PICParams &pic_params, const MeshParams &mesh_params);

std::vector<Particle> initParticles(const PICParams &pic_params, const MeshParams &mesh_params);

}
