#include "output.h"
#include "params.h"

#include <fstream>
#include <map>
#include <set>

namespace pic {

namespace {
    std::string fieldName(OutputField field) {
        switch (field) {
            case OUT_PARTICLES: return "particles";
            case OUT_DENSITY: return "density";
            case OUT_DENSITY_XY: return "density_xy";
            case OUT_DENSITY_XZ: return "density_xz";
            case OUT_DENSITY_YZ: return "density_yz";
            case OUT_POTENTIAL: return "potential";
        }
        return "";
    }

    std::string fileExt(OutputField field) {
        switch (field) {
            case OUT_PARTICLES:
                return "vtp";
            case OUT_DENSITY:
            case OUT_DENSITY_XY:
            case OUT_DENSITY_XZ:
            case OUT_DENSITY_YZ:
            case OUT_POTENTIAL:
                return "vti";
        }
        return "";
    }

    std::array<size_t, 3> reduceDims(OutputField field) {
        switch (field) {
            case OUT_DENSITY_XY: return {0, 1, 2};
            case OUT_DENSITY_XZ: return {0, 2, 1};
            case OUT_DENSITY_YZ: return {1, 2, 0};
            default: return {0, 1, 2};
        }
    }

    std::string pieceSource(const std::string &path, OutputField field, int iter, int this_node) {
        return path + "." + fieldName(field) + "." + std::to_string(iter) + ".piece." + std::to_string(this_node) + "." + fileExt(field);
    }

    std::string parallelSource(const std::string &path, OutputField field, int iter) {
        return path + "." + fieldName(field) + "." + std::to_string(iter) + ".p" + fileExt(field);
    }

    std::string baseName(const std::string &filename) {
        auto base_name = filename;
        auto p = base_name.find_last_of("/");
        if (p == std::string::npos) {
            p = base_name.find_last_of("\\");
        }
        if (p != std::string::npos) {
            base_name = base_name.substr(p + 1);
        }
        return base_name;
    }

    double sumDouble(double a, double b) {
        return a + b;
    }

    static const std::string DENSITY_NAME = "Density";
    static const std::string POTENTIAL_NAME = "Potential";

    const DArray3& blockDensity(std::shared_ptr<CellBlock> cb) {
        return cb->getDensity();
    }

    const DArray3& blockPotential(std::shared_ptr<CellBlock> cb) {
        return cb->getPotential();
    }
}

void VTKOutput::outputNodeAll(const std::string &path, int iter, const CellBlockArray &blocks) {
    outputNode(path, OUT_PARTICLES, iter, blocks);
    outputNode(path, OUT_DENSITY, iter, blocks);
}

void VTKOutput::outputNode(const std::string &path, const std::vector<OutputField> &fields, int iter, const CellBlockArray &blocks) {
    for (auto field: fields) {
        outputNode(path, field, iter, blocks);
    }
}

void VTKOutput::outputNode(const std::string &path, OutputField field, int iter, const CellBlockArray &blocks) {
    outputPiece(path, field, iter, blocks);
    if (this_node == 0) {
        outputParallel(path, field, iter);
    }
}

void VTKOutput::outputPiece(const std::string &path, OutputField field, int iter, const CellBlockArray &blocks) {
    const auto filename = pieceSource(path, field, iter, this_node);
    std::ofstream out(filename.c_str());
    switch (field) {
        case OUT_PARTICLES:
            outputPieceParticles(out, blocks);
            break;
        case OUT_DENSITY:
            outputPieceArray(out, blocks, DENSITY_NAME, &blockDensity);
            break;
        case OUT_DENSITY_XY:
        case OUT_DENSITY_XZ:
        case OUT_DENSITY_YZ:
            outputPieceArray2D(out, reduceDims(field), blocks, DENSITY_NAME, &blockDensity);
            break;
        case OUT_POTENTIAL:
            outputPieceArray(out, blocks, POTENTIAL_NAME, &blockPotential);
            break;
    }
}

void VTKOutput::outputParallel(const std::string &path, OutputField field, int iter) {
    const auto filename = parallelSource(path, field, iter);
    std::ofstream out(filename.c_str());
    const auto sources = getSourceFiles(path, field, iter);
    switch (field) {
        case OUT_PARTICLES:
            outputParallelParticles(out, sources);
            break;
        case OUT_DENSITY:
            outputParallelArray(out, sources, DENSITY_NAME);
            break;
        case OUT_DENSITY_XY:
        case OUT_DENSITY_XZ:
        case OUT_DENSITY_YZ:
            outputParallelArray2D(out, reduceDims(field), sources, DENSITY_NAME);
            break;
        case OUT_POTENTIAL:
            outputParallelArray(out, sources, POTENTIAL_NAME);
            break;
    }
}

void VTKOutput::outputPieceParticles(std::ostream &out, const CellBlockArray &blocks) {
    out << "<?xml version=\"1.0\"?>\n";
    out << "<VTKFile type=\"PolyData\" version=\"1.0\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n";
    out << "<PolyData>\n";
    for (auto &block: blocks) {
        const auto num_of_particles = block->getNumOfParticles();
        out << "<Piece NumberOfPoints=\"" << num_of_particles << "\" " <<
               "NumberOfVerts=\"" << num_of_particles << "\" " <<
               "NumberOfLines=\"0\" NumberOfStrips=\"0\" NumberOfPolys=\"0\">\n";

        out << "<Points>\n";
        out << "<DataArray type=\"Float64\" NumberOfComponents=\"3\" name=\"Position\" format=\"ascii\">\n";
        block->forEachParticle([&out](const Particle &particle) {
            out << particle.pos.x << " " << particle.pos.y << " " << particle.pos.z << " ";
        });
        out << "\n</DataArray>\n";
        out << "</Points>\n";

        out << "<PointData Vectors=\"Velocity\">\n";
        out << "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Velocity\" format=\"ascii\">\n";
        block->forEachParticle([&out](const Particle &particle) {
            out << particle.velocity.x << " " << particle.velocity.y << " " << particle.velocity.z << " ";
        });
        out << "\n</DataArray>\n";
        out << "</PointData>\n";

        out << "<Verts>\n";
        out << "<DataArray type=\"Int64\" Name=\"connectivity\" format=\"ascii\">\n";
        for (size_t i = 0; i < num_of_particles; i++) {
            out << i << " ";
        }
        out << "\n</DataArray>\n";
        out << "<DataArray type=\"Int64\" Name=\"offsets\" format=\"ascii\">\n";
        for (size_t i = 0; i < num_of_particles; i++) {
            out << i + 1 << " ";
        }
        out << "\n</DataArray>\n";
        out << "</Verts>\n";

        out << "</Piece>\n";
    }
    out << "</PolyData>\n";
    out << "</VTKFile>\n";
}

void VTKOutput::outputPieceArray(std::ostream &out, const CellBlockArray &blocks, const std::string &array_name, AccessFunc func) {
    const auto nx = mesh_params.mesh_size[0];
    const auto ny = mesh_params.mesh_size[1];
    const auto nz = mesh_params.mesh_size[2];
    const auto hx = mesh_params.step[0];
    const auto hy = mesh_params.step[1];
    const auto hz = mesh_params.step[2];
    const auto ox = mesh_params.origin[0] + hx * 0.5;
    const auto oy = mesh_params.origin[1] + hy * 0.5;
    const auto oz = mesh_params.origin[2] + hz * 0.5;
    out << "<?xml version=\"1.0\"?>\n";
    out << "<VTKFile type=\"ImageData\" version=\"1.0\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n";
    out << "<ImageData WholeExtent=\"0 " << nx - 1 << " 0 " << ny - 1 << " 0 " << nz - 1 << "\" " <<
        "Origin=\"" << ox << " " << oy << " " << oz << "\" " <<
        "Spacing=\"" << hx << " " << hy << " " << hz << "\">\n";
    for (auto &block: blocks) {
        const auto &params = block->getParams();
        out << "<Piece Extent=\"" <<
            params.shift[0] << " " << params.shift[0] + params.size[0] - 1 << " " <<
            params.shift[1] << " " << params.shift[1] + params.size[1] - 1 << " " <<
            params.shift[2] << " " << params.shift[2] + params.size[2] - 1 << "\">\n";

        out << "<PointData Scalars=\"" << array_name << "\">\n";
        out << "<DataArray type=\"Float64\" Name=\"" << array_name << "\" format=\"ascii\">\n";
        block->forEachMeshPointFlip(func(block), [&out](double value) {
            out << value << " ";
        });
        out << "\n</DataArray>\n";        
        out << "</PointData>\n";

        out << "</Piece>\n";
    }
    out << "</ImageData>\n";
    out << "</VTKFile>\n";
}

void VTKOutput::outputPieceArray2D(std::ostream &out, const std::array<size_t, 3> &dims, const CellBlockArray &blocks, const std::string &array_name, AccessFunc func) {
    const auto n1 = mesh_params.mesh_size[dims[0]];
    const auto n2 = mesh_params.mesh_size[dims[1]];
    const auto h1 = mesh_params.step[dims[0]];
    const auto h2 = mesh_params.step[dims[1]];
    const auto o1 = mesh_params.origin[dims[0]] + h1 * 0.5;
    const auto o2 = mesh_params.origin[dims[1]] + h2 * 0.5;
    out << "<?xml version=\"1.0\"?>\n";
    out << "<VTKFile type=\"ImageData\" version=\"1.0\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n";
    out << "<ImageData GhostLevel=\"0\" WholeExtent=\"0 " << n1 - 1 << " 0 " << n2 - 1 << " 0 0\" " <<
        "Origin=\"" << o1 << " " << o2 << " 0\" " <<
        "Spacing=\"" << h1 << " " << h2 << " 0\">\n";
    const std::array<size_t, 3> dims_flipped {dims[1], dims[0], dims[2]}; // to write data in VTK (reversed) order
    for (auto &block: blocks) {
        const auto &params = block->getParams();
        out << "<Piece Extent=\"" <<
            params.shift[dims[0]] << " " << params.shift[dims[0]] + params.size[dims[0]] - 1 << " " <<
            params.shift[dims[1]] << " " << params.shift[dims[1]] + params.size[dims[1]] - 1 << " 0 0\">\n";

        out << "<PointData Scalars=\"" << array_name << "\">\n";
        out << "<DataArray type=\"Float64\" Name=\"" << array_name << "\" format=\"ascii\">\n";
        block->forEachMeshPointReduce(func(block), &sumDouble, 0, dims_flipped, [&out](double value) {
            out << value << " ";
        });
        out << "\n</DataArray>\n";
        out << "</PointData>\n";

        out << "</Piece>\n";
    }
    out << "</ImageData>\n";
    out << "</VTKFile>\n";
}

void VTKOutput::outputParallelParticles(std::ostream &out, const std::vector<std::string> &sources_files) {
    out << "<?xml version=\"1.0\"?>\n";
    out << "<VTKFile type=\"PPolyData\" version=\"1.0\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n";
    out << "<PPolyData GhostLevel=\"0\">\n";
    out << "<PPoints>\n";
    out << "<PDataArray type=\"Float64\" NumberOfComponents=\"3\" name=\"Position\"/>\n";
    out << "</PPoints>\n";
    out << "<PPointData Vectors=\"Velocity\">\n";
    out << "<PDataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Velocity\"/>\n";
    out << "</PPointData>\n";
    for (const auto &file: sources_files) {
        out << "<Piece Source=\"" << file << "\"/>\n";
    }
    out << "</PPolyData>\n";
    out << "</VTKFile>\n";
}

void VTKOutput::outputParallelArray(std::ostream &out, const std::vector<std::string> &sources_files, const std::string &array_name) {
    const auto nx = mesh_params.mesh_size[0];
    const auto ny = mesh_params.mesh_size[1];
    const auto nz = mesh_params.mesh_size[2];
    const auto hx = mesh_params.step[0];
    const auto hy = mesh_params.step[1];
    const auto hz = mesh_params.step[2];
    const auto ox = mesh_params.origin[0] + hx * 0.5;
    const auto oy = mesh_params.origin[1] + hy * 0.5;
    const auto oz = mesh_params.origin[2] + hz * 0.5;
    out << "<?xml version=\"1.0\"?>\n";
    out << "<VTKFile type=\"PImageData\" version=\"1.0\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n";
    out << "<PImageData GhostLevel=\"0\" WholeExtent=\"0 " << nx - 1 << " 0 " << ny - 1 << " 0 " << nz - 1 << "\" " <<
        "Origin=\"" << ox << " " << oy << " " << oz << "\" " <<
        "Spacing=\"" << hx << " " << hy << " " << hz << "\">\n";
    out << "<PPointData Scalars=\"" << array_name << "\">\n";
    out << "<PDataArray type=\"Float64\" Name=\"" << array_name << "\"/>\n";
    out << "</PPointData>\n";
    for (const auto &file: sources_files) {
        out << "<Piece Extent=\"0 " << nx - 1 << " 0 " << ny - 1 << " 0 " << nz - 1 << "\" " <<
               "Source=\"" << file << "\"/>\n";
    }
    out << "</PImageData>\n";
    out << "</VTKFile>\n";
}

void VTKOutput::outputParallelArray2D(std::ostream &out, const std::array<size_t, 3> &dims, const std::vector<std::string> &sources_files, const std::string &array_name) {
    const auto n1 = mesh_params.mesh_size[dims[0]];
    const auto n2 = mesh_params.mesh_size[dims[1]];
    const auto h1 = mesh_params.step[dims[0]];
    const auto h2 = mesh_params.step[dims[1]];
    const auto o1 = mesh_params.origin[dims[0]] + h1 * 0.5;
    const auto o2 = mesh_params.origin[dims[1]] + h2 * 0.5;
    out << "<?xml version=\"1.0\"?>\n";
    out << "<VTKFile type=\"PImageData\" version=\"1.0\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n";
    out << "<PImageData GhostLevel=\"0\" WholeExtent=\"0 " << n1 - 1 << " 0 " << n2 - 1 << " 0 0\" " <<
        "Origin=\"" << o1 << " " << o2 << " 0\" " <<
        "Spacing=\"" << h1 << " " << h2 << " 0\">\n";
    out << "<PPointData Scalars=\"" << array_name << "\">\n";
    out << "<PDataArray type=\"Float64\" Name=\"" << array_name << "\"/>\n";
    out << "</PPointData>\n";
    for (const auto &file: sources_files) {
        out << "<Piece Extent=\"0 " << n1 - 1 << " 0 " << n2 - 1 << " 0 0\" " <<
               "Source=\"" << file << "\"/>\n";
    }
    out << "</PImageData>\n";
    out << "</VTKFile>\n";
}

std::vector<std::string> VTKOutput::getSourceFiles(const std::string &path, OutputField field, int iter) {
    std::vector<std::string> files;
    for (int i = 0; i < num_of_nodes; i++) {
        files.push_back(baseName(pieceSource(path, field, iter, i)));
    }
    return files;
}

}
