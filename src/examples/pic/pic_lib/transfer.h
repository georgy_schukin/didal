#pragma once

#include <map>
#include <array>

#include "index.h"

namespace pic {

struct CellBlockParams;
struct MeshParams;
class NeighborIndices;

enum Dimension {
    DIM_X = 0,
    DIM_Y,
    DIM_Z
};

struct InputTransfer {
    int dimension;
    int src1, src2, src3;
    int dst1, dst2, dst3;
    int size1, size2, size3;
    std::array<bool, 3> src_shadow;

    InputTransfer() {}
    InputTransfer(int dim, int s1, int s2, int s3,
                  int d1, int d2, int d3, int sz1, int sz2, int sz3,
                  const std::array<bool, 3> &src_shadow = {false, false, false}) :
        dimension(dim),
        src1(s1), src2(s2), src3(s3),
        dst1(d1), dst2(d2), dst3(d3),
        size1(sz1), size2(sz2), size3(sz3),
        src_shadow(src_shadow) {
    }
};

struct OutputTransfer {
    int dimension;
    int src1, src2, src3;
    int size1, size2, size3;
    std::array<bool, 3> src_shadow;

    OutputTransfer() {}
    OutputTransfer(int dim, int s1, int s2, int s3, int sz1, int sz2, int sz3,
                   const std::array<bool, 3> &src_shadow = {false, false, false}) :
        dimension(dim),
        src1(s1), src2(s2), src3(s3),
        size1(sz1), size2(sz2), size3(sz3),
        src_shadow(src_shadow) {
    }
};

template <typename T>
using IndexMap = std::map<Index3, T>;
using InputTransferMap = IndexMap<InputTransfer>;
using OutputTransferMap = IndexMap<OutputTransfer>;

// 26 neighbors
InputTransferMap getInputTransfers(const Index3 &block_index, const CellBlockParams &params,
                                    const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                    bool src_shadow = false);

// 26 neighbors
InputTransferMap getInputTransfersFull(const Index3 &block_index, const CellBlockParams &params,
                                        const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                        int width = 1,
                                        bool src_shadow = false);

// 6 neighbors
InputTransferMap getInputTransfersCross(const Index3 &block_index, const CellBlockParams &params,
                                        const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                        bool src_shadow = false);

OutputTransferMap getOutputTransfersFull(const Index3 &block_index, const CellBlockParams &params,
                                    const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                    int width = 1,
                                    bool src_shadow = false);

OutputTransferMap getOutputTransfersCross(const Index3 &block_index, const CellBlockParams &params,
                                    const NeighborIndices &neigh_indices, const MeshParams &mesh_params,
                                    bool src_shadow = false);

}

