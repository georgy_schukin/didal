TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

include(../pic_lib.pri)
DIDAL_OUT_DIR = $$OUT_PWD/../../../didal
include(../../../didal_lib.pri)

SOURCES += \
        main.cpp

HEADERS += \
    mesh3d.h
