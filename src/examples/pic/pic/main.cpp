#include "pic.h"
#include "mesh3d.h"
#include "didal/didal.h"
#include "didal/util/args.h"
#include "didal/util/slicer.h"
#include "didal/util/timer.h"
#include "didal/util/lattice.h"
#include "didal/algorithms/reduce.h"
#include "didal/algorithms/barrier.h"

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <stdexcept>
#include <fstream>
#include <cmath>
#include <cstdio>

using namespace pic;

namespace {

static int debug_level = 0;

void setDebug(int level) {
    debug_level = level;
}

bool isDebug(int level) {
    return debug_level >= level;
}

}

static double reduce_time = 0;
static double wait_time = 0;
static double local_barrier_time = 0;
static double calc_potential_iter_time = 0;

SyncIndex4 syncInd4(int iter, const Index3 &ind) {
    return std::make_pair(iter, ind);
}

int toRank(const Index3 &index, const MeshDistribution &distr) {
    return index[0] * static_cast<int>(distr.numOfNodes(1)) * static_cast<int>(distr.numOfNodes(2)) +
           index[1] * static_cast<int>(distr.numOfNodes(2)) +
           index[2];
}

double inExtent(double value, double start, double end) {
    if (value < start) {
        return start + std::fmod(start - value, end - start);
    } else if (value >= end) {
        return start + std::fmod(value - start, end - start);
    } else {
        return value;
    }
}

Index3 getBlockIndex(const Vector3 &pos, const MeshParams &mesh_params, const MeshDecomposition &mesh_decomp) {
    Index3 index;
    std::array<double, 3> p {pos.x, pos.y, pos.z};
    for (size_t i = 0; i < 3; i++) {
        if (p[i] >= mesh_params.origin[i]) {
            const auto cell_index = static_cast<int>((p[i] - mesh_params.origin[i]) / mesh_params.step[i]);
            index[i] = mesh_decomp.blockIndex(static_cast<int>(i), cell_index);
        } else {
            index[i] = -1;
        }
    }
    return index;
}

SyncFuture getWhenReady(SyncMesh4 &mesh, const SyncIndex4 &index, int node) {
    return mesh.getWhenReadyFrom(node, index);
}

SyncFuture getWhenReady(SyncMesh3 &mesh, const SyncIndex3 &index, int node) {
    return mesh.getWhenReadyFrom(node, index);
}

double waitForF(SyncFuture &future) {
    ddl::Timer timer;
    future.get();
    return timer.time();
}

template <typename Container>
void waitFor(Container &futures) {
    ddl::Timer timer;
    for (auto &f: futures) {
        f.get();
    }
    wait_time += timer.time();
}

void waitForAll(SyncFutureMap &f_map) {
    for (auto &p: f_map) {
        waitFor(p.second);
    }
}

void signalAll(Mesh3D &mesh, SyncMesh4 &sync, int iter) {
    for (auto &p: mesh.localContents()) {
        sync.create(syncInd4(iter, p.first), true);
    }
}

SyncFutureMap syncWithNeighbors(SyncMesh3 &sync, const NeighMap &neigh, const BlockMap &block_distr) {
    SyncFutureMap sync_f;
    for (auto &p: neigh) {
        for (const auto &neigh_index: p.second) {
            sync_f[p.first].emplace_back(getWhenReady(sync, neigh_index, block_distr.at(neigh_index)));
        }
    }
    return sync_f;
}

SyncFutureMap syncWithNeighbors(SyncMesh4 &sync, const NeighMap &neigh, const BlockMap &block_distr, int iter) {
    SyncFutureMap sync_f;
    for (auto &p: neigh) {
        for (const auto &neigh_index: p.second) {
            const auto sync_index = syncInd4(iter, neigh_index);
            sync_f[p.first].emplace_back(getWhenReady(sync, sync_index, block_distr.at(neigh_index)));
        }
    }
    return sync_f;
}

std::shared_ptr<ddl::Fence> global_fence;

void localBarrier(ddl::Environment *env, const NeighMap &neighbors, const BlockMap &block_distr) {
    ddl::Timer tm;
    std::set<int> neigh_nodes;
    for (const auto &p: neighbors) {
        for (const auto &neigh_index: p.second) {
            neigh_nodes.insert(block_distr.at(neigh_index));
        }
    }
    //ddl::localBarrier(env, neigh_nodes);
    global_fence->barrier(neigh_nodes);
    local_barrier_time += tm.time();
}

void distributeParticles(Mesh3D &mesh, const std::vector<Particle> &particles, const MeshParams &mesh_params,
                           const MeshDecomposition &decomp, const BlockMap &block_distr, const NeighMap &neighbors) {
    std::map<Index3, std::vector<Particle>> particles_for_blocks;

    //std::cout << "distr start\n";

    for (const auto &particle: particles) {
        particles_for_blocks[getBlockIndex(particle.pos, mesh_params, decomp)].push_back(particle);
    }

    std::vector<std::future<void>> futures;
    for (auto &p: particles_for_blocks) {
        auto it = block_distr.find(p.first);
        if (it != block_distr.end()) {
            //std::cout << "add " << p.second.size() << " on " << it->second << std::endl;
            futures.push_back(mesh.addParticlesOn(it->second, it->first, p.second));
        } else {
            //std::cerr << "lost " << p.second.size() << std::endl;
        }
    }
    // Wait when remote operations are completed.
    for (auto &f: futures) {
        f.get();
    }

    localBarrier(mesh.getEnvironment(), neighbors, block_distr);
    //barrier(mesh.getEnvironment());

    //std::cout << "distr done\n";
}

template <typename Proc>
void initParticles(Proc proc, Mesh3D &mesh, int my_rank, const MeshParams &mesh_params,
                   const MeshDecomposition &mesh_decomp, const BlockMap &block_distr, const NeighMap &neighbors) {
    std::vector<Particle> particles;
    if (my_rank == 0) {
        particles = proc();
    }
    distributeParticles(mesh, particles, mesh_params, mesh_decomp, block_distr, neighbors);
}

void moveParticles(Mesh3D &mesh, const MeshParams &mesh_params,
                   const MeshDecomposition &decomp, const BlockMap &block_distr, const NeighMap &neighbors) {
    //const auto extent = mesh_params.getExtent();
    //const bool account_periodic = mesh_params.periodic[0] || mesh_params.periodic[1] || mesh_params.periodic[2];

    std::vector<Particle> moved_particles;
    for (auto &p: mesh.localContents()) {
        const auto out_particles = p.second->extractAndMoveOutParticles(mesh_params);
        moved_particles.insert(moved_particles.end(), out_particles.begin(), out_particles.end());
    }

    distributeParticles(mesh, moved_particles, mesh_params, decomp, block_distr, neighbors);
}

size_t numOfParticles(const Mesh3D &mesh) {
    size_t num = 0;
    for (const auto &p: mesh.localContents()) {
        num += p.second->getNumOfParticles();
    }
    return num;
}

size_t numOfBlocks(const Mesh3D &mesh) {
    return mesh.localContents().size();
}

struct Transfer {
    int dimension;
    int x, y, z;
    std::future<DArray3> data;

    Transfer(int dim, int xx, int yy, int zz, std::future<DArray3> &&dt) :
        dimension(dim), x(xx), y(yy), z(zz), data(std::move(dt)) {
    }
};

template <CellBlock::FieldType InputField, typename Proc>
void doNeighborExchanges(Mesh3D &mesh, const BlockMap &block_distr, const std::map<Index3, InputTransferMap> &transfer_inputs, Proc proc) {
    std::vector<std::pair<CellBlock*, Transfer>> transfers;
    for (auto &p: mesh.localContents()) {
        const auto index = p.first;
        auto *block = p.second.get();
        for (const auto &p: transfer_inputs.at(index)) {
            const auto &neigh_index = p.first;
            const auto &t_input = p.second;
            auto it = block_distr.find(neigh_index);
            if (it != block_distr.end()) {
                const auto neigh_node = it->second;
                auto future = mesh.getSliceFrom(neigh_node, neigh_index,
                                        InputField,
                                        t_input.src1, t_input.src2, t_input.src3,
                                        t_input.size1, t_input.size2, t_input.size3, t_input.src_shadow);
                Transfer transfer(t_input.dimension, t_input.dst1, t_input.dst2, t_input.dst3, std::move(future));
                transfers.push_back(std::make_pair(block, std::move(transfer)));
            }
        }
    }

    for (auto &p: transfers) {
        proc(*p.first, p.second);
    }
}

void syncDencity(Mesh3D &mesh, const BlockMap &block_distr, const std::map<Index3, InputTransferMap> &transfer_inputs) {
    // We will take partial densities and sum them to obtain full density.
    doNeighborExchanges<CellBlock::FT_DENSITY_PARTIAL>(mesh, block_distr, transfer_inputs, [&mesh](CellBlock &block, Transfer &t) {
        auto data = t.data.get();
        block.addSlice(CellBlock::FT_DENSITY, t.x, t.y, t.z, data, true);
        if (isDebug(2)) {
            std::ostringstream out;
            const auto &index = block.getParams().index;
            out << mesh.getEnvironment()->thisNode().rank() << ": [" <<
                index[0] << ", " << index[1] << ", " << index[2] <<"]: add dencity sz(" <<
                data.size(0) << ", " << data.size(1) << ", " << data.size(2) << ") to (" <<
                t.x << ", " << t.y << ", " << t.z << ")" << std::endl;
            std::cout << out.str();
        }
    });
}

void syncPotentialPoisson(Mesh3D &mesh, const BlockMap &block_distr, const std::map<Index3, InputTransferMap> &transfer_inputs) {
    doNeighborExchanges<CellBlock::FT_POTENTIAL>(mesh, block_distr, transfer_inputs, [](CellBlock &block, Transfer &t) {
        auto data = t.data.get();
        block.setSlice(CellBlock::FT_POTENTIAL, t.x, t.y, t.z, data, true);
    });
}

void updateParticles(Mesh3D &mesh, const PICParams &pic_params, const MeshParams &mesh_params) {
    for (auto &p: mesh.localContents()) {
        auto *block = p.second.get();
        block->calcParticlesForce(pic_params, mesh_params);
        block->updateParticlesVelocityAndPosition(pic_params, mesh_params);
    }
}

void calcForces(Mesh3D &mesh, const PICParams &pic_params, const MeshParams &mesh_params) {
    for (auto &p: mesh.localContents()) {
        auto *block = p.second.get();
        block->calcForces(pic_params, mesh_params);
    }
}

void calcDensity(Mesh3D &mesh, const PICParams &pic_params, const MeshParams &mesh_params,
                 const BlockMap &block_distr, const std::map<Index3, InputTransferMap> &transfer_inputs, const NeighMap &neighbors) {
    //SyncMesh3 dencity_calculated(mesh.getEnvironment());

    //int rank = mesh.getEnvironment()->thisNode().rank();

    //std::cout << "dens start " << rank << std::endl;

    for (auto &p: mesh.localContents()) {
        auto *block = p.second.get();
        block->calcDensity(pic_params, mesh_params);
        //dencity_calculated.create(p.first, true);
    }

    //std::cout << "calc dens done " << rank << std::endl;

    //auto sync_f = syncWithNeighbors(dencity_calculated, neighbors, block_distr);
    //waitForAll(sync_f);

    localBarrier(mesh.getEnvironment(), neighbors, block_distr);

    //std::cout << "wait dens done " << rank << std::endl;

    syncDencity(mesh, block_distr, transfer_inputs);

    //std::cout << "sync dens done " << rank << std::endl;

    localBarrier(mesh.getEnvironment(), neighbors, block_distr);

    //std::cout << "dens done " << rank << std::endl;
}

void calcPotential(Mesh3D &mesh, const PICParams &pic_params, const MeshParams &mesh_params,
                   const BlockMap &block_distr, const std::map<Index3, InputTransferMap> &transfer_inputs, const NeighMap &neighbors, bool init) {
    //std::cout << "pot start " << rank << std::endl;

    // init boundary
    for (auto &p: mesh.localContents()) {
        if (init) {
            p.second->calcPotentialInitWithValue(0.0);
            p.second->calcPotentialInitBoundary(pic_params, mesh_params);
        } else {
            p.second->calcPotentialInitFromPrev(); // use last calc potential as initial solution
        }
    }

    localBarrier(mesh.getEnvironment(), neighbors, block_distr);

    static const auto norm_op = ddl::ReduceOperationMax<double> {};

    int iter = 0;
    double norm = 0.0;
    do {
        // update boundaries
        syncPotentialPoisson(mesh, block_distr, transfer_inputs);

        //std::cout << "norm start " << rank << std::endl;

        // next iteration
        ddl::Timer timer;
        double local_norm = 0;
        for (auto &p: mesh.localContents()) {
            local_norm = norm_op(local_norm, p.second->calcPotentialIterate(pic_params, mesh_params));
        }
        calc_potential_iter_time += timer.time();

        // compute norm
        timer.reset();
        norm = ddl::reduceValues(mesh.getEnvironment(), local_norm, norm_op, 0.0);
        reduce_time += timer.time();
        iter++;
        //std::cout << norm << std::endl;

        localBarrier(mesh.getEnvironment(), neighbors, block_distr);
    }
    while ((norm > pic_params.epsilon) && (iter < pic_params.max_poisson_iters));

    for (auto &p: mesh.localContents()) {
        p.second->calcPotentialCopyIntoPrev(); // save last calc potential
        p.second->addPotentialFromCentralBody(pic_params, mesh_params);
    }

    localBarrier(mesh.getEnvironment(), neighbors, block_distr);

    //std::cout << " potential iters: " << iter << std::endl;
}

template <typename Proc, typename... Args>
double timed(Proc proc, Args&&... args) {
    ddl::Timer timer;
    proc(std::forward<Args>(args)...);
    return timer.time();
}

double timedBarrier(ddl::CommService *cs) {
    ddl::Timer timer;
    cs->getCommunicator()->barrier();
    return timer.time();
}

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::ArgsParser args(argc, argv);

    int size_x = args.intArg(100);
    int size_y = args.intArg(100);
    int size_z = args.intArg(100);
    int num_of_fragments_x = args.intArg(10);
    int num_of_fragments_y = args.intArg(10);
    int num_of_fragments_z = args.intArg(10);
    int num_of_particles = args.intArg(1000);
    int num_of_iters = args.intArg(100);
    const auto conf_file = args.strArg("conf.txt");
    int nodes_by_x = args.intArg(0);
    int nodes_by_y = args.intArg(0);
    int nodes_by_z = args.intArg(0);
    const bool scale_size = args.intArg(0);
    const bool scale_fragments = args.intArg(0);
    const bool scale_particles = args.intArg(0);
    const int debug = args.intArg(0);

    global_fence = std::make_shared<ddl::Fence>(&env);

    setDebug(debug);

    const int my_rank = env.thisNode().rank();
    const int num_of_nodes = static_cast<int>(env.allNodes().size());

    std::tie(nodes_by_x, nodes_by_y, nodes_by_z) = ddl::Lattice::getLatticeSides3D(num_of_nodes, nodes_by_x, nodes_by_y, nodes_by_z);

    if (nodes_by_x <=0 || nodes_by_y <= 0 || nodes_by_z <= 0 || nodes_by_x * nodes_by_y * nodes_by_z != num_of_nodes) {
        throw std::runtime_error("Incorrect grid sizes!");
    }

    // Size scaling mode: mesh size denotes mesh block size for each node.
    // Scale global mesh size appropriately.
    if (scale_size) {
        size_x *= nodes_by_x;
        size_y *= nodes_by_y;
        size_z *= nodes_by_z;
    }

    // Num of fragments scaling mode: num of fragments denotes num of fragments on each node.
    // Scale global num of fragments appropriately.
    if (scale_fragments) {
        num_of_fragments_x *= nodes_by_x;
        num_of_fragments_y *= nodes_by_y;
        num_of_fragments_z *= nodes_by_z;
    }

    if (scale_particles) {
        num_of_particles *= num_of_nodes;
    }

    PICParams pic_params;

    pic_params.load(conf_file);
    pic_params.nx = size_x;
    pic_params.ny = size_y;
    pic_params.nz = size_z;
    pic_params.num_of_particles = num_of_particles;
    pic_params.particle_mass = pic_params.particles_total_mass / pic_params.num_of_particles;
    pic_params.hx = pic_params.area_size[0] / pic_params.nx;
    pic_params.hy = pic_params.area_size[1] / pic_params.ny;
    pic_params.hz = pic_params.area_size[2] / pic_params.nz;

    const bool do_output = !pic_params.output.empty();

    MeshParams mesh_params;
    mesh_params.mesh_size = {size_x, size_y, size_z};
    mesh_params.num_blocks = {num_of_fragments_x, num_of_fragments_y, num_of_fragments_z};
    mesh_params.step = {pic_params.hx, pic_params.hy, pic_params.hz};
    mesh_params.origin = pic_params.area_origin;
    mesh_params.area_size = pic_params.area_size;

    MeshDecomposition mesh_decomp {{mesh_params.mesh_size[0], mesh_params.num_blocks[0]},
                                  {mesh_params.mesh_size[1], mesh_params.num_blocks[1]},
                                  {mesh_params.mesh_size[2], mesh_params.num_blocks[2]}};

    MeshDistribution mesh_distr {{mesh_params.num_blocks[0], nodes_by_x},
                                 {mesh_params.num_blocks[1], nodes_by_y},
                                 {mesh_params.num_blocks[2], nodes_by_z}};

    BlockMap block_distr;
    for (int i = 0; i < mesh_params.num_blocks[0]; i++) {
        for (int j = 0; j < mesh_params.num_blocks[1]; j++) {
            for (int k = 0; k < mesh_params.num_blocks[2]; k++) {
                Index3 index {i, j, k};
                const auto ni = mesh_distr.nodeIndex(index);
                const auto node = toRank(ni, mesh_distr);
                block_distr[index] = node;
            }
        }
    }

    Mesh3D mesh(&env, mesh_params);

    NeighborIndices neighbor_indices;

    for (const auto &p: block_distr) {
        const auto &index = p.first;
        if (my_rank == p.second) {
            CellBlockParams b_params;
            for (size_t i = 0; i < 3; i++) {
                b_params.index[i] = p.first[i];
                b_params.size[i] = mesh_decomp.blockSize(static_cast<int>(i), index[i]);
                b_params.shift[i] = mesh_decomp.blockShift(static_cast<int>(i), index[i]);
                b_params.shadow_start[i] = 1; //mesh_params.periodic[i] ? 1 : (index[i] > 0 ? 1 : 0);
                b_params.shadow_end[i] = 1; //mesh_params.periodic[i] ? 1 : (index[i] < mesh_params.num_blocks[i] - 1 ? 1 : 0);
                b_params.full_size[i] = b_params.size[i] + b_params.shadow_start[i] + b_params.shadow_end[i];
                b_params.full_shift[i] = b_params.shift[i] - b_params.shadow_start[i];
            }
            b_params.neighbor_indices_type = NeighborIndices::getType(
                        index[0] > 0, index[0] < mesh_params.num_blocks[0] - 1,
                        index[1] > 0, index[1] < mesh_params.num_blocks[1] - 1,
                        index[2] > 0, index[2] < mesh_params.num_blocks[2] - 1);
            mesh.create(index, CellBlock(b_params));
        }
    }

    NeighMap density_neighbors;
    NeighMap potential_neighbors;

    std::map<Index3, InputTransferMap> density_transfers;
    std::map<Index3, InputTransferMap> potential_transfers;

    for (const auto &p: mesh.localContents()) {
        const auto index = p.first;
        const auto &params = p.second->getParams();
        // Init transfer maps.
        density_transfers[index] = getInputTransfersFull(index, params, neighbor_indices, mesh_params, 2, true);
        potential_transfers[index] = getInputTransfersCross(index, params, neighbor_indices, mesh_params, false);
        // Init neighbor maps.
        for (const auto &p: density_transfers.at(index)) {
            density_neighbors[index].push_back(p.first);
        }
        for (const auto &p: potential_transfers.at(index)) {
            potential_neighbors[index].push_back(p.first);
        }
    }

    ddl::barrier(&env);

    initParticles([&pic_params, &mesh_params]() {
        return initParticles(pic_params, mesh_params);
    }, mesh, my_rank, mesh_params, mesh_decomp, block_distr, density_neighbors);

    ddl::barrier(&env);

    VTKOutput output(my_rank, num_of_nodes, mesh_params);

    double update_particles_time = 0;
    double move_particles_time = 0;
    double calc_density_time = 0;
    double calc_potential_time = 0;
    double calc_forces_time = 0;

    ddl::Timer total_timer;
    ddl::Timer tm;

    // Calc density
    tm.reset();
    calcDensity(mesh, pic_params, mesh_params, block_distr, density_transfers, density_neighbors);
    calc_density_time += tm.time();

    // Calc potential
    tm.reset();
    calcPotential(mesh, pic_params, mesh_params, block_distr, potential_transfers, potential_neighbors, true);
    calc_potential_time += tm.time();

    // Calc forces
    tm.reset();
    calcForces(mesh, pic_params, mesh_params);
    calc_forces_time += tm.time();

    if (do_output) {
        output.outputNode(pic_params.output, pic_params.output_fields, 0, mesh.localObjects());
    }

    size_t total_num_of_particles = 0;

    for (int iter = 1; iter <= num_of_iters; iter++) {

        // Update positions
        tm.reset();
        updateParticles(mesh, pic_params, mesh_params);
        update_particles_time += tm.time();

        // Move particles
        tm.reset();
        moveParticles(mesh, mesh_params, mesh_decomp, block_distr, density_neighbors);
        move_particles_time += tm.time();

        // Calc density
        tm.reset();
        calcDensity(mesh, pic_params, mesh_params, block_distr, density_transfers, density_neighbors);
        calc_density_time += tm.time();

        // Calc potential
        tm.reset();
        calcPotential(mesh, pic_params, mesh_params, block_distr, potential_transfers, potential_neighbors, false);
        calc_potential_time += tm.time();

        // Calc forces
        tm.reset();
        calcForces(mesh, pic_params, mesh_params);
        calc_forces_time += tm.time();

        if (do_output && (iter % pic_params.output_step == 0)) {
            output.outputNode(pic_params.output, pic_params.output_fields, iter, mesh.localObjects());
        }

        const auto np = numOfParticles(mesh);
        total_num_of_particles += np;

        if (isDebug(1)) {
            std::ostringstream out;
            out << my_rank << ": iter: " << iter <<
                ", particles: " << np <<
                ", blocks: " << numOfBlocks(mesh) << std::endl;
            std::cout << out.str();
        }
    }

    ddl::barrier(&env);

    const auto own_time = update_particles_time + move_particles_time + calc_density_time + calc_potential_time + calc_forces_time;
    const auto time = total_timer.time();

    std::ostringstream out;
    if (my_rank == 0) {
        out << "Mesh: " << mesh_params.mesh_size[0] << " x " << mesh_params.mesh_size[1] << " x " << mesh_params.mesh_size[2] <<
               ", fragments: " << mesh_params.num_blocks[0] << " x " << mesh_params.num_blocks[1] << " x " << mesh_params.num_blocks[2] <<
               ", grid: " << nodes_by_x << " x " << nodes_by_y << " x " << nodes_by_z <<
               ", particles: " << num_of_particles <<
               ", iters: " << num_of_iters <<
               ", time: " << time <<
               std::endl;
    }
    out << "[Node " << my_rank << "]: own time: " << own_time <<
           ", calc density time: " << calc_density_time <<
           ", calc potential time: " << calc_potential_time <<
           ", calc forces time: " << calc_forces_time <<
           ", update particles time: " << update_particles_time <<
           ", move particles time: " << move_particles_time <<
           ", calc potential iter time: " << calc_potential_iter_time <<
           ", reduce time: " << reduce_time <<
           ", local barrier time: " << local_barrier_time <<
           ", wait time: " << wait_time <<
           ", avg particles: " << total_num_of_particles / num_of_iters <<
           std::endl;
    std::cout << out.str();

    global_fence.reset();

    ddl::barrier(&env);

    return 0;
}
