TEMPLATE = subdirs

SUBDIRS += \
    pic \
    pic_sync \
    pic_mpi \
    pic_lib 

pic.depends = pic_lib
pic_sync.depends = pic_lib
pic_mpi.depends = pic_lib