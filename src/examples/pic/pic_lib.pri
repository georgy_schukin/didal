
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../pic_lib/release/ -lpic_lib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../pic_lib/debug/ -lpic_lib
else:unix: LIBS += -L$$OUT_PWD/../pic_lib/ -lpic_lib

INCLUDEPATH += $$PWD/pic_lib
DEPENDPATH += $$PWD/pic_lib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pic_lib/release/libpic_lib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pic_lib/debug/libpic_lib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pic_lib/release/pic_lib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pic_lib/debug/pic_lib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../pic_lib/libpic_lib.a
