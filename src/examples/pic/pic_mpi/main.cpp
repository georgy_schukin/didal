#include <mpi.h>
#include "pic.h"
#include "index.h"
#include "didal/didal.h"
#include "didal/util/args.h"
#include "didal/util/slicer.h"
#include "didal/util/timer.h"
#include "didal/util/lattice.h"
#include "didal/algorithms/reduce.h"
#include "didal/algorithms/barrier.h"
#include "didal/decomp/multi_block_decomposition.h"
#include "didal/distr/mesh_block_distribution.h"

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <stdexcept>
#include <fstream>
#include <cmath>
#include <cstdio>
#include <numeric>

using namespace pic;

using MeshDecomposition = ddl::MultuBlockDecomposition<3>;
using MeshDistribution = ddl::MeshBlockDistribution<3>;
using BlockMap = std::map<Index3, int>;

namespace {

static int debug_level = 0;

void setDebug(int level) {
    debug_level = level;
}

bool isDebug(int level) {
    return debug_level >= level;
}

MPI_Datatype mpi_particle_type;

static const int PARTICLE_TAG = 1;
static const int DENSITY_TAG = 2;
static const int POTENTIAL_TAG = 3;

void initMPITypes() {
    Particle p;
    int lengths[2] = {3, 3};
    MPI_Aint base;
    MPI_Aint addr[2];
    MPI_Get_address(&p, &base);
    MPI_Get_address(&p.pos, &addr[0]);
    MPI_Get_address(&p.velocity, &addr[1]);
    MPI_Aint displs[2] = {addr[0] - base, addr[1] - base};
    MPI_Datatype types[2] = {MPI_DOUBLE, MPI_DOUBLE};
    MPI_Datatype tmp_type;
    MPI_Type_create_struct(2, lengths, displs, types, &tmp_type);
    MPI_Type_create_resized(tmp_type, 0, sizeof(Particle), &mpi_particle_type);
    MPI_Type_commit(&mpi_particle_type);
}

}

static double reduce_time = 0;
static double calc_potential_iter_time = 0;
static double sync_density_time = 0;
static double sync_potential_time = 0;

/*int toRank(const Index3 &index, const MeshDistribution &distr) {
    return index[0] * static_cast<int>(distr.numOfNodes(1)) * static_cast<int>(distr.numOfNodes(2)) +
           index[1] * static_cast<int>(distr.numOfNodes(2)) +
           index[2];
}*/

double inExtent(double value, double start, double end) {
    if (value < start) {
        return start + std::fmod(start - value, end - start);
    } else if (value >= end) {
        return start + std::fmod(value - start, end - start);
    } else {
        return value;
    }
}

Index3 getBlockIndex(const Vector3 &pos, const MeshParams &mesh_params, const MeshDecomposition &mesh_decomp) {
    Index3 index;
    std::array<double, 3> p {pos.x, pos.y, pos.z};
    for (size_t i = 0; i < 3; i++) {
        if (p[i] >= mesh_params.origin[i]) {
            const auto cell_index = static_cast<int>((p[i] - mesh_params.origin[i]) / mesh_params.step[i]);
            index[i] = mesh_decomp.blockIndex(static_cast<int>(i), cell_index);
        } else {
            index[i] = -1;
        }
    }
    return index;
}

void distributeParticles(CellBlock &block, MPI_Comm &comm, const std::vector<Particle> &particles, const MeshParams &mesh_params,
                           const MeshDecomposition &decomp, const BlockMap &block_distr, const std::vector<int> &src_nodes, const std::vector<int> &dst_nodes) {
    std::map<int, std::vector<Particle>> particles_for_blocks;

    for (const auto n: dst_nodes) {
        particles_for_blocks[n].clear();
    }
    for (const auto &particle: particles) {
        const auto index = getBlockIndex(particle.pos, mesh_params, decomp);
        const auto it = block_distr.find(index);
        if (it != block_distr.end()) {
            particles_for_blocks[it->second].push_back(particle);
        }
    }

    std::vector<MPI_Request> reqs;
    for (auto &p: particles_for_blocks) {
        reqs.push_back(MPI_Request {});
        MPI_Isend(p.second.data(), static_cast<int>(p.second.size()), mpi_particle_type, p.first, PARTICLE_TAG, comm, &reqs.back());
    }

    auto cnt = src_nodes.size();
    while (cnt > 0) {
        MPI_Status stat;
        MPI_Probe(MPI_ANY_SOURCE, PARTICLE_TAG, comm, &stat);
        int count;
        MPI_Get_count(&stat, mpi_particle_type, &count);
        std::vector<Particle> p_recv;
        if (count > 0) {
            p_recv.resize(static_cast<size_t>(count));
        }
        MPI_Recv(p_recv.data(), count, mpi_particle_type, stat.MPI_SOURCE, stat.MPI_TAG, comm, MPI_STATUS_IGNORE);
        block.addParticles(p_recv, mesh_params);
        cnt--;
    }

    MPI_Waitall(static_cast<int>(reqs.size()), reqs.data(), MPI_STATUSES_IGNORE);
}

template <typename Proc>
void initParticles(Proc proc, CellBlock &block, MPI_Comm &comm, int my_rank, int num_of_nodes, const MeshParams &mesh_params,
                   const MeshDecomposition &mesh_decomp, const BlockMap &block_distr) {
    std::vector<Particle> particles;
    std::vector<int> dst_nodes;
    if (my_rank == 0) {
        particles = proc();
        dst_nodes.resize(static_cast<size_t>(num_of_nodes));
        std::iota(dst_nodes.begin(), dst_nodes.end(), 0);
    }
    distributeParticles(block, comm, particles, mesh_params, mesh_decomp, block_distr, {0}, dst_nodes);
}

void moveParticles(CellBlock &block, MPI_Comm &comm, const MeshParams &mesh_params,
                   const MeshDecomposition &decomp, const BlockMap &block_distr, const std::vector<Index3> &neighbors) {
    const auto moved_particles = block.extractAndMoveOutParticles(mesh_params);
    std::vector<int> neigh_ranks;
    for (const auto &ind: neighbors) {
        neigh_ranks.push_back(block_distr.at(ind));
    }
    distributeParticles(block, comm, moved_particles, mesh_params, decomp, block_distr, neigh_ranks, neigh_ranks);
}

struct Transfer {
    int dimension;
    int x, y, z;
    std::future<DArray3> data;

    Transfer(int dim, int xx, int yy, int zz, std::future<DArray3> &&dt) :
        dimension(dim), x(xx), y(yy), z(zz), data(std::move(dt)) {
    }
};

template <CellBlock::FieldType InputField, int tag, typename Proc>
void doNeighborExchanges(CellBlock &block, MPI_Comm &comm, const BlockMap &block_distr, const InputTransferMap &inputs, const OutputTransferMap &outputs, Proc proc) {
    std::vector<Transfer> transfers;

    std::map<int, DArray3> output_data;
    for (const auto &p: outputs) {
        const auto &output = p.second;
        output_data[block_distr.at(p.first)] = block.getSlice(InputField, output.src1, output.src2, output.src3,
                                                              output.size1, output.size2, output.size3, output.src_shadow);
    }
    std::vector<MPI_Request> output_reqs;
    for (const auto &p: output_data) {
        output_reqs.push_back(MPI_Request {});
        const auto &array = p.second;
        MPI_Isend(array.data().data(), static_cast<int>(array.size()), MPI_DOUBLE, p.first, tag, comm, &output_reqs.back());
    }

    std::map<int, DArray3> input_data;
    for (const auto &p: inputs) {
        const auto &input = p.second;
        input_data[block_distr.at(p.first)] = DArray3(ddl::IndexRangeND<3,size_t>{static_cast<size_t>(input.size1),
                                                        static_cast<size_t>(input.size2),
                                                        static_cast<size_t>(input.size3)});
    }
    std::vector<MPI_Request> input_reqs;
    for (auto &p: input_data) {
        input_reqs.push_back(MPI_Request {});
        auto &array = p.second;
        MPI_Irecv(array.data().data(), static_cast<int>(array.size()), MPI_DOUBLE, p.first, tag, comm, &input_reqs.back());
    }

    MPI_Waitall(static_cast<int>(input_reqs.size()), input_reqs.data(), MPI_STATUSES_IGNORE);
    for (const auto &p: inputs) {
        proc(block, p.second, input_data[block_distr.at(p.first)]);
    }

    MPI_Waitall(static_cast<int>(output_reqs.size()), output_reqs.data(), MPI_STATUSES_IGNORE);
}

void syncDencity(CellBlock &block, MPI_Comm &comm, const BlockMap &block_distr, const InputTransferMap &inputs, const OutputTransferMap &outputs) {
    // We will take partial densities and sum them to obtain full density.
    doNeighborExchanges<CellBlock::FT_DENSITY_PARTIAL, DENSITY_TAG>(block, comm, block_distr, inputs, outputs,
        [](CellBlock &block, const InputTransfer &in, const DArray3 &data) {
            block.addSlice(CellBlock::FT_DENSITY, in.dst1, in.dst2, in.dst3, data, true);
    });
}

void syncPotentialPoisson(CellBlock &block, MPI_Comm &comm, const BlockMap &block_distr, const InputTransferMap &inputs, const OutputTransferMap &outputs) {
    doNeighborExchanges<CellBlock::FT_POTENTIAL, POTENTIAL_TAG>(block, comm, block_distr, inputs, outputs,
        [](CellBlock &block, const InputTransfer &in, const DArray3 &data) {
            block.setSlice(CellBlock::FT_POTENTIAL, in.dst1, in.dst2, in.dst3, data, true);
    });
}

void updateParticles(CellBlock &block, const PICParams &pic_params, const MeshParams &mesh_params) {
    block.calcParticlesForce(pic_params, mesh_params);
    block.updateParticlesVelocityAndPosition(pic_params, mesh_params);
}

void calcForces(CellBlock &block, const PICParams &pic_params, const MeshParams &mesh_params) {
    block.calcForces(pic_params, mesh_params);
}

void calcDensity(CellBlock &block, MPI_Comm &comm, const PICParams &pic_params, const MeshParams &mesh_params,
                 const BlockMap &block_distr, const InputTransferMap &inputs, const OutputTransferMap &outputs) {
    block.calcDensity(pic_params, mesh_params);

    ddl::Timer timer;
    syncDencity(block, comm, block_distr, inputs, outputs);
    sync_density_time += timer.time();
}

void calcPotential(CellBlock &block, MPI_Comm &comm, const PICParams &pic_params, const MeshParams &mesh_params,
                   const BlockMap &block_distr, const InputTransferMap &inputs, const OutputTransferMap &outputs, bool init) {
    // init boundary
    if (init) {
        block.calcPotentialInitWithValue(0.0);
        block.calcPotentialInitBoundary(pic_params, mesh_params);
    } else {
        block.calcPotentialInitFromPrev(); // use last calc potential as initial solution
    }

    int iter = 0;
    double norm = 0.0;
    do {
        // update boundaries
        ddl::Timer timer;
        syncPotentialPoisson(block, comm, block_distr, inputs, outputs);
        sync_potential_time += timer.time();

        // next iteration
        timer.reset();
        double local_norm = block.calcPotentialIterate(pic_params, mesh_params);
        calc_potential_iter_time += timer.time();

        // compute norm
        timer.reset();
        MPI_Allreduce(&local_norm, &norm, 1, MPI_DOUBLE, MPI_MAX, comm);
        reduce_time += timer.time();
        iter++;
        //std::cout << norm << std::endl;
    }
    while ((norm > pic_params.epsilon) && (iter < pic_params.max_poisson_iters));

    block.calcPotentialCopyIntoPrev(); // save last calc potential
    block.addPotentialFromCentralBody(pic_params, mesh_params);
    //std::cout << " potential iters: " << iter << std::endl;
}

int main(int argc, char **argv) {

    MPI_Init(&argc, &argv);

    int num_of_nodes;
    MPI_Comm_size(MPI_COMM_WORLD, &num_of_nodes);

    MPI_Comm comm;
    int dims[3] = {0, 0, 0};
    MPI_Dims_create(num_of_nodes, 3, dims);
    int periods[3] = {0, 0, 0};
    MPI_Cart_create(MPI_COMM_WORLD, 3, dims, periods, 1, &comm);

    int my_rank;
    MPI_Comm_rank(comm, &my_rank);

    int my_coords[3];
    MPI_Cart_coords(comm, my_rank, 3, my_coords);

    Index3 my_index {my_coords[0], my_coords[1], my_coords[2]};

    initMPITypes();

    ddl::ArgsParser args(argc, argv);

    int size_x = args.intArg(100);
    int size_y = args.intArg(100);
    int size_z = args.intArg(100);
    int num_of_particles = args.intArg(1000);
    int num_of_iters = args.intArg(100);
    const auto conf_file = args.strArg("conf.txt");
    const bool scale_size = args.intArg(0);
    const bool scale_particles = args.intArg(0);
    const int debug = args.intArg(0);

    setDebug(debug);

    // Size scaling mode: mesh size denotes mesh block size for each node.
    // Scale global mesh size appropriately.
    if (scale_size) {
        size_x *= dims[0];
        size_y *= dims[1];
        size_z *= dims[2];
    }

    if (scale_particles) {
        num_of_particles *= num_of_nodes;
    }

    PICParams pic_params;

    pic_params.load(conf_file);
    pic_params.nx = size_x;
    pic_params.ny = size_y;
    pic_params.nz = size_z;
    pic_params.num_of_particles = num_of_particles;
    pic_params.particle_mass = pic_params.particles_total_mass / pic_params.num_of_particles;
    pic_params.hx = pic_params.area_size[0] / pic_params.nx;
    pic_params.hy = pic_params.area_size[1] / pic_params.ny;
    pic_params.hz = pic_params.area_size[2] / pic_params.nz;

    const bool do_output = !pic_params.output.empty();

    MeshParams mesh_params;
    mesh_params.mesh_size = {size_x, size_y, size_z};
    mesh_params.num_blocks = {dims[0], dims[1], dims[2]};
    mesh_params.step = {pic_params.hx, pic_params.hy, pic_params.hz};
    mesh_params.origin = pic_params.area_origin;
    mesh_params.area_size = pic_params.area_size;

    MeshDecomposition mesh_decomp {{mesh_params.mesh_size[0], mesh_params.num_blocks[0]},
                                  {mesh_params.mesh_size[1], mesh_params.num_blocks[1]},
                                  {mesh_params.mesh_size[2], mesh_params.num_blocks[2]}};

    BlockMap block_distr;
    for (int i = 0; i < mesh_params.num_blocks[0]; i++) {
        for (int j = 0; j < mesh_params.num_blocks[1]; j++) {
            for (int k = 0; k < mesh_params.num_blocks[2]; k++) {
                int coords[3] = {i, j, k};
                int dst_rank;
                MPI_Cart_rank(comm, coords, &dst_rank);
                block_distr[{i, j, k}] = dst_rank;
            }
        }
    }

    NeighborIndices neighbor_indices;

    CellBlockParams block_params;
    for (size_t i = 0; i < 3; i++) {
        block_params.index[i] = my_index[i];
        block_params.size[i] = mesh_decomp.blockSize(static_cast<int>(i), my_index[i]);
        block_params.shift[i] = mesh_decomp.blockShift(static_cast<int>(i), my_index[i]);
        block_params.shadow_start[i] = 1; //mesh_params.periodic[i] ? 1 : (index[i] > 0 ? 1 : 0);
        block_params.shadow_end[i] = 1; //mesh_params.periodic[i] ? 1 : (index[i] < mesh_params.num_blocks[i] - 1 ? 1 : 0);
        block_params.full_size[i] = block_params.size[i] + block_params.shadow_start[i] + block_params.shadow_end[i];
        block_params.full_shift[i] = block_params.shift[i] - block_params.shadow_start[i];
    }
    block_params.neighbor_indices_type = NeighborIndices::getType(
                my_index[0] > 0, my_index[0] < mesh_params.num_blocks[0] - 1,
                my_index[1] > 0, my_index[1] < mesh_params.num_blocks[1] - 1,
                my_index[2] > 0, my_index[2] < mesh_params.num_blocks[2] - 1);

    auto block_ptr = std::make_shared<CellBlock>(block_params);
    CellBlock &block = *(block_ptr.get());

    std::vector<Index3> density_neighbors;
    std::vector<Index3> potential_neighbors;

    const auto density_input_transfers = getInputTransfersFull(my_index, block_params, neighbor_indices, mesh_params, 2, true);
    const auto potential_input_transfers = getInputTransfersCross(my_index, block_params, neighbor_indices, mesh_params, false);

    const auto density_output_transfers = getOutputTransfersFull(my_index, block_params, neighbor_indices, mesh_params, 2, true);
    const auto potential_output_transfers = getOutputTransfersCross(my_index, block_params, neighbor_indices, mesh_params, false);

    // Init neighbor maps.
    for (const auto &p: density_input_transfers) {
        density_neighbors.push_back(p.first);
    }
    for (const auto &p: potential_input_transfers) {
        potential_neighbors.push_back(p.first);
    }

    initParticles([&pic_params, &mesh_params]() {
        return initParticles(pic_params, mesh_params);
    }, block, comm, my_rank, num_of_nodes, mesh_params, mesh_decomp, block_distr);

    VTKOutput output(my_rank, num_of_nodes, mesh_params);

    double update_particles_time = 0;
    double move_particles_time = 0;
    double calc_density_time = 0;
    double calc_potential_time = 0;
    double calc_forces_time = 0;

    ddl::Timer total_timer;
    ddl::Timer tm;

    // Calc density
    tm.reset();
    calcDensity(block, comm, pic_params, mesh_params, block_distr, density_input_transfers, density_output_transfers);
    calc_density_time += tm.time();

    // Calc potential
    tm.reset();
    calcPotential(block, comm, pic_params, mesh_params, block_distr, potential_input_transfers, potential_output_transfers, true);
    calc_potential_time += tm.time();

    // Calc forces
    tm.reset();
    calcForces(block, pic_params, mesh_params);
    calc_forces_time += tm.time();

    if (do_output) {
        output.outputNode(pic_params.output, pic_params.output_fields, 0, {block_ptr});
    }

    size_t total_num_of_particles = 0;

    for (int iter = 1; iter <= num_of_iters; iter++) {

        // Update positions
        tm.reset();
        updateParticles(block, pic_params, mesh_params);
        update_particles_time += tm.time();

        // Move particles
        tm.reset();
        moveParticles(block, comm, mesh_params, mesh_decomp, block_distr, density_neighbors);
        move_particles_time += tm.time();

        // Calc density
        tm.reset();
        calcDensity(block, comm, pic_params, mesh_params, block_distr, density_input_transfers, density_output_transfers);
        calc_density_time += tm.time();

        // Calc potential
        tm.reset();
        calcPotential(block, comm, pic_params, mesh_params, block_distr, potential_input_transfers, potential_output_transfers, false);
        calc_potential_time += tm.time();

        // Calc forces
        tm.reset();
        calcForces(block, pic_params, mesh_params);
        calc_forces_time += tm.time();

        if (do_output && (iter % pic_params.output_step == 0)) {
            output.outputNode(pic_params.output, pic_params.output_fields, iter, {block_ptr});
        }

        const auto np = block.getNumOfParticles();
        total_num_of_particles += np;

        if (isDebug(1)) {
            std::ostringstream out;
            out << my_rank << ": iter: " << iter <<
                ", particles: " << np << std::endl;
            std::cout << out.str();
        }
    }

    MPI_Barrier(comm);

    const auto own_time = update_particles_time + move_particles_time + calc_density_time + calc_potential_time + calc_forces_time;
    const auto time = total_timer.time();

    std::ostringstream out;
    if (my_rank == 0) {
        out << "Mesh: " << mesh_params.mesh_size[0] << " x " << mesh_params.mesh_size[1] << " x " << mesh_params.mesh_size[2] <<
               ", grid: " << dims[0] << " x " << dims[1] << " x " << dims[2] <<
               ", particles: " << num_of_particles <<
               ", iters: " << num_of_iters <<
               ", time: " << time <<
               std::endl;
    }
    out << "[Node " << my_rank << "]: own time: " << own_time <<
           ", calc density time: " << calc_density_time <<
           ", calc potential time: " << calc_potential_time <<
           ", calc forces time: " << calc_forces_time <<
           ", update particles time: " << update_particles_time <<
           ", move particles time: " << move_particles_time <<
           ", calc potential iter time: " << calc_potential_iter_time <<
           ", reduce time: " << reduce_time <<
           ", avg particles: " << total_num_of_particles / static_cast<size_t>(num_of_iters) <<
           ", sync density time: " << sync_density_time <<
           ", sync potential time: " << sync_potential_time <<
           std::endl;
    std::cout << out.str();

    MPI_Finalize();

    return 0;
}
