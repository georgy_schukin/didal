#include "didal.h"
#include "algorithms/reduce.h"
#include "algorithms/foreach.h"

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>

int main(int argc, char **argv) {

    /*ddl::Environment env(&argc, &argv);

    const size_t arr_size = (argc > 1 ? std::stoul(argv[1]) : 20);

    auto start = std::chrono::high_resolution_clock::now();

    ddl::DistributedArray<double> a(&env, arr_size, ddl::BlockDecomposition(5), ddl::UniformDistribution(), env.allNodes());

    ddl::forEach (a, [](size_t, ddl::Array<double> &block) {
        for (size_t i = 0; i < block.size(); i++) {
            block[i] = block.toGlobalIndex(i) * 2;
        }
    });

    ddl::forEach (a, [](size_t, const ddl::Array<double> &block) {
        for (size_t i = 0; i < block.size(); i++) {
            std::ostringstream out;
            out << "Elem " << block.toGlobalIndex(i) << " is " << block[i] << std::endl;
            std::cout << out.str();
        }
    });

    const auto sum = ddl::reduce(a, [](const double &el, double curr) {return el + curr;}, 0.0);

    auto end = std::chrono::high_resolution_clock::now();

    const auto time = (std::chrono::duration<double> {end - start}).count();

    std::ostringstream out;
    out << "Sum of elems is " << sum << " (took " << time << " sec)" << std::endl;
    std::cout << out.str();*/

    return 0;
}
