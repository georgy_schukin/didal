#include "didal/didal.h"
#include "didal/util/slicer.h"
#include "didal/util/timer.h"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <chrono>
#include <map>

size_t getSize(const std::vector<double> &v) {
    return v.size();
}

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    const int data_size = (argc > 1 ? std::stoi(argv[1]) : 1000);
    const int num_of_fragments = (argc > 2 ? std::stoi(argv[2]) : 10);
    const int num_of_repeats = (argc > 3 ? std::stoi(argv[3]) : 1);
    bool async_recv = (argc > 4 ? std::stoi(argv[4]) : false);
    bool use_getter = (argc > 5 ? std::stoi(argv[5]) : false);

    const int my_rank = env.thisNode().rank();
    const int num_of_nodes = env.allNodes().size();

    const auto frag_sizes = ddl::Slicer::getSlices(data_size, num_of_fragments);
    const auto frag_distribution = ddl::Slicer::getShifts(num_of_fragments, num_of_nodes);

    std::map<int, int> distribution;
    std::vector<int> my_indices;

    for (int i = 0; i < num_of_fragments; i++) {
        distribution[i] = ddl::Slicer::shiftIndexFor(frag_distribution, i);
        if (distribution[i] == my_rank) {
            my_indices.push_back(i);
        }
    }

    ddl::DistributedStorage<int, std::vector<double>> a(&env);

    auto size_getter = a.makeGetter<decltype(getSize), size_t>(getSize);

    for (auto index: my_indices) {
        a.create(index, std::vector<double>(frag_sizes[index], 1.0f));
    }

    env.getCommService()->getCommunicator()->barrier();

    ddl::Timer timer;

    int recv_size = 0;
    int result = 0;

    for (int r = 0; r < num_of_repeats; r++) {
        if (use_getter) {
            if (async_recv) {
                std::vector<std::future<size_t>> futures;
                for (int i = 0; i < num_of_fragments; i++) {
                    futures.push_back(size_getter(ddl::Node(distribution[i]), i));
                }
                for (auto &f: futures) {
                    auto sz = f.get();
                    result += sz;
                    recv_size += sizeof (sz);
                }
            } else {
                for (int i = 0; i < num_of_fragments; i++) {
                    auto sz = size_getter(ddl::Node(distribution[i]), i).get();
                    result += sz;
                    recv_size += sizeof (sz);
                }
            }
        } else {
            if (async_recv) {
                std::vector<std::future<std::shared_ptr<std::vector<double>>>> futures;
                for (int i = 0; i < num_of_fragments; i++) {
                    futures.push_back(a.getWhenReadyFrom(ddl::Node(distribution[i]), i));
                }
                for (auto &f: futures) {
                    auto a_r = f.get();
                    result += a_r->size();
                    recv_size += a_r->size() * sizeof (double);
                }
            } else {
                for (int i = 0; i < num_of_fragments; i++) {
                    auto a_r = a.getWhenReadyFrom(ddl::Node(distribution[i]), i).get();
                    result += a_r->size();
                    recv_size += a_r->size() * sizeof (double);
                }
            }
        }
    }

    const auto own_time = timer.time();

    env.getCommService()->getCommunicator()->barrier();

    const auto time = timer.time();

    std::ostringstream out;

    if (my_rank == 0) {
        out << "Data size: " << data_size <<
               ", fragments: " << num_of_fragments <<
               ", repeats: " << num_of_repeats <<
               ", async recv: " << async_recv <<
               ", use getter: " << use_getter <<
               ", time: " << time << std::endl;
    }
    out << "[Node " << my_rank << "]: own time: " << own_time <<
           ", result: " << result <<
           ", recv size: " << recv_size << std::endl;

    std::cout << out.str();

    return 0;
}
