#pragma once

#include "didal/didal.h"

#include <vector>
#include <functional>

struct PoissonParams {
    double area_size_x, area_size_y, area_size_z;  // size of area
    int mesh_size_x, mesh_size_y, mesh_size_z; // num of points (mesh size)
    double step_x, step_y, step_z;
    int num_blocks_x, num_blocks_y;
};

struct BlockParams {
    int index_x; // block index by X
    int index_y; // block index by Y
    int size_x; // block size by X
    int size_y; // block size by Y
    int size_z; // block size by Z
    int full_size_x;
    int full_size_y;
    int full_size_z;
    int shift_x; // indices shift by X
    int shift_y; // indices shift by Y
    int shadow_x[2]; // if X starts or ends with shadow (0/1)
    int shadow_y[2]; // if Y starts or ends with shadow (0/1)

    inline int index(int i, int j, int k) const {
        return i*size_y*size_z + j*size_z + k;
    }
};

ddl::Writable& operator<<(ddl::Writable &w, const BlockParams &p);
ddl::Readable& operator>>(ddl::Readable &r, BlockParams &p);

using Block2D = ddl::ArrayND<double, 2>;
using Block3D = ddl::ArrayND<double, 3>;
using Range2D = ddl::IndexRangeND<2>;
using Range3D = ddl::IndexRangeND<3>;
using Func3D = std::function<double(double, double, double)>;

BlockParams getBlockParams(const PoissonParams &params, int block_size_x, int block_size_y, int block_shift_x, int block_shift_y, int ix, int iy);
Block3D initBlock(const PoissonParams &params, const BlockParams &block_params, Func3D init_func);
Block3D emptyBlock(const BlockParams &block_params);
Block3D fillBlock(const PoissonParams &params, const BlockParams &block_params, Func3D init_func);
void calcBlock(const PoissonParams &params, const BlockParams &block_params, Block3D &block, const Block3D &prev_block, const Block3D &right);
Block2D getSliceX(const Block3D &block, const BlockParams &block_params, int x);
Block2D getSliceY(const Block3D &block, const BlockParams &block_params, int y);
void setSliceX(const BlockParams &block_params, Block3D &block, const Block2D &slice, int x);
void setSliceY(const BlockParams &block_params, Block3D &block, const Block2D &slice, int y);
