#include "poisson.h"

#include <cmath>
#include <cassert>

ddl::Writable& operator<<(ddl::Writable &w, const BlockParams &p) {
    return w << p.size_x << p.size_y << p.size_z <<
                p.full_size_x << p.full_size_y << p.full_size_z <<
                p.index_x << p.index_y <<
                p.shift_x << p.shift_y <<
                p.shadow_x[0] << p.shadow_x[1] <<
                p.shadow_y[0] << p.shadow_y[1];
}

ddl::Readable& operator>>(ddl::Readable &r, BlockParams &p) {
    return r >> p.size_x >> p.size_y >> p.size_z >>
                p.full_size_x >> p.full_size_y >> p.full_size_z >>
                p.index_x >> p.index_y >>
                p.shift_x >> p.shift_y >>
                p.shadow_x[0] >> p.shadow_x[1] >>
                p.shadow_y[0] >> p.shadow_y[1];
}

BlockParams getBlockParams(const PoissonParams &params, int block_size_x, int block_size_y, int block_shift_x, int block_shift_y, int ix, int iy) {
    BlockParams block_params;
    block_params.index_x = ix;
    block_params.index_y = iy;
    block_params.shadow_x[0] = (ix > 0);
    block_params.shadow_y[0] = (iy > 0);
    block_params.shadow_x[1] = (ix < params.num_blocks_x - 1);
    block_params.shadow_y[1] = (iy < params.num_blocks_y - 1);
    block_params.size_x = block_size_x; // size of fragment by X
    block_params.size_y = block_size_y; // size of fragment by Y
    block_params.size_z = params.mesh_size_z;
    block_params.full_size_x = block_params.size_x + block_params.shadow_x[0] + block_params.shadow_x[1]; // size of fragment by X (incl. shadow)
    block_params.full_size_y = block_params.size_y + block_params.shadow_y[0] + block_params.shadow_y[1]; // size of fragment by Y (incl. shadow)
    block_params.full_size_z = block_params.size_z;
    block_params.shift_x = block_shift_x; // shift of fragment by X
    block_params.shift_y = block_shift_y; // shift of fragment by Y
    return block_params;
}

Block3D initBlock(const PoissonParams &params, const BlockParams &block_params, Func3D init_func) {
    Block3D block(Range3D {static_cast<size_t>(block_params.full_size_x),
                           static_cast<size_t>(block_params.full_size_y),
                           static_cast<size_t>(block_params.full_size_z)});
    for (int i = 0; i < block_params.full_size_x; i++)
    for (int j = 0; j < block_params.full_size_y; j++)
    for (int k = 0; k < block_params.full_size_z; k++) {
        const int gi = i + block_params.shift_x - block_params.shadow_x[0];
        const int gj = j + block_params.shift_y - block_params.shadow_y[0];
        const int gk = k;
        if ((gi == 0) || (gi == params.mesh_size_x - 1) ||
            (gj == 0) || (gj == params.mesh_size_y - 1) ||
            (gk == 0) || (gk == params.mesh_size_z - 1)) {
            block(i, j, k) = init_func(gi * params.step_x, gj * params.step_y, gk * params.step_z);
        }
    }
    return block;
}

Block3D emptyBlock(const BlockParams &block_params) {
    return Block3D(Range3D {static_cast<size_t>(block_params.full_size_x),
                            static_cast<size_t>(block_params.full_size_y),
                            static_cast<size_t>(block_params.full_size_z)});
}

Block3D fillBlock(const PoissonParams &params, const BlockParams &block_params, Func3D init_func) {
    Block3D block(Range3D {static_cast<size_t>(block_params.full_size_x),
                           static_cast<size_t>(block_params.full_size_y),
                           static_cast<size_t>(block_params.full_size_z)});
    for (int i = 0; i < block_params.full_size_x; i++)
    for (int j = 0; j < block_params.full_size_y; j++)
    for (int k = 0; k < block_params.full_size_z; k++) {
        const int gi = i + block_params.shift_x - block_params.shadow_x[0];
        const int gj = j + block_params.shift_y - block_params.shadow_y[0];
        const int gk = k;
        block(i, j, k) = init_func(gi * params.step_x, gj * params.step_y, gk * params.step_z);
    }
    return block;
}

void calcBlock(const PoissonParams &params, const BlockParams &block_params, Block3D &block,
               const Block3D &prev_block, const Block3D &right) {
    const double OWX = params.step_x * params.step_x;
    const double OWY = params.step_y * params.step_y;
    const double OWZ = params.step_z * params.step_z;
    const double OWX1 = 1.0/OWX;
    const double OWY1 = 1.0/OWY;
    const double OWZ1 = 1.0/OWZ;
    const double C = 2.0*OWX1 + 2.0*OWY1 + 2.0*OWZ1;
    const double C1 = 1.0/C;
    for (int i = 1; i < block_params.full_size_x - 1; i++)
    for (int j = 1; j < block_params.full_size_y - 1; j++)
    for (int k = 1; k < block_params.full_size_z - 1; k++) {
        const double Fi = (prev_block(i + 1, j, k) + prev_block(i - 1, j, k)) * OWX1;
        const double Fj = (prev_block(i, j + 1, k) + prev_block(i, j - 1, k)) * OWY1;
        const double Fk = (prev_block(i, j, k + 1) + prev_block(i, j, k - 1)) * OWZ1;
        const double Ra = right(i, j, k);
        block(i, j, k) = (Fi + Fj + Fk - Ra) * C1;
    }
}

Block2D getSliceX(const Block3D &block, const BlockParams &block_params, int x) {
    assert (x >= 0 && x < block_params.full_size_x);
    Block2D slice(Range2D {static_cast<size_t>(block_params.size_y),
                           static_cast<size_t>(block_params.size_z)});
    for (int j = 0; j < block_params.size_y; j++)
    for (int k = 0; k < block_params.size_z; k++) {
        slice(j, k) = block(x, j + block_params.shadow_y[0], k);
    }
    return slice;
}

Block2D getSliceY(const Block3D &block, const BlockParams &block_params, int y) {
    assert (y >= 0 && y < block_params.full_size_y);
    Block2D slice(Range2D {static_cast<size_t>(block_params.size_x),
                           static_cast<size_t>(block_params.size_z)});
    for (int i = 0; i < block_params.size_x; i++)
    for (int k = 0; k < block_params.size_z; k++) {
        slice(i, k) = block(i + block_params.shadow_x[0], y, k);
    }
    return slice;
}

void setSliceX(const BlockParams &block_params, Block3D &block, const Block2D &slice, int x) {
    assert (x >= 0 && x < block_params.full_size_x);
    for (int j = 0; j < block_params.size_y; j++)
    for (int k = 0; k < block_params.size_z; k++) {
        block(x, j + block_params.shadow_y[0], k) = slice(j, k);
    }
}

void setSliceY(const BlockParams &block_params, Block3D &block, const Block2D &slice, int y) {
    assert (y >= 0 && y < block_params.full_size_y);
    for (int i = 0; i < block_params.size_x; i++)
    for (int k = 0; k < block_params.size_z; k++) {
        block(i + block_params.shadow_x[0], y, k) = slice(i, k);
    }
}

double CalcBlockMaxDiff(const PoissonParams &params, const BlockParams &block, double *prev_data, double *data) {
    double maxdiff = 0.0;
    for (int i = 1; i < block.size_x - 1; i++)
    for (int j = 1; j < block.size_y - 1; j++)
    for (int k = 1; k < params.mesh_size_z - 1; k++) {
        const double diff = std::fabs(data[block.index(i, j, k)] - prev_data[block.index(i, j, k)]);
        if (diff > maxdiff) {
            maxdiff = diff;
        }
    }
    return maxdiff;
}
