win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../poisson_lib/release/ -lpoisson_lib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../poisson_lib/debug/ -lpoisson_lib
else:unix: LIBS += -L$$OUT_PWD/../poisson_lib/ -lpoisson_lib

INCLUDEPATH += $$PWD/poisson_lib
DEPENDPATH += $$PWD/poisson_lib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../poisson_lib/release/libpoisson_lib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../poisson_lib/debug/libpoisson_lib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../poisson_lib/release/poisson_lib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../poisson_lib/debug/poisson_lib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../poisson_lib/libpoisson_lib.a
