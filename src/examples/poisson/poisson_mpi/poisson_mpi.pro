TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

DIDAL_OUT_DIR = $$OUT_PWD/../../../didal
include(../../../didal_lib.pri)
include(../poisson_lib.pri)

SOURCES += \
    main.cpp 

HEADERS += 
