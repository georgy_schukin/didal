#include "poisson.h"
#include "didal/util/timer.h"
#include "didal/decomp/multi_block_decomposition.h"
#include "didal/distr/mesh_block_distribution.h"

#include <mpi.h>
#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <stdexcept>

using Index2D = ddl::Index<2>;
using Index3D = ddl::Index<3>;
using MeshDecomposition = ddl::MultuBlockDecomposition<3>;
using MeshDistribution = ddl::MeshBlockDistribution<2>;

int intArg(int arg_num, int default_value, int argc, char **argv) {
    return (argc > arg_num ? std::stoi(argv[arg_num]) : default_value);
}

double init_func(double x, double y, double z) {
     return x + y + z;
}

double right_func(double x, double y, double z) {
     return 0;
}

double timedBarrier(ddl::CommService *cs) {
    ddl::Timer timer;
    cs->getCommunicator()->barrier();
    return timer.time();
}

MPI_Request& newReq(std::vector<MPI_Request> &reqs) {
    reqs.push_back(MPI_Request {});
    return reqs.back();
}

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    int size_x = intArg(1, 100, argc, argv);
    int size_y = intArg(2, 100, argc, argv);
    int size_z = intArg(3, 100, argc, argv);
    int num_of_iters = intArg(4, 100, argc, argv);
    const bool scale_size = intArg(5, 0, argc, argv);

    MPI_Comm cart_comm;
    int rank, size;

    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int dims[2] = {0, 0};
    MPI_Dims_create(size, 2, dims);

    int periods[2] = {0, 0};
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &cart_comm);

    MPI_Comm_rank(cart_comm, &rank);

    int coords[2];
    MPI_Cart_coords(cart_comm, rank, 2, coords); // get process indices by X and Y

    // Size scaling mode: mesh size by X and Y denote mesh block size for each node.
    // Scale global mesh size by X and Y appropriately.
    if (scale_size) {
        size_x *= dims[0];
        size_y *= dims[1];
    }

    PoissonParams params;
    params.mesh_size_x = size_x;
    params.mesh_size_y = size_y;
    params.mesh_size_z = size_z;
    params.num_blocks_x = dims[0];
    params.num_blocks_y = dims[1];
    params.area_size_x = 1.0;
    params.area_size_y = 1.0;
    params.area_size_z = 1.0;
    params.step_x = params.area_size_x / (params.mesh_size_x - 1);
    params.step_y = params.area_size_y / (params.mesh_size_y - 1);
    params.step_z = params.area_size_z / (params.mesh_size_z - 1);

    MeshDecomposition mesh_decomp {{params.mesh_size_x, params.num_blocks_x}, {params.mesh_size_y, params.num_blocks_y}};
    MeshDistribution mesh_distr {{params.num_blocks_x, dims[0]}, {params.num_blocks_y, dims[1]}};

    const auto block_params = getBlockParams(params,
                                             mesh_decomp.blockSize(0, coords[0]), mesh_decomp.blockSize(1, coords[1]),
                                             mesh_decomp.blockShift(0, coords[0]), mesh_decomp.blockShift(1, coords[1]),
                                             coords[0], coords[1]);

    Block3D a = initBlock(params, block_params, init_func);
    Block3D a_new = emptyBlock(block_params);
    Block3D right = fillBlock(params, block_params, right_func);

    MPI_Datatype x_type, y_type; // type for shadow exchange by X and Y
    MPI_Type_vector(block_params.full_size_y, block_params.full_size_z, 0, MPI_DOUBLE, &x_type);
    MPI_Type_vector(block_params.full_size_x, block_params.full_size_z, block_params.full_size_y * block_params.full_size_z, MPI_DOUBLE, &y_type);
    MPI_Type_commit(&x_type);
    MPI_Type_commit(&y_type);

    int neigh_x[2], neigh_y[2];
    MPI_Cart_shift(cart_comm, 0, 1, &neigh_x[0], &neigh_x[1]); // by X: left nd right neighbour
    MPI_Cart_shift(cart_comm, 1, 1, &neigh_y[0], &neigh_y[1]); // by Y: top and bottom neighbour

    double comp_time = 0;
    double update_time = 0;
    double copy_time = 0;

    ddl::Timer total_timer;

    std::vector<MPI_Request> reqs;

    for (int iter = 0; iter < num_of_iters; iter++) {
        ddl::Timer update_timer;
        // Update right shadow by x.
        if (block_params.shadow_x[1]) {
            MPI_Isend(&a(block_params.full_size_x - 2,0,0), 1, x_type, neigh_x[1], 1, cart_comm, &newReq(reqs)); // send data to shadow
            MPI_Irecv(&a(block_params.full_size_x - 1,0,0), 1, x_type, neigh_x[1], 2, cart_comm, &newReq(reqs)); // recv data in shadow
        }
        // Update left shadow by x.
        if (block_params.shadow_x[0]) {
            MPI_Isend(&a(1,0,0), 1, x_type, neigh_x[0], 2, cart_comm, &newReq(reqs)); // send to shadow
            MPI_Irecv(&a(0,0,0), 1, x_type, neigh_x[0], 1, cart_comm, &newReq(reqs)); // recv in shadow
        }
        // Update top shadow by y.
        if (block_params.shadow_y[1]) {
            MPI_Isend(&a(0,block_params.full_size_y - 2,0), 1, y_type, neigh_y[1], 3, cart_comm, &newReq(reqs)); // send data to shadow
            MPI_Irecv(&a(0,block_params.full_size_y - 1,0), 1, y_type, neigh_y[1], 4, cart_comm, &newReq(reqs)); // recv data in shadow
        }
        // Update bottom shadow by y.
        if (block_params.shadow_y[0]) {
            MPI_Isend(&a(0,1,0), 1, y_type, neigh_y[0], 4, cart_comm, &newReq(reqs)); // send to shadow
            MPI_Irecv(&a(0,0,0), 1, y_type, neigh_y[0], 3, cart_comm, &newReq(reqs)); // recv in shadow
        }
        MPI_Waitall(static_cast<int>(reqs.size()), reqs.data(), MPI_STATUSES_IGNORE);
        update_time += update_timer.time();

        ddl::Timer comp_timer;
        calcBlock(params, block_params, a_new, a, right);
        comp_time += comp_timer.time();

        ddl::Timer copy_timer;
        a = a_new;
        copy_time += copy_timer.time();
    }

    const auto own_time = total_timer.time();

    MPI_Barrier(cart_comm);

    const auto time = total_timer.time();

    std::ostringstream out;

    if (rank == 0) {
        out << "Mesh: " << params.mesh_size_x << " x " << params.mesh_size_y << " x " << params.mesh_size_z <<
               ", grid: " << dims[0] << " x " << dims[1] <<
               ", iters: " << num_of_iters <<
               ", time: " << time <<
               std::endl;
    }
    out << "[Node " << rank << "]: own time: " << own_time <<
           ", comp time: " << comp_time <<
           ", update time: " << update_time <<
           ", copy time: " << copy_time <<
           std::endl;

    std::cout << out.str();

    MPI_Type_free(&x_type);
    MPI_Type_free(&y_type);
    MPI_Finalize();

    return 0;
}
