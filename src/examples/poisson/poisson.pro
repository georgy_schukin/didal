TEMPLATE = subdirs

SUBDIRS += \  
    poisson \
    poisson_fp \
    poisson_lib \
    poisson_mpi \
    poisson_sync

poisson.depends = poisson_lib
poisson_fp.depends = poisson_lib
poisson_mpi.depends = poisson_lib
poisson_sync.depends = poisson_lib
