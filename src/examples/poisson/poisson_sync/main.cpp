#include "poisson.h"
#include "didal/didal.h"
#include "didal/util/slicer.h"
#include "didal/util/timer.h"
#include "didal/util/lattice.h"
#include "didal/decomp/multi_block_decomposition.h"
#include "didal/distr/mesh_block_distribution.h"

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <stdexcept>

using Index2D = ddl::Index<2>;
using Index3D = ddl::Index<3>;
using BlockPtr = std::shared_ptr<Block3D>;
using Mesh3D = ddl::DistributedStorage<Index2D, Block3D>;
using SyncMesh3D = ddl::DistributedStorage<Index3D, bool>;
using SyncFuture = std::future<std::shared_ptr<bool>>;
using MeshDecomposition = ddl::MultuBlockDecomposition<3>;
using MeshDistribution = ddl::MeshBlockDistribution<2>;

double init_func(double x, double y, double z) {
     return x + y + z;
}

double right_func(double x, double y, double z) {
     return 0;
}

enum BorderType {
    LEFT = 0,
    RIGHT,
    TOP,
    BOTTOM
};

template <typename ReturnType, typename Getter, typename... Args>
std::future<ReturnType> callGetter(Getter getter, double &time, Args... args) {
    ddl::Timer timer;
    auto result = getter(args...);
    time += timer.time();
    return result;
}

double timedBarrier(ddl::CommService *cs) {
    ddl::Timer timer;
    cs->getCommunicator()->barrier();
    return timer.time();
}

Index3D iterIndex(const Index2D &index, int iter) {
    return Index3D {iter, index[0], index[1]};
}

SyncFuture getWhenReady(SyncMesh3D &mesh, const Index3D &index, int node) {
    return mesh.getWhenReadyFrom(node, index);
}

double waitForF(SyncFuture &future) {
    ddl::Timer timer;
    future.get();
    return timer.time();
}

double waitFor(SyncMesh3D &mesh, const Index3D &index, int node) {
    ddl::Timer timer;
    mesh.getWhenReadyFrom(node, index).get();
    return timer.time();
}

std::vector<std::pair<Index2D, BorderType>> getBorders(const Index2D &index, const PoissonParams &params) {
    std::vector<std::pair<Index2D, BorderType>> result;
    if (index[0] > 0) {
        auto left_index = Index2D {index[0] - 1, index[1]};
        result.push_back(std::make_pair(left_index, LEFT));
    }
    if (index[0] < params.num_blocks_x - 1) {
        auto right_index = Index2D {index[0] + 1, index[1]};
        result.push_back(std::make_pair(right_index, RIGHT));
    }
    if (index[1] > 0) {
        auto top_index = Index2D {index[0], index[1] - 1};
        result.push_back(std::make_pair(top_index, TOP));
    }
    if (index[1] < params.num_blocks_y - 1) {
        auto bottom_index = Index2D {index[0], index[1] + 1};
        result.push_back(std::make_pair(bottom_index, BOTTOM));
    }
    return result;
}

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    int size_x = (argc > 1 ? std::stoi(argv[1]) : 100);
    int size_y = (argc > 2 ? std::stoi(argv[2]) : 100);
    int size_z = (argc > 3 ? std::stoi(argv[3]) : 100);
    int num_of_fragments_x = (argc > 4 ? std::stoi(argv[4]) : 10);
    int num_of_fragments_y = (argc > 5 ? std::stoi(argv[5]) : 10);
    int num_of_iters = (argc > 6 ? std::stoi(argv[6]) : 100);
    int nodes_by_x = (argc > 7 ? std::stoi(argv[7]) : 0);
    int nodes_by_y = (argc > 8 ? std::stoi(argv[8]) : 0);
    const bool scale_size = (argc > 9 ? std::stoi(argv[9]) : 0);
    const bool scale_fragments = (argc > 10 ? std::stoi(argv[10]) : 0);

    const int my_rank = env.thisNode().rank();
    const int num_of_nodes = env.allNodes().size();

    std::tie(nodes_by_x, nodes_by_y) = ddl::Lattice::getLatticeSides2D(num_of_nodes, nodes_by_x, nodes_by_y);

    if (nodes_by_x <=0 || nodes_by_y <= 0 || nodes_by_x * nodes_by_y != num_of_nodes) {
        throw std::runtime_error("Incorrect grid sizes!");
    }

    // Size scaling mode: mesh size by X and Y denote mesh block size for each node.
    // Scale global mesh size by X and Y appropriately.
    if (scale_size) {
        size_x *= nodes_by_x;
        size_y *= nodes_by_y;
    }

    // Num of fragments scaling mode: num of fragments by X and Y denote num of fragments on each node.
    // Scale global num of fragments by X and Y appropriately.
    if (scale_fragments) {
        num_of_fragments_x *= nodes_by_x;
        num_of_fragments_y *= nodes_by_y;
    }

    PoissonParams params;
    params.mesh_size_x = size_x;
    params.mesh_size_y = size_y;
    params.mesh_size_z = size_z;
    params.num_blocks_x = num_of_fragments_x;
    params.num_blocks_y = num_of_fragments_y;
    params.area_size_x = 1.0;
    params.area_size_y = 1.0;
    params.area_size_z = 1.0;
    params.step_x = params.area_size_x / (params.mesh_size_x - 1);
    params.step_y = params.area_size_y / (params.mesh_size_y - 1);
    params.step_z = params.area_size_z / (params.mesh_size_z - 1);

    MeshDecomposition mesh_decomp {{params.mesh_size_x, params.num_blocks_x}, {params.mesh_size_y, params.num_blocks_y}};
    MeshDistribution mesh_distr {{params.num_blocks_x, nodes_by_x}, {params.num_blocks_y, nodes_by_y}};

    std::map<Index2D, int> distribution;
    std::vector<Index2D> my_indices;
    std::map<Index2D, BlockParams> block_params;

    for (int i = 0; i < params.num_blocks_x; i++) {
        for (int j = 0; j < params.num_blocks_y; j++) {
            Index2D index {i, j};
            const auto n_index = mesh_distr.nodeIndex(index);
            const auto node = n_index[0] * nodes_by_y + n_index[1];
            distribution[index] = node;
            if (node == my_rank) {
                my_indices.push_back(index);
            }
        }
    }

    Mesh3D a(&env);
    Mesh3D a_new(&env);
    Mesh3D right(&env);
    SyncMesh3D a_actual(&env), a_updated(&env);

    double update_get_time = 0;

    auto getSliceXTimed = [&update_get_time](const Block3D &block, const BlockParams &block_params, int x) -> Block2D {
        ddl::Timer timer;
        auto result = getSliceX(block, block_params, x);
        update_get_time += timer.time();
        return result;
    };

    auto getSliceYTimed = [&update_get_time](const Block3D &block, const BlockParams &block_params, int y) -> Block2D {
        ddl::Timer timer;
        auto result = getSliceY(block, block_params, y);
        update_get_time += timer.time();
        return result;
    };

    auto x_getter = a.makeGetter<decltype(getSliceXTimed), Block2D, const BlockParams&, int>(getSliceXTimed);
    auto y_getter = a.makeGetter<decltype(getSliceYTimed), Block2D, const BlockParams&, int>(getSliceYTimed);

    for (const auto &p: distribution) {
        const auto &index = p.first;
        const auto block_p = getBlockParams(params,
                                            mesh_decomp.blockSize(0, index[0]), mesh_decomp.blockSize(1, index[1]),
                                            mesh_decomp.blockShift(0, index[0]), mesh_decomp.blockShift(1, index[1]),
                                            index[0], index[1]);
        block_params[index] = block_p;
        if (p.second == my_rank) {
            a.create(index, initBlock(params, block_p, init_func));
            a_new.create(index, emptyBlock(block_p));
            right.create(index, fillBlock(params, block_p, right_func));
            a_actual.create(iterIndex(index, 0), true);
        }
    }

    env.getCommService()->getCommunicator()->barrier();

    double comp_time = 0;
    double comp_func_time = 0;
    double update_time = 0;
    double update_set_time = 0;
    double update_getter_time = 0;
    double update_wait_time = 0;
    double copy_time = 0;
    double copy_func_time = 0;
    double sync_init_time = 0;
    double sync_wait_time = 0;

    ddl::Timer total_timer;

    for (int iter = 0; iter < num_of_iters; iter++) {
        ddl::Timer update_timer;
        std::map<Index2D, std::map<Index2D, SyncFuture>> border_sync;
        ddl::Timer border_sync_timer;
        for (const auto &index: my_indices) {
            for(const auto &b: getBorders(index, params)) {
                const auto &neigh_index = b.first;
                border_sync[index][neigh_index] = getWhenReady(a_actual, iterIndex(neigh_index, iter), distribution[neigh_index]);
            }
        }
        sync_init_time += border_sync_timer.time();
        std::map<Index2D, std::vector<std::tuple<BorderType, std::future<Block2D>>>> borders;
        for (const auto &index: my_indices) {
            borders[index].clear();
            for (const auto &b: getBorders(index, params)) {
                const auto &neigh_index = b.first;
                const auto border_type = b.second;
                // Wait when neighbor data are actual so we can safely read it.
                sync_wait_time += waitForF(border_sync[index][neigh_index]); // wait for sync
                switch (border_type) {
                case LEFT: {
                    auto future = callGetter<Block2D>(x_getter, update_getter_time,
                                                distribution[neigh_index], neigh_index,
                                                block_params[neigh_index], block_params[neigh_index].full_size_x - 2);
                    borders[index].push_back(std::make_tuple(LEFT, std::move(future)));
                    break;
                }
                case RIGHT: {
                    auto future = callGetter<Block2D>(x_getter, update_getter_time,
                                                distribution[neigh_index], neigh_index,
                                                block_params[neigh_index], 1);
                    borders[index].push_back(std::make_tuple(RIGHT, std::move(future)));
                    break;
                }
                case TOP: {
                    auto future = callGetter<Block2D>(y_getter, update_getter_time,
                                                distribution[neigh_index], neigh_index,
                                                block_params[neigh_index], block_params[neigh_index].full_size_y - 2);
                    borders[index].push_back(std::make_tuple(TOP, std::move(future)));
                    break;
                }
                case BOTTOM: {
                    auto future = callGetter<Block2D>(y_getter, update_getter_time,
                                                distribution[neigh_index], neigh_index,
                                                block_params[neigh_index], 1);
                    borders[index].push_back(std::make_tuple(BOTTOM, std::move(future)));
                    break;
                }
                }
            }
        }
        for (auto &b: borders) {
            const auto &index = b.first;
            const auto &b_params = block_params[index];
            for (auto &p: b.second) {
                ddl::Timer update_wait_timer;
                const auto type = std::get<0>(p);
                auto &block = *(a.get(index).get());
                auto slice = std::get<1>(p).get();
                update_wait_time += update_wait_timer.time();
                ddl::Timer update_set_timer;
                switch (type) {
                    case LEFT: setSliceX(b_params, block, slice, 0); break;
                    case RIGHT: setSliceX(b_params, block, slice, b_params.full_size_x - 1); break;
                    case TOP: setSliceY(b_params, block, slice, 0); break;
                    case BOTTOM: setSliceY(b_params, block, slice, b_params.full_size_y - 1); break;
                }
                update_set_time += update_set_timer.time();
            }
            a_updated.create(iterIndex(index, iter), true);
        }
        update_time += update_timer.time();

        ddl::Timer comp_timer;
        for (const auto &index: my_indices) {
            auto &block = *(a_new.get(index).get());
            const auto &prev_block = *(a.get(index).get());
            const auto &right_block = *(right.get(index).get());
            ddl::Timer comp_func_timer;
            calcBlock(params, block_params[index], block, prev_block, right_block);
            comp_func_time += comp_func_timer.time();
        }
        comp_time += comp_timer.time();

        ddl::Timer copy_timer;
        std::map<Index2D, std::map<Index2D, SyncFuture>> copy_sync;
        ddl::Timer copy_sync_timer;
        for (const auto &index: my_indices) {
            for (const auto &b: getBorders(index, params)) {
                const auto &neigh_index = b.first;
                copy_sync[index][neigh_index] = getWhenReady(a_updated, iterIndex(neigh_index, iter), distribution[neigh_index]);
            }
        }
        sync_init_time += copy_sync_timer.time();
        for (const auto &index: my_indices) {
            // Wait when neighbors are updated so we can safely rewrite the data.
            for (const auto &b: getBorders(index, params)) {
                sync_wait_time += waitForF(copy_sync[index][b.first]);
            }
            const auto &block = *(a_new.get(index).get());
            auto &prev_block = *(a.get(index).get());
            ddl::Timer copy_func_timer;
            prev_block = block;
            copy_func_time += copy_func_timer.time();
            a_actual.create(iterIndex(index, iter + 1), true); // make actual for the next iter
        }
        copy_time += copy_timer.time();
    }

    const auto own_time = total_timer.time();

    env.getCommService()->getCommunicator()->barrier();

    const auto time = total_timer.time();

    std::ostringstream out;

    if (my_rank == 0) {
        out << "Mesh: " << params.mesh_size_x << " x " << params.mesh_size_y << " x " << params.mesh_size_z <<
               ", fragments: " << params.num_blocks_x << " x " << params.num_blocks_y <<
               ", grid: " << nodes_by_x << " x " << nodes_by_y <<
               ", iters: " << num_of_iters <<
               ", time: " << time <<
               std::endl;
    }
    out << "[Node " << my_rank << "]: own time: " << own_time <<
           ", comp time: " << comp_time <<
           ", comp func time: " << comp_func_time <<
           ", update time: " << update_time <<
           ", update wait time: " << update_wait_time <<
           ", update get time: " << update_get_time <<
           ", update getter time: " << update_getter_time <<
           ", update set time: " << update_set_time <<
           ", copy time: " << copy_time <<
           ", copy func time: " << copy_func_time <<
           ", sync time: " << sync_init_time + sync_wait_time <<
           std::endl;

    std::cout << out.str();

    return 0;
}
