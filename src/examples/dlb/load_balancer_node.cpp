#include "load_balancer_node.h"

double LoadBalancerNode::getTotalLoad() const {
    double load = 0;
    for (const auto &bin: bins) {
        load += bin.load;
    }
    return load;
}

const std::vector<double> LoadBalancerNode::getLoads() const {
    std::vector<double> loads;
    for (const auto &bin: bins) {
        loads.push_back(bin.load);
    }
    return loads;
}

const std::vector<int> LoadBalancerNode::getIds() const {
    std::vector<int> ids;
    for (const auto &bin: bins) {
        ids.push_back(bin.id);
    }
    return ids;
}

void LoadBalancerNode::addBins(const std::vector<LoadBin> &new_bins, bool to_end) {
    if (to_end) {
        bins.insert(bins.end(), new_bins.begin(), new_bins.end());
    } else {
        bins.insert(bins.begin(), new_bins.begin(), new_bins.end());
    }
}

std::vector<LoadBin> LoadBalancerNode::extractForLoad(double load_to_extract, bool from_start) {
    std::vector<LoadBin> extracted;
    return extracted;
}
