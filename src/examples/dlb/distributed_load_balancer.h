#pragma once

#include "didal/data/sync_distributed_object.h"
#include "load_balancer_node.h"

class DistributedLoadBalancer : public ddl::SyncDistributedObject<LoadBalancerNode> {
public:
    DistributedLoadBalancer(ddl::Environment *env):
        ddl::SyncDistributedObject<LoadBalancerNode>(env) {
        get_load = this->template makeMethodGetter<decltype(&LoadBalancerNode::getTotalLoad),
                double>(&LoadBalancerNode::getTotalLoad);
    }

    void setNeighbors(ddl::Node left, ddl::Node right) {
        left_neighbor = left;
        right_neighbor = right;
    }

    std::future<double> getLoadFrom(ddl::Node node) {
        return get_load(node);
    }

    void balance() {

    }

    void balanceStep(ddl::Node node, double load) {

    }

private:
    ddl::RemoteFunction<double> get_load;
    ddl::Node left_neighbor, right_neighbor;
};
