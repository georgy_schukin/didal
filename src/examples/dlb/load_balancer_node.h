#pragma once

#include <vector>

struct LoadBin {
    int id;
    double load;
    bool is_primary;
};

class LoadBalancerNode {
public:
    LoadBalancerNode() {}

    double getTotalLoad() const;
    const std::vector<double> getLoads() const;
    const std::vector<int> getIds() const;

    void addBins(const std::vector<LoadBin> &new_bins, bool to_end = true);

    std::vector<LoadBin> extractForLoad(double load_to_extract, bool from_start = true);

    const std::vector<LoadBin>& getBins() const {
        return bins;
    }

private:
    std::vector<LoadBin> bins;
    int start, end;
};
