TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        load_balancer_node.cpp \
        main.cpp

DIDAL_OUT_DIR = $$OUT_PWD/../../didal
include(../../didal_lib.pri)

HEADERS += \
    distributed_load_balancer.h \
    load_balancer_node.h
