#include "didal/didal.h"
#include "didal/util/args.h"
#include "didal/util/slicer.h"
#include "didal/util/timer.h"
#include "didal/util/lattice.h"
#include "didal/algorithms/reduce.h"
#include "didal/algorithms/barrier.h"
#include "didal/decomp/multi_block_decomposition.h"
#include "didal/distr/mesh_block_distribution.h"

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <stdexcept>
#include <fstream>
#include <cmath>
#include <cstdio>

using Index3 = ddl::Index<3>;
using MeshDecomposition = ddl::MultuBlockDecomposition<3>;
using MeshDistribution = ddl::MeshBlockDistribution<3>;
using BlockMap = std::map<Index3, int>;

int toRank(const Index3 &index, const MeshDistribution &distr) {
    return index[0] * static_cast<int>(distr.numOfNodes(1)) * static_cast<int>(distr.numOfNodes(2)) +
           index[1] * static_cast<int>(distr.numOfNodes(2)) +
           index[2];
}

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::ArgsParser args(argc, argv);

    int num_of_fragments_x = args.intArg(10);
    int num_of_fragments_y = args.intArg(10);
    int num_of_fragments_z = args.intArg(10);
    int num_of_iters = args.intArg(100);
    int nodes_by_x = args.intArg(0);
    int nodes_by_y = args.intArg(0);
    int nodes_by_z = args.intArg(0);

    const int my_rank = env.thisNode().rank();
    const int num_of_nodes = static_cast<int>(env.allNodes().size());

    std::tie(nodes_by_x, nodes_by_y, nodes_by_z) = ddl::Lattice::getLatticeSides3D(num_of_nodes, nodes_by_x, nodes_by_y, nodes_by_z);

    if (nodes_by_x <=0 || nodes_by_y <= 0 || nodes_by_z <= 0 || nodes_by_x * nodes_by_y * nodes_by_z != num_of_nodes) {
        throw std::runtime_error("Incorrect grid sizes!");
    }

    MeshDistribution mesh_distr {{num_of_fragments_x, nodes_by_x},
                                 {num_of_fragments_y, nodes_by_y},
                                 {num_of_fragments_z, nodes_by_z}};

    BlockMap block_distr;
    for (int i = 0; i < num_of_fragments_x; i++) {
        for (int j = 0; j < num_of_fragments_y; j++) {
            for (int k = 0; k < num_of_fragments_z; k++) {
                Index3 index {i, j, k};
                const auto ni = mesh_distr.nodeIndex(index);
                const auto node = toRank(ni, mesh_distr);
                block_distr[index] = node;
            }
        }
    }

    return 0;
}
