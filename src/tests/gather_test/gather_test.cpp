#include "didal/didal.h"
#include "didal/algorithms/gather.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <chrono>
#include <memory>

using namespace ddl;
using namespace std;

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    const int num_of_iters = (argc > 1 ? stoi(argv[1]) : 1000);    

    auto cs = env.getCommService();
    const auto this_rank = cs->thisNode().rank();
    const auto num_of_nodes = cs->allNodes().size();

    const auto ts = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_of_iters; i++) {
        vector<int> values;
        gather(&env, this_rank, back_inserter(values));
        if (values.size() != num_of_nodes) {
            cerr << this_rank << " gather fail" << endl;
        }
    }
    const auto te = chrono::high_resolution_clock::now();

    ostringstream out;
    out << "Rank " << this_rank << " time = " << chrono::duration<double>(te - ts).count() << endl;
    cout << out.str();

    return 0;
}
