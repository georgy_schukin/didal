#include "didal/didal.h"
#include "didal/locators/tracking_locator.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>

std::string getId(const ddl::Node &node, int num) {
    return std::string("data.") + std::to_string(node.rank()) + "." + std::to_string(num);
}

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::TrackingLocator<std::string> loc(&env, env.allNodes());

    for (int i = 0; i < 10; i++) {
        const auto id = getId(env.thisNode(), i);
        loc.setLocation(id, env.thisNode());
        std::ostringstream out;
        out << env.thisNode() << " : Set location of " << id << " to " << env.thisNode() << std::endl;
        std::cout << out.str();
    }

    for (int i = 0; i < 10; i++) {
        const auto id = getId(ddl::Node((env.thisNode().rank() + 1) % env.allNodes().size()), i);
        std::ostringstream out;
        try {
            const auto node = loc.getLocation(id);
            out << env.thisNode() << " : Location of " << id << " is " << node << std::endl;
        }
        catch (const ddl::TrackingLocator<std::string>::NoLocation&) {
            out << env.thisNode() << " : Failed to get location of " << id << std::endl;
        }
        std::cout << out.str();
    }

    env.getCommService()->getCommunicator()->barrier();

    return 0;
}
