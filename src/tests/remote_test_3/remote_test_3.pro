TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    remote_test_3.cpp

QMAKE_CXXFLAGS += -pg
QMAKE_LFLAGS += -pg

DIDAL_OUT_DIR = $$OUT_PWD/../../didal
include(../../didal_lib.pri)
