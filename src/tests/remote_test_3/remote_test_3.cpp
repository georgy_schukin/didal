#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <atomic>
#include <thread>
#include <chrono>
#include <vector>
#include <algorithm>

using namespace std;
using namespace std::chrono;

class Counter {
public:
    Counter() {}

    void inc(size_t num) {
        count += num;
    }

    size_t get() const {
        return count;
    }

    void reset() {
        count = 0;
    }

private:
    size_t count {0};
};

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    Counter counter;

    ddl::Remote remote(env.getCommService(), &counter);
    ddl::RemoteMethodMaker<Counter> maker(&remote, &counter);

    auto remote_inc = maker.method(&Counter::inc).template proc<size_t>();
    auto remote_get = maker.method(&Counter::get).template func<size_t>();

    size_t n = 1e7;

    const auto this_node = env.thisNode();

    auto ts = high_resolution_clock::now();
    for (size_t i = 0; i < n; i++) {
        counter.inc(i);
    }
    auto te = high_resolution_clock::now();
    std::cout << "Local inc time = " << duration<double>(te - ts).count() << ", count = " << counter.get() << endl;

    counter.reset();

    ts = high_resolution_clock::now();
    for (size_t i = 0; i < n; i++) {
        remote_inc(this_node, i);
    }
    te = high_resolution_clock::now();
    std::cout << "Remote inc time = " << duration<double>(te - ts).count() << ", count = " << counter.get() << endl;

    size_t value = 0;

    ts = high_resolution_clock::now();
    for (size_t i = 0; i < n; i++) {
        value = std::max(value, counter.get());
    }
    te = high_resolution_clock::now();
    std::cout << "Local get time = " << duration<double>(te - ts).count() << ", count = " << value << endl;

    value = 0;

    ts = high_resolution_clock::now();
    for (size_t i = 0; i < n; i++) {
        value = std::max(value, remote_get(this_node).get());
    }
    te = high_resolution_clock::now();
    std::cout << "Remote get time = " << duration<double>(te - ts).count() << ", count = " << value << endl;

    return 0;
}
