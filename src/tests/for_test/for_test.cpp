#include "didal/didal.h"
#include "didal/algorithms/foreach.h"

#include <iostream>
#include <string>
#include <sstream>

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    using DArray = ddl::DistributedArray<int>;

    /*DArray a(&env, 100, ddl::BlockDecomposition(5), ddl::UniformDistribution(), env.allNodes());

    ddl::forEach (a, [](size_t, DArray::FragmentType &block) {
        for (size_t i = 0; i < block.size(); i++) {
            block[i] = block.toGlobalIndex(i) * 2;
        }
    });

    ddl::forOne (a, 1, [](size_t id, const DArray::FragmentType &block) {
        std::cout << "Start ob block " << id << " is " << block.range().start() << std::endl;
    });

    ddl::forSeveral (a, std::vector<int> {2, 4, 6}, [](size_t id, const DArray::FragmentType &block) {
        std::cout << "Start ob block " << id << " is " << block.range().start() << std::endl;
    });

    ddl::forSelected (a, [](size_t id) { return id % 2 == 0; }, [](size_t id, const DArray::FragmentType &block) {
        std::cout << "Start ob block " << id << " is " << block.range().start() << std::endl;
    });*/

    return 0;
}
