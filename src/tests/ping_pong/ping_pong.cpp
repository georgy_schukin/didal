#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::DistributedStorage<int, int> storage(&env);

    int my_rank = env.thisNode().rank();

    if (my_rank == 0) {
        storage.add(my_rank, 0);
    } else if (my_rank == env.allNodes().size() - 1) {
        int val = *storage.getFrom(0, 0).get();
        std::cout << "Value is " << val << std::endl;
    }

    return 0;
}
