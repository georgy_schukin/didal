#include "didal/didal.h"
#include "didal/util/timer.h"

#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <chrono>

int main(int argc, char **argv) {
    const size_t size = (argc > 1 ? std::stoi(argv[1]) : 100);
    const size_t repeats = (argc > 2 ? std::stoi(argv[2]) : 1);

    ddl::ArrayND<double, 1> ddl_array_1(ddl::IndexRangeND<1>{size}, 1.0);
    ddl::ArrayND<double, 2> ddl_array_2(ddl::IndexRangeND<2>{size, size}, 1.0);
    ddl::ArrayND<double, 3> ddl_array_3(ddl::IndexRangeND<3>{size, size, size}, 1.0);

    std::vector<double> array_1(size, 1.0);
    std::vector<double> array_2(size*size, 1.0);
    std::vector<double> array_3(size*size*size, 1.0);

    ddl::Timer timer;
    for (size_t n = 0; n < repeats; n++)
    for (size_t i = 1; i < size - 1; i++)
    {
        ddl_array_1(i) = ddl_array_1(i - 1) + ddl_array_1(i + 1);
    }
    auto time = timer.time();
    std::cout << "DDL array1d time: " << time << std::endl;

    timer.reset();
    for (size_t n = 0; n < repeats; n++)
    for (size_t i = 1; i < size - 1; i++)
    for (size_t j = 1; j < size - 1; j++)
    {
        ddl_array_2(i, j) = ddl_array_2(i - 1, j) + ddl_array_2(i + 1, j) +
                            ddl_array_2(i, j - 1) + ddl_array_2(i, j + 1);
    }
    time = timer.time();
    std::cout << "DDL array2d time: " << time << std::endl;

    timer.reset();
    for (size_t n = 0; n < repeats; n++)
    for (size_t i = 1; i < size - 1; i++)
    for (size_t j = 1; j < size - 1; j++)
    for (size_t k = 1; k < size - 1; k++)
    {
        ddl_array_3(i, j, k) = ddl_array_3(i - 1, j, k) + ddl_array_3(i + 1, j, k) +
                               ddl_array_3(i, j - 1, k) + ddl_array_3(i, j + 1, k) +
                               ddl_array_3(i, j, k - 1) + ddl_array_3(i, j, k + 1);
    }
    time = timer.time();
    std::cout << "DDL array3d time: " << time << std::endl;

    timer.reset();
    for (size_t n = 0; n < repeats; n++)
    for (size_t i = 1; i < size - 1; i++)
    {
        array_1[i] = array_1[i - 1] + array_1[i + 1];
    }
    time = timer.time();
    std::cout << "STD array1d time: " << time << std::endl;

    timer.reset();
    for (size_t n = 0; n < repeats; n++)
    for (size_t i = 1; i < size - 1; i++)
    for (size_t j = 1; j < size - 1; j++)
    {
        array_2[i*size + j] = array_2[(i - 1)*size + j] + array_2[(i + 1)*size + j] +
                              array_2[i*size + j - 1] + array_2[i*size + j + 1];
    }
    time = timer.time();
    std::cout << "STD array2d time: " << time << std::endl;

    timer.reset();
    for (size_t n = 0; n < repeats; n++)
    for (size_t i = 1; i < size - 1; i++)
    for (size_t j = 1; j < size - 1; j++)
    for (size_t k = 1; k < size - 1; k++)
    {
        array_3[i*size*size + j*size + k] = array_3[(i - 1)*size*size + j*size + k] + array_3[(i + 1)*size*size + j*size + k] +
                                            array_3[i*size*size + (j - 1)*size + k] + array_3[i*size*size + (j + 1)*size + k] +
                                            array_3[i*size*size + j*size + k - 1] + array_3[i*size*size + j*size + k + 1];
    }
    time = timer.time();
    std::cout << "STD array3d time: " << time << std::endl;

    return 0;
}
