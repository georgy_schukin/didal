#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::SyncDistributedStorage<int, std::string> storage(&env);

    int my_rank = env.thisNode().rank();

    storage.add(my_rank, std::string("EP1.") + std::to_string(my_rank));

    env.getCommService()->getCommunicator()->barrier();

    for (auto node: env.allNodes()) {
        const auto data_id = node.rank();
        std::ostringstream out;
        auto data = storage.getWhenReadyFrom(node, data_id);
        out << "Node " << my_rank << ": 1st epoch data " << data_id << " is " << *data.get() << std::endl;
        std::cout << out.str();
    }

    env.getCommService()->getCommunicator()->barrier();

    storage.nextEpoch();

    auto my_data = storage.get(my_rank);
    *(my_data.get()) = std::string("EP2.") + std::to_string(my_rank);
    storage.signalEpoch(my_rank);

    for (auto node: env.allNodes()) {
        const auto data_id = node.rank();
        std::ostringstream out;
        auto data = storage.getWhenReadyFrom(node, data_id);
        out << "Node " << my_rank << ": 2nd epoch data " << data_id << " is " << *data.get() << std::endl;
        std::cout << out.str();
    }

    env.getCommService()->getCommunicator()->barrier();

    return 0;
}
