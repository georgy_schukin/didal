TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    sync_storage_test.cpp

DIDAL_OUT_DIR = $$OUT_PWD/../../didal
include(../../didal_lib.pri)
