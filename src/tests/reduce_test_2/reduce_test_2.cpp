#include "didal/didal.h"
#include "didal/algorithms/reduce.h"
#include "didal/algorithms/barrier.h"
#include "didal/util/timer.h"

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;
using namespace ddl;

template <typename T, typename Oper>
T myReduce(Environment *env, const T &value, Oper oper, T init) {
    DistributedStorage<int, T> data(env);
    data.create(0, value);
    T result = init;
    vector<future<shared_ptr<T>>> fs;
    for (auto node: env->allNodes()) {
        fs.push_back(data.getWhenReadyFrom(node, 0));
    }
    for (auto &f: fs) {
        result = oper(result, *(f.get().get()));
    }
    localBarrier(env, env->allNodes());
    return result;
}

int main(int argc, char **argv) {

    Environment env(&argc, &argv);

    const int num_of_tries = (argc > 1 ? stoi(argv[1]) : 1000);
    const int use_my_reduce = (argc > 2 ? stoi(argv[2]) : 0);

    const int my_rank = env.thisNode().rank();
    const int num_of_nodes = env.allNodes().size();

    Timer timer;
    for (int i = 0; i < num_of_tries; i++) {
        const auto value = use_my_reduce ?
                    myReduce(&env, my_rank, ReduceOperationMax<int> {}, 0) :
                    ddl::reduceValues(&env, my_rank, ReduceOperationMax<int> {}, 0);
        if (value != num_of_nodes - 1) {
            cerr << my_rank << " reduce failed" << endl;
        }
    }
    const auto time = timer.time();

    ostringstream out;
    out << my_rank << " time = " << time << endl;
    cout << out.str();

    return 0;
}
