#include "didal/didal.h"
#include "didal/algorithms/reduce.h"

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::DistributedStorage<int, int> data(&env);

    for (int i = 0; i < 10; i++) {
        data.add(i, i);
    }

    const auto sum = ddl::reduce(data, [](shared_ptr<int> a) {return *(a.get());}, [](int a, int b) { return a + b; }, 0);

    cout << "Sum is " << sum << endl;

    return 0;
}
