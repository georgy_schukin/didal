#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <memory>

using namespace ddl;
using namespace std;

class Listener : public CommListener {
public:
    Listener(int rank, int num_of_nodes) :
        rank(rank), num_of_nodes(num_of_nodes) {
    }

    void onMessage(const Message &msg, const Node &src) override {
        lock_guard<mutex> lock(mut);
        if (msg.tag() == 1) {
            exit_cnt++;
            cond.notify_all();
        } else {
            msg_cnt++;
        }
    }

    int getMessagesCount() const {
        lock_guard<mutex> lock(mut);
        return msg_cnt;
    }

    void wait() {
        unique_lock<mutex> lock(mut);
        cond.wait(lock, [this]() { return this->exit_cnt >= this->num_of_nodes; });
    }

private:
    int msg_cnt {0};
    int rank;
    int num_of_nodes {0};
    int exit_cnt {0};
    mutable mutex mut;
    mutable condition_variable cond;
};

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    const int num_of_messages = (argc > 1 ? stoi(argv[1]) : 1000);
    const int max_msg_size = (argc > 2 ? stoi(argv[2]) : 65535);
    const int max_delay = (argc > 3 ? stoi(argv[3]) : 0);
    const int max_listeners = (argc > 4 ? stoi(argv[4]) : 1);

    auto cs = env.getCommService();

    const auto this_rank = cs->thisNode().rank();
    const auto num_of_nodes = cs->allNodes().size();

    vector<shared_ptr<Listener>> listeners;
    vector<CommId> cids;

    for (int i = 0; i < max_listeners; i++) {
        auto listener = make_shared<Listener>(this_rank, num_of_nodes);
        auto cid = cs->newCommId<Listener>();
        cs->subscribe(cid, listener.get());
        listeners.push_back(listener);
        cids.push_back(cid);
    }

    for (int i = 0; i < num_of_messages; i++) {
        Message msg(rand() % max_msg_size, cids[rand() % max_listeners], 0);
        cs->sendMessage(msg, rand() % num_of_nodes);
        if (max_delay > 0) {
            this_thread::sleep_for(chrono::microseconds(rand() % max_delay));
        }
    }

    for (int i = 0; i < num_of_nodes; i++) {
        for (const auto &cid: cids) {
            Message msg(cid, 1);
            cs->sendMessage(msg, i);
        }
    }

    size_t cnt = 0;
    for (auto listener: listeners) {
        listener->wait();
        ostringstream out;
        out << "Rank " << this_rank << " listener " << cids[cnt] << " received " << listener->getMessagesCount() << " messages " << endl;
        cout << out.str();
        cnt++;
    }

    return 0;
}
