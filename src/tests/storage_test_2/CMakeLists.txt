add_executable(storage_test_2 storage_test_2.cpp)

target_link_libraries(storage_test_2 PUBLIC didal)

target_link_libraries(storage_test_2 PUBLIC MPI::MPI_CXX)

install(TARGETS storage_test_2 DESTINATION tests/storage_test_2)
