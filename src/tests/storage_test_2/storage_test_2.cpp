#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>
#include <numeric>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <thread>
#include <chrono>

using namespace ddl;
using namespace std;

void do_requests(DistributedStorage<int, string> &storage, int num_of_elems, int num_of_requests, int num_of_nodes, int max_delay) {
    vector<future<shared_ptr<string>>> futures;

    for (int i = 0; i < num_of_requests; i++) {
        futures.emplace_back(storage.getWhenReadyFrom(rand() % num_of_nodes, rand() % num_of_elems));
        if (max_delay > 0) {
            this_thread::sleep_for(chrono::microseconds(rand() % max_delay));
        }
    }

    for (auto &f: futures) {
        f.get();
    }
}

int main(int argc, char **argv) {

    Environment env(&argc, &argv);

    const int num_of_elems = (argc > 1 ? stoi(argv[1]) : 1000);
    const int num_of_requests = (argc > 2 ? stoi(argv[2]) : 10000);
    const int max_delay = (argc > 3 ? stoi(argv[3]) : 0);

    DistributedStorage<int, string> storage(&env);

    const int my_rank = env.thisNode().rank();
    const int num_of_nodes = env.allNodes().size();

    thread req_thread(&do_requests, ref(storage), num_of_elems, num_of_requests, num_of_nodes, max_delay);

    vector<int> ids(num_of_elems);
    iota(ids.begin(), ids.end(), 0);
    random_shuffle(ids.begin(), ids.end());

    for (auto id: ids) {
        storage.create(id, "hello");
    }

    req_thread.join();

    env.getCommService()->getCommunicator()->barrier();

    ostringstream out;
    out << "Rank " << my_rank << " is done" << endl;
    cout << out.str();

    return 0;
}
