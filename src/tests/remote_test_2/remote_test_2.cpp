#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <atomic>
#include <thread>
#include <chrono>
#include <vector>

class RemoteTest {
public:
    RemoteTest(ddl::Node node, ddl::Environment *env) :
        node(node), remote(env->getCommService(), this)
    {
        ddl::RemoteMethodMaker<RemoteTest> maker(&remote, this);
        get_remote = maker.method(&RemoteTest::getString).func<std::string, int>();
        remote.startListening();
    }

    std::string get(int index, const ddl::Node &dest) {
        return get_remote(dest, index).get();
    }

    std::string getString(int index) const {
        return strings[index];
    }

    void setStrings(const std::vector<std::string> &strings) {
        this->strings = strings;
        remote.startListening();
    }

    void wait(int num) {
        while (count < num) {
            std::this_thread::sleep_for(std::chrono::microseconds(1000));
        }
    }

private:
    ddl::Node node;
    ddl::Remote remote;
    ddl::RemoteFunction<std::string, int> get_remote;
    std::vector<std::string> strings;
    std::atomic<int> count {0};
};

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    const int my_rank = env.thisNode().rank();

    RemoteTest test(env.thisNode(), &env);

    std::vector<std::string> strings;
    for (const auto &node: env.allNodes()) {
        strings.push_back("This is string #" + std::to_string(node.rank()) + " on node " + std::to_string(my_rank));
    }

    test.setStrings(strings);

    env.getCommService()->getCommunicator()->barrier();

    for (const auto &node: env.allNodes()) {
        const auto str = test.get(my_rank, node);
        std::ostringstream out;
        out << env.thisNode() << " : string #" << my_rank << " on " << node << " is '" << str << "'" << std::endl;
        std::cout << out.str();
    }

    env.getCommService()->getCommunicator()->barrier();

    return 0;
}
