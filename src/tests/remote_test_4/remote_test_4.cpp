#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <atomic>
#include <thread>
#include <chrono>
#include <vector>
#include <algorithm>

using namespace std;
using namespace std::chrono;

class Data {
public:
    Data() { cout << "Created\n"; }
    Data(int n) : value(n) { cout << "Init\n"; }
    Data(const Data &dt) : value(dt.value) { cout << "Copy\n"; }
    Data(Data &&dt) : value(dt.value) { cout << "Move\n"; }

    Data& operator=(const Data &dt) {
        value = dt.value;
        cout << "OpCopy\n";
        return *this;
    }

    Data& operator=(Data &&dt) {
        value = dt.value;
        cout << "OpMove\n";
        return *this;
    }

    friend ddl::Writable& operator <<(ddl::Writable &w, const Data &d) {
        return w << d.value;
    }

    friend ddl::Readable& operator >>(ddl::Readable &r, Data &d) {
        return r >> d.value;
    }

public:
    int value {0};
};

void func(const Data &dt) {
    std::cout << "Data " << dt.value << endl;
}

Data func2(int n) {
    return Data(n);
}

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::Remote remote(env.getCommService());

    auto rem_func = remote.remoteFunction<decltype(&func), void, const Data&>(&func);
    auto rem_func2 = remote.remoteFunction<decltype(&func2), Data, int>(&func2);

    remote.startListening();

    const int this_rank = env.thisNode().rank();
    const auto num_of_nodes = static_cast<int>(env.allNodes().size());

    Data data(this_rank);

    auto neigh_rank = (this_rank + 1) % num_of_nodes;

    rem_func(ddl::Node(neigh_rank), data).get();

    std::cout << "Phase 2\n";

    rem_func2(ddl::Node(neigh_rank), neigh_rank).get();

    return 0;
}
