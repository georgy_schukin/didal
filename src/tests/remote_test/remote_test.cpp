#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <atomic>
#include <thread>
#include <chrono>

class RemoteTest {
public:
    RemoteTest(ddl::Node node, ddl::Environment *env) :
        node(node), remote(env->getCommService(), this)
    {        
        ddl::RemoteMethodMaker<RemoteTest> maker(&remote, this);
        send_remote = maker.method(&RemoteTest::onMessage).proc<const std::string&>();
        remote.startListening();
    }

    void send(const std::string &str, const ddl::Node &dest) {
        send_remote(dest, str);
    }

    void onMessage(const std::string &msg) {
        std::ostringstream out;
        out << node << " receive: " << msg << std::endl;
        std::cout << out.str();
        ++count;
    }

    void wait(int num) {
        while (count < num) {
            std::this_thread::sleep_for(std::chrono::microseconds(1000));
        }
    }

private:
    ddl::Node node;
    ddl::Remote remote;
    ddl::RemoteProcedure<const std::string&> send_remote;
    std::atomic<int> count {0};
};

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    RemoteTest test(env.thisNode(), &env);

    for (int i = 0; i < env.allNodes().size(); i++) {
        std::ostringstream out;
        out << "Send from " << env.thisNode() << " to " << i;
        test.send(out.str(), ddl::Node(i));
    }

    test.wait(env.allNodes().size());

    return 0;
}
