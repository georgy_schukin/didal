#include "didal/didal.h"
#include "didal/algorithms/barrier.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <memory>

using namespace ddl;
using namespace std;

class Container {
public:
    Container() {}
    Container(const Container &c) : elems(c.elems) {}
    Container(Container &&c) : elems(move(c.elems)) {}

    void add(int elem) {
        lock_guard<mutex> lock(mut);
        elems.push_back(elem);
        //std::cout << "add " << elem << "\n";
    }

    int size() const {
        lock_guard<mutex> lock(mut);
        return elems.size();
    }    

    friend Writable& operator << (Writable &w, const Container &c) {
        return w << c.elems;
    }

    friend Readable& operator >> (Readable &r, Container &c) {
        return r >> c.elems;
    }

private:
    vector<int> elems;
    mutable mutex mut;    
};

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    const int num_of_iters = (argc > 1 ? stoi(argv[1]) : 1000);
    const int max_delay = (argc > 2 ? stoi(argv[2]) : 0);
    const int use_global = (argc > 3 ? stoi(argv[3]) : 0);

    auto cs = env.getCommService();

    const auto this_rank = cs->thisNode().rank();
    const auto num_of_nodes = cs->allNodes().size();

    DistributedStorage<int, Container> data(&env);

    auto getSize = data.makeMethodGetter<decltype(&Container::size), int>(&Container::size);
    auto addElem = data.makeMethodGetter<decltype(&Container::add), void,int>(&Container::add);

    data.create(0, Container {});

    for (int i = 0; i < num_of_iters; i++) {
        //std::cout << this_rank << " iter " << i << std::endl;
        for (auto node: env.allNodes()) {
            auto f = addElem(node, 0, i);
            if (max_delay > 0) {
                this_thread::sleep_for(chrono::microseconds(rand() % max_delay));
            }
            f.get();
        }
        //std::cout << this_rank << " barrier on iter " << i << std::endl;
        if (use_global) {
            barrier(&env);
        } else {
            localBarrier(&env, env.allNodes());
        }
        //std::cout << this_rank << " done barrier on iter " << i << std::endl;
    }

    ostringstream out;
    out << "Rank " << this_rank << " has " << data.get(0)->size() << " elems" << endl;
    cout << out.str();

    return 0;
}
