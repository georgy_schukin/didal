#include "didal/didal.h"
#include "didal/util/timer.h"
#include "didal/algorithms/barrier.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <memory>
#include <vector>
#include <numeric>

using namespace ddl;
using namespace std;

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    const int num_of_iters = (argc > 1 ? stoi(argv[1]) : 1000);
    const bool all_neighbors = (argc > 2 ? stoi(argv[2]) : false);

    auto cs = env.getCommService();

    const int this_rank = cs->thisNode().rank();
    const int num_of_nodes = cs->allNodes().size();

    set<int> neighbors;
    if (all_neighbors) {
        for (auto n: env.allNodes()) {
            neighbors.insert(n.rank());
        }
    } else {
        neighbors.insert((this_rank + 1) % num_of_nodes);
        neighbors.insert((this_rank + num_of_nodes - 1) % num_of_nodes);
    }

    Timer timer;
    for (int i = 0; i < num_of_iters; i++) {        
        barrier(&env);
    }    
    const auto gb_time = timer.time();

    timer.reset();
    for (int i = 0; i < num_of_iters; i++) {
        localBarrier(&env, neighbors);
    }
    const auto lb_time = timer.time();

    Fence fence(&env);
    timer.reset();
    for (int i = 0; i < num_of_iters; i++) {
        fence.barrier(neighbors);
    }
    const auto fence_time = timer.time();

    ostringstream out;
    out << "Rank " << this_rank <<
           ": global time: " << gb_time <<
           ", local time: " << lb_time <<
           ", fence time: " << fence_time << endl;
    cout << out.str();

    barrier(&env);

    return 0;
}
