TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    barrier_test_2.cpp

DIDAL_OUT_DIR = $$OUT_PWD/../../didal
include(../../didal_lib.pri)
