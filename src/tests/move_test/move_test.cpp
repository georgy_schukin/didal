#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::SyncDistributedStorage<int, std::string> storage(&env);

    int my_rank = env.thisNode().rank();

    if (env.allNodes().size() < 3) {
        std::cerr << "Not enough nodes" << std::endl;
        return -1;
    }

    const int data_id = 101;

    if (my_rank == 0) {
        storage.nextEpoch();
        auto data = storage.getFrom(2, data_id);
        std::cout << "Data value is: " << *(data.get()) << std::endl;
    } else if (my_rank == 1) {
        storage.nextEpoch();
        storage.add(data_id, "This is some data");
        storage.moveTo(data_id, 2);
    }

    env.getCommService()->getCommunicator()->barrier();

    return 0;
}
