TEMPLATE = subdirs

SUBDIRS += \
    array_test \
    barrier_test \
    barrier_test_2 \
    comm_test \
    for_test \
    gather_test \
    move_test \
    ping_pong \
    reduce_test \
    reduce_test_2 \
    remote_test \
    remote_test_2 \
    remote_test_3 \
    remote_test_4 \
    storage_test \
    storage_test_2 \
    sync_storage_test \
    tracking_test
