#include "didal/didal.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>

int main(int argc, char **argv) {

    ddl::Environment env(&argc, &argv);

    ddl::DistributedStorage<int, std::string> storage(&env);

    int my_rank = env.thisNode().rank();

    storage.add(my_rank, std::string("data.") + std::to_string(my_rank));

    env.getCommService()->getCommunicator()->barrier();

    for (auto node: env.allNodes()) {
        const auto data_id = node.rank();
        std::ostringstream out;
        auto has = storage.hasOn(node, data_id);
        out << "Node " << my_rank << ": node " << node << " has data " << data_id << ": " << has << std::endl;
        std::cout << out.str();
    }

    for (auto node: env.allNodes()) {
        const auto data_id = node.rank();
        std::ostringstream out;
        auto data = storage.getFrom(node, data_id);
        out << "Sync: Node " << my_rank << ": data " << data_id << " is " << *data.get() << std::endl;
        std::cout << out.str();
    }

    for (auto node: env.allNodes()) {
        const auto data_id = node.rank();
        std::ostringstream out;
        auto future = storage.getFromAsync(node, data_id);
        auto data = future.get();
        out << "Async: Node " << my_rank << ": data " << data_id << " is " << *data.get() << std::endl;
        std::cout << out.str();
    }

    ddl::DistributedStorage<int, std::string> storage2(&env);

    std::vector<std::pair<int, std::future<std::shared_ptr<std::string>>>> requests;

    for (auto node: env.allNodes()) {
        const auto data_id = node.rank();
        requests.push_back(std::make_pair(data_id, storage2.getWhenReadyFrom(node, data_id)));
    }

    storage2.add(my_rank, std::string("data.") + std::to_string(my_rank));

    for (auto &f: requests) {
        std::ostringstream out;
        auto data_id = f.first;
        auto data = f.second.get();
        out << "When ready: Node " << my_rank << ": data " << data_id << " is " << *data.get() << std::endl;
        std::cout << out.str();
    }

    env.getCommService()->getCommunicator()->barrier();

    auto cont = storage.localContents();

    return 0;
}
