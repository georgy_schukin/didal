TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        array_tests.cpp \
        main.cpp \
        mesh_topology_tests.cpp \
        serialization_tests.cpp

INCLUDEPATH += ../third_party

DIDAL_OUT_DIR = $$OUT_PWD/../didal
include(../didal_lib.pri)
