#include "catch.hpp"
#include "didal/base/array_nd.h"
#include "didal/base/array_nd_ops.h"

#include <algorithm>
#include <vector>

using namespace ddl;

TEST_CASE ("1D Array") {
    ArrayND<int, 1> arr {10};
    CHECK(arr.size() == 10);
    CHECK(arr.size(0) == 10);
    arr[2] = 4;
    arr(3) = 7;
    CHECK(arr[2] == 4);
    CHECK(arr(3) == 7);
}

TEST_CASE ("2D Array") {
    ArrayND<int, 2> arr {10, 15};
    CHECK(arr.size() == 150);
    CHECK(arr.size(0) == 10);
    CHECK(arr.size(1) == 15);
    arr(2, 3) = 4;
    CHECK(arr(2, 3) == 4);
}


TEST_CASE ("3D Array") {
    ArrayND<int, 3> arr {10, 15, 2};
    CHECK(arr.size() == 300);
    CHECK(arr.size(0) == 10);
    CHECK(arr.size(1) == 15);
    CHECK(arr.size(2) == 2);
    arr(2, 3, 1) = 4;
    CHECK(arr(2, 3, 1) == 4);
}

