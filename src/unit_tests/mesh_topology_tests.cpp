#include "catch.hpp"
#include "didal/tp/mesh_topology.h"

#include <algorithm>
#include <vector>

using namespace ddl;

TEST_CASE ("Mesh Topology: 1D Mesh") {
    MeshTopology<1> tp(0, 8);
    using Ind1 = Index<1>;
    CHECK(tp.numOfNodes() == 8);
    CHECK(tp.thisNode() == 0);
    CHECK(tp.indexForNode(4) == Ind1{4});
    CHECK(tp.nodeForIndex({4}) == 4);
    CHECK(tp.neighborsForIndex({0}) == std::vector<Ind1>{Ind1{1}});
    CHECK(tp.neighborsForIndex({7}) == std::vector<Ind1>{Ind1{6}});
    CHECK(tp.neighborsForIndex({4}) == std::vector<Ind1>{Ind1{3}, Ind1{5}});
}

TEST_CASE ("Mesh Topology: 2D Mesh") {
    MeshTopology<2> tp(0, 8, {2, 4});
    using Ind2 = Index<2>;
    CHECK(tp.numOfNodes() == 8);
    CHECK(tp.thisNode() == 0);
    CHECK(tp.thisIndex() == Ind2{0, 0});
    CHECK(tp.indexForNode(5) == Ind2{1, 1});
    CHECK(tp.nodeForIndex({1, 1}) == 5);
}

TEST_CASE ("Mesh Topology: 3D Mesh") {
    MeshTopology<3> tp(0, 8);
    using Ind3 = Index<3>;
    CHECK(tp.numOfNodes() == 8);
    CHECK(tp.getDimensions() == std::array<int, 3>{2, 2, 2});
    CHECK(tp.thisNode() == 0);
    CHECK(tp.thisIndex() == Ind3{0, 0, 0});
    CHECK(tp.indexForNode(5) == Ind3{1, 0, 1});
    CHECK(tp.nodeForIndex({1, 0, 1}) == 5);
}
