#include "catch.hpp"
#include "didal/comm/serialize/serializing_ops.h"
#include "didal/comm/message_serialize.h"

template <typename T>
T serializeAndDeserialize(const T &value) {
    ddl::Message msg;
    ddl::msgSerialize(msg, value);
    T deserialized_value;
    ddl::msgDeserialize(msg, deserialized_value);
    return deserialized_value;
}

template <typename T>
void checkSerialization(const T &value) {
    REQUIRE(serializeAndDeserialize<T>(value) == value);
}

TEST_CASE ("Serialization: scalar values") {
    checkSerialization(true);
    checkSerialization(false);
    checkSerialization(1);
    checkSerialization(-25);
    checkSerialization(4.5f);
    checkSerialization(1.6e+3);
}

TEST_CASE ("Serialization: strings") {
    checkSerialization(std::string("hello"));
    checkSerialization(std::string(""));
}

TEST_CASE ("Serialization: vector") {
    checkSerialization(std::vector<int> {});
    checkSerialization(std::vector<int> {1, 5, 7, 9, 34});
    checkSerialization(std::vector<std::string> {"a", "yes", "no", "big", "-34"});
}

TEST_CASE ("Serialization: list") {
    checkSerialization(std::list<int> {});
    checkSerialization(std::list<int> {1, 5, 7, 9, 34});
    checkSerialization(std::list<std::string> {"a", "yes", "no", "big", "-34"});
}

TEST_CASE ("Serialization: set") {
    checkSerialization(std::set<int> {});
    checkSerialization(std::set<int> {1, 5, 7, 9, 34});
    checkSerialization(std::set<std::string> {"a", "yes", "no", "big", "-34"});
}

TEST_CASE ("Serialization: map") {
    checkSerialization(std::map<int, int> {});
    checkSerialization(std::map<int, int> {{1, 2}, {3, 4}, {5, 6}, {-1, 0}, {9, -8}});
    checkSerialization(std::map<int, std::string> {{1, "a"}, {2, "yes"}, {4, "no"}, {5, "big"}, {-3, "-34"}});
}

TEST_CASE ("Serialization: smart pointers") {
    auto ptr = std::make_shared<std::string>("hello");
    auto d_ptr = serializeAndDeserialize(ptr);
    REQUIRE(*ptr.get() == *d_ptr.get());
}
