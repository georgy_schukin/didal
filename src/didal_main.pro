TEMPLATE = subdirs

#CONFIG += ordered

SUBDIRS = \
    didal \
    examples \
    tests \
    unit_tests

didal.subdir = didal
examples.subdir = examples
tests.subdir = tests
unit_tests.subdir = unit_tests

examples.depends = didal
tests.depends = didal
unit_tests.depends = didal


